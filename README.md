# MOO translation by Nealus (aka digital wisp)

## In English
This Repo contains folders with scripts/books/names exported from different versions of **Maskar's Oblivion Overhaul**, both original and translated by Nealus.
Should be useful for:

* comparing scripts between different versions of MOO to see what was changed and added
* for updating translations for new/updated scripts and keeping the rest unchanged
* updating the translation through pull requests
* keeping track of translation's progress

Plus there are some python scripts for convenience.

Thanks Maskar for making this awesome mod and the TesAll community for feedback and suggestions!

## На русском
Репозиторий содержит папки со скриптами/книгами/именами, экспортированными из **Maskar's Oblivion Overhaul**, как оригинальными, так и переведенными Nealus'ом (т.е. мной).

Может быть полезен для следующего:

* сравненения скриптов между версиями MOO для отслеживания изменений
* обновления перевода только для новых/измененных скриптов, не трогая остальные
* улучшения перевода посредством пулл реквестов
* отслеживания прогресса

Еще тут есть кое-какие питонячьи скрипты для удобства.

Спасибо Maskar'у за крутейший мод и сообществу TesAll за отзывы и предложения!