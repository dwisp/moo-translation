import os
# a small script that creates full folder structure for a new version of MOO

def create_dir_dict():
    version = input("Input MOO version: ")
    # full folder structure, easy to modify
    substruct = {
        'scripts': None,
        'names': None,
        'inis': None,
        'readme': None,
        'books': None,
    }

    struct = {
        '../versions': {
            version: {
                'ru': substruct,
                'en': substruct
            }
        }
    }
    return(struct)
    # creating all the paths

def create_directory_structure():
    """
    Creates a directory tree from nested dictionary
    Doesn't overwrite already created dirs, which allows 
    to add new subdirs if needed
    """
    struct = create_dir_dict()
    
    def spawn_fld(struct = struct):
        for key, value in struct.items():
            if isinstance(value, dict):
                # we need to go deeper
                if not os.path.exists(key):
                    os.mkdir(key)
                    # in and out of nested dir
                os.chdir(key)
                spawn_fld(value)
                os.chdir('..')
            else:
                # create or just go up
                if key is None:
                    os.chdir('..')
                if not os.path.exists(key):
                    os.mkdir(key)
    # creating the whole dir tree
    spawn_fld(struct)

if __name__ == '__main__':
    create_directory_structure()