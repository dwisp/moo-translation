Maskar's Oblivion Overhaul.esp
0x302B20
MOOTameReleaseFollowerFunctionScript
Scn MOOTameReleaseFollowerFunctionScript

ref me

short i

Begin Function { me }

Call MOOTameRemoveTagsFromActorNameFunctionScript me

set i to me.GetItemCount MOOTokenCreatureTame

let MOOTame.followers[i] := 0
let MOOTame.followershappiness[i] := 0
let MOOTame.followersXP[i] := 0

me.RemoveItemNS MOOTokenCreatureTame i

me.ClearOwnership
me.SetFactionRank PlayerFaction -1
me.AddScriptPackage MOOAIFollowerWander

me.SetLowLevelProcessing 0
me.SetRefEssential 0

set i to me.GetItemCount MOOTokenCreatureAggression
me.RemoveItemNS MOOTokenCreatureAggression i
set i to i * 5
me.SetAv Aggression i

if me.GetAv Aggression >= 80
if Call MOOIsEvilActorFunctionScript me
	me.ModDisposition player -100
endif
endif

; can not ride wild creatures
if me.GetCreatureType == 4
	me.SetCreatureType 0
endif

; remove guard token
set i to me.GetItemCount MOOTokenCreatureGuarding
me.RemoveItemNS MOOTokenCreatureGuarding i

End