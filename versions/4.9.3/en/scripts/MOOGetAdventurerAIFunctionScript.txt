Maskar's Oblivion Overhaul.esp
0x48DBD4
MOOGetAdventurerAIFunctionScript
Scn MOOGetAdventurerAIFunctionScript

ref myAI
short i

Begin Function {  }

set i to Rand 1 37

if i == 1
	set myAI to MOOAIAdventurerAleswellInn
elseif i == 2
	set myAI to MOOAIAdventurerTheDrunkenDragonInn
elseif i == 3
	set myAI to MOOAIAdventurerBleakersWayGoodwillInn
elseif i == 4
	set myAI to MOOAIAdventurerBorderWatchInn
elseif i == 5
	set myAI to MOOAIAdventurerBrinaCrossInn
elseif i == 6
	set myAI to MOOAIAdventurerFaregylInterior
elseif i == 7
	set myAI to MOOAIAdventurerHackdirtMoslinsInn
elseif i == 8
	set myAI to MOOAIAdventurerPellGateTheSleepingMare
elseif i == 9
	set myAI to MOOAIAdventurerGottshawInnTavern
elseif i == 10
	set myAI to MOOAIAdventurerImperialBridgeInnTavern
elseif i == 11
	set myAI to MOOAIAdventurerInnIllOmen
elseif i == 12
	set myAI to MOOAIAdventurerWawnetInnTavern
elseif i == 13
	set myAI to MOOAIAdventurerAnvilTheCountsArms
elseif i == 14
	set myAI to MOOAIAdventurerAnvilTheFlowingBowl
elseif i == 15
	set myAI to MOOAIAdventurerAnvilTheFocsle
elseif i == 16
	set myAI to MOOAIAdventurerBravilLonelySuitorLodge
elseif i == 17
	set myAI to MOOAIAdventurerBravilSilverHomeonthewater
elseif i == 18
	set myAI to MOOAIAdventurerBrumaJerallView
elseif i == 19
	set myAI to MOOAIAdventurerBrumaOlavsTapAndTack
elseif i == 20
	set myAI to MOOAIAdventurerCheydinhalBridgeInn
elseif i == 21
	set myAI to MOOAIAdventurerCheydinhalNewlandsLodge
elseif i == 22
	set myAI to MOOAIAdventurerChorrolTheGreyMare
elseif i == 23
	set myAI to MOOAIAdventurerChorrolTheOakandCrosierTavern
elseif i == 24
	set myAI to MOOAIAdventurerICTempleDistrictTheAllSaintsInn
elseif i == 25
	set myAI to MOOAIAdventurerICWaterfrontTheBloatedFloat
elseif i == 26
	set myAI to MOOAIAdventurerICMarketDistrictTheFeedBag
elseif i == 27
	set myAI to MOOAIAdventurerICTalosPlazaTheFoamingFlask
elseif i == 28
	set myAI to MOOAIAdventurerICElvenGardensTheKingandQueenTavern
elseif i == 29
	set myAI to MOOAIAdventurerICElvenGardensLutherBroadsBoardingHouse
elseif i == 30
	set myAI to MOOAIAdventurerICMarketDistrictTheMerchantsInn
elseif i == 31
	set myAI to MOOAIAdventurerICTalosPlazaTheTiberSeptimHotel
elseif i == 32
	set myAI to MOOAIAdventurerLeyawiinFiveClawsLodge
elseif i == 33
	set myAI to MOOAIAdventurerLeyawiinThreeSistersInn
elseif i == 34
	set myAI to MOOAIAdventurerRoxeyInn
elseif i == 35
	set myAI to MOOAIAdventurerSkingradTwoSistersLodge
else
	set myAI to MOOAIAdventurerSkingradWestWealdInn
endif

;set myAI to MOOAIFollowerFollow

SetFunctionValue myAI

End