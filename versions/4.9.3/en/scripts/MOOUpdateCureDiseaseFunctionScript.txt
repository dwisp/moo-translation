Maskar's Oblivion Overhaul.esp
0x2656F1
MOOUpdateCureDiseaseFunctionScript
Scn MOOUpdateCureDiseaseFunctionScript

short restarted
short i

Begin Function {  }

if GetGameRestarted
	set restarted to 1
endif

if MOO.ini_add_remedialtouch

	if restarted

		; MOOBookR3RemedialTouch					-> MOOLL0Book3Restoration

		AddToLeveledList MOOLL0Book3Restoration MOOBookR3RemedialTouch 1 1

		; MOOScrollRemedialTouch						-> scroll loot lists
		;														-> scroll vendor lists

		AddToLeveledList LL1LootScroll1Novice100 MOOScrollRemedialTouch 1 1
		AddToLeveledList LL1LootScroll2Apprentice100 MOOScrollRemedialTouch 6 1

		AddToLeveledList VendorScroll1Novice MOOScrollRemedialTouch 1 1
		AddToLeveledList VendorScroll2Apprentice MOOScrollRemedialTouch 6 1

	endif

	; MOOSpellRemedialTouch						-> npc spell vendors

	IsaRamanRef.AddSpellNS MOOSpellRemedialTouch
	OhtesseRef.AddSpellNS MOOSpellRemedialTouch
	OragGraBargolRef01.AddSpellNS MOOSpellRemedialTouch
	AvrusAdasRef.AddSpellNS MOOSpellRemedialTouch
	TumindilRef.AddSpellNS MOOSpellRemedialTouch
	TrevaiaRef.AddSpellNS MOOSpellRemedialTouch
	UravasaOthrelasRef.AddSpellNS MOOSpellRemedialTouch

endif


if MOO.ini_disease_groups

	if restarted

		; ScrollStandardCureDiseaseApprentice		-> scroll loot list
		;														-> scroll vendor lists

		AddToLeveledList LL1LootScroll1Novice100 ScrollStandardCureDiseaseApprentice 1 1
		AddToLeveledList VendorScroll1Novice ScrollStandardCureDiseaseApprentice 1 1

		; MOOLL0PotionsCureDisease					-> potion loot list
		;														-> potion vendor lists

		AddToLeveledList MOOLL0Potions3Heal MOOLL0PotionsCureDisease 1 1
		AddToLeveledList LL0LootPotionsHealingDisease50 MOOLL0PotionsCureDisease 1 1
		AddToLeveledList LL0LootPotionsHealingDisease75 MOOLL0PotionsCureDisease 1 1
		AddToLeveledList LL0LootPotionsHealingDisease100 MOOLL0PotionsCureDisease 1 1
		AddToLeveledList LL1LootRestorationNecromancer50 MOOLL0PotionsCureDisease 1 1
		AddToLeveledList LL1LootRestorationTomb50 MOOLL0PotionsCureDisease 1 1
		AddToLeveledList LL1LootRestorationVampire50 MOOLL0PotionsCureDisease 1 1

		AddToLeveledList VendorPotionsStandard PotionCureDisease 1 1
		AddToLeveledList VendorPotionsStandard MOOPotionCureDiseaseSerious 1 1
		AddToLeveledList VendorPotionsStandard MOOPotionCureDiseaseAcute 1 1

		AddToLeveledList VendorPotionsWeak PotionCureDisease 1 1
		AddToLeveledList VendorPotionsWeak MOOPotionCureDiseaseSerious 1 1
		AddToLeveledList VendorPotionsWeak MOOPotionCureDiseaseAcute 1 1
		AddToLeveledList VendorPotionsWeak PotionCureDisease 1 1
		AddToLeveledList VendorPotionsWeak MOOPotionCureDiseaseSerious 1 1
		AddToLeveledList VendorPotionsWeak MOOPotionCureDiseaseAcute 1 1

	endif

	; rename cure potion

	SetName "Weak Potion of Cure Disease" PotionCureDisease

	SetGoldValue 32 PotionCureDisease
	SetGoldValue 68 MOOPotionCureDiseaseSerious
	SetGoldValue 140 MOOPotionCureDiseaseAcute

	if MOO.currentversion == 0
		set i to Rand 0 7
		if i == 0
			CGGenericAssassinsParent.AddItemNS MOOPotionCureDiseaseAcute 1
		elseif i == 1
			CGGenericAssassin1.AddItemNS MOOPotionCureDiseaseAcute 1
		elseif i == 2
			CGGenericAssassin2.AddItemNS MOOPotionCureDiseaseAcute 1
		elseif i == 3
			CGGenericAssassin3.AddItemNS MOOPotionCureDiseaseAcute 1
		elseif i == 4
			CGAssassin05Ref.AddItemNS MOOPotionCureDiseaseAcute 1
		elseif i == 5
			CGAssassin06Ref.AddItemNS MOOPotionCureDiseaseAcute 1
		else
			CGAssassinFinal.AddItemNS MOOPotionCureDiseaseAcute 1
		endif
	endif

endif

End