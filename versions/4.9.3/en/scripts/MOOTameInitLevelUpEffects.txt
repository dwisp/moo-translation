Maskar's Oblivion Overhaul.esp
0x31743F
MOOTameInitLevelUpEffects
Scn MOOTameInitLevelUpEffects

Begin Function {  }

let MOOTame.levelupeffects[0] := MOOEffect1Fire
let MOOTame.levelupeffects[1] := MOOEffect2Fire
let MOOTame.levelupeffects[2] := MOOEffect3Fire

let MOOTame.levelupeffects[3] := MOOEffect1Frost
let MOOTame.levelupeffects[4] := MOOEffect2Frost
let MOOTame.levelupeffects[5] := MOOEffect3Frost

let MOOTame.levelupeffects[6] := MOOEffect1Shock
let MOOTame.levelupeffects[7] := MOOEffect2Shock
let MOOTame.levelupeffects[8] := MOOEffect3Shock

let MOOTame.levelupeffects[9] := MOOEffect1Drain
let MOOTame.levelupeffects[10] := MOOEffect2Drain
let MOOTame.levelupeffects[11] := MOOEffect3Drain

End

