Maskar's Oblivion Overhaul.esp
0x4C0223
MOODungeonCheckDirectionAllowedFunctionScript
Scn MOODungeonCheckDirectionAllowedFunctionScript

short mydigger
short mydirection

short mytile
short tilecode
short min
short max

short myx
short myy

short allowed

Begin Function { mydigger mydirection }

set min to 1
set max to 28
set allowed to 1

; GET NEW LOCATION

let myx := MOO.roomx[mydigger]
let myy := MOO.roomy[mydigger]

if mydirection == 1 ; move up
	set myy to myy + 1
elseif mydirection == 2 ; move right
	set myx to myx + 1
elseif mydirection == 4 ; move down
	set myy to myy - 1
else ; move left
	set myx to myx - 1
endif


; OUT OF BOUNDS

if myx < min || myx > max || myy < min || myy > max
	set allowed to 0
else

; CHECK TILES ALLOWED ( 9 TILES )

	; TOP LEFT
	let mytile := MOO.dungeonmap[myx - 1][myy + 1]
	let tilecode := 4 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; TOP MIDDLE
	let mytile := MOO.dungeonmap[myx][myy + 1]
	let tilecode := 12 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; TOP RIGHT
	let mytile := MOO.dungeonmap[myx + 1][myy + 1]
	let tilecode := 8 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; MIDDLE LEFT
	let mytile := MOO.dungeonmap[myx - 1][myy]
	let tilecode := 6 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; MIDDLE MIDDLE
	let mytile := MOO.dungeonmap[myx][myy]
	let tilecode := 15 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; MIDDLE RIGHT
	let mytile := MOO.dungeonmap[myx + 1][myy]
	let tilecode := 9 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; BOTTOM LEFT
	let mytile := MOO.dungeonmap[myx - 1][myy - 1]
	let tilecode := 2 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; BOTTOM MIDDLE
	let mytile := MOO.dungeonmap[myx][myy - 1]
	let tilecode := 3 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif

	; BOTTOM RIGHT
	let mytile := MOO.dungeonmap[myx + 1][myy - 1]
	let tilecode := 1 << 4
	let mytile := mytile | tilecode
	if Call MOODungeonTileUnknownFunctionScript mytile
		set allowed to 0
	endif
endif

SetFunctionValue allowed

End