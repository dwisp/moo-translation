Maskar's Oblivion Overhaul.esp
0x41CCEB
MOOGetEncumbranceFunctionScript
Scn MOOGetEncumbranceFunctionScript

ref me

ref myitem

float myenc

Begin Function { me }


foreach myitem <- me
	set myenc to myenc + myitem.GetWeight * myitem.GetRefCount
loop

SetFunctionValue myenc

End