Maskar's Oblivion Overhaul.esp
0x0C16DA
MOOGetCurrentRegionFunctionScript
Scn MOOGetCurrentRegionFunctionScript

ref me

ref myworldspace

Begin Function { me }

if ( me.IsInInterior )
	if ( MOO.myregion == 0 || MOO.myregion >= 10 ) ; skip for MOO.myregion == -1 || SE || Oblivion
		set me to Call MOOGetOutdoorMarkerFromCellFunctionScript me
	endif
else
	set MOO.mydungeon to 0
endif

Call MOOGetSafetyZoneFunctionScript me

if me.IsInInterior
	if MOO.myregion == 0
		set MOO.myregion to -1
	endif

else
	if GetPlayerInSEWorld
		set MOO.myregion to 1

	elseif me.IsInOblivion
		set MOO.myregion to 2

	elseif me == player

		set myworldspace to me.GetParentWorldSpace
		if myworldspace
		if GetWorldSpaceParentWorldSpace myworldspace
			set myworldspace to GetWorldSpaceParentWorldSpace myworldspace
		endif
		endif

		if myworldspace == Tamriel
			Call MOOGetCurrentRegionFromxyFunctionScript me
		else
			set MOO.myregion to -1
		endif

	else ; me is marker/door through activation/door found outside
		Call MOOGetCurrentRegionFromxyFunctionScript me
	endif
endif

if me.IsDoor
	Call MOOUpdateCurrentRegionFunctionScript ; update lists + linked to dungeon/door) MOOUpdateCurrentRegionDoorFunctionScript
endif

if ( MOO.myregion != MOO.oldregion )
	set MOO.oldregion to MOO.myregion

	Call MOOUpdateCurrentRegionFunctionScript
endif

End