Maskar's Oblivion Overhaul.esp
0x54FCAF
MOOEventCheckDistanceFunctionScript
Scn MOOEventCheckDistanceFunctionScript

ref me
short eventtype

short i
ref myworldspace
float myx
float myy
float myeventx
float myeventy
float mydistance
float maxdistance

short fail

Begin Function { me eventtype }

set myworldspace to me.GetParentWorldSpace
set myx to me.GetPos x
set myy to me.GetPos y

; GET MAX DISTANCE
if eventtype == 1
	set maxdistance to MOO.ini_event_distance_outpost
else
	set maxdistance to MOO.ini_event_distance_shipwreck
endif

; CHECK DISTANCE
while i < MOO.totalspecialevents

	if eval MOO.eventtypes[i] == eventtype
	if eval MOO.eventworldspaces[i] == myworldspace
		let myeventx := MOO.eventx[i]
		let myeventy := MOO.eventy[i]

		set mydistance to ( ( myx - myeventx ) * ( myx - myeventx ) ) + ( ( myy - myeventy ) * ( myy - myeventy ) )
		let mydistance := sqrt mydistance


		if mydistance < maxdistance
			set fail to 1
			break
		endif
	endif
	endif

	set i to i + 1
loop

SetFunctionValue fail

End