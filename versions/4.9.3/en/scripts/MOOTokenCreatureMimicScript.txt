Maskar's Oblivion Overhaul.esp
0x41C615
MOOTokenCreatureMimicScript
Scn MOOTokenCreatureMimicScript

ref me
short i

Begin Gamemode

set i to i + 1
if i > 20
	RemoveMe
elseif i > 10
	set me to GetContainer
	if me
		MOOCellChestStorage.RemoveAllItems me
		Call MOOMessageCarryWeightFunctionScript me 1
		set i to 100
	endif
endif

End