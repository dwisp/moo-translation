Maskar's Oblivion Overhaul.esp
0x0730CC
MOOActivatorTrapPoison03Script
Scn MOOActivatorTrapPoison03Script

float time
short hits

Begin Gamemode

if MOO.togglepoison03
	set MOO.togglepoison03 to 0
	set hits to 9
	set time to 0
	PlaySound TRPGasRelease
	return
endif

if hits <= 0
	return
endif

set time to time + GetSecondsPassed

if time >= 1.7
	set hits to hits - 1
	set time to 0
	Call MOOTrapHitFunctionScript -36
endif

if hits <= 0
	MoveTo MOOCellMarker
endif

End