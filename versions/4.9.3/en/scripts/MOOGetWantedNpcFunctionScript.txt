Maskar's Oblivion Overhaul.esp
0x23281B
MOOGetWantedNpcFunctionScript
Scn MOOGetWantedNpcFunctionScript

short myregion

ref me
ref basenpc
short i

ref myclass
short npctype

short elementaltype
short creaturetype

short dungeontype ; 0 default 1 vampire 2 necromancer (undead) 3 conjurer (daedra)

ref clonenpc

Begin Function { myregion }

; defining npc

; set npc type
let npctype := MOO.questsubtyperegions[myregion]

; npc class
set myclass to Call MOOSetNpcClassFunctionScript npctype

; npc preferences
set elementaltype to Rand 0 3.99 ; 0 fire 1 frost 2 shock 3 other

if npctype == 6 || npctype == 4 || myclass == Necromancer || GetRandomPercent < 33
	set creaturetype to 0 ; undead
else
	set creaturetype to 1 ; daedra
endif

; set dungeon type
if npctype == 4 ; vampire
	set dungeontype to 1
elseif IsMajorC 21 myclass ; 21: Conjuration
	if creaturetype == 0
		set dungeontype to 2
	else
		set dungeontype to 3
	endif
else
	set dungeontype to 0
endif

; ==========

set basenpc to Call MOOGetWantedBaseNpcFunctionScript myregion
set clonenpc to CloneForm basenpc

; stop npcs from resetting
SetLowLevelProcessing 1 clonenpc

; set name
Call MOOSetNpcNameFunctionScript clonenpc

; set level
Call MOOSetNpcLevelFunctionScript clonenpc npctype


; create npc
set me to Call MOOPlaceWantedNpcFunctionScript clonenpc myregion dungeontype

; add npc token -> to stop releveling
me.AddItemNS MOOTokenNpcLoot 1

;		0 escaped prisoner
;		1 horse thief
;		2 murderer
;		3 thief
;		4 vampire
;		5 mythical dawn agent
;		6 grave digger
;		7 dark brotherhood assassin
;		8 skooma dealer


; set class (can't be done when getting class as npc doesn't exist yet -> class required to place npc)
me.SetClass myclass

; set combat style
Call MOOSetNpcCombatStyleFunctionScript me npctype

; set AI
Call MOOSetNpcAIFunctionScript me npctype

; add fitting equipment
Call MOOSetNpcEquipmentFunctionScript me npctype (isfemale clonenpc)
Call MOOSetNpcInventoryFunctionScript me npctype

; add fitting abilties and spells (maybe requires race spells)
Call MOOSetNpcMagicFunctionScript me npctype elementaltype creaturetype

; update factions -> still has old factions
Call MOOSetNpcFactionsFunctionScript me npctype


SetFunctionValue me

End