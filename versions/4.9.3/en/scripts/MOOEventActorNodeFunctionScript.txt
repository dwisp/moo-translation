Maskar's Oblivion Overhaul.esp
0x50736D
MOOEventActorNodeFunctionScript
Scn MOOEventActorNodeFunctionScript

ref myplacer

short isplaced

Begin Function { myplacer }

Call MOOPlaceWorldSpaceActorFunctionScript myplacer

set isplaced to 1

SetFunctionValue isplaced

End