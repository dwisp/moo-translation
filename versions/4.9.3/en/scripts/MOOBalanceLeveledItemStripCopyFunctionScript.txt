Maskar's Oblivion Overhaul.esp
0x5C5710
MOOBalanceLeveledItemStripCopyFunctionScript
Scn MOOBalanceLeveledItemStripCopyFunctionScript

ref leveledlist
ref templist

short arraysize
short myentry
short i

ref myitem
ref mylevel

Begin Function { leveledlist templist }

ClearLeveledList templist

set arraysize to GetNumLevItems leveledlist

while i < arraysize

	set myitem to GetNthLevItem i leveledlist

	set myentry to GetLevItemIndexByForm templist myitem

	if myentry == -1
		set mylevel to GetNthLevItemLevel i leveledlist
		AddToLeveledList templist myitem mylevel
	endif

	set i to i + 1
loop

End