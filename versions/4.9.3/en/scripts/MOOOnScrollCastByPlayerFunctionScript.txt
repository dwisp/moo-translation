Maskar's Oblivion Overhaul.esp
0x0A3F07
MOOOnScrollCastByPlayerFunctionScript
Scn MOOOnScrollCastByPlayerFunctionScript

ref me
ref myenchantment

ref mybook
ref myspell

Begin Function { me myenchantment }

if me != player
	return
endif

set mybook to Call MOOGetBookfromEnchantmentFunctionScript myenchantment
if mybook == 0
	return
endif

set myspell to Call MOOGetSpellfromBookFunctionScript mybook
if myspell == 0
	return
endif

if player.HasSpell myspell
	message "You already know this spell."
	player.AddItemNS mybook 1
else
	message "You have learned a new spell."
	player.AddSpellNS myspell
endif

End