Maskar's Oblivion Overhaul.esp
0x56C7F9
MOOCompanionUpdateActionsCorpseCheckGetTargetFunctionScript
Scn MOOCompanionUpdateActionsCorpseCheckGetTargetFunctionScript

ref mychecker

ref me

ref mytarget

Begin Function { mychecker }

set me to GetFirstRef 69 1

while me

	if me.GetDisabled == 0
	if me.GetDead
	if mychecker.GetLOS me
	if me.GetDistance mychecker < 100

		if mytarget == 0
			set mytarget to me
			break
		else
			set mytarget to 0
			break
		endif

	endif
	endif
	endif
	endif

	set me to GetNextRef
loop

SetFunctionValue mytarget

End