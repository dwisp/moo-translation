Maskar's Oblivion Overhaul.esp
0x072316
MOOGetNextTrapFunctionScript
Scn MOOGetNextTrapFunctionScript

ref me
short trapnumber

short locklevel
short traptype
float seed1
float seed2

Begin Function { me trapnumber }

set locklevel to me.GetLockLevel

set seed1 to ( me.GetPos x ) * GameDay * trapnumber
set seed2 to ( me.GetPos y ) * GameDay * trapnumber

set traptype to 0

if eval ( trapnumber == 1 && seed2 & 4 == 4 ) || ( trapnumber == 2 && locklevel >= 8 && seed2 & 8 == 8 ) || ( trapnumber == 3 && locklevel >= 81 && seed2 & 16 == 16 )

	if eval ( locklevel >= 81 ) && ( seed1 & 1 == 1 )
		set traptype to 13
	elseif eval ( locklevel >= 41 ) && ( seed1 & 2 == 2 )
		set traptype to 9
	elseif eval ( locklevel >= 21 ) && ( seed1 & 4 == 4 )
		set traptype to 5
	elseif eval ( seed1 & 8 == 8 )
		set traptype to 1
	endif
endif

if traptype
	if eval seed2 & 3 == 1
		set traptype to traptype + 1
	elseif eval seed2 & 3 == 2
		set traptype to traptype + 2
	elseif eval seed2 & 3 == 3
		set traptype to traptype + 3
	endif
endif

SetFunctionValue traptype

End