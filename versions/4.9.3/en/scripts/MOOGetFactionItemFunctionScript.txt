Maskar's Oblivion Overhaul.esp
0x4404FF
MOOGetFactionItemFunctionScript
Scn MOOGetFactionItemFunctionScript

ref myitem

short i

Begin Function { myitem }

if myitem == MOOCapeCrimsonCape || myitem == MOOCapeCrimsonCloak
	set i to 1
elseif myitem == MOOCapeRedCape || myitem == MOOCapeRedCloak
	set i to 2
elseif myitem == MOOCapeWhiteCape || myitem == MOOCapeWhiteCloak
	set i to 3
elseif myitem == MOOCapeTanCape || myitem == MOOCapeTanCloak
	set i to 4
elseif myitem == MOOCapeBrownCape || myitem == MOOCapeBrownCloak
	set i to 5
elseif myitem == MOOCapeBlackandRedCape || myitem == MOOCapeBlackandRedCloak
	set i to 6
elseif myitem == MOOCapeGreenCape || myitem == MOOCapeGreenCloak
	set i to 7
endif

SetFunctionValue i

End