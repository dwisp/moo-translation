Maskar's Oblivion Overhaul.esp
0x3F031B
MOOUpdateRandomRegionFactionsFunctionScript
Scn MOOUpdateRandomRegionFactionsFunctionScript

short dayspassed

Begin Function {  }

set dayspassed to GameDaysPassed + MOO.ini_npc_invasion_days - MOO.invasiondayspassed - 1
if dayspassed > 100
	set dayspassed to 100
elseif dayspassed < 0
	set dayspassed to 0
endif


set MOO.invasiondayspassed to MOO.ini_npc_invasion_days + GameDaysPassed - 1

while dayspassed > 0
	Call MOOUpdateRandomRegionFactionsDayPassedFunctionScript
	set dayspassed to dayspassed - 1
loop

End