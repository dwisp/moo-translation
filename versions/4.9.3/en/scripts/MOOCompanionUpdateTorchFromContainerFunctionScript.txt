Maskar's Oblivion Overhaul.esp
0x53E439
MOOCompanionUpdateTorchFromContainerFunctionScript
Scn MOOCompanionUpdateTorchFromContainerFunctionScript

ref me

ref mycontainer
short i

Begin Function { me }

; MOO.torchcontainers[]
; MOO.torchactors[]
; MOO.totaltorchcontainers

while i < MOO.totaltorchcontainers

	if eval MOO.torchactors[i] == me
		let mycontainer := MOO.torchcontainers[i]
		mycontainer.RemoveAllItems me
		let MOO.torchactors[i] := 0
	endif

	set i to i + 1
loop

End