Maskar's Oblivion Overhaul.esp
0x4EBC2D
MOOInitDungeonCellsFunctionScript
Scn MOOInitDungeonCellsFunctionScript

ref mycell
short i

Begin Function {  }

set MOO.currentcell to 1
set MOO.totalcells to 500

let MOO.cells[0] := MOOCellDungeon

let MOO.cells[1] := MOOCell001
let MOO.cells[2] := MOOCell002
let MOO.cells[3] := MOOCell003
let MOO.cells[4] := MOOCell004
let MOO.cells[5] := MOOCell005
let MOO.cells[6] := MOOCell006
let MOO.cells[7] := MOOCell007
let MOO.cells[8] := MOOCell008
let MOO.cells[9] := MOOCell009
let MOO.cells[10] := MOOCell010

let MOO.cells[11] := MOOCell011
let MOO.cells[12] := MOOCell012
let MOO.cells[13] := MOOCell013
let MOO.cells[14] := MOOCell014
let MOO.cells[15] := MOOCell015
let MOO.cells[16] := MOOCell016
let MOO.cells[17] := MOOCell017
let MOO.cells[18] := MOOCell018
let MOO.cells[19] := MOOCell019
let MOO.cells[20] := MOOCell020

let MOO.cells[21] := MOOCell021
let MOO.cells[22] := MOOCell022
let MOO.cells[23] := MOOCell023
let MOO.cells[24] := MOOCell024
let MOO.cells[25] := MOOCell025
let MOO.cells[26] := MOOCell026
let MOO.cells[27] := MOOCell027
let MOO.cells[28] := MOOCell028
let MOO.cells[29] := MOOCell029
let MOO.cells[30] := MOOCell030

let MOO.cells[31] := MOOCell031
let MOO.cells[32] := MOOCell032
let MOO.cells[33] := MOOCell033
let MOO.cells[34] := MOOCell034
let MOO.cells[35] := MOOCell035
let MOO.cells[36] := MOOCell036
let MOO.cells[37] := MOOCell037
let MOO.cells[38] := MOOCell038
let MOO.cells[39] := MOOCell039
let MOO.cells[40] := MOOCell040

let MOO.cells[41] := MOOCell041
let MOO.cells[42] := MOOCell042
let MOO.cells[43] := MOOCell043
let MOO.cells[44] := MOOCell044
let MOO.cells[45] := MOOCell045
let MOO.cells[46] := MOOCell046
let MOO.cells[47] := MOOCell047
let MOO.cells[48] := MOOCell048
let MOO.cells[49] := MOOCell049
let MOO.cells[50] := MOOCell050

let MOO.cells[51] := MOOCell051
let MOO.cells[52] := MOOCell052
let MOO.cells[53] := MOOCell053
let MOO.cells[54] := MOOCell054
let MOO.cells[55] := MOOCell055
let MOO.cells[56] := MOOCell056
let MOO.cells[57] := MOOCell057
let MOO.cells[58] := MOOCell058
let MOO.cells[59] := MOOCell059
let MOO.cells[60] := MOOCell060

let MOO.cells[61] := MOOCell061
let MOO.cells[62] := MOOCell062
let MOO.cells[63] := MOOCell063
let MOO.cells[64] := MOOCell064
let MOO.cells[65] := MOOCell065
let MOO.cells[66] := MOOCell066
let MOO.cells[67] := MOOCell067
let MOO.cells[68] := MOOCell068
let MOO.cells[69] := MOOCell069
let MOO.cells[70] := MOOCell070

let MOO.cells[71] := MOOCell071
let MOO.cells[72] := MOOCell072
let MOO.cells[73] := MOOCell073
let MOO.cells[74] := MOOCell074
let MOO.cells[75] := MOOCell075
let MOO.cells[76] := MOOCell076
let MOO.cells[77] := MOOCell077
let MOO.cells[78] := MOOCell078
let MOO.cells[79] := MOOCell079
let MOO.cells[80] := MOOCell080

let MOO.cells[81] := MOOCell081
let MOO.cells[82] := MOOCell082
let MOO.cells[83] := MOOCell083
let MOO.cells[84] := MOOCell084
let MOO.cells[85] := MOOCell085
let MOO.cells[86] := MOOCell086
let MOO.cells[87] := MOOCell087
let MOO.cells[88] := MOOCell088
let MOO.cells[89] := MOOCell089
let MOO.cells[90] := MOOCell090

let MOO.cells[91] := MOOCell091
let MOO.cells[92] := MOOCell092
let MOO.cells[93] := MOOCell093
let MOO.cells[94] := MOOCell094
let MOO.cells[95] := MOOCell095
let MOO.cells[96] := MOOCell096
let MOO.cells[97] := MOOCell097
let MOO.cells[98] := MOOCell098
let MOO.cells[99] := MOOCell099
let MOO.cells[100] := MOOCell100

let MOO.cells[101] := MOOCell101
let MOO.cells[102] := MOOCell102
let MOO.cells[103] := MOOCell103
let MOO.cells[104] := MOOCell104
let MOO.cells[105] := MOOCell105
let MOO.cells[106] := MOOCell106
let MOO.cells[107] := MOOCell107
let MOO.cells[108] := MOOCell108
let MOO.cells[109] := MOOCell109
let MOO.cells[110] := MOOCell110

let MOO.cells[111] := MOOCell111
let MOO.cells[112] := MOOCell112
let MOO.cells[113] := MOOCell113
let MOO.cells[114] := MOOCell114
let MOO.cells[115] := MOOCell115
let MOO.cells[116] := MOOCell116
let MOO.cells[117] := MOOCell117
let MOO.cells[118] := MOOCell118
let MOO.cells[119] := MOOCell119
let MOO.cells[120] := MOOCell120

let MOO.cells[121] := MOOCell121
let MOO.cells[122] := MOOCell122
let MOO.cells[123] := MOOCell123
let MOO.cells[124] := MOOCell124
let MOO.cells[125] := MOOCell125
let MOO.cells[126] := MOOCell126
let MOO.cells[127] := MOOCell127
let MOO.cells[128] := MOOCell128
let MOO.cells[129] := MOOCell129
let MOO.cells[130] := MOOCell130

let MOO.cells[131] := MOOCell131
let MOO.cells[132] := MOOCell132
let MOO.cells[133] := MOOCell133
let MOO.cells[134] := MOOCell134
let MOO.cells[135] := MOOCell135
let MOO.cells[136] := MOOCell136
let MOO.cells[137] := MOOCell137
let MOO.cells[138] := MOOCell138
let MOO.cells[139] := MOOCell139
let MOO.cells[140] := MOOCell140

let MOO.cells[141] := MOOCell141
let MOO.cells[142] := MOOCell142
let MOO.cells[143] := MOOCell143
let MOO.cells[144] := MOOCell144
let MOO.cells[145] := MOOCell145
let MOO.cells[146] := MOOCell146
let MOO.cells[147] := MOOCell147
let MOO.cells[148] := MOOCell148
let MOO.cells[149] := MOOCell149
let MOO.cells[150] := MOOCell150

let MOO.cells[151] := MOOCell151
let MOO.cells[152] := MOOCell152
let MOO.cells[153] := MOOCell153
let MOO.cells[154] := MOOCell154
let MOO.cells[155] := MOOCell155
let MOO.cells[156] := MOOCell156
let MOO.cells[157] := MOOCell157
let MOO.cells[158] := MOOCell158
let MOO.cells[159] := MOOCell159
let MOO.cells[160] := MOOCell160

let MOO.cells[161] := MOOCell161
let MOO.cells[162] := MOOCell162
let MOO.cells[163] := MOOCell163
let MOO.cells[164] := MOOCell164
let MOO.cells[165] := MOOCell165
let MOO.cells[166] := MOOCell166
let MOO.cells[167] := MOOCell167
let MOO.cells[168] := MOOCell168
let MOO.cells[169] := MOOCell169
let MOO.cells[170] := MOOCell170

let MOO.cells[171] := MOOCell171
let MOO.cells[172] := MOOCell172
let MOO.cells[173] := MOOCell173
let MOO.cells[174] := MOOCell174
let MOO.cells[175] := MOOCell175
let MOO.cells[176] := MOOCell176
let MOO.cells[177] := MOOCell177
let MOO.cells[178] := MOOCell178
let MOO.cells[179] := MOOCell179
let MOO.cells[180] := MOOCell180

let MOO.cells[181] := MOOCell181
let MOO.cells[182] := MOOCell182
let MOO.cells[183] := MOOCell183
let MOO.cells[184] := MOOCell184
let MOO.cells[185] := MOOCell185
let MOO.cells[186] := MOOCell186
let MOO.cells[187] := MOOCell187
let MOO.cells[188] := MOOCell188
let MOO.cells[189] := MOOCell189
let MOO.cells[190] := MOOCell190

let MOO.cells[191] := MOOCell191
let MOO.cells[192] := MOOCell192
let MOO.cells[193] := MOOCell193
let MOO.cells[194] := MOOCell194
let MOO.cells[195] := MOOCell195
let MOO.cells[196] := MOOCell196
let MOO.cells[197] := MOOCell197
let MOO.cells[198] := MOOCell198
let MOO.cells[199] := MOOCell199
let MOO.cells[200] := MOOCell200

let MOO.cells[201] := MOOCell201
let MOO.cells[202] := MOOCell202
let MOO.cells[203] := MOOCell203
let MOO.cells[204] := MOOCell204
let MOO.cells[205] := MOOCell205
let MOO.cells[206] := MOOCell206
let MOO.cells[207] := MOOCell207
let MOO.cells[208] := MOOCell208
let MOO.cells[209] := MOOCell209
let MOO.cells[210] := MOOCell210

let MOO.cells[211] := MOOCell211
let MOO.cells[212] := MOOCell212
let MOO.cells[213] := MOOCell213
let MOO.cells[214] := MOOCell214
let MOO.cells[215] := MOOCell215
let MOO.cells[216] := MOOCell216
let MOO.cells[217] := MOOCell217
let MOO.cells[218] := MOOCell218
let MOO.cells[219] := MOOCell219
let MOO.cells[220] := MOOCell220

let MOO.cells[221] := MOOCell221
let MOO.cells[222] := MOOCell222
let MOO.cells[223] := MOOCell223
let MOO.cells[224] := MOOCell224
let MOO.cells[225] := MOOCell225
let MOO.cells[226] := MOOCell226
let MOO.cells[227] := MOOCell227
let MOO.cells[228] := MOOCell228
let MOO.cells[229] := MOOCell229
let MOO.cells[230] := MOOCell230

let MOO.cells[231] := MOOCell231
let MOO.cells[232] := MOOCell232
let MOO.cells[233] := MOOCell233
let MOO.cells[234] := MOOCell234
let MOO.cells[235] := MOOCell235
let MOO.cells[236] := MOOCell236
let MOO.cells[237] := MOOCell237
let MOO.cells[238] := MOOCell238
let MOO.cells[239] := MOOCell239
let MOO.cells[240] := MOOCell240

let MOO.cells[241] := MOOCell241
let MOO.cells[242] := MOOCell242
let MOO.cells[243] := MOOCell243
let MOO.cells[244] := MOOCell244
let MOO.cells[245] := MOOCell245
let MOO.cells[246] := MOOCell246
let MOO.cells[247] := MOOCell247
let MOO.cells[248] := MOOCell248
let MOO.cells[249] := MOOCell249
let MOO.cells[250] := MOOCell250

let MOO.cells[251] := MOOCell251
let MOO.cells[252] := MOOCell252
let MOO.cells[253] := MOOCell253
let MOO.cells[254] := MOOCell254
let MOO.cells[255] := MOOCell255
let MOO.cells[256] := MOOCell256
let MOO.cells[257] := MOOCell257
let MOO.cells[258] := MOOCell258
let MOO.cells[259] := MOOCell259
let MOO.cells[260] := MOOCell260

let MOO.cells[261] := MOOCell261
let MOO.cells[262] := MOOCell262
let MOO.cells[263] := MOOCell263
let MOO.cells[264] := MOOCell264
let MOO.cells[265] := MOOCell265
let MOO.cells[266] := MOOCell266
let MOO.cells[267] := MOOCell267
let MOO.cells[268] := MOOCell268
let MOO.cells[269] := MOOCell269
let MOO.cells[270] := MOOCell270

let MOO.cells[271] := MOOCell271
let MOO.cells[272] := MOOCell272
let MOO.cells[273] := MOOCell273
let MOO.cells[274] := MOOCell274
let MOO.cells[275] := MOOCell275
let MOO.cells[276] := MOOCell276
let MOO.cells[277] := MOOCell277
let MOO.cells[278] := MOOCell278
let MOO.cells[279] := MOOCell279
let MOO.cells[280] := MOOCell280

let MOO.cells[281] := MOOCell281
let MOO.cells[282] := MOOCell282
let MOO.cells[283] := MOOCell283
let MOO.cells[284] := MOOCell284
let MOO.cells[285] := MOOCell285
let MOO.cells[286] := MOOCell286
let MOO.cells[287] := MOOCell287
let MOO.cells[288] := MOOCell288
let MOO.cells[289] := MOOCell289
let MOO.cells[290] := MOOCell290

let MOO.cells[291] := MOOCell291
let MOO.cells[292] := MOOCell292
let MOO.cells[293] := MOOCell293
let MOO.cells[294] := MOOCell294
let MOO.cells[295] := MOOCell295
let MOO.cells[296] := MOOCell296
let MOO.cells[297] := MOOCell297
let MOO.cells[298] := MOOCell298
let MOO.cells[299] := MOOCell299
let MOO.cells[300] := MOOCell300

let MOO.cells[301] := MOOCell301
let MOO.cells[302] := MOOCell302
let MOO.cells[303] := MOOCell303
let MOO.cells[304] := MOOCell304
let MOO.cells[305] := MOOCell305
let MOO.cells[306] := MOOCell306
let MOO.cells[307] := MOOCell307
let MOO.cells[308] := MOOCell308
let MOO.cells[309] := MOOCell309
let MOO.cells[310] := MOOCell310

let MOO.cells[311] := MOOCell311
let MOO.cells[312] := MOOCell312
let MOO.cells[313] := MOOCell313
let MOO.cells[314] := MOOCell314
let MOO.cells[315] := MOOCell315
let MOO.cells[316] := MOOCell316
let MOO.cells[317] := MOOCell317
let MOO.cells[318] := MOOCell318
let MOO.cells[319] := MOOCell319
let MOO.cells[320] := MOOCell320

let MOO.cells[321] := MOOCell321
let MOO.cells[322] := MOOCell322
let MOO.cells[323] := MOOCell323
let MOO.cells[324] := MOOCell324
let MOO.cells[325] := MOOCell325
let MOO.cells[326] := MOOCell326
let MOO.cells[327] := MOOCell327
let MOO.cells[328] := MOOCell328
let MOO.cells[329] := MOOCell329
let MOO.cells[330] := MOOCell330

let MOO.cells[331] := MOOCell331
let MOO.cells[332] := MOOCell332
let MOO.cells[333] := MOOCell333
let MOO.cells[334] := MOOCell334
let MOO.cells[335] := MOOCell335
let MOO.cells[336] := MOOCell336
let MOO.cells[337] := MOOCell337
let MOO.cells[338] := MOOCell338
let MOO.cells[339] := MOOCell339
let MOO.cells[340] := MOOCell340

let MOO.cells[341] := MOOCell341
let MOO.cells[342] := MOOCell342
let MOO.cells[343] := MOOCell343
let MOO.cells[344] := MOOCell344
let MOO.cells[345] := MOOCell345
let MOO.cells[346] := MOOCell346
let MOO.cells[347] := MOOCell347
let MOO.cells[348] := MOOCell348
let MOO.cells[349] := MOOCell349
let MOO.cells[350] := MOOCell350

let MOO.cells[351] := MOOCell351
let MOO.cells[352] := MOOCell352
let MOO.cells[353] := MOOCell353
let MOO.cells[354] := MOOCell354
let MOO.cells[355] := MOOCell355
let MOO.cells[356] := MOOCell356
let MOO.cells[357] := MOOCell357
let MOO.cells[358] := MOOCell358
let MOO.cells[359] := MOOCell359
let MOO.cells[360] := MOOCell360

let MOO.cells[361] := MOOCell361
let MOO.cells[362] := MOOCell362
let MOO.cells[363] := MOOCell363
let MOO.cells[364] := MOOCell364
let MOO.cells[365] := MOOCell365
let MOO.cells[366] := MOOCell366
let MOO.cells[367] := MOOCell367
let MOO.cells[368] := MOOCell368
let MOO.cells[369] := MOOCell369
let MOO.cells[370] := MOOCell370

let MOO.cells[371] := MOOCell371
let MOO.cells[372] := MOOCell372
let MOO.cells[373] := MOOCell373
let MOO.cells[374] := MOOCell374
let MOO.cells[375] := MOOCell375
let MOO.cells[376] := MOOCell376
let MOO.cells[377] := MOOCell377
let MOO.cells[378] := MOOCell378
let MOO.cells[379] := MOOCell379
let MOO.cells[380] := MOOCell380

let MOO.cells[381] := MOOCell381
let MOO.cells[382] := MOOCell382
let MOO.cells[383] := MOOCell383
let MOO.cells[384] := MOOCell384
let MOO.cells[385] := MOOCell385
let MOO.cells[386] := MOOCell386
let MOO.cells[387] := MOOCell387
let MOO.cells[388] := MOOCell388
let MOO.cells[389] := MOOCell389
let MOO.cells[390] := MOOCell390

let MOO.cells[391] := MOOCell391
let MOO.cells[392] := MOOCell392
let MOO.cells[393] := MOOCell393
let MOO.cells[394] := MOOCell394
let MOO.cells[395] := MOOCell395
let MOO.cells[396] := MOOCell396
let MOO.cells[397] := MOOCell397
let MOO.cells[398] := MOOCell398
let MOO.cells[399] := MOOCell399
let MOO.cells[400] := MOOCell400

let MOO.cells[401] := MOOCell401
let MOO.cells[402] := MOOCell402
let MOO.cells[403] := MOOCell403
let MOO.cells[404] := MOOCell404
let MOO.cells[405] := MOOCell405
let MOO.cells[406] := MOOCell406
let MOO.cells[407] := MOOCell407
let MOO.cells[408] := MOOCell408
let MOO.cells[409] := MOOCell409
let MOO.cells[410] := MOOCell410

let MOO.cells[411] := MOOCell411
let MOO.cells[412] := MOOCell412
let MOO.cells[413] := MOOCell413
let MOO.cells[414] := MOOCell414
let MOO.cells[415] := MOOCell415
let MOO.cells[416] := MOOCell416
let MOO.cells[417] := MOOCell417
let MOO.cells[418] := MOOCell418
let MOO.cells[419] := MOOCell419
let MOO.cells[420] := MOOCell420

let MOO.cells[421] := MOOCell421
let MOO.cells[422] := MOOCell422
let MOO.cells[423] := MOOCell423
let MOO.cells[424] := MOOCell424
let MOO.cells[425] := MOOCell425
let MOO.cells[426] := MOOCell426
let MOO.cells[427] := MOOCell427
let MOO.cells[428] := MOOCell428
let MOO.cells[429] := MOOCell429
let MOO.cells[430] := MOOCell430

let MOO.cells[431] := MOOCell431
let MOO.cells[432] := MOOCell432
let MOO.cells[433] := MOOCell433
let MOO.cells[434] := MOOCell434
let MOO.cells[435] := MOOCell435
let MOO.cells[436] := MOOCell436
let MOO.cells[437] := MOOCell437
let MOO.cells[438] := MOOCell438
let MOO.cells[439] := MOOCell439
let MOO.cells[440] := MOOCell440

let MOO.cells[441] := MOOCell441
let MOO.cells[442] := MOOCell442
let MOO.cells[443] := MOOCell443
let MOO.cells[444] := MOOCell444
let MOO.cells[445] := MOOCell445
let MOO.cells[446] := MOOCell446
let MOO.cells[447] := MOOCell447
let MOO.cells[448] := MOOCell448
let MOO.cells[449] := MOOCell449
let MOO.cells[450] := MOOCell450

let MOO.cells[451] := MOOCell451
let MOO.cells[452] := MOOCell452
let MOO.cells[453] := MOOCell453
let MOO.cells[454] := MOOCell454
let MOO.cells[455] := MOOCell455
let MOO.cells[456] := MOOCell456
let MOO.cells[457] := MOOCell457
let MOO.cells[458] := MOOCell458
let MOO.cells[459] := MOOCell459
let MOO.cells[460] := MOOCell460

let MOO.cells[461] := MOOCell461
let MOO.cells[462] := MOOCell462
let MOO.cells[463] := MOOCell463
let MOO.cells[464] := MOOCell464
let MOO.cells[465] := MOOCell465
let MOO.cells[466] := MOOCell466
let MOO.cells[467] := MOOCell467
let MOO.cells[468] := MOOCell468
let MOO.cells[469] := MOOCell469
let MOO.cells[470] := MOOCell470

let MOO.cells[471] := MOOCell471
let MOO.cells[472] := MOOCell472
let MOO.cells[473] := MOOCell473
let MOO.cells[474] := MOOCell474
let MOO.cells[475] := MOOCell475
let MOO.cells[476] := MOOCell476
let MOO.cells[477] := MOOCell477
let MOO.cells[478] := MOOCell478
let MOO.cells[479] := MOOCell479
let MOO.cells[480] := MOOCell480

let MOO.cells[481] := MOOCell481
let MOO.cells[482] := MOOCell482
let MOO.cells[483] := MOOCell483
let MOO.cells[484] := MOOCell484
let MOO.cells[485] := MOOCell485
let MOO.cells[486] := MOOCell486
let MOO.cells[487] := MOOCell487
let MOO.cells[488] := MOOCell488
let MOO.cells[489] := MOOCell489
let MOO.cells[490] := MOOCell490

let MOO.cells[491] := MOOCell491
let MOO.cells[492] := MOOCell492
let MOO.cells[493] := MOOCell493
let MOO.cells[494] := MOOCell494
let MOO.cells[495] := MOOCell495
let MOO.cells[496] := MOOCell496
let MOO.cells[497] := MOOCell497
let MOO.cells[498] := MOOCell498
let MOO.cells[499] := MOOCell499
let MOO.cells[500] := MOOCell500


; INIT NAMES
while i <= 500
	let MOO.cellsnames[i] := "Unknown Location"
	set i to i + 1
loop

End