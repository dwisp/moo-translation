Maskar's Oblivion Overhaul.esp
0x4C689F
MOODungeonCreateTunnelDiggersFunctionScript
Scn MOODungeonCreateTunnelDiggersFunctionScript

short mydigger

short myx
short myy
short mydirection

short i
short j
short roomtile
short tunneltile

short roomx
short roomy
short tunnelx
short tunnely

Begin Function { mydigger }

set i to 64 ; normal room
set j to 64 + 32 ; room converter

let myx := MOO.roomx[mydigger]
let myy := MOO.roomy[mydigger]
let mydirection := MOO.roomdirection[mydigger]

if myy > 27 || myy < 2 || myx > 27 || myx < 2
	return
endif

if mydirection == 1 ; up
	set roomx to myx
	set roomy to myy + 1
	set tunnelx to myx
	set tunnely to myy + 2
elseif mydirection == 2 ; right
	set roomx to myx + 1
	set roomy to myy
	set tunnelx to myx + 2
	set tunnely to myy
elseif mydirection == 4 ; down
	set roomx to myx
	set roomy to myy - 1
	set tunnelx to myx
	set tunnely to myy - 2
elseif mydirection == 8 ; left
	set roomx to myx - 1
	set roomy to myy
	set tunnelx to myx - 2
	set tunnely to myy
else
	return
endif

let roomtile := MOO.dungeonmap[roomx][roomy]
let tunneltile := MOO.dungeonmap[tunnelx][tunnely]


; CHECK IF TUNNEL TILE IS AVAILABLE FOR DIGGER
if tunneltile != 0
	return
endif

; CHECK IF ROOM WALL CAN BE CONVERTED TO DOORWAY + CONVERT
if eval ( mydirection == 1 ) && ( roomtile == 12 << 4 )
	let MOO.dungeonmap[roomx][roomy] := 1 + ( 12 << 4 )
elseif eval ( mydirection == 2 ) && ( roomtile == 9 << 4 )
	let MOO.dungeonmap[roomx][roomy] := 2 + ( 9 << 4 )
elseif eval ( mydirection == 4 ) && ( roomtile == 3 << 4 )
	let MOO.dungeonmap[roomx][roomy] := 4 + ( 3 << 4 )
elseif eval ( mydirection == 8 ) && ( roomtile == 6 << 4 )
	let MOO.dungeonmap[roomx][roomy] := 8 + ( 6 << 4 )
else
	return
endif

; ADD DIGGER TO TUNNEL ARRAYS
set MOO.tunneldiggers to MOO.tunneldiggers + 1
let MOO.tunnelx[MOO.tunneldiggers] := tunnelx
let MOO.tunnely[MOO.tunneldiggers] := tunnely
let MOO.tunneldirection[MOO.tunneldiggers] := mydirection


; CREATE FILLER TILE
if mydirection == 1
	let MOO.dungeonmap[tunnelx][tunnely] := 4
elseif mydirection == 2
	let MOO.dungeonmap[tunnelx][tunnely] := 8
elseif mydirection == 4
	let MOO.dungeonmap[tunnelx][tunnely] := 1
elseif mydirection == 8
	let MOO.dungeonmap[tunnelx][tunnely] := 2
endif

End