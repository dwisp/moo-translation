Maskar's Oblivion Overhaul.esp
0x4A4637
MOOFixMMMCarrionRatsFunctionScript
Scn MOOFixMMMCarrionRatsFunctionScript

ref mypackage

Begin Function {  }

set mypackage to GetFormFromMod "Mart's Monster Mod.esm" "00DCB5" ; aaaCarrionStay

if GetObjectType mypackage == 61

	let MOO.mydata := GetPackageTargetData mypackage
	let MOO.mydata["ObjectType"] := "Food"
	SetPackageTargetData mypackage MOO.mydata

	if MOO.ini_debug
		printc "[MOO] Fixing MMM Carrion Rat AI."
	endif

endif

End