Maskar's Oblivion Overhaul.esp
0x4C9871
MOODungeonPickDirectionTunnelFunctionScript
Scn MOODungeonPickDirectionTunnelFunctionScript

short mydigger

short myrand

short mydirection

Begin Function { mydigger }

; DIGGER
; MOO.dungeonx[myx]
; MOO.dungeony[myy]
; MOO.dungeondirection[mydigger] ; 1, 2, 4, 8 == top, left, bottom, right

let mydirection := MOO.tunneldirection[mydigger]

set myrand to GetRandomPercent
if myrand < 30 ; 20
	; turn left
	let mydirection := mydirection << 1
	if mydirection == 16
		set mydirection to 1
	endif

elseif myrand < 60 ; 40
	; turn right
	let mydirection := mydirection >> 1
	if mydirection == 0
		set mydirection to 8
	endif

elseif myrand < 70 ; 50
	; stop
	set mydirection to 0

endif

SetFunctionValue mydirection

End
