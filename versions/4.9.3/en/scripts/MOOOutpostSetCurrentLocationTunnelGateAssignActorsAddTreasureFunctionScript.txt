Maskar's Oblivion Overhaul.esp
0x528D3E
MOOOutpostSetCurrentLocationTunnelGateAssignActorsAddTreasureFunctionScript
Scn MOOOutpostSetCurrentLocationTunnelGateAssignActorsAddTreasureFunctionScript

short i
short mytreasureid
short mychance
float randminx
float randmaxx
float randminy
float randmaxy

short myx
short myy

Begin Function { i mytreasureid mychance randminx randmaxx randminy randmaxy }

while i
	if GetRandomPercent < mychance
		set myx to Rand randminx randmaxx
		set myy to Rand randminy randmaxy
		let MOO.dungeonmapevent[myx][myy] := mytreasureid
	endif
	set i to i - 1
loop

End