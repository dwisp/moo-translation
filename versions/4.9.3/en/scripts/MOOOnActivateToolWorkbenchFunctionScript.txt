Maskar's Oblivion Overhaul.esp
0x37EC9B
MOOOnActivateToolWorkbenchFunctionScript
Scn MOOOnActivateToolWorkbenchFunctionScript

ref mycontainer
ref myactivator

Begin Function { mycontainer myactivator }

if mycontainer
	messageex "Add boards, ingots and/or cloth to create furniture."
	return
endif

; ACTIVATOR

set mycontainer to Call MOOCraftGetContainerFromActivatorFunctionScript myactivator

; START MENU
set MOOCraft.mystage to 1
set MOOCraft.mycontainer to mycontainer
set MOOCraft.mytype to MOO.type_Workbench
StartQuest MOOCraft

End