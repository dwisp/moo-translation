Maskar's Oblivion Overhaul.esp
0x5FF90B
MOOInitScrollFactionRatingsGetRatingFunctionScript
Scn MOOInitScrollFactionRatingsGetRatingFunctionScript

short myfame
short factionstrength

string_var mystring

Begin Function { myfame factionstrength }

if myfame == -1
	let mystring := "<IMG src=%qIcons/MOO/Books/crown.dds%q width=40 height=32>- <IMG src=%qIcons/MOO/Books/damage.dds%q width=40 height=32>" + $factionstrength
else
	let mystring := "<IMG src=%qIcons/MOO/Books/crown.dds%q width=40 height=32>" + $myfame + " <IMG src=%qIcons/MOO/Books/damage.dds%q width=40 height=32>" + $factionstrength
endif

SetFunctionValue mystring

sv_Destruct mystring

End