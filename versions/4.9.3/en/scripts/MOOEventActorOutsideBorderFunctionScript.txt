Maskar's Oblivion Overhaul.esp
0x507370
MOOEventActorOutsideBorderFunctionScript
Scn MOOEventActorOutsideBorderFunctionScript

ref myplacer

short isplaced

Begin Function { myplacer }

Call MOOPlaceWorldSpaceActorFunctionScript myplacer

set isplaced to 1

SetFunctionValue isplaced

End