Maskar's Oblivion Overhaul.esp
0x496B7F
MOOAICompanionRepairFunctionScript
Scn MOOAICompanionRepairFunctionScript

ref me
ref mytarget

ref myitem

short totalhammers
short totalitemsrepaired

float hammerhealthcurrent
float hammerhealthmax

float itemhealthperc
float itemhealthcurrent
float itemhealthmax
float itemhealthrequired
float itemhealthmod

float myskill

short maxhammers
ref mycontainer
short costhammer
short totalcost
float mygold
short i

short brokenitemfixed

float totalhealthrepaired

short busy

Begin Function { me mytarget }

set costhammer to 25


if ( me.OffersRepair ) && ( me != player )

	set mycontainer to me.GetMerchantContainer
	set totalhammers to mycontainer.GetItemCount RepairHammer

	set i to Call MOOHelperCompanionGetEntryFunctionScript mytarget
	let mygold := MOO.companionsgold[i]

	if ( mygold < costhammer ) || ( mycontainer == 0 )
		return
	endif

else

	set totalhammers to me.GetItemCount RepairHammer

endif

set maxhammers to totalhammers

if totalhammers == 0
	return
endif


set myskill to me.GetAV Armorer

set hammerhealthmax to ( myskill + 3 ) * ( 100 / ( 30 + ( myskill * -0.2 ) ) )
if myskill >= 25
	set hammerhealthmax to hammerhealthmax * 2
endif
if myskill >= 75
	set itemhealthperc to 1.25
else
	set itemhealthperc to 1
endif


foreach myitem <- mytarget

	if myitem.IsArmor || ( myitem.IsWeapon && myitem.GetWeaponType != 4 ) ; can't repair staff
	if myitem.GetEnchantment == 0 || myskill >= 50 ; repair magic

		set itemhealthmax to myitem.GetObjectHealth * itemhealthperc
		set itemhealthcurrent to myitem.GetCurrentHealth
		set itemhealthrequired to itemhealthmax - itemhealthcurrent

		; PAY GOLD IF REPAIR IS DONE BY MERCHANT
		while ( itemhealthrequired > 0 ) && ( mycontainer == 0 || mygold >= costhammer )

			set mygold to mygold - costhammer

			set totalhealthrepaired to totalhealthrepaired + itemhealthrequired

			; NOT ENOUGH HEALTH ON HAMMER
			if hammerhealthcurrent < itemhealthrequired
				if totalhammers
					set hammerhealthcurrent to hammerhealthcurrent + hammerhealthmax

					if myskill < 100 ; 100 skill can repair without breaking hammers
						set totalhammers to totalhammers - 1
					endif

				; NO MORE HAMMERS - STOP
				else

					; REPAIR SOME HEALTH
					if hammerhealthcurrent
						set itemhealthmod to hammerhealthcurrent + itemhealthcurrent
						myitem.SetCurrentHealth itemhealthmod

						if itemhealthcurrent <= 0
							set brokenitemfixed to 1
						endif
					endif

					set itemhealthrequired to 0
				endif

			; REPAIR ITEM
			else
				set hammerhealthcurrent to hammerhealthcurrent - itemhealthrequired
				set itemhealthrequired to 0

				myitem.SetCurrentHealth itemhealthmax

				if itemhealthcurrent <= 0
					set brokenitemfixed to 1
				endif

				set totalitemsrepaired to totalitemsrepaired + 1

			endif

		loop

	endif
	endif
loop


; CHECK IF ANYTHING REPAIRED
if totalhealthrepaired

; 1+ ITEMS REPAIRED
if totalitemsrepaired
	PlaySound UIArmorWeaponRepair
	;set MOOAI.dialogcompanionrepair to 0

	; MERCHANT USING HAMMERS FROM CONTAINER
	if mycontainer

		set totalhammers to maxhammers - totalhammers

		if totalhammers
			mycontainer.RemoveItemNS RepairHammer totalhammers
		endif

		if mytarget.GetDistance player < 1000
			PlaySound ITMGoldUp
		endif

	; NPC OR PLAYER CHARACTER
	else
		set totalhammers to ( me.GetItemCount RepairHammer ) - totalhammers
		if totalhammers
			me.RemoveItemNS RepairHammer totalhammers
		endif
	endif

	; RESET EQUIPMENT IF BROKEN ITEM IS FIXED
	if brokenitemfixed
		Call MOOHelperCompanionResetEquipmentFunctionScript mytarget
	endif

	set busy to 1

; NO ITEMS REPAIRED WHILE BREAKING HAMMER
elseif totalhammers != me.GetItemCount RepairHammer
	PlaySound UIArmorWeaponRepairBreak

endif
endif


; REMOVE GOLD FROM COMPANION
if mycontainer
	let MOO.companionsgold[i] := mygold
endif


; UPDATE DIALOG ( ALWAYS DISABLE )
set MOOAI.dialogcompanionrepair to 0


SetFunctionValue busy

End