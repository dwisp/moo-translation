Maskar's Oblivion Overhaul.esp
0x5D0F7B
MOOTokenVampireFleeFunctionScript
Scn MOOTokenVampireFleeFunctionScript

ref me

short flee

Begin Function { me }

set flee to 1
if me.IsInCombat
	me.StopCombat
	me.SetAV Aggression 5
	me.AddScriptPackage MOOAIFleeFromSun
elseif me.IsInInterior
	set flee to 0
	me.SetAV Aggression 100
	me.Evp
endif

SetFunctionValue flee

End