Maskar's Oblivion Overhaul.esp
0x3EBEBB
MOOEquipCapeFunctionScript
Scn MOOEquipCapeFunctionScript

ref me
ref myrace
ref mycape
ref mycontainer

Begin Function { me }

set myrace to me.GetRace
if myrace == Argonian
	if me.GetFactionRank MOOBlackMarshFaction >= 3
		set mycape to MOOLL0RegionCloakArgonian
	else
		set mycape to MOOLL0RegionCapeArgonian
	endif
elseif myrace == DarkElf
	if me.GetFactionRank MOOMorrowindFaction >= 3
		set mycape to MOOLL0RegionCloakDunmer
	else
		set mycape to MOOLL0RegionCapeDunmer
	endif
elseif myrace == Khajiit
	if me.GetFactionRank MOOElsweyrFaction >= 3
		set mycape to MOOLL0RegionCloakKhajiit
	else
		set mycape to MOOLL0RegionCapeKhajiit
	endif
elseif myrace == Nord
	if me.GetFactionRank MOOSkyrimFaction >= 3
		set mycape to MOOLL0RegionCloakNord
	else
		set mycape to MOOLL0RegionCapeNord
	endif
elseif myrace == Orc
	if me.GetFactionRank MOOOrsiniumFaction >= 3
		set mycape to MOOLL0RegionCloakOrc
	else
		set mycape to MOOLL0RegionCapeOrc
	endif
elseif myrace == Redguard
	if me.GetFactionRank MOOHammerfellFaction >= 3
		set mycape to MOOLL0RegionCloakRedguard
	else
		set mycape to MOOLL0RegionCapeRedguard
	endif
elseif myrace == WoodElf
	if me.GetFactionRank MOOValenwoodFaction >= 3
		set mycape to MOOLL0RegionCloakBosmer
	else
		set mycape to MOOLL0RegionCapeBosmer
	endif
endif

if mycape
	set mycape to CalcLeveledItem mycape 99
	if mycape
		if me.GetItemCount MOOTokenCapeAdded == 0
			me.AddItemNS MOOTokenCapeAdded 1
			me.AddItemNS mycape 1
		endif
		if me.GetEquippedObject 15 == 0
			me.EquipItemNS mycape
		endif
	endif
endif

End