Maskar's Oblivion Overhaul.esp
0x429355
MOORuneUpdateSpellsFunctionScript
Scn MOORuneUpdateSpellsFunctionScript

ref me
ref myspell1
ref myspell2

ref basespell1
ref basespell2
short i

Begin Function { me myspell1 myspell2 }

set basespell1 to MOO.runespell1
set basespell2 to MOO.runespell2


; templates spell used to update used spells
; MOO.runespell1
; MOO.runespell2

if MOO.ini_debug == 2
	printc "[MOO] Updating %n and %n on %n." myspell1 myspell2 me
endif

set i to 2 * me.GetItemCount MOORuneAttribute01
SetNthEIMagnitude i basespell1 0
set i to 2 * me.GetItemCount MOORuneAttribute02
SetNthEIMagnitude i basespell1 1
set i to 2 * me.GetItemCount MOORuneAttribute03
SetNthEIMagnitude i basespell1 2
set i to 2 * me.GetItemCount MOORuneAttribute04
SetNthEIMagnitude i basespell1 3
set i to 2 * me.GetItemCount MOORuneAttribute05
SetNthEIMagnitude i basespell1 4
set i to 2 * me.GetItemCount MOORuneAttribute06
SetNthEIMagnitude i basespell1 5
set i to 2 * me.GetItemCount MOORuneAttribute07
SetNthEIMagnitude i basespell1 6
set i to 2 * me.GetItemCount MOORuneAttribute08
SetNthEIMagnitude i basespell1 7

set i to 2 * me.GetItemCount MOORuneSkill01
SetNthEIMagnitude i basespell2 0
set i to 2 * me.GetItemCount MOORuneSkill02
SetNthEIMagnitude i basespell2 1
set i to 2 * me.GetItemCount MOORuneSkill03
SetNthEIMagnitude i basespell2 2
set i to 2 * me.GetItemCount MOORuneSkill04
SetNthEIMagnitude i basespell2 3
set i to 2 * me.GetItemCount MOORuneSkill05
SetNthEIMagnitude i basespell2 4
set i to 2 * me.GetItemCount MOORuneSkill06
SetNthEIMagnitude i basespell2 5
set i to 2 * me.GetItemCount MOORuneSkill07
SetNthEIMagnitude i basespell2 6
set i to 2 * me.GetItemCount MOORuneSkill08
SetNthEIMagnitude i basespell2 7
set i to 2 * me.GetItemCount MOORuneSkill09
SetNthEIMagnitude i basespell2 8
set i to 2 * me.GetItemCount MOORuneSkill10
SetNthEIMagnitude i basespell2 9
set i to 2 * me.GetItemCount MOORuneSkill11
SetNthEIMagnitude i basespell2 10
set i to 2 * me.GetItemCount MOORuneSkill12
SetNthEIMagnitude i basespell2 11
set i to 2 * me.GetItemCount MOORuneSkill13
SetNthEIMagnitude i basespell2 12
set i to 2 * me.GetItemCount MOORuneSkill14
SetNthEIMagnitude i basespell2 13
set i to 2 * me.GetItemCount MOORuneSkill15
SetNthEIMagnitude i basespell2 14
set i to 2 * me.GetItemCount MOORuneSkill16
SetNthEIMagnitude i basespell2 15
set i to 2 * me.GetItemCount MOORuneSkill17
SetNthEIMagnitude i basespell2 16
set i to 2 * me.GetItemCount MOORuneSkill18
SetNthEIMagnitude i basespell2 17
set i to 2 * me.GetItemCount MOORuneSkill19
SetNthEIMagnitude i basespell2 18
set i to 2 * me.GetItemCount MOORuneSkill20
SetNthEIMagnitude i basespell2 19
set i to 2 * me.GetItemCount MOORuneSkill21
SetNthEIMagnitude i basespell2 20



; copy all effects with magnitude > 0

Call MOOCopyMagicEffectsFunctionScript basespell1 myspell1
Call MOOCopyMagicEffectsFunctionScript basespell2 myspell2



End