Maskar's Oblivion Overhaul.esp
0x555C8E
MOOXPMissingPetsFunctionScript
Scn MOOXPMissingPetsFunctionScript

short petsfound

string_var mystring
short xp

Begin Function { petsfound }

set xp to petsfound * MOO.ini_XP_return_missingpet

set mystring to sv_Construct "set ObXPMain.interOpGainedXP to %.0f" xp
RunScriptLine "set ObXPMain.interOpGainedXPMessage to sv_Construct %q Returned Missing Pet.%q"
RunScriptLine $mystring

sv_Destruct mystring

End