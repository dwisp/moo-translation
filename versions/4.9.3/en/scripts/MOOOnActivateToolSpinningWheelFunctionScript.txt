Maskar's Oblivion Overhaul.esp
0x37EC99
MOOOnActivateToolSpinningWheelFunctionScript
Scn MOOOnActivateToolSpinningWheelFunctionScript

ref mycontainer
ref myactivator

Begin Function { mycontainer myactivator }

if mycontainer
	messageex "Add wool to create yarn."
	return
endif

; ACTIVATOR

set mycontainer to Call MOOCraftGetContainerFromActivatorFunctionScript myactivator

set MOOCraft.mystage to 1
set MOOCraft.mycontainer to mycontainer
set MOOCraft.mytype to MOO.type_SpinningWheel
StartQuest MOOCraft


End