Maskar's Oblivion Overhaul.esp
0x559B15
MOOActivatorEventActorPlaceOnLoadCampsiteBanditsFunctionScript
Scn MOOActivatorEventActorPlaceOnLoadCampsiteBanditsFunctionScript

ref me
short myseed

ref myboss
ref mygrunt

Begin Function { me myseed }

; OUTLAWS + PILLAGERS + BANDITS + MARAUDERS
if MOO.safetyzone == 3
	if myseed < 50
		set myboss to MOOLL0OutlawBoss
		set mygrunt to MOOLL2Outlaw
	else
		set myboss to MOOLL0PlundererBoss
		set mygrunt to MOOLL2Plunderer
	endif
else
	if myseed < 50
		set myboss to MOOLL0BanditBoss
		set mygrunt to MOOLL2BanditGrunt
	else
		set myboss to MOOLL0MarauderBoss
		set mygrunt to MOOLL2MarauderGrunt
	endif
endif


; NOT EVERY CAMPSITE HAS A BOSS
if GetRandomPercent < 65
	set myboss to mygrunt
endif


; BOSS
if me.GetBaseObject == MOOMarkerEventActorCampsiteBoss
	me.PlaceAtMe myboss

; GRUNT
else
	me.PlaceAtMe mygrunt
endif

End