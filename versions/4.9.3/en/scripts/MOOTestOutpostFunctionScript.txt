Maskar's Oblivion Overhaul.esp
0x50B1E4
MOOTestOutpostFunctionScript
Scn MOOTestOutpostFunctionScript

short myx
short myy

short mytileid
short myeventid

Begin Function {  }

set myy to 0
while myy < 30

	set myx to 0
	while myx < 30

		let mytileid := MOO.dungeonmap[myx][myy]
		let myeventid := MOO.dungeonmapevent[myx][myy] & 31

		if mytileid != 5 && mytileid != 10
			if myeventid == MOO.id_dungeon_lever
				messagebox "LEVER ON WRONG TILE!!"
			elseif myeventid == MOO.id_dungeon_plate
				messagebox "PLATE ON WRONG TILE!!"
			endif
		endif

		set myx to myx + 1
	loop

	set myy to myy + 1
loop


End