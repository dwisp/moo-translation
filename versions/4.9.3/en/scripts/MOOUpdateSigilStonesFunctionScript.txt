Maskar's Oblivion Overhaul.esp
0x1F2021
MOOUpdateSigilStonesFunctionScript
Scn MOOUpdateSigilStonesFunctionScript

Begin Function {  }

if MOO.ini_update_sigilstones == 1 ; completely unleveled
	Call MOOUpdateLeveledItemFunctionScript LL2SigilStones100
elseif MOO.ini_update_sigilstones == 2 ; always best available
	Call MOOUpdateLeveledRewardFunctionScript LL2SigilStones100
endif

End