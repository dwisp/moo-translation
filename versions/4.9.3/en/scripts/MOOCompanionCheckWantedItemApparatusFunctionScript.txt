Maskar's Oblivion Overhaul.esp
0x5EF50A
MOOCompanionCheckWantedItemApparatusFunctionScript
Scn MOOCompanionCheckWantedItemApparatusFunctionScript

ref me
ref myitem

ref mymortar

ref mywanteditem

Begin Function { me myitem }

; ALCHEMY
if me.IsMajorC 19
if myitem.GetApparatusType == 0 ; mortar
	set mymortar to Call MOOCompanionUpdateTradeItemsWithMerchantApparatusGetBestFunctionScript me 0

	if GetQuality mymortar < GetQuality myitem
		set mywanteditem to myitem
	endif
endif
endif


SetFunctionValue mywanteditem

End