Maskar's Oblivion Overhaul.esp
0x547ADD
MOOCompanionUpdateTradeItemsWithMerchantArmorFunctionScript
Scn MOOCompanionUpdateTradeItemsWithMerchantArmorFunctionScript

ref mycompanion
ref mymerchant
ref mycontainer
short i

ref myitem
short mytotal

short busy

Begin Function { mycompanion mymerchant mycontainer i }

; 0	/ 1head / hair
; 2	upper body
; 3	lower body
; 4	hand
; 5	foot
; 13: shield

set myitem to Call MOOHelperGetUnwantedItemForSlotFunctionScript mycompanion mymerchant 0
if myitem == 0
	set myitem to Call MOOHelperGetUnwantedItemForSlotFunctionScript mycompanion mymerchant 1
endif
if myitem == 0
	set myitem to Call MOOHelperGetUnwantedItemForSlotFunctionScript mycompanion mymerchant 2
endif
if myitem == 0
	set myitem to Call MOOHelperGetUnwantedItemForSlotFunctionScript mycompanion mymerchant 3
endif
if myitem == 0
	set myitem to Call MOOHelperGetUnwantedItemForSlotFunctionScript mycompanion mymerchant 4
endif
if myitem == 0
	set myitem to Call MOOHelperGetUnwantedItemForSlotFunctionScript mycompanion mymerchant 5
endif
if myitem == 0
	set myitem to Call MOOHelperGetUnwantedItemForSlotFunctionScript mycompanion mymerchant 13
endif

if myitem
	set mytotal to mycompanion.GetItemCount myitem
	set busy to Call MOOCompanionUpdateTradeItemsWithMerchantSellFunctionScript mycompanion mymerchant i myitem mytotal
endif

SetFunctionValue busy

End