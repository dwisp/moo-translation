Maskar's Oblivion Overhaul.esp
0x4BF476
MOOGetGameLoadedEventHandlersFunctionScript
Scn MOOGetGameLoadedEventHandlersFunctionScript

Begin Function {  }

SetEventHandler "OnActorUnequip" MOOOnUnequipByPlayerFunctionScript ref::PlayerRef
SetEventHandler "OnActorEquip" MOOOnEquipByPlayerFunctionScript ref::PlayerRef
SetEventHandler "OnHit" MOOOnHitByCreatureFunctionScript
SetEventHandler "OnHitWith" MOOOnHitWithWeaponFunctionScript
SetEventHandler "OnAttack" MOOOnAttackByPlayerFunctionScript ref::PlayerRef
SetEventHandler "OnDeath" MOOOnDeathByCreatureFunctionScript
SetEventHandler "OnRelease" MOOOnReleaseFunctionScript
;SetEventHandler "OnActivate" MOOOnActivateByPlayerFunctionScript object::PlayerRef
SetEventHandler "OnActivate" MOOOnActivateByCreatureFunctionScript ; object::PlayerRef
SetEventHandler "OnScrollCast" MOOOnScrollCastByPlayerFunctionScript ref::PlayerRef
SetEventHandler "OnSpellCast" MOOOnSpellCastByThiefFunctionScript object::MOOSpellSteal
SetEventHandler "OnMagicApply" MOOOnMagicApplyFunctionScript
SetEventHandler "OnStartCombat" MOOOnStartCombatFunctionScript
SetEventHandler "OnCreatePotion" MOOOnCreatePotionFunctionScript
SetEventHandler "OnMagicEffectHit" MOOOnMagicEffectHitFunctionScript
SetEventHandler "OnKnockout" MOOOnKnockoutFunctionScript
SetEventHandler "OnActivateDrinkSource" MOOOnActivateDrinkSourceFunctionScript
SetEventHandler "OnActorDrop" MOOOnDropByPlayerFunctionScript ref::PlayerRef

if MOO.ini_damage_combat != -1
	set MOO.ini_damage_combat to MOO.ini_damage_combat - 1
	SetEventHandler "OnHealthDamage" MOOOnHealthDamageFunctionScript
endif

End