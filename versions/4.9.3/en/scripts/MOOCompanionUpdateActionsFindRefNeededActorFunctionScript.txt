Maskar's Oblivion Overhaul.esp
0x55FAC8
MOOCompanionUpdateActionsFindRefNeededActorFunctionScript
Scn MOOCompanionUpdateActionsFindRefNeededActorFunctionScript

ref myactor

ref me

ref tempitem
float tempdistance

ref myitem
float mydistance

Begin Function { myactor }

set mydistance to 99999999

set me to GetFirstRef 69 1 ; Actors
while me
	; CHECK QUALIFYING
	if me.GetDead
	if Call MOOCompanionUpdateActionsFindRefNeededContainerCheckContentsFunctionScript myactor me

		set tempitem to me
		set tempdistance to me.GetDistance myactor

		; CHECK DISTANCE
		if tempdistance <= mydistance
			set myitem to tempitem
			set mydistance to tempdistance
		endif
	endif
	endif

	set me to GetNextRef
Loop

SetFunctionValue myitem

End