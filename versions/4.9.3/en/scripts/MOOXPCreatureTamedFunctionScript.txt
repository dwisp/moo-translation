Maskar's Oblivion Overhaul.esp
0x53842D
MOOXPCreatureTamedFunctionScript
Scn MOOXPCreatureTamedFunctionScript

ref mycreature

short xp
string_var mystring

Begin Function { mycreature }

set xp to mycreature.GetLevel * MOO.ini_XP_tame_creature

set mystring to sv_Construct "set ObXPMain.interOpGainedXP to %.0f" xp
RunScriptLine "set ObXPMain.interOpGainedXPMessage to sv_Construct %q Tamed Creature.%q"
RunScriptLine $mystring

sv_Destruct mystring

End