Maskar's Oblivion Overhaul.esp
0x313691
MOOGetHammerTypeFunctionScript
Scn MOOGetHammerTypeFunctionScript

ref me

ref myweapon
short type

Begin Function { me }

set myweapon to me.GetEquippedObject 16
if myweapon
if myweapon == MOOWeapHammer
	set type to 1
elseif CompareModelPath "\fsb\hammer01.nif" myweapon
	set type to 1
endif
endif

SetFunctionValue type

End
