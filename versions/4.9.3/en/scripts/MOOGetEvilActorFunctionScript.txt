Maskar's Oblivion Overhaul.esp
0x12763D
MOOGetEvilActorFunctionScript
Scn MOOGetEvilActorFunctionScript

ref me
ref attacker

ref myfaction
short i

short evil

Begin Function { me attacker }

set evil to 0

if me.GetAV Confidence > 20

set i to me.GetNumFactions
while i > 0
	set i to i - 1
	set myfaction to me.GetNthFaction i
	if IsFactionEvil myfaction
		if attacker
			if attacker.GetInFaction myfaction
				set evil to 1
			endif
		else
			set evil to 1
		endif
	endif
loop

endif

SetFunctionValue evil

End