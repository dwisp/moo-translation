Maskar's Oblivion Overhaul.esp
0x4688E0
MOOInitNpcCallHealingTargetFunctionScript
Scn MOOInitNpcCallHealingTargetFunctionScript

ref me
ref target
short iscaster ; 1 touch 2 target

ref myspell
float myx
float myy
float myangle

Begin Function { me target iscaster }

; SKIP CLONES - EXCEPT MOO ADVENTURERS

if IsClonedForm me
if me.GetInFaction MOOCompanionFaction == 0
	return
endif
endif


; GET SPELL ( HEAL OR CURE )

if iscaster == 2
	if target.GetItemCount MOOTokenDiseaseCode && target.GetItemCount MOOTokenDiseaseNoDamage == 0
		set myspell to MOOSpellCureDiseaseTarget
	elseif target.GetVampire && target.GetTotalAEAllSpellsMagnitude LGHT
		set myspell to MOOSpellDispelTarget
	elseif me.GetLevel > 20
		set myspell to MOOSpellRestoreHealthTarget5Master
	elseif me.GetLevel > 10
		set myspell to MOOSpellRestoreHealthTarget4Expert
	else
		set myspell to MOOSpellRestoreHealthTarget3Journeyman
	endif

else
	if target.GetItemCount MOOTokenDiseaseCode && target.GetItemCount MOOTokenDiseaseNoDamage == 0
		set myspell to MOOSpellCureDiseaseTouch
	elseif target.GetVampire && target.GetTotalAEAllSpellsMagnitude LGHT
		set myspell to MOOSpellDispelTouch
	elseif me.GetLevel > 20
		set myspell to MOOSpellRestoreHealthTouch5Master
	elseif me.GetLevel > 10
		set myspell to MOOSpellRestoreHealthTouch4Expert
	else
		set myspell to MOOSpellRestoreHealthTouch3Journeyman
	endif
endif


; SET ANGLE

if me.GetDistance target < 400

	set myx to target.getpos x - me.getpos x
	set myy to target.getpos y - me.getpos y
	set myangle to ATan2 myy myx

	set myangle to 360 - myangle + 90
	if myangle >= 360
		set myangle to myangle - 360
	endif
	me.SetAngle z myangle
endif


; SET TRAVEL TARGET

let MOO.mydata := GetPackageTargetData MOOAICastHealingTravel
let MOO.mydata["Object"] := target
SetPackageTargetData MOOAICastHealingTravel MOO.mydata

me.AddScriptPackage MOOAICastHealingTravel

if MOO.ini_debug
	printc "[MOO] Added travel package to %n to heal %n." me target
endif


; SET HEAL QUEST

set MOOHeal.iscaster to iscaster

set MOOHeal.maxtime to 5
if me.IsInInterior == 0
	set MOOHeal.maxtime to MOOHeal.maxtime + 5
endif
if me.GetCreatureType != -1
	set MOOHeal.maxtime to MOOHeal.maxtime + 5
endif
if iscaster == 1
	set MOOHeal.maxtime to MOOHeal.maxtime + 5
endif

set MOOHeal.myspell to myspell
set MOOHeal.time to 0
set MOOHeal.me to me
set MOOHeal.target to target
StartQuest MOOHeal

End