Maskar's Oblivion Overhaul.esp
0x0C7D91
MOOPlaceBeetlesFunctionScript
Scn MOOPlaceBeetlesFunctionScript

ref me

Begin Function {  }

Call MOOPlaceSpawnSpotTargetFunctionScript MOOCellBeetleGeorge CGThiefSkeletonRef 522 (-104) (-102)
Call MOOPlaceSpawnSpotTargetFunctionScript MOOCellBeetleJohn CGThiefSkeletonRef 1434 1934 (-451)
Call MOOPlaceSpawnSpotTargetFunctionScript MOOCellBeetlePaul CGMarkerE2Baurus 2327 4439 (-674)
Call MOOPlaceSpawnSpotTargetFunctionScript MOOCellBeetleRingo CGSewerExitMarker 2912 2046 6892

if MOO.ini_beetle_name

	if MOO.mybeetle == 0
		set MOO.mybeetle to Rand 1 4.99
	endif

	set me to MOOBeetleYoung01

	if MOO.mybeetle == 1
		SetNameEx "George" me
	elseif MOO.mybeetle == 2
		SetNameEx "Ringo" me
	elseif MOO.mybeetle == 3
		SetNameEx "Paul" me
	elseif MOO.mybeetle == 4
		SetNameEx "John" me
	endif

endif

End