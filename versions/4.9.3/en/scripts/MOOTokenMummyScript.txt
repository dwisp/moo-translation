Maskar's Oblivion Overhaul.esp
0x0F9243
MOOTokenMummyScript
Scn MOOTokenMummyScript

ref me
float myrand
short loadcounter
float deadtime

Begin OnAdd

set myrand to ( Rand 1 5 )
set loadcounter to MOO.loadcounter

End


Begin Gamemode

set me to GetContainer
if me

	if me.GetTimeDead <= 0.003 * TimeScale * myrand
		return
	endif

	if me.GetDead && myrand && loadcounter == MOO.loadcounter

		if MOO.ini_mummy_wrappings
			if me.GetItemCount MOOMummyWraps
				me.Resurrect 1
			endif
		elseif GetRandomPercent < MOO.ini_mummy_resurrect
			me.Resurrect 1
		endif
	endif

	RemoveMe

endif

End