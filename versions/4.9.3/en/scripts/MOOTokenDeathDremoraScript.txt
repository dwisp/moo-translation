Maskar's Oblivion Overhaul.esp
0x1783CD
MOOTokenDeathDremoraScript
Scn MOOTokenDeathDremoraScript

ref me
short i

Begin Gamemode

if i
	if i > 5
		RemoveMe
	else
		set i to i + 1
	endif
	return
endif

set me to GetContainer
if me
	set i to 1

	me.AddItemNS LL0DeathDremora100 1

	if me.HasSpell AbDremora0ChurllResist
		me.AddItemNS MOOLL0LootDremora0Churl 1
	elseif me.HasSpell AbDremora1CaitlifflResist
		me.AddItemNS MOOLL0LootDremora1Caitiff 1
	elseif me.HasSpell AbDremora2KynvalResist
		me.AddItemNS MOOLL0LootDremora2Kynval 1
	elseif me.HasSpell AbDremora3KynreeveResist
		me.AddItemNS MOOLL0LootDremora3Kynreeve 1
	elseif me.HasSpell AbDremora4KynmarcherResist
		me.AddItemNS MOOLL0LootDremora4Kynmarcher 1
	elseif me.HasSpell AbDremora5MarkynazResist
		me.AddItemNS MOOLL0LootDremora5Markynaz 1
	elseif me.HasSpell AbDremora6ValkynazResist
		me.AddItemNS MOOLL0LootDremora6Valkynaz 1
	elseif me.HasSpell MOOAbDremoraDaedricLordResist
		me.AddItemNS MOOLL0LootDaedricLord 1
	endif
endif

End