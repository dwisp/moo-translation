Maskar's Oblivion Overhaul.esp
0x4B49BB
MOOGetMapMarkerForCellFunctionScript
Scn MOOGetMapMarkerForCellFunctionScript

ref me

short i
short j
short total
short found

ref mycell
ref linkedcell
ref mydoor
ref linkeddoor

short markertotal
short j

ref mymapmarker
string_var mystring

Begin Function { mycell }

sv_Erase MOO.temparray

let MOO.temparray[0] := mycell

set total to 1

while i < total

	let mycell := MOO.temparray[i]
	set mydoor to GetFirstRefInCell mycell 24

	while mydoor
		if mydoor.IsLoadDoor

			set linkeddoor to mydoor.GetLinkedDoor

			if linkeddoor
			if linkeddoor.IsInInterior

			set linkedcell to linkeddoor.GetParentCell

			set found to 0
			set j to total
			while j > 0
				set j to j - 1
				if eval MOO.temparray[j] == linkedcell
					set found to 1
				endif
			loop
			if found == 0
				let MOO.temparray[total] := linkedcell
				set total to total + 1
			endif

			endif
			endif

		endif

		set mydoor to GetNextRef
	loop

	set i to i + 1

loop


; GET MAPMARKERS

let MOO.mymapmarkers := GetMapMarkers 2
set markertotal to ar_Size MOO.mymapmarkers

set MOO.markerid to -1
set i to 0
while i < total

	let mycell := MOO.temparray[i]

	set j to 0
	while j < markertotal
			if eval $MOO.temparray[i] == $MOO.mymapmarkers[j]

				; MOO.markerid is used to determine dungeon spawn (slaves, grave diggers, etc.)
				set mystring to GetFormIDString mycell
				set MOO.markerid to ToNumber mystring 1

				let mymapmarker := MOO.mymapmarkers[j]
				set i to total
				set j to markertotal
			endif
		set j to j + 1
	loop

	set i to i + 1
loop


sv_Destruct mystring

SetFunctionValue mymapmarker

End
