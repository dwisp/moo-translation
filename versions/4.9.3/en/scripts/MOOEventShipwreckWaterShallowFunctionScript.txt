Maskar's Oblivion Overhaul.esp
0x507367
MOOEventShipwreckWaterShallowFunctionScript
Scn MOOEventShipwreckWaterShallowFunctionScript

ref myplacer

short isplaced

Begin Function { myplacer }

set isplaced to Call MOOPlaceWorldSpaceObjectShipwreckFunctionScript myplacer

SetFunctionValue isplaced

End