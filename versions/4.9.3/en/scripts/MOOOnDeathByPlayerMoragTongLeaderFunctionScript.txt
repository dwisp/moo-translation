Maskar's Oblivion Overhaul.esp
0x5631A6
MOOOnDeathByPlayerMoragTongLeaderFunctionScript
Scn MOOOnDeathByPlayerMoragTongLeaderFunctionScript

ref target
ref attacker

Begin Function { target attacker }

if target.GetFactionRank MOOMoragTongFaction == 9
if player.GetInFaction DarkBrotherhood
	set MOO.MTdayspaused to GameDaysPassed + MOO.ini_npc_moragtong_paused
endif
endif

End