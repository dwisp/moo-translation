Maskar's Oblivion Overhaul.esp
0x51E17B
MOOOutpostCheckDirectionAllowedTunnelDoneFunctionScript
Scn MOOOutpostCheckDirectionAllowedTunnelDoneFunctionScript

short myx
short myy

short mytileid

short done

Begin Function { myx myy }

; TUNNEL NOT DONE WHEN ONLY 2 TILES LONG
set done to 1

let mytileid := MOO.dungeonmap[myx][myy] & 31

; 2 TILE ENTRANCE
if mytileid == 1
	if eval MOO.dungeonmap[myx][myy + 1] == 256 + 5 + ( 1 << 4 )
		set done to 0
	endif

elseif mytileid == 2
	if eval MOO.dungeonmap[myx + 1][myy] == 256 + 10 + ( 2 << 4 )
		set done to 0
	endif

elseif mytileid == 4
	if eval MOO.dungeonmap[myx][myy - 1] == 256 + 5 + ( 4 << 4 )
		set done to 0
	endif

elseif mytileid == 8
	if eval MOO.dungeonmap[myx - 1][myy] == 256 + 10 + ( 8 << 4 )
		set done to 0
	endif

endif


; 2 TILE TELEPORTER
if eval MOO.dungeonmapevent[myx][myy] == MOO.id_dungeon_teleporter_marker

	printc "*** blue teleporter needs more tiles adding"

	set done to 0
endif


SetFunctionValue done

End