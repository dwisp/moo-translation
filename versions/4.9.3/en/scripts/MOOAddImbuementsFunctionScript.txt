Maskar's Oblivion Overhaul.esp
0x593E98
MOOAddImbuementsFunctionScript
Scn MOOAddImbuementsFunctionScript

ref myobject

Begin Function {  }

if IsModLoaded $MOO.ini_object_filename == 0
	return
endif

; SHIVERING ISLES USES OBLIVION.ESM
if ( sv_Count "DLCShiveringIsles.esp" MOO.ini_object_filename > 0 )
	let MOO.ini_object_filename := "Oblivion.esm"
endif

set myobject to GetFormFromMod $MOO.ini_object_filename $MOO.ini_object_target_id
if myobject == 0
	return
endif
if IsFormValid myobject == 0
	return
endif

let MOO.imbuementsid[MOO.totalimbuements] := myobject

set MOO.totalimbuements to MOO.totalimbuements + 1

End