Maskar's Oblivion Overhaul.esp
0x5DCF00
MOOObjectLightPlateCandlesScript
Scn MOOObjectLightPlateCandlesScript

ref me

Begin Gamemode

set me to GetContainer
if me
	me.RemoveItemNS MOOLightPlateCandles 1
	me.AddItemNS MOOMiscPlateCandles 1
endif

End