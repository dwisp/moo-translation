Maskar's Oblivion Overhaul.esp
0x002359
MOOCheckNewListFunctionScript
Scn MOOCheckNewListFunctionScript

ref myitem
short i ; arraysize

short isnew

Begin Function { myitem i }

set isnew to 1
while i > 0
	set i to i - 1

	if eval myitem == MOO.leveleditem[i]
		set isnew to 0
		break
	endif		
loop

SetFunctionValue isnew

End