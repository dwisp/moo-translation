Maskar's Oblivion Overhaul.esp
0x498DA9
MOOUpdateCompanionLevelFunctionScript
Scn MOOUpdateCompanionLevelFunctionScript

short i
ref me

short mylevel
short levelmod
ref myclass
short myconfidence

Begin Function {  }

if MOO.pclevel == player.GetLevel
	return
endif

; UPDATE LEVEL
set levelmod to player.GetLevel - MOO.pclevel
if levelmod == 0
	return
endif
set MOO.pclevel to player.GetLevel

; UPDATE COMPANION LEVELS
while i < MOO.totalcompanions

	let me := MOO.companions[i]
	if me

		set myconfidence to ( me.GetAv Confidence ) + ( levelmod * MOO.ini_companion_confidence_level ) 
		if myconfidence > 100
			set myconfidence to 100
		endif
		me.SetAv Confidence myconfidence

		set mylevel to me.GetLevel + levelmod
		if mylevel < 1
			set mylevel to 1
		endif

		me.SetPCLevelOffset 1 mylevel mylevel

		set myclass to me.GetClass
		me.SetClass myclass

		if MOO.ini_debug
			printc "[MOO] %n has increased %.0f level(s) and is now level %.0f." me levelmod mylevel
		endif

	endif

	set i to i + 1
loop

End