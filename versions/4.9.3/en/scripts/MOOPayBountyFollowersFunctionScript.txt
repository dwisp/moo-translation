Maskar's Oblivion Overhaul.esp
0x542FA7
MOOPayBountyFollowersFunctionScript
Scn MOOPayBountyFollowersFunctionScript

ref me
short i

Begin Function {  }

; PETS
set i to ar_Size MOOTame.followers

while i > 0
	set i to i - 1

	let me := MOOTame.followers[i]
	if me
		me.SetCrimeGold 0
	endif
loop

; COMPANIONS
set i to 0
while i < MOO.totalcompanions

	let me := MOO.companions[i]
	if me
		me.SetCrimeGold 0
	endif

	set i to i + 1
loop

; REMOVE GOLD
player.RemoveItem Gold001 MOO.bountyfollowers
set MOO.bountyfollowers to 0

End