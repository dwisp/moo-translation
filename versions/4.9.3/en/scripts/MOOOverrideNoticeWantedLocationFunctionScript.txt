Maskar's Oblivion Overhaul.esp
0x247F34
MOOOverrideNoticeWantedLocationFunctionScript
Scn MOOOverrideNoticeWantedLocationFunctionScript

short myregion

string_var mylocation

Begin Function { myregion }

; wanted npc was last seen..

if myregion == 1
	let mylocation := "in the Shivering Isles"
elseif myregion == 2
	let mylocation := "in Oblivion"
elseif myregion == 10
	let mylocation := "in The Colovian Highlands"
elseif myregion == 11
	let mylocation := "in The Jerall Mountains"
elseif myregion == 12
	let mylocation := "heading towards the Morrowind border"
elseif myregion == 13
	let mylocation := "in Blackwood"
elseif myregion == 14
	let mylocation := "heading towards the Elsweyr border"
elseif myregion == 20
	let mylocation := "in The Great Forest"
elseif myregion == 21
	let mylocation := "in The West Weald"
elseif myregion == 22
	let mylocation := "heading towards the Valenwood border"
elseif myregion == 23
	let mylocation := "at The Gold Coast"
elseif myregion == 24
	let mylocation := "at Topal Bay"
elseif myregion == 25
	let mylocation := "at Niben Bay"
elseif myregion == 26
	let mylocation := "at The Nibenay Basin"
elseif myregion == 27
	let mylocation := "near the Imperial City"
else
	let mylocation := "in an unknown location"
endif


SetFunctionValue mylocation

sv_Destruct mylocation

End