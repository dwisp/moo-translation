Maskar's Oblivion Overhaul.esp
0x47EC16
MOOUpdateProjectilesAlertFunctionScript
Scn MOOUpdateProjectilesAlertFunctionScript

ref me
ref myattacker
ref myprojectile

short myaggression
short myconfidence
short myresponsibility
short mydisposition
short myleveldifference

ref myspell
short projectilehostile

short evilme
short evilattacker

short myaction
ref myclass

ref myrider

Begin Function { me myattacker myprojectile }

; INIT
if me.GetCreatureType == 4
if me.GetRider
	set myrider to me.GetRider
	if myrider.IsRidingHorse
		if myrider.GetIsAlerted || myrider.IsInCombat
			return
		else
			set me to myrider
		endif
	endif
endif
endif

set evilme to Call MOOIsEvilActorFunctionScript me
set evilattacker to Call MOOIsEvilActorFunctionScript myattacker

set myaggression to me.GetAV aggression
set myconfidence to me.GetAV confidence
set myresponsibility to me.GetAV responsibility
set mydisposition to me.GetDisposition myattacker
set myleveldifference to me.GetLevel - myattacker.GetLevel

; CHECK IF FRIENDLY SPELL
set myspell to myprojectile.GetMagicProjectileSpell
if myspell
	set projectilehostile to Call MOOGetMagicHostileFunctionScript myspell
else
	set projectilehostile to 1
endif


; FLEE, ALERT OR ATTACK

; status
; 1 flee
; 2 alert friendly
; 3 alert hostile
; 4 attack

; EVIL TARGET
if evilme
	if projectilehostile

		; FLEE
		if myaggression < 100 && myconfidence <= 50 && myresponsibility <= 50 && myleveldifference <= 0 && me.GetCreatureType <= 0
			set myaction to 1

		; ATTACK
		elseif myaggression == 100 && mydisposition < 60 && ( ( GetRandomPercent < 33 && me.GetCreatureType == -1 ) || ( GetRandomPercent < 50 && me.GetCreatureType != -1 ) )
			set myaction to 4

		; ALERT HOSTILE
		elseif myconfidence > 50 && mydisposition < 60
			set myaction to 3
		endif

	else
		; ALERT HOSTILE
		if myaggression == 100 && mydisposition < 60
			set myaction to 3
		endif
	endif

; FRIENDLY TARGET
else
	if projectilehostile

		; FLEE
		if myaggression < 100 && myconfidence <= 50 && myresponsibility <= 50
			set myaction to 1

		; ALERT HOSTILE
		elseif evilattacker
			set myaction to 3

		; ALERT FRIENDLY
		elseif me.GetCreatureType == -1 && ( mydisposition < 60 || me.IsGuard || me.GetAV Responsibility == 100 )
			set myaction to 2

		endif
	endif
endif

; LOWER DISPOSITION
if projectilehostile
if me.GetDisposition myattacker > 1
	me.ModDisposition myattacker -1
endif
endif

; DO NOTHING
if myaction == 0
	return
endif

; LOWER DISPOSITION FURTHER
if projectilehostile
if me.GetDisposition myattacker > 1
	me.ModDisposition myattacker -1
endif
endif

; STOP TALKING
if me.GetCurrentAIPackage == 6
	me.ForceFlee
endif


if MOO.ini_debug
	printc "[MOO] %n notices projectile and executes action: %.0f" me myaction
endif


; FLEE
if myaction == 1

	if me.GetCreatureType == -1
		me.SayTo myattacker Flee
		me.AddScriptPackage MOOAIFlee
	else
		me.SetAlert 1
		me.AddItemNS MOOTokenAlerted 1
		me.AddScriptPackage MOOAIWanderFlee
	endif

; ALERT FRIENDLY
elseif myaction == 2

	me.SayTo myattacker AssaultNoCrime
	me.SetAlert 1
	me.AddItemNS MOOTokenAlerted 1
	if myattacker == player
		me.AddScriptPackage MOOAIWanderPlayer
	endif

; ALERT HOSTILE
elseif myaction == 3

		me.SetAlert 1
		me.AddItemNS MOOTokenAlerted 1

		; PLAYER ATTACKER
		if myattacker == player

			if me.GetCreatureType == -1

				set myclass to me.GetClass

				; 13: Athletics
				; 26: Acrobatics
				; 30: Security
				; 31: Sneak

				if IsMajorC 13 myclass || IsMajorC 26 myclass ; run
					if IsMajorC 30 myclass || IsMajorC 21 myclass ; sneak
						; run + sneak
						me.AddScriptPackage MOOAIWanderPlayerRunSneak
					else
						; run
						me.AddScriptPackage MOOAIWanderPlayerRun
					endif

				elseif IsMajorC 30 myclass || IsMajorC 21 myclass ; sneak
					; sneak
					me.AddScriptPackage MOOAIWanderPlayerSneak
				else
					; none
					me.AddScriptPackage MOOAIWanderPlayer
				endif

			else ; creature
				; run
				me.AddScriptPackage MOOAIWanderPlayerRun
			endif

		; OTHER ATTACKER
		else
			me.AddScriptPackage MOOAIWanderRun
		endif

; ATTACK
elseif myaction == 4
	me.StartCombat myattacker
endif

End