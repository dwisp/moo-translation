Maskar's Oblivion Overhaul.esp
0x4337C7
MOOOnHitBySaboteurFunctionScript
Scn MOOOnHitBySaboteurFunctionScript

ref target
ref attacker

Begin Function { target attacker }

if attacker.GetEquippedObject 16
	return
endif

if attacker.IsPowerAttacking

	MOOCellPlacer.MoveTo target 0 0 50
	if attacker.GetLevel < 12
		MOOCellPlacer.Cast MOOSpellSaboteur1 target
	elseif attacker.GetLevel < 16
		MOOCellPlacer.Cast MOOSpellSaboteur2 target
	else
		MOOCellPlacer.Cast MOOSpellSaboteur3 target
	endif
endif

End