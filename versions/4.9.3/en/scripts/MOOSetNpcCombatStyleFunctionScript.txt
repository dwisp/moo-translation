Maskar's Oblivion Overhaul.esp
0x239601
MOOSetNpcCombatStyleFunctionScript
Scn MOOSetNpcCombatStyleFunctionScript

ref me
short npctype

ref myclass
short i

Begin Function { me npctype }

set myclass to me.GetClass

if GetClassSpecialization myclass == 1 ; magic

	set i to Rand 0 3
	if i == 0
		me.SetCombatStyle MOONpcDynamicDefaultLich
	elseif i == 1
		me.SetCombatStyle MOONpcDynamicMankarCamoranStyle
	else
		me.SetCombatStyle MOONpcDynamicDefaultAtronachFire
	endif

elseif IsMajorC 28 myclass ; marksman

	set i to Rand 0 3
	if i == 0
		me.SetCombatStyle MOONpcDynamicDefaultArcher
	elseif i == 1
		me.SetCombatStyle MOONpcDynamicMankarCamoranStyle
	else
		me.SetCombatStyle MOONpcDynamicDefaultSkeletonRanged
	endif

else
	set i to Rand 0 7
	if i == 0
		me.SetCombatStyle MOONpcDynamicDefaultGoblinWarlord
	elseif i == 1
		me.SetCombatStyle MOONpcDynamicDefaultAtronachStorm
	elseif i == 2
		me.SetCombatStyle MOONpcDynamicDefaultXivilai
	elseif i == 3
		me.SetCombatStyle MOONpcDynamicDefaultBearBrown
	elseif i == 4
		me.SetCombatStyle MOONpcDynamicDefaultClannfear
	elseif i == 5
		me.SetCombatStyle MOONpcDynamicDefaultOgre
	else
		me.SetCombatStyle MOONpcDynamicDefaultSkeletonChampion
	endif

endif

End