Maskar's Oblivion Overhaul.esp
0x5B5390
MOOFixGameIssuesFunctionScript
Scn MOOFixGameIssuesFunctionScript

Begin Function {  }

; fix MOO carrion rats - if installed
if IsModLoaded "Mart's Monster Mod.esm"
	Call MOOFixMMMCarrionRatsFunctionScript
endif

; GAMEDAY CAN ONLY BE WHOLE NUMBER
set GameDay to Floor GameDay

; REMOVE INCORRECT LIGHT ARMOR FROM HEAVY ARMOR LISTS
RemoveFromLeveledList LL0NPCArmorHeavyShield25 ElvenShield
RemoveFromLeveledList LL0NPCArmorHeavyShield50 ElvenShield
RemoveFromLeveledList LL0NPCArmorHeavyShield75 ElvenShield
RemoveFromLeveledList LL0NPCArmorHeavyShield100 ElvenShield

RemoveFromLeveledList LL0NPCArmorHeavyShieldLvl25 ElvenShield
RemoveFromLeveledList LL0NPCArmorHeavyShieldLvl50 ElvenShield
RemoveFromLeveledList LL0NPCArmorHeavyShieldLvl75 ElvenShield
RemoveFromLeveledList LL0NPCArmorHeavyShieldLvl100 ElvenShield

End