Maskar's Oblivion Overhaul.esp
0x08ECA5
MOOOnStartCombatFunctionScript
Scn MOOOnStartCombatFunctionScript

ref attacker
ref target

Begin Function { attacker target }

if attacker && target

	if attacker.GetCreatureType == -1
		Call MOOOnStartCombatNpcFunctionScript attacker target
	elseif attacker.GetItemCount MOOTokenDiseaseCode
		Call MOOOnStartCombatDiseasedFunctionScript attacker
	endif

	set MOO.attacker to attacker
	set MOO.target to target

endif

End