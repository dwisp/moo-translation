Maskar's Oblivion Overhaul.esp
0x236CFC
MOOGetWantedNpcItemFunctionScript
Scn MOOGetWantedNpcItemFunctionScript

ref me
ref myitem
short j
short k

ref ench
short i
short found

ref myscript
ref cloneitem

string_var itemname

Begin Function { me myitem j k }

set myitem to Call MOOHelperGetItemFunctionScript myitem

if j != k

	; MOD ADDED ITEMS
	set myitem to Call MOOGetWantedNpcItemModsFunctionScript myitem

	if myitem == 0
		set myitem to WristIrons
	endif

	me.AddItemNS myitem 1
	sv_Destruct itemname
	SetFunctionValue myitem
	return
endif

set cloneitem to CloneForm myitem

if IsWeapon cloneitem
	set ench to Call MOOGetEnWeapFunctionScript
	set i to Rand 1200 1600
	SetObjectCharge i cloneitem
else
	set ench to Call MOOGetEnApparelFunctionScript
endif

; ============

let itemname := GetName myitem

set i to sv_Length itemname
set found to 0
while i > 0 && found == 0
	set i to i - 1
	if eval ( sv_Find " " itemname i 1 ) == i
		set found to 1
	endif
loop

if i
	set i to i + 1
	sv_Erase itemname 0 i
endif

; ============

SetNameEx "%n's %z" me itemname cloneitem

SetEnchantment ench cloneitem

set myscript to MOOObjectQuestWantedNpcItemScript
SetScript myscript cloneitem

me.AddItemNS cloneitem 1

sv_Destruct itemname
SetFunctionValue cloneitem

End