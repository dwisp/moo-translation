Maskar's Oblivion Overhaul.esp
0x515F28
MOOOutpostCreateEventTeleporterOnTileFunctionScript
Scn MOOOutpostCreateEventTeleporterOnTileFunctionScript

short mydigger
short mydirection

; digger
short myx
short myy

; teleporter/target 1/2
short fromx1
short fromy1
short tox1
short toy1

short fromx2
short fromy2
short tox2
short toy2

short mytileid
short mytileid1
short mytileid2
short mytileid4
short mytileid8

short mytileidstart
short mytileidnext

short myheight
short fail
short myrand

short allowed

short i1
short i2

Begin Function { mydigger mydirection }

; INIT
set allowed to 0

let myx := MOO.tunnelx[mydigger]
let myy := MOO.tunnely[mydigger]
let myheight := MOO.dungeonmapheight[myx][myy]
let mytileid := MOO.dungeonmap[myx][myy]


; TELEPORTER 1
set fromx1 to myx
set fromy1 to myy

; DETERMINE TARGET 2 ( behind teleporter 1 )
if mydirection == 1
	set tox2 to myx
	set toy2 to myy - 1
elseif mydirection == 2
	set tox2 to myx - 1
	set toy2 to myy
elseif mydirection == 4
	set tox2 to myx
	set toy2 to myy + 1
else ; 8
	set tox2 to myx + 1
	set toy2 to myy
endif


; DETERMINE TELEPORTER 2 AND TARGET 1
while allowed == 0 && fail < 25

	set myx to Rand 1 28.99
	set myy to Rand 1 28.99
	let mytileid := MOO.dungeonmap[myx][myy]

	let mytileid1 := MOO.dungeonmap[myx][myy + 1]
	let mytileid2 := MOO.dungeonmap[myx + 1][myy]
	let mytileid4 := MOO.dungeonmap[myx][myy - 1]
	let mytileid8 := MOO.dungeonmap[myx - 1][myy]

	if mytileid == 0

		set myrand to Rand 1 4.999

		if myrand == 1 && mytileid1 == 0
			set allowed to 1
			set mydirection to 1
			set tox1 to myx
			set toy1 to myy + 1

			set mytileidstart to 1
			set mytileidnext to 4

		elseif myrand == 2 && mytileid2 == 0
			set allowed to 1
			set mydirection to 2
			set tox1 to myx + 1
			set toy1 to myy

			set mytileidstart to 2
			set mytileidnext to 8

		elseif myrand == 3 && mytileid4 == 0
			set allowed to 1
			set mydirection to 4
			set tox1 to myx
			set toy1 to myy - 1

			set mytileidstart to 4
			set mytileidnext to 1

		elseif myrand == 4 && mytileid8 == 0
			set allowed to 1
			set mydirection to 8
			set tox1 to myx - 1
			set toy1 to myy

			set mytileidstart to 8
			set mytileidnext to 2
		endif
	endif

	if allowed
		set fromx2 to myx
		set fromy2 to myy
	else
		set fail to fail + 1
	endif

	; CHECK FOR EMPTY EVENT FROM ALCOVE
	if allowed
		set allowed to Call MOOOutpostCheckTileEmptyToSidesFunctionScript tox1 toy1 mydirection
	endif
	if allowed
		set allowed to Call MOOOutpostCheckTileEmptyToSidesFunctionScript fromx2 fromy2 mydirection
	endif

loop


; CHECK IF EVENT TILES ARE EMPTY
if allowed
	if eval MOO.dungeonmapevent[fromx1][fromy1] != 0 || MOO.dungeonmapevent[tox1][toy1] != 0 || MOO.dungeonmapevent[fromx2][fromy2] != 0 || MOO.dungeonmapevent[tox2][toy2] != 0
		set allowed to 0
	endif
endif


; MAKE CERTAIN TELEPORTERS AND MARKERS ARE ALL DIFFERENT TILES
if allowed

	; FROM 1
	let MOO.dungeonmapevent[fromx1][fromy1] := MOO.id_dungeon_reserved

	; TO 1
	if eval MOO.dungeonmapevent[tox1][toy1] == 0
		let MOO.dungeonmapevent[tox1][toy1] := MOO.id_dungeon_reserved
	else
		set allowed to 0
	endif

	; FROM 2
	if eval MOO.dungeonmapevent[fromx2][fromy2] == 0
		let MOO.dungeonmapevent[fromx2][fromy2] := MOO.id_dungeon_reserved
	else
		set allowed to 0
	endif

	; TO 2
	if eval MOO.dungeonmapevent[tox2][toy2] == 0
		let MOO.dungeonmapevent[tox2][toy2] := MOO.id_dungeon_reserved
	else
		set allowed to 0
	endif

	if allowed == 0
		let MOO.dungeonmapevent[fromx1][fromy1] := 0
		let MOO.dungeonmapevent[tox1][toy1] := 0
		let MOO.dungeonmapevent[fromx2][fromy2] := 0
		let MOO.dungeonmapevent[tox2][toy2] := 0
	endif
endif


; PLACE TELEPORTERS AND MARKERS
if allowed

	; TELEPORTER NUMBER << 5
	set MOO.currentdungeonteleporter to MOO.currentdungeonteleporter + 1
	let i1 := MOO.currentdungeonteleporter << 5
	set MOO.currentdungeonteleporter to MOO.currentdungeonteleporter + 1
	let i2 := MOO.currentdungeonteleporter << 5

	; UPDATE TUNNEL DIGGER LOCATION AND DIRECTION
	let MOO.tunnelx[MOO.tunneldiggers] := tox1
	let MOO.tunnely[MOO.tunneldiggers] := toy1
	let MOO.tunneldirection[MOO.tunneldiggers] := mydirection

	; UPDATE MAP ( 2 STARTING TILES )
	let MOO.dungeonmap[fromx2][fromy2] := mytileidstart
	let MOO.dungeonmap[tox1][toy1] := mytileidnext

	; CREATE TELEPORTER 1
	let MOO.dungeonmapevent[fromx1][fromy1] := 2 + i1

	; CREATE TARGET 2
	let MOO.dungeonmapevent[tox2][toy2] := 3 + i2

	; CREATE TELEPORTER 2
	let MOO.dungeonmapevent[fromx2][fromy2] := 2 + i2

	; CREATE TARGET 1
	let MOO.dungeonmapevent[tox1][toy1] := 3 + i1

endif


SetFunctionValue allowed

End