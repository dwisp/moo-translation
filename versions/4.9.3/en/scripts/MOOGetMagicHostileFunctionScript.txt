Maskar's Oblivion Overhaul.esp
0x483E0D
MOOGetMagicHostileFunctionScript
Scn MOOGetMagicHostileFunctionScript

ref mymagic ; spell or enchantment

short myeffectcode
short i

short ishostile

Begin Function { mymagic }

set i to GetMagicItemEffectCount mymagic

while i > 0
	set i to i - 1

	let MOO.effectitem := GetNthEffectItem mymagic i

	if IsNthEffectItemScripted mymagic i

		if IsNthEffectItemScriptHostile mymagic i
			set ishostile to 1
			break
		endif

	else
		set myeffectcode to GetNthEICode mymagic i
		if IsMagicEffectHostileC myeffectcode
			set ishostile to 1
			break
		endif
	endif
loop


SetFunctionValue ishostile

End