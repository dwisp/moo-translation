Maskar's Oblivion Overhaul.esp
0x4EC302
MOOPlaceObjectOnTerrainFunctionScript
Scn MOOPlaceObjectOnTerrainFunctionScript

float xpos
float ypos
ref mymarker
ref myobject
short myradius
short zposmod
short zanglemod
short xylock ; xy angle can't be touched 
short zlock ; z angle is provided - can't be random

ref me
float zpos
float xangle
float yangle
float zangle
ref mytarget

float myx
float myy
float mya
float myb
float myheight1
float myheight2
float myheight3
float myheight4
float myscale

short fail

short mymodifier

Begin Function { xpos ypos mymarker myobject myradius zposmod zanglemod xylock zlock }

; MYOBJECT CAN BE BASE OR REFERENCE -> REFERENCE CAN BE USED FOR XY ANGLE
if IsReference myobject
	set xangle to myobject.GetAngle x
	set yangle to myobject.GetAngle y
	set myscale to myobject.GetScale
	set myobject to myobject.GetBaseObject
else
	set myscale to 1
endif

set me to MOOCellMarkerLocal
me.MoveTo mymarker


; CLEAR AREA IF NEEDED ( NOT ALL OBJECTS REQUIRE CLEARING )
Call MOOPlaceObjectOnTerrainClearTerrainFunctionScript mymarker myobject


; OVERRIDE XYLOCK IF NEEDED
if Call MOOPlaceObjectOnTerrainCheckXYLockedFunctionScript myobject
	set xylock to 1
endif

; OVERRIDE STATIC IF NEEDED
set myobject to Call MOOPlaceObjectOnTerrainCheckStaticFunctionScript myobject

; OVERRIDE HEIGHT IF NEEDED
set zposmod to Call MOOPlaceObjectOnTerrainCheckHeightFunctionScript myobject zposmod

; RANDOM Z ANGLE (0) OR FIXED (1)
if zlock
	set zangle to zanglemod
else
	set zangle to Rand 0 360
endif

; XY ANGLE BASED ON TERRAIN (0) OR STRAIGHT UP (1)
if xylock ; currently default plants

	set zpos to GetTerrainHeight xpos ypos

else ; mushrooms have locked z to make x/y angle easier -> might not be needed

	set myx to xpos - myradius
	set myy to ypos + myradius
	set myheight1 to GetTerrainHeight myx myy

	set myx to xpos + myradius
	set myy to ypos + myradius
	set myheight2 to GetTerrainHeight myx myy

	set myx to xpos + myradius
	set myy to ypos - myradius
	set myheight3 to GetTerrainHeight myx myy

	set myx to xpos - myradius
	set myy to ypos - myradius
	set myheight4 to GetTerrainHeight myx myy

	; SKIP PLACEMENT WHEN TERRAIN HEIGHT RETURNED 0
	if myheight1 == 0 || myheight2 == 0 || myheight3 == 0 || myheight4 == 0
		set fail to 1
	endif

	; calculate angles
	set mya to myradius * 2

	; y angle
	set myb to ( ( myheight2 + myheight3 ) / 2 ) - ( ( myheight1 + myheight4 ) / 2 )

	if myb < 0
		set mymodifier to -1
	else
		set mymodifier to 1
	endif
	set myb to Abs myb

	set yangle to myb / mya
	set yangle to aTan yangle
	set yangle to yangle * mymodifier


	; x angle
	set myb to ( ( myheight3 + myheight4 ) / 2 ) - ( ( myheight1 + myheight2 ) / 2 )

	if myb < 0
		set mymodifier to -1
	else
		set mymodifier to 1
	endif
	set myb to Abs myb

	set xangle to myb / mya
	set xangle to aTan xangle
	set xangle to xangle * mymodifier


;	set zpos to ( myheight1 + myheight2 + myheight3 + myheight4 ) / 4
	set zpos to GetTerrainHeight xpos ypos

endif

; ZPOS MODIFIER
set zpos to zpos + zposmod

; PLACE OBJECT
me.setpos x xpos
me.setpos y ypos
me.setpos z zpos

me.setangle x xangle
me.setangle y yangle
me.setangle z zangle

; SKIP PLACEMENT WHEN NOT ALLOWED
if fail == 0
	set mytarget to me.PlaceAtMe myobject
	mytarget.SetScale myscale
endif


SetFunctionValue mytarget

End