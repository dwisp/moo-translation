Maskar's Oblivion Overhaul.esp
0x30C84F
MOOTameUpdateCombatGuardianFunctionScript
Scn MOOTameUpdateCombatGuardianFunctionScript

ref me

ref target

Begin Function { me }

set target to GetFirstRef 69 1
while target

	if target.GetDisabled == 0
	if target.GetDead == 0
	if target.GetCombatTarget == player
	if Call MOOIsEvilActorFunctionScript target
		target.SetFactionRank MOOSpecialCombatFaction 0
		me.StartCombat target
		if MOO.ini_debug
			printc "[MOO] %n decides to help you." me
		endif
		break
	endif
	endif
	endif
	endif

	set target to GetNextRef
loop

End