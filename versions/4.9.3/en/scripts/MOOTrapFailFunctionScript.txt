Maskar's Oblivion Overhaul.esp
0x0759C3
MOOTrapFailFunctionScript
Scn MOOTrapFailFunctionScript

ref mycontainer

ref myitem
short stackcount
ref mytrap
short i

Begin Function { mycontainer }

if ( IsFormValid mycontainer ) == 0
	return
endif

if MOO.trapfail == 2
	if MOO.trapcount == 1
		message "You break the lock, but set off a trap."
	elseif MOO.trapcount > 1
		message "You break the lock, but set off the traps."
	else
		message "You successfully break the lock."
		return
	endif
elseif MOO.trapfail == 3
	if MOO.trapcount == 1
		message "You have set off a trap."
	elseif MOO.trapcount > 1
		message "You have set off the traps."
	else
		return
	endif
else
	message "You fail to disarm the trap."
endif

MOOCellActivator.MoveTo mycontainer 0 0 10

if MOO.trap1
	let mytrap := MOO.trapspells[MOO.trap1]
	MOOCellActivator.Cast mytrap mycontainer
	set MOO.trap1 to 0
endif
if MOO.trap2
	let mytrap := MOO.trapspells[MOO.trap2]
	MOOCellActivator.Cast mytrap mycontainer
	set MOO.trap2 to 0
endif
if MOO.trap3
	let mytrap := MOO.trapspells[MOO.trap3]
	MOOCellActivator.Cast mytrap mycontainer
	set MOO.trap3 to 0
endif

Call MOOWarnIntruderFunctionScript
Call MOORemoveTrapsContainerFunctionScript mycontainer
set MOO.trapcount to 0

set i to 0
foreach myitem <- mycontainer
	if myitem.IsScripted == 0 && myitem.IsQuestItem == 0 && myitem.IsPlayable2 && myitem.IsKey == 0
	if GetRandomPercent < MOO.ini_trap_destroy
		let MOO.leveleditem[i] := myitem.GetBaseObject
		set i to i + 1
	endif
	endif
loop

while i > 0
	set i to i - 1
	let myitem := MOO.leveleditem[i]
	set stackcount to mycontainer.GetItemCount myitem
	if stackcount
		mycontainer.RemoveItemNS myitem stackcount
		if MOO.ini_debug
			printc "[MOO] Destroyed %.0f %n." stackcount myitem
		endif
	endif
loop

ar_Erase MOO.leveleditem

End