Maskar's Oblivion Overhaul.esp
0x1AB7F3
MOORareOpenScript
Scn MOORareOpenScript

ref me

Begin OnEquip

set me to GetContainer
if me == player
	if ( me.GetItemCount MOORareA5OpenV1 ) && ( me.GetItemCount MOORareA5OpenV2 ) && ( me.GetItemCount MOORareA5OpenV3 ) && ( me.GetItemCount MOORareA5OpenV4 )
		messagebox "You combine the book parts to create a new spellbook!"

		me.RemoveItemns MOORareA5OpenV1 1
		me.RemoveItemns MOORareA5OpenV2 1
		me.RemoveItemns MOORareA5OpenV3 1
		me.RemoveItemns MOORareA5OpenV4 1

		me.AddItemns MOOBookA5Open 1

	else
		message "You do not have all the parts to this spellbook."
	endif
endif

End