Maskar's Oblivion Overhaul.esp
0x547AD6
MOOCompanionUpdateTradeItemsWithMerchantApparatusFunctionScript
Scn MOOCompanionUpdateTradeItemsWithMerchantApparatusFunctionScript

ref mycompanion
ref mymerchant
ref mycontainer
short i

ref mymortar
ref merchantmortar
float mymortarquality
float merchantmortarquality

short busy

Begin Function { mycompanion mymerchant mycontainer i }

; GET BEST MORTAR
set mymortar to Call MOOCompanionUpdateTradeItemsWithMerchantApparatusGetBestFunctionScript mycompanion 0
set merchantmortar to Call MOOCompanionUpdateTradeItemsWithMerchantApparatusGetBestFunctionScript mycontainer 0

; GET QUALITY
if mymortar
	set mymortarquality to GetQuality mymortar
endif
if merchantmortar
	set merchantmortarquality to GetQuality merchantmortar
endif

; BUY MORTAR
if merchantmortarquality > mymortarquality
	set busy to Call MOOCompanionUpdateTradeItemsWithMerchantBuyFunctionScript mycompanion mymerchant i merchantmortar 1
endif

End