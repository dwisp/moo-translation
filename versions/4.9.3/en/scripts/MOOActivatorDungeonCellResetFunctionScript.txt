Maskar's Oblivion Overhaul.esp
0x521170
MOOActivatorDungeonCellResetFunctionScript
Scn MOOActivatorDungeonCellResetFunctionScript

short loadcounter
short i

Begin OnReset

set loadcounter to 0

End


Begin Gamemode

if loadcounter == MOO.loadcounter
	return
endif

if i == 0
	Call MOOUpdateMOOCellFunctionScript 0
elseif i == 10
	Call MOOUpdateMOOCellFunctionScript 1
elseif i > 20

	if loadcounter == 0
		Call MOOOutpostResetAllFunctionScript
	endif

	set i to 0
	set loadcounter to MOO.loadcounter
	return
endif

set i to i + 1

End