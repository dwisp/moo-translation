Maskar's Oblivion Overhaul.esp
0x5481B3
MOOCompanionUpdateTradeItemsWithMerchantBuyFunctionScript
Scn MOOCompanionUpdateTradeItemsWithMerchantBuyFunctionScript

ref mycompanion
ref mymerchant
short i
ref myitem
short totalitems

short mygold
ref mycontainer

short itemcost
short totalcost

short busy

Begin Function { mycompanion mymerchant i myitem totalitems }

let mygold := MOO.companionsgold[i]
set mycontainer to mymerchant.GetMerchantContainer

; CHECK MERCHANT ITEMS
if totalitems > mycontainer.GetItemCount myitem
	set totalitems to mycontainer.GetItemCount myitem
endif


; CHECK AMOUNT YOU CAN AFFORD
set itemcost to ( ( GetFullGoldValue myitem ) * ( MOO.ini_companion_buy / 100 ) )

if itemcost
if mygold < itemcost * totalitems
	set totalitems to mygold / itemcost
endif
endif


; BUY
if totalitems
	mycontainer.RemoveItemNS myitem totalitems
	mycompanion.AddItemNS myitem totalitems

	set totalcost to totalitems * itemcost
	set mygold to mygold - totalcost

	let MOO.companionsgold[i] := mygold

	set busy to 1
	MessageEx "%n purchased %.0f %n(s) from %n for %.0f gold." mycompanion totalitems myitem mymerchant totalcost

	if mycompanion.GetDistance player < 1000
		PlaySound ITMGoldUp
	endif
endif

SetFunctionValue busy

End
