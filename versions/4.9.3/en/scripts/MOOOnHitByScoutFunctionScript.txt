Maskar's Oblivion Overhaul.esp
0x43DBF0
MOOOnHitByScoutFunctionScript
Scn MOOOnHitByScoutFunctionScript

ref target

Begin Function { target }

if GetRandomPercent < 20
	MOOCellActivator.MoveTo target 0 0 50
	MOOCellActivator.Cast MOOSpellScoutInvisibility target
	MOOCellActivator.MoveTo MOOCellMarker
endif

End