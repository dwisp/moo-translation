Maskar's Oblivion Overhaul.esp
0x5EF505
MOOCompanionCheckWantedItemArmorFunctionScript
Scn MOOCompanionCheckWantedItemArmorFunctionScript

ref me
ref myitem

ref mywanteditem

Begin Function { me myitem }

; LIGHT ARMOR
if myitem.GetArmorType == 0
	if me.IsMajorC 27
		set mywanteditem to Call MOOCompanionCheckWantedItemArmorOfTypeFunctionScript me myitem 0
	endif

; HEAVY ARMOR
else
	if me.IsMajorC 18
		set mywanteditem to Call MOOCompanionCheckWantedItemArmorOfTypeFunctionScript me myitem 0
	endif
endif


SetFunctionValue mywanteditem

End