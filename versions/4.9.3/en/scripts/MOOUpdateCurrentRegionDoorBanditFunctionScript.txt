Maskar's Oblivion Overhaul.esp
0x3DCF17
MOOUpdateCurrentRegionDoorBanditFunctionScript
Scn MOOUpdateCurrentRegionDoorBanditFunctionScript

short mymapmarkertype

ref newbanditlist
short myseed
short myrating
short maxrating

Begin Function { mymapmarkertype }

; VANILLA
set newbanditlist to MOOLL0VanillaBanditMelee

if MOO.mydungeon
if mymapmarkertype

	; SLAVERS
	if MOO.ini_add_slavers
		let myrating := MOO.regioncontrol[4][MOO.myregion]

		if myrating >= MOO.ini_npc_invasion_dungeon ; dunmer
			set myseed to MOO.markerid + MOO.gameseed1 + GameDay
			if eval myseed & 2 == 2
			if mymapmarkertype == 6 ; mine
			if maxrating < myrating
				set newbanditlist to MOOLL2RandomSlave
				set maxrating to myrating
			endif
			endif
			endif
		endif
	endif

	; MERCENARIES
	if MOO.ini_add_mercenaries
		let myrating := MOO.regioncontrol[2][MOO.myregion]

		if myrating >= MOO.ini_npc_invasion_dungeon ; hammerfell
			set myseed to MOO.markerid + MOO.gameseed2 + GameDay
			if eval myseed & 2 == 2
			if maxrating < myrating
				set newbanditlist to MOOLL2RandomMercenaryGrunt
				set maxrating to myrating
			endif
			endif
		endif
	endif

	; SMUGGLERS
	if MOO.ini_add_smugglers
		let myrating := MOO.regioncontrol[6][MOO.myregion] ; khajiit

		if myrating >= MOO.ini_npc_invasion_dungeon
			set myseed to MOO.markerid + MOO.gameseed3 + GameDay
			if eval myseed & 2 == 2
			if maxrating < myrating
				set newbanditlist to MOOLL2RandomSkoomaGrunt
				set maxrating to myrating
			endif
			endif
		endif
	endif

endif
endif


; CHECK LEVELED
if MOO.ini_levelscaling_special
if player.GetLevel < MOO.ini_levelmin_region_rank2 - 5
	; VANILLA
	set newbanditlist to MOOLL0VanillaBanditMelee
endif
endif


SetFunctionValue newbanditlist

End