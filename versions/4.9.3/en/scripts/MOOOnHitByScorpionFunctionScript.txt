Maskar's Oblivion Overhaul.esp
0x5A6483
MOOOnHitByScorpionFunctionScript
Scn MOOOnHitByScorpionFunctionScript

ref target
ref attacker

Begin Function { target attacker }

MOOCellActivator.MoveTo target 0 0 50

if attacker.GetLevel > 18
	MOOCellActivator.Cast MOOPoisonScorpion04 target
elseif attacker.GetLevel > 15
	MOOCellActivator.Cast MOOPoisonScorpion03 target
elseif attacker.GetLevel > 12
	MOOCellActivator.Cast MOOPoisonScorpion02 target
else
	MOOCellActivator.Cast MOOPoisonScorpion01 target
endif

MOOCellActivator.MoveTo MOOCellMarker

End