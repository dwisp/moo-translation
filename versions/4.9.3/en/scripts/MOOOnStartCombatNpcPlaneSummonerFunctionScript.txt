Maskar's Oblivion Overhaul.esp
0x4337C5
MOOOnStartCombatNpcPlaneSummonerFunctionScript
Scn MOOOnStartCombatNpcPlaneSummonerFunctionScript

ref me

ref myportal

Begin Function { me }

if GetRandomPercent < 50
	return
endif

me.Cast MOOSpellFakeSummon me

if MOO.currentsummonedportal == 0
	set myportal to MOOCellActivatorSummonedPortal01
	set MOOCellActivatorSummonedPortal01.init to 1
	set MOOCellActivatorSummonedPortal01.mycaster to me
elseif MOO.currentsummonedportal == 1
	set myportal to MOOCellActivatorSummonedPortal02
	set MOOCellActivatorSummonedPortal02.init to 1
	set MOOCellActivatorSummonedPortal02.mycaster to me
elseif MOO.currentsummonedportal == 2
	set myportal to MOOCellActivatorSummonedPortal03
	set MOOCellActivatorSummonedPortal03.init to 1
	set MOOCellActivatorSummonedPortal03.mycaster to me
elseif MOO.currentsummonedportal == 3
	set myportal to MOOCellActivatorSummonedPortal04
	set MOOCellActivatorSummonedPortal04.init to 1
	set MOOCellActivatorSummonedPortal04.mycaster to me
elseif MOO.currentsummonedportal == 4
	set myportal to MOOCellActivatorSummonedPortal05
	set MOOCellActivatorSummonedPortal05.init to 1
	set MOOCellActivatorSummonedPortal05.mycaster to me
elseif MOO.currentsummonedportal == 5
	set myportal to MOOCellActivatorSummonedPortal06
	set MOOCellActivatorSummonedPortal06.init to 1
	set MOOCellActivatorSummonedPortal06.mycaster to me
elseif MOO.currentsummonedportal == 6
	set myportal to MOOCellActivatorSummonedPortal07
	set MOOCellActivatorSummonedPortal07.init to 1
	set MOOCellActivatorSummonedPortal07.mycaster to me
elseif MOO.currentsummonedportal == 7
	set myportal to MOOCellActivatorSummonedPortal08
	set MOOCellActivatorSummonedPortal08.init to 1
	set MOOCellActivatorSummonedPortal08.mycaster to me
endif

set MOO.currentsummonedportal to MOO.currentsummonedportal + 1
if MOO.currentsummonedportal == 8
	set MOO.currentsummonedportal to 0
endif


myportal.Disable
myportal.MoveTo me
myportal.Enable

End