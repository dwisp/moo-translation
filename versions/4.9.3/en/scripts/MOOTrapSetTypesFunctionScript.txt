Maskar's Oblivion Overhaul.esp
0x0759C4
MOOTrapSetTypesFunctionScript
Scn MOOTrapSetTypesFunctionScript

ref mycontainer

ref myitem
short trap

Begin Function { mycontainer }

set MOO.trap1 to 0
set MOO.trap2 to 0
set MOO.trap3 to 0
set MOO.trapcount to 0

foreach myitem <- mycontainer
	set trap to 0
	set myitem to myitem.GetBaseObject
	if myitem == MOOTrapFire01
		set trap to 1
	elseif myitem == MOOTrapFrost01
		set trap to 2
	elseif myitem == MOOTrapShock01
		set trap to 3
	elseif myitem == MOOTrapPoison01
		set trap to 4
	elseif myitem == MOOTrapFire02
		set trap to 5
	elseif myitem == MOOTrapFrost02
		set trap to 6
	elseif myitem == MOOTrapShock02
		set trap to 7
	elseif myitem == MOOTrapPoison02
		set trap to 8
	elseif myitem == MOOTrapFire03
		set trap to 9
	elseif myitem == MOOTrapFrost03
		set trap to 10
	elseif myitem == MOOTrapShock03
		set trap to 11
	elseif myitem == MOOTrapPoison03
		set trap to 12
	elseif myitem == MOOTrapFire04
		set trap to 13
	elseif myitem == MOOTrapFrost04
		set trap to 14
	elseif myitem == MOOTrapShock04
		set trap to 15
	elseif myitem == MOOTrapPoison04
		set trap to 16
	endif

	if trap
		set MOO.trapcount to MOO.trapcount + 1
		if MOO.trapcount == 1
			set MOO.trap1 to trap
		elseif MOO.trapcount == 2
			set MOO.trap2 to trap
		elseif MOO.trapcount == 3
			set MOO.trap3 to trap
			break
		endif
	endif

loop

End