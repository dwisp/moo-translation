Maskar's Oblivion Overhaul.esp
0x5203C2
MOOOutpostCreateStaticsEventDisableOnStartFunctionScript
Scn MOOOutpostCreateStaticsEventDisableOnStartFunctionScript

short myeventid
ref myplacedobject

Begin Function { myeventid myplacedobject }

if myeventid == MOO.id_dungeon_actor
	myplacedobject.Disable
endif

End