Maskar's Oblivion Overhaul.esp
0x572E70
MOOCompanionUpdateNeedsGetFoodFunctionScript
Scn MOOCompanionUpdateNeedsGetFoodFunctionScript

ref me
short foodtype

ref myitem

ref myfood

Begin Function { me foodtype }

foreach myitem <- me
	if myitem.IsQuestItem == 0

		; FOOD
		if foodtype == 0
			if myitem.IsIngredient
			if myitem.IsFood
			if myitem.GetBaseObject != MS39Nirnroot
				set myfood to myitem.GetBaseObject
				break
			endif
			endif
			endif

		; DRINK
		else
			if myitem.IsPotion
			if myitem.IsFood
				set myfood to myitem.GetBaseObject
				break
			endif
			endif
		endif
	endif
loop


SetFunctionValue myfood

End