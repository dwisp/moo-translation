Maskar's Oblivion Overhaul.esp
0x384C3A
MOOCraftInitMenuFunctionScript
Scn MOOCraftInitMenuFunctionScript

ref mycontainer
short mytype

Begin Function { mycontainer mytype }

; init array
if eval ( ar_Size MOOCraft.myresources ) == -1
	let MOOCraft.myresources := ar_Construct Array

	let MOOCraft.recipiesentry := ar_Construct Array
	let MOOCraft.recipiesamount := ar_Construct Array
endif

ar_Erase MOOCraft.myresources
ar_Erase MOOCraft.recipiesentry
ar_Erase MOOCraft.recipiesamount
ar_Resize MOOCraft.myresources MOO.totalids

set MOOCraft.currententry to 0 ; show first item in menu (used for scrolling through items)

; check resources in container
Call MOOCraftInitMenuMyResourcesFunctionScript mycontainer

; check recipies if any can be made with resources in container
Call MOOCraftInitMenuRecipiesFunctionScript

End