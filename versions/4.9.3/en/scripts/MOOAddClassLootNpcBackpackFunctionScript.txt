Maskar's Oblivion Overhaul.esp
0x48D4FE
MOOAddClassLootNpcBackpackFunctionScript
Scn MOOAddClassLootNpcBackpackFunctionScript

ref me

ref myitem
short mychance

Begin Function { me }

if ( me.GetInFaction AdventurerFaction == 0 ) && ( me.GetInFaction MOOCompanionFaction == 0 )
	return
endif


if me.GetInFaction MOOCompanionFaction
	set mychance to MOO.ini_add_backpacks_companions
else
	set mychance to MOO.ini_add_backpacks_adventurers
endif


if GetRandomPercent < mychance
if MOO.ini_add_backpacks

	if me.GetAV Encumbrance < 35
		set myitem to MOOBackpack1Static
	elseif me.GetAV Encumbrance < 75
		set myitem to MOOBackpack2Static
	elseif me.GetAV Encumbrance < 150
		set myitem to MOOBackpack3Static
	elseif me.GetAV Encumbrance < 250
		set myitem to MOOBackpack4Static
	else
		set myitem to MOOBackpack5Static
	endif

	me.AddItemNS myitem 1
	me.EquipItem2NS myitem

	if me.GetItemCount MOOTokenAmulet == 0
		me.AddItemNS MOOTokenAmulet 1
	endif

endif
endif

SetFunctionValue myitem

End