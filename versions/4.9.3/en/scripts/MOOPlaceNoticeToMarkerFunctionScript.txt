Maskar's Oblivion Overhaul.esp
0x210C25
MOOPlaceNoticeToMarkerFunctionScript
Scn MOOPlaceNoticeToMarkerFunctionScript

ref noticeboard
ref basenotice
float xposmod
float yposmod
float xangle
float yangle

float xpos
float ypos
float boardheight

float myrand
float boardmin
float boardmax

ref me
ref notice

;Begin Function { mymarker basenotice xpos ypos xangle yangle boardheight }
Begin Function { noticeboard basenotice xposmod yposmod xangle yangle }

set xpos to ( noticeboard.GetPos x ) - xposmod
set ypos to ( noticeboard.GetPos y ) - yposmod
set boardheight to noticeboard.GetPos z

set me to MOOCellPlacer
;me.MoveTo mymarker
me.MoveTo noticeboard

me.setpos x xpos
me.setpos y ypos
me.setangle x xangle
me.setangle y yangle

set boardmin to boardheight + 25
set boardmax to boardheight + 50

set myrand to Rand boardmin boardmax
me.setpos z myrand
set myrand to Rand 175 185
me.setangle z myrand

set notice to me.PlaceAtMe basenotice

SetFunctionValue notice

End