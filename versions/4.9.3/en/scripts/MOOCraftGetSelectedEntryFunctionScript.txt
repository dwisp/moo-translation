Maskar's Oblivion Overhaul.esp
0x38E989
MOOCraftGetSelectedEntryFunctionScript
Scn MOOCraftGetSelectedEntryFunctionScript

short button

short selectedentry

Begin Function { button }

set selectedentry to button

if MOOCraft.morebutton

	if button == 8 ; there's a more button
		set selectedentry to 999 ; more
	elseif button == 9
		set selectedentry to -1 ; cancel
	endif

elseif button == MOOCraft.currentcount
	set selectedentry to -1 ; cancel

endif

SetFunctionValue selectedentry

End