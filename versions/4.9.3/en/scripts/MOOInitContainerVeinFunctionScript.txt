Maskar's Oblivion Overhaul.esp
0x2C7BD8
MOOInitContainerVeinFunctionScript
Scn MOOInitContainerVeinFunctionScript

ref me

ref myclone
ref mybase
ref placedclone
float myscale
short gemtype

Begin Function { me }

if me.GetDisabled || me.GetParentRef || me.IsScripted == 0 || MOO.ini_add_veins == 0
	return
endif

set mybase to me.GetBaseObject

if mybase == SilverRock01
	set gemtype to 1
	set myclone to MOO.clonesilver1
elseif mybase == SilverRock02
	set gemtype to 1
	set myclone to MOO.clonesilver2
elseif mybase == SilverRock03
	set gemtype to 1
	set myclone to MOO.clonesilver3
elseif mybase == GoldRock01
	set gemtype to 2
	set myclone to MOO.clonegold1
elseif mybase == GoldRock02
	set gemtype to 2
	set myclone to MOO.clonegold2
elseif mybase == GoldRock03
	set gemtype to 2
	set myclone to MOO.clonegold3
else
	return
endif

set myscale to me.GetScale
set placedclone to me.PlaceAtMe myclone

if gemtype == 1
	placedclone.AddItemNS LL0NuggetSilver50 3
else
	placedclone.AddItemNS LL0NuggetGold50 3
endif

RemoveScript myclone
placedclone.SetScaleEX myscale
me.Disable

End