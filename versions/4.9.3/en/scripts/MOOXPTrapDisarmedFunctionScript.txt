Maskar's Oblivion Overhaul.esp
0x4DE049
MOOXPTrapDisarmedFunctionScript
Scn MOOXPTrapDisarmedFunctionScript

ref mypotion

float xp
string_var mystring

Begin Function { mypotion }

if CompareModelPath "1.nif" mypotion
	set xp to MOO.ini_XP_disarm_trap_weak
elseif CompareModelPath "2.nif" mypotion
	set xp to MOO.ini_XP_disarm_trap_average
elseif CompareModelPath "3.nif" mypotion
	set xp to MOO.ini_XP_disarm_trap_hard
else ; 4
	set xp to MOO.ini_XP_disarm_trap_leathal
endif

set mystring to sv_Construct "set ObXPMain.interOpGainedXP to %.0f" xp
RunScriptLine "set ObXPMain.interOpGainedXPMessage to sv_Construct %q Disarmed Trap.%q"
RunScriptLine $mystring

sv_Destruct mystring

End