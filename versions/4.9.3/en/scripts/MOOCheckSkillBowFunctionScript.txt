Maskar's Oblivion Overhaul.esp
0x0059F9
MOOCheckSkillBowFunctionScript
Scn MOOCheckSkillBowFunctionScript

ref myitem

float skillrequired
short skillcurrent
short skilloverridden
short bounditem

Begin Function { myitem }

if Call MOOGetEquipmentAllowedFunctionScript myitem
	set skillrequired to ( GetAttackDamage myitem ) * ( GetWeaponSpeed myitem )
	set bounditem to 0
else
	set skillrequired to 0
	set bounditem to 1
endif


if bounditem == 0

if skillrequired < ( GetAttackDamage WeapSilverBow ) * ( GetWeaponSpeed WeapSilverBow )
	set skillrequired to 0
elseif skillrequired < ( GetAttackDamage WeapDwarvenBow ) * ( GetWeaponSpeed WeapDwarvenBow )
	set skillrequired to 25
elseif skillrequired < ( GetAttackDamage WeapGlassBow ) * ( GetWeaponSpeed WeapGlassBow )
	set skillrequired to 50
elseif skillrequired < ( GetAttackDamage WeapDaedricBow ) * ( GetWeaponSpeed WeapDaedricBow )
	set skillrequired to 75
else
	set skillrequired to 100
endif

; check for filename and materialtype
set skilloverridden to Call MOOGetMaterialRequirementFunctionScript myitem
if skilloverridden != -1
	set skillrequired to skilloverridden
endif

; check ini file for changed requirements
set skilloverridden to Call MOOGetEquipmentRequirementFunctionScript myitem
if skilloverridden != -1
	set skillrequired to skilloverridden
endif

endif

set skillcurrent to player.GetBaseAV2 Marksman

if MOO.ini_debug
	printc "[MOO] %n requires: %.0f ( current: %.0f )" myitem skillrequired skillcurrent
endif

if skillcurrent < skillrequired
	SetFunctionValue ( 22000 + skillrequired )
else
	SetFunctionValue 0
endif

End