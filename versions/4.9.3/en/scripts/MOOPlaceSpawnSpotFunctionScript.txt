Maskar's Oblivion Overhaul.esp
0x0A6857
MOOPlaceSpawnSpotFunctionScript
Scn MOOPlaceSpawnSpotFunctionScript

ref spawnlist
float myx
float myy
float myz
short allowed

Begin Function { spawnlist myx myy myz allowed }

if allowed
	MOOCellPlacer.MoveTo DrakeloweMapMarker
	MOOCellPlacer.setpos x myx 
	MOOCellPlacer.setpos y myy 
	MOOCellPlacer.setpos z myz 
else
	if MOO.ini_debug == 2
		printc "[MOO] Disabled creature spawn point."
	endif

	MOOCellPlacer.MoveTo MOOCellMarker
endif

spawnlist.MoveTo MOOCellPlacer
spawnlist.Update3D

MOOCellPlacer.MoveTo MOOCellMarker

End