Maskar's Oblivion Overhaul.esp
0x547AD9
MOOCompanionUpdateTradeItemsWithMerchantLockpicksFunctionScript
Scn MOOCompanionUpdateTradeItemsWithMerchantLockpicksFunctionScript

ref mycompanion
ref mymerchant
ref mycontainer
short i

ref myitem
short minitems
short maxitems

short busy

Begin Function { mycompanion mymerchant mycontainer i }

set myitem to Lockpick
set minitems to MOO.ini_companion_loot_lockpicks_min
set maxitems to MOO.ini_companion_loot_lockpicks_max

set busy to Call MOOCompanionUpdateTradeItemsWithMerchantMiscItemFunctionScript mycompanion mymerchant i myitem minitems maxitems

SetFunctionValue busy

End