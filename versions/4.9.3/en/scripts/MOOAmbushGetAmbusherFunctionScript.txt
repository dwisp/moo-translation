Maskar's Oblivion Overhaul.esp
0x53AD80
MOOAmbushGetAmbusherFunctionScript
Scn MOOAmbushGetAmbusherFunctionScript

ref me
short tempcode
short creaturecode
ref myfaction

ref ambusher

Begin Function {  }

set me to GetFirstRef 69 2
while me
	if me.GetDisabled == 0
	if me.GetDead == 0
	if me.SameFaction Player == 0
	if me.GetDisposition Player < 50

		; NPC
		if me.GetCreatureType == -1

			if me.GetDistance player < MOO.ini_actors_ambush_distance_npc
			if me.GetAV Aggression == 100
				set myfaction to Call MOOGetEvilFactionFunctionScript me
				if myfaction
					set ambusher to me
					set creaturecode to 100
					break
				endif
			endif
			endif

		; CREATURE
		elseif me.GetDistance player < MOO.ini_actors_ambush_distance_creature

			set tempcode to Call MOOGetCreatureCodeFunctionScript me
			if tempcode
				set creaturecode to tempcode
				set ambusher to me
				break
			endif

		endif

	endif
	endif
	endif
	endif

	set me to GetNextRef
loop

SetFunctionValue ambusher

End