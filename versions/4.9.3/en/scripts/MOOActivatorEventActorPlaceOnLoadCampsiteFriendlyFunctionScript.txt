Maskar's Oblivion Overhaul.esp
0x5841CD
MOOActivatorEventActorPlaceOnLoadCampsiteFriendlyFunctionScript
Scn MOOActivatorEventActorPlaceOnLoadCampsiteFriendlyFunctionScript

ref me
short mytype ; 0 forester
short myseed

ref myboss
ref mygrunt

Begin Function { me mytype myseed }

; ADD MORE FRIENDLY ENCOUNTERS

; ADVENTURERS
set myboss to MOOLL0Adventurer
set mygrunt to MOOLL0Adventurer


; BOSS
if me.GetBaseObject == MOOMarkerEventActorCampsiteBoss
	me.PlaceAtMe myboss

; GRUNT
else
	me.PlaceAtMe mygrunt
endif

End