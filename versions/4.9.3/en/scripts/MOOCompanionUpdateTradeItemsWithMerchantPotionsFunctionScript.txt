Maskar's Oblivion Overhaul.esp
0x547AD8
MOOCompanionUpdateTradeItemsWithMerchantPotionsFunctionScript
Scn MOOCompanionUpdateTradeItemsWithMerchantPotionsFunctionScript

ref mycompanion
ref mymerchant
ref mycontainer
short i

short miningredients
short maxingredients
short ingredienttype

short busy

Begin Function { mycompanion mymerchant mycontainer i }

set miningredients to MOO.ini_companion_loot_potions_min
set maxingredients to MOO.ini_companion_loot_potions_max
set ingredienttype to 40

set busy to MOOCompanionUpdateTradeItemsWithMerchantMagicEffectItemsFunctionScript mycompanion mymerchant i miningredients maxingredients ingredienttype

SetFunctionValue busy

End