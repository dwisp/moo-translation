Maskar's Oblivion Overhaul.esp
0x34584E
MOOUpdateClutterFunctionScript
Scn MOOUpdateClutterFunctionScript

short myvalue

Begin Function {  }

; unicorn horn not quest item when more unicorns spawn
if MOO.ini_add_unicorns
	SetQuestItem 0 UnicornHorn
else
	SetQuestItem 1 UnicornHorn
endif

; update equipment

SetWeight 1.5 PitBoots
SetWeight 7.5 PitCuirass
SetWeight 1.5 PitGauntlets
SetWeight 4.5 PitGreaves
SetWeight 1.5 PitHelmet

SetGoldValue 15 PitBoots
SetGoldValue 75 PitCuirass
SetGoldValue 15 PitGauntlets
SetGoldValue 30 PitGreaves
SetGoldValue 15 PitHelmet

SetObjectHealth 80 PitBoots
SetObjectHealth 160 PitCuirass
SetObjectHealth 80 PitGauntlets
SetObjectHealth 120 PitGreaves
SetObjectHealth 80 PitHelmet

; hide to show indiana hat for tomb raiders
SetHidesAmulet 0 MiddleShirt06


; override gold value of backpacks

if GetFullGoldValue MOOBackpack1Static != MOO.ini_gold_backpack_tiny

	SetGoldValue 0 MOOBackpack1Static
	set myvalue to MOO.ini_gold_backpack_tiny - GetFullGoldValue MOOBackpack1Static
	SetGoldValue myvalue MOOBackpack1Static

	SetGoldValue 0 MOOBackpack2Static
	set myvalue to MOO.ini_gold_backpack_small - GetFullGoldValue MOOBackpack2Static
	SetGoldValue myvalue MOOBackpack2Static

	SetGoldValue 0 MOOBackpack3Static
	set myvalue to MOO.ini_gold_backpack_medium - GetFullGoldValue MOOBackpack3Static
	SetGoldValue myvalue MOOBackpack3Static

	SetGoldValue 0 MOOBackpack4Static
	set myvalue to MOO.ini_gold_backpack_large - GetFullGoldValue MOOBackpack4Static
	SetGoldValue myvalue MOOBackpack4Static

	SetGoldValue 0 MOOBackpack5Static
	set myvalue to MOO.ini_gold_backpack_huge - GetFullGoldValue MOOBackpack5Static
	SetGoldValue myvalue MOOBackpack5Static
endif


if MOO.ini_update_clutter == 0
	return
endif

SetWeight MOO.ini_weight_lockpick Lockpick
SetWeight MOO.ini_weight_skeletonkey DASkeletonKey
SetWeight MOO.ini_weight_repairhammer RepairHammer

SetWeight MOO.ini_weight_handscythe HandScythe01
SetWeight MOO.ini_weight_handscythe MOOWeapHandScythe
SetWeight MOO.ini_weight_pickaxe Pickaxe01
SetWeight MOO.ini_weight_pickaxe Pickaxe01dirty
SetWeight MOO.ini_weight_pickaxe MOOWeapPickaxe
SetWeight MOO.ini_weight_pickaxe MOOWeapPickaxe2

SetWeight MOO.ini_weight_herderscrook MOOStaffofHerding
SetWeight MOO.ini_weight_magnifyingglass MOOMagnifyingGlass

SetWeight MOO.ini_weight_blacksmithshammer MOOWeapHammer
SetWeight MOO.ini_weight_fletchingjig MOOToolFletchingJig
SetWeight MOO.ini_weight_graingrinder MOOToolGrainGrinder
SetWeight MOO.ini_weight_rollingpin MOOToolRollingPin
SetWeight MOO.ini_weight_scissors MOOToolScissors
SetWeight MOO.ini_weight_scissors MOOToolScissorsWorn
SetWeight MOO.ini_weight_sewingkit MOOToolSewingKit
SetWeight MOO.ini_weight_skillet MOOToolSkillet
SetWeight MOO.ini_weight_skinningknife MOOWeapSkinningKnife
SetWeight MOO.ini_weight_tinkerstools MOOToolTinkersTools

SetWeight 0.5 Flour
SetGoldValue 4 Flour

SetGoldValue 3 HandScythe01
SetGoldValue 8 Pickaxe01
SetGoldValue 8 Pickaxe01dirty

SetWeight 1 LowerClassPitcher01
SetWeight 1 LowerPitcherTan01
SetWeight 1 MiddleClassPewterPitcher01
SetWeight 1 MiddlePitcherRed01
SetWeight 1 MiddlePitcherRed02
SetWeight 1 MiddlePitcherRed03
SetWeight 1 MiddlePitcherTan01
SetWeight 1 MiddlePitcherTan02
SetWeight 1 MiddlePitcherTan03
SetWeight 1 UpperPitcherCeramic01
SetWeight 1 UpperPitcherCeramic02
SetWeight 1 UpperSilverPitcher01
SetWeight 1 UpperSilverPitcher02

SetGoldValue 2 LowerClassPitcher01
SetGoldValue 2 LowerPitcherTan01
SetGoldValue 4 MiddleClassPewterPitcher01
SetGoldValue 4 MiddlePitcherRed01
SetGoldValue 4 MiddlePitcherRed02
SetGoldValue 4 MiddlePitcherRed03
SetGoldValue 4 MiddlePitcherTan01
SetGoldValue 4 MiddlePitcherTan02
SetGoldValue 4 MiddlePitcherTan03
SetGoldValue 8 UpperPitcherCeramic01
SetGoldValue 8 UpperPitcherCeramic02
SetGoldValue 12 UpperSilverPitcher01
SetGoldValue 12 UpperSilverPitcher02

if MOO.ini_ability_crafting

SetName "Empty Stone Pitcher" LowerClassPitcher01
SetName "Empty Tan Pitcher" LowerPitcherTan01
SetName "Empty Pewter Pitcher" MiddleClassPewterPitcher01
SetName "Empty Clay Pitcher" MiddlePitcherRed01
SetName "Empty Clay Pitcher" MiddlePitcherRed02
SetName "Empty Clay Pitcher" MiddlePitcherRed03
SetName "Empty Tan Pitcher" MiddlePitcherTan01
SetName "Empty Tan Pitcher" MiddlePitcherTan02
SetName "Empty Tan Pitcher" MiddlePitcherTan03
SetName "Empty Ceramic Pitcher" UpperPitcherCeramic01
SetName "Empty Ceramic Pitcher" UpperPitcherCeramic02
SetName "Empty Silver Pitcher" UpperSilverPitcher01
SetName "Empty Silver Pitcher" UpperSilverPitcher02

endif

End