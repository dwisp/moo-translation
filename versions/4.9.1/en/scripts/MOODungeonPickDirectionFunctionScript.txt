Maskar's Oblivion Overhaul.esp
0x4C0222
MOODungeonPickDirectionFunctionScript
Scn MOODungeonPickDirectionFunctionScript

short mydigger

short myrand

short mydirection

Begin Function { mydigger }

; DIGGER
; MOO.dungeonx[myx]
; MOO.dungeony[myy]
; MOO.dungeondirection[mydigger] ; 1, 2, 4, 8 == top, left, bottom, right

let mydirection := MOO.roomdirection[mydigger]


set myrand to GetRandomPercent
if myrand < 25
	; turn left
	let mydirection := mydirection << 1
	if mydirection == 16
		set mydirection to 1
	endif

elseif myrand < 50
	; turn right
	let mydirection := mydirection >> 1
	if mydirection == 0
		set mydirection to 8
	endif

elseif myrand < 75
	; stop
	set mydirection to 0

endif

SetFunctionValue mydirection

End