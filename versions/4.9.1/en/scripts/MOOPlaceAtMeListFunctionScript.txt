Maskar's Oblivion Overhaul.esp
0x56FE9C
MOOPlaceAtMeListFunctionScript
Scn MOOPlaceAtMeListFunctionScript

ref myplacer
ref mylist
short i

ref myobject

Begin Function { myplacer mylist i }

while i
	set myobject to CalcLeveledItem mylist 99
	if myobject
		myplacer.PlaceAtMe myobject
	endif

	set i to i - 1
loop

End