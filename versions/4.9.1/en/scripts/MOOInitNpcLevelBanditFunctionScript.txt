Maskar's Oblivion Overhaul.esp
0x3FF35E
MOOInitNpcLevelBanditFunctionScript
Scn MOOInitNpcLevelBanditFunctionScript

ref me

Begin Function { me }

if me.GetScript == BanditRobberSCRIPT
	Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_bandit_highwayman MOO.ini_levelmax_bandit_highwayman MOO.ini_levelmod_bandit_highwayman
	return
endif


if me.GetClass == BanditMelee || me.GetClass == BanditMissile || me.GetClass == BanditWizard || me.GetClass == BanditBoss

	if me.GetAV Confidence == 100

		if me.GetInFaction MOOBanditFaction
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_bandit_hard_boss MOO.ini_levelmax_bandit_hard_boss MOO.ini_levelmod_bandit_hard_boss
		else
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_bandit_boss MOO.ini_levelmax_bandit_boss MOO.ini_levelmod_bandit_boss
		endif

	else

		if me.GetInFaction MOOBanditFaction
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_bandit_hard_grunt MOO.ini_levelmax_bandit_hard_grunt MOO.ini_levelmod_bandit_hard_grunt
		else
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_bandit_grunt MOO.ini_levelmax_bandit_grunt MOO.ini_levelmod_bandit_grunt
		endif

	endif

elseif me.GetClass == MarauderMelee || me.GetClass == MarauderMissile || me.GetClass == MarauderMage || me.GetClass == MarauderBoss

	if me.GetAV Confidence == 100

		if me.GetInFaction MOOMarauderFaction
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_marauder_hard_boss MOO.ini_levelmax_marauder_hard_boss MOO.ini_levelmod_marauder_hard_boss
		else
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_marauder_boss MOO.ini_levelmax_marauder_boss MOO.ini_levelmod_marauder_boss
		endif

	else

		if me.GetInFaction MOOMarauderFaction
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_marauder_hard_grunt MOO.ini_levelmax_marauder_hard_grunt MOO.ini_levelmod_marauder_hard_grunt
		else
			Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_marauder_grunt MOO.ini_levelmax_marauder_grunt MOO.ini_levelmod_marauder_grunt
		endif

	endif

endif


End