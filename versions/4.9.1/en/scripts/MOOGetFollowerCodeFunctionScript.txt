Maskar's Oblivion Overhaul.esp
0x2FEDA2
MOOGetFollowerCodeFunctionScript
Scn MOOGetFollowerCodeFunctionScript

short total
short i

short followercode

Begin Function {  }

set total to ar_Size MOOTame.Followers

set i to 1
while i < total
	if eval MOOTame.followers[i] == 0
		set followercode to i
		break
	endif
	set i to i + 1
loop

if followercode == 0
	; make array bigger
	set followercode to total
endif

SetFunctionValue followercode

End