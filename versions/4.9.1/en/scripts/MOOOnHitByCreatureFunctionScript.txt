Maskar's Oblivion Overhaul.esp
0x01DF75
MOOOnHitByCreatureFunctionScript
Scn MOOOnHitByCreatureFunctionScript

ref attacker
ref target

short resistchance

Begin Function { target attacker }

if target == 0 || attacker == 0
	return
elseif target.IsActor == 0 || attacker.IsActor == 0
	return
endif

if attacker.HasEffectShader MOOeffectBloodlust
	Call MOOOnHitByBloodlustFunctionScript target attacker
endif

if MOO.ini_damage_flat
	Call MOOOnHitByCreatureFlatDamageFunctionScript target attacker
endif

if attacker.GetCreatureType == -1
if attacker.GetClass == MOOClassSaboteur
	Call MOOOnHitBySaboteurFunctionScript target attacker
endif
endif

if target.HasSpell MOOAbScoutInvisibility || target.GetInFaction MOOMoragTongFaction
	Call MOOOnHitByScoutFunctionScript target
endif

if target.GetDisposition attacker == 100
	Call MOOOnHitDisposition100FunctionScript target attacker
endif

if target.IsSpellTarget MOOSpellBandage
	target.AddItemNS MOOTokenHealingInterrupt 1
endif

if attacker.CompareModelPath "MOO\OreAtronach\"
	Call MOOOnHitByOreAtronachFunctionScript target attacker
endif

if target != player
	if target.GetAV Confidence < 100
		; resistchance == healthmod - levelmod - confidencemod
		set resistchance to ( ( target.GetAV Health ) / ( target.GetMaxAV Health ) * 100 )   -   ( attacker.GetLevel - target.GetLevel )   -   ( ( ( attacker.GetAV Confidence ) - ( target.GetAV Confidence ) ) / 3 )
		if resistchance < 30
			Call MOOOnHitWithLowHealthFunctionScript target attacker
		endif
	endif

	if attacker != target.GetCombatTarget
	if GetRandomPercent < 20
		Call MOOOnHitByCreatureSwitchTargetFunctionScript target attacker
	endif
	endif
endif

if attacker.GetItemCount MOOTokenDiseaseCode
	Call MOOOnHitWithDiseaseFunctionScript target attacker
endif

if attacker.GetCreatureType == 5
	Call MOOOnHitToGiantFunctionScript target attacker
endif

if attacker.GetCombatStyle == MOOScorpionCombatStyle
	Call MOOOnHitByScorpionFunctionScript target attacker
elseif attacker.GetCombatStyle == DefaultMudCrab
	Call MOOOnHitByMudCrabFunctionScript target attacker
endif

End