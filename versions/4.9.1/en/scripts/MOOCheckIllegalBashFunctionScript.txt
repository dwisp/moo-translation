Maskar's Oblivion Overhaul.esp
0x06F319
MOOCheckIllegalBashFunctionScript
Scn MOOCheckIllegalBashFunctionScript

ref mytarget

ref itemowner
ref targetcell
short allowed

Begin Function { mytarget }

set itemowner to mytarget.GetOwner
if itemowner == 0
	set itemowner to mytarget.GetParentCellOwner
endif

if mytarget.IsDoor
	set mytarget to mytarget.GetLinkedDoor
	set itemowner to mytarget.GetOwner
	if itemowner == 0
		set itemowner to mytarget.GetParentCellOwner
	endif
endif

if itemowner
	if ( GetObjectType itemowner ) == 6
		if player.GetInFaction itemowner
			set allowed to 1
		endif
	else
		if itemowner == Player.GetBaseObject
			set allowed to 1
		endif
	endif
else
	set allowed to 1
endif

if allowed == 0
	player.SendTrespassAlarm player

	if MOO.ini_debug
		printc "[MOO] Lock bash on owned object. Alarm activated."
	endif
endif

End