Maskar's Oblivion Overhaul.esp
0x244F49
MOOGetCell23FunctionScript
Scn MOOGetCell23FunctionScript

short dungeontype ; 0 default 1 vampire 2 necromancer 3 conjurer

short i

ref mycell

Begin Function { dungeontype }

if dungeontype == 1 ; vampire

	set i to Rand 0 2
	if i == 0
		set mycell to Crowhaven01
	else
		set mycell to Crowhaven02
	endif

elseif dungeontype == 2 ; necromancer

	set mycell to GarlasAgea

elseif dungeontype == 3 ; conjurer

	set i to Rand 0 2
	if i == 0
		set mycell to Beldaburo
	else
		set mycell to Beldaburo02
	endif

else ; default

	set i to Rand 0 5
	if i == 0
		set mycell to FortStrand01
	elseif i == 1
		set mycell to FortStrand02
	elseif i == 2
		set mycell to FortWariel
	elseif i == 3
		set mycell to HrotaCave
	else
		set mycell to FortSutchInterior
	endif

endif


SetFunctionValue mycell

End