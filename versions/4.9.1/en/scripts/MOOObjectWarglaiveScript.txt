Maskar's Oblivion Overhaul.esp
0x12F13F
MOOObjectWarglaiveScript
scn MOOObjectWarglaiveScript

ref me
short doOnce

Begin GameMode

	Set me to GetContainer
	if me == 0
		return
	endif

	if ( doOnce == 2 )
		Set doOnce to 0
		Return
	endif

	if ( doOnce == 0 )
		Set doOnce to 0
		if ( me.IsActor == 1 && me.GetIsCreature != 1 && me.GetEquipped MOOWeapWarglaive == 1 )
			Set doOnce to 1
		endif
		Return
	endif

	if ( me.GetEquipped MOOWeapWarglaive == 1 && me.GetEquipped MOOArmorWarglaiveShield == 1 )
		if ( me.IsWeaponOut == 0 )
			if ( me.GetItemCount MOOWeapWarglaiveBack == 0 )
				me.AddItemNS MOOWeapWarglaiveBack 1
			endif
			me.EquipItemNS MOOWeapWarglaiveBack
			Set doOnce to 2
		endif
	endif

End