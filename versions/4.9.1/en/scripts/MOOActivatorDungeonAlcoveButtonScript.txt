Maskar's Oblivion Overhaul.esp
0x51C622
MOOActivatorDungeonAlcoveButtonScript
Scn MOOActivatorDungeonAlcoveButtonScript

short open
short fail

Begin OnActivate

if open
	set fail to Call MOODungeonActivatorPlayGroupSameLocationFunctionScript GetSelf 1
	if fail == 0
		set open to 0
	endif

else
	set fail to Call MOODungeonActivatorPlayGroupSameLocationFunctionScript GetSelf 0
	if fail == 0
		set open to 1
	endif
endif

End