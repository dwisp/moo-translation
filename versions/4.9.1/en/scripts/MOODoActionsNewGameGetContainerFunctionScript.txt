Maskar's Oblivion Overhaul.esp
0x5B2A95
MOODoActionsNewGameGetContainerFunctionScript
Scn MOODoActionsNewGameGetContainerFunctionScript

ref me
ref mycontainer

short totalcontainers
short i
short myrand
float randmax

Begin Function {  }

set totalcontainers to GetNumRefs 23

if totalcontainers

	set randmax to totalcontainers - 0.01
	set myrand to Rand 0 randmax

	set me to GetFirstRef 23
	while me
		if i == myrand
			set mycontainer to me
			break
		endif
		set i to i + 1

		set me to GetNextRef
	loop
endif

SetFunctionValue mycontainer

End