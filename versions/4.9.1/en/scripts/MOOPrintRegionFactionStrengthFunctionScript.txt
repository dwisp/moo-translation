Maskar's Oblivion Overhaul.esp
0x444283
MOOPrintRegionFactionStrengthFunctionScript
Scn MOOPrintRegionFactionStrengthFunctionScript

short i
short j
short totalfactions
short totalregions
short totalinvaders
ref myfaction

Begin Function {  }

set totalfactions to -1 + ar_Size MOO.regionalfactions
set totalregions to 27

printc "totalfactions: %.0f" totalfactions

printc "============================================"

set i to 1
while i <= totalfactions

	set totalinvaders to 0
	set j to 0
	while j <= totalregions
		let totalinvaders := totalinvaders + MOO.regioncontrol[i][j] ; [faction][region]

		set j to j + 1
	loop

	let myfaction := MOO.regionalfactions[i]
	printc "%n (%.0f) has %.0f invaders" myfaction i totalinvaders

	set i to i + 1
loop

printc "============================================"

End