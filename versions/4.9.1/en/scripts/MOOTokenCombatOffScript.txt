Maskar's Oblivion Overhaul.esp
0x17984F
MOOTokenCombatOffScript
Scn MOOTokenCombatOffScript

ref me

short xpos
short ypos
short xposold
short yposold

short checkblocked
float stucktime
short fly
float speedmod

Begin Gamemode

set me to GetContainer

if fly
	me.SetVerticalVelocity 100
	set fly to 0
endif

if me.IsAnimGroupPlaying FastForward
	if me.CompareModelPath "\Cat\"
		set speedmod to 100
	else
		set speedmod to 20
	endif
elseif me.IsAnimGroupPlaying Forward
	set speedmod to 4
else
	return
endif

if me.IsCasting || me.IsPowerAttacking || me.IsAnimGroupPlaying SpecialIdle
	return
endif

if speedmod > 0
if checkblocked
	set xpos to ( me.GetPos x ) / speedmod
	set ypos to ( me.Getpos y ) / speedmod
	if xpos == xposold && ypos == yposold
		set stucktime to stucktime + GetSecondsPassed
	else
		set checkblocked to 0
	endif
else
	set checkblocked to 1
	set stucktime to 0
	set xposold to ( me.GetPos x ) / speedmod
	set yposold to ( me.GetPos y ) / speedmod
endif
if stucktime > 0.7
	set fly to 1
endif
endif

End
