Maskar's Oblivion Overhaul.esp
0x49BD81
MOOAICompanionFightAggressivelyFunctionScript
Scn MOOAICompanionFightAggressivelyFunctionScript

ref me

Begin Function { me }

if me.GetItemCount MOOTokenCompanionAggressive == 0
	me.AddItemNS MOOTokenCompanionAggressive 1
endif

me.SetAV Aggression 25

End