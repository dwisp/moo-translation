Maskar's Oblivion Overhaul.esp
0x4C689A
MOODungeonExecuteTunnelDiggersFunctionScript
Scn MOODungeonExecuteTunnelDiggersFunctionScript

short mydigger
short allowed
short mydirection
short failed

Begin Function {  }

while MOO.tunneldiggers

	set allowed to 0
	set failed to 0
	while allowed == 0 && failed < 25
		set mydirection to Call MOODungeonPickDirectionTunnelFunctionScript MOO.tunneldiggers
		if mydirection == 0
			break
		endif
		set allowed to Call MOODungeonCheckDirectionAllowedTunnelFunctionScript MOO.tunneldiggers mydirection
		if allowed == 0
			set failed to failed + 1
		endif
	loop


	if mydirection == 0 || failed >= 25 ; stop

		if GetRandomPercent < 75
			Call MOODungeonCreateRoomDiggersContinuedFunctionScript MOO.tunneldiggers
		endif
		set MOO.tunneldiggers to MOO.tunneldiggers - 1

	else
		Call MOODungeonSetCurrentLocationTunnelFunctionScript MOO.tunneldiggers mydirection
	endif
loop

End
