Maskar's Oblivion Overhaul.esp
0x444958
MOOUpdateCurrentRegionDoorFactionFunctionScript
Scn MOOUpdateCurrentRegionDoorFactionFunctionScript

ref b1
ref b2
ref b3
ref b4
ref m1
ref m2
ref m3
ref m4

ref sourcelist
ref targetlist

Begin Function { b1 b2 b3 b4 m1 m2 m3 m4 }

set sourcelist to b1
set targetlist to MOOLL2VanillaBanditMelee
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

set sourcelist to b2
set targetlist to MOOLL2VanillaBanditMissile
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

set sourcelist to b3
set targetlist to MOOLL2VanillaBanditWizard
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

set sourcelist to b4
set targetlist to MOOLL2VanillaBanditBoss
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

set sourcelist to m1
set targetlist to MOOLL2VanillaMarauderMelee
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

set sourcelist to m2
set targetlist to MOOLL2VanillaMarauderMissile
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

set sourcelist to m3
set targetlist to MOOLL2VanillaMarauderBattlemage
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

set sourcelist to m4
set targetlist to MOOLL2VanillaMarauderBoss
ClearLeveledList targetlist
AddToLeveledList targetlist sourcelist 1 1

End