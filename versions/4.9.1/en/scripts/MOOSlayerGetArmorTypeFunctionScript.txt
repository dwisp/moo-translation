Maskar's Oblivion Overhaul.esp
0x2D4232
MOOSlayerGetArmorTypeFunctionScript
Scn MOOSlayerGetArmorTypeFunctionScript

ref me

ref myarmor
ref combatstyle

short armortype

Begin Function { me }

; armortype 0 == light/ranged creature 1 == close combat creature 2 == heavy armor npc

if me.GetCreatureType == -1 ; npc

	if GetRandomPercent < 50
		set myarmor to me.GetEquippedObject 2
	else
		set myarmor to me.GetEquippedObject 3
	endif
	if myarmor
	if IsArmor myarmor
	if GetArmorType myarmor == 1
		set armortype to 2
	endif
	endif
	endif

else

	set combatstyle to me.GetCombatStyle
	if combatstyle
		if GetCombatStylePrefersRanged combatstyle
			set armortype to 0
		else
			set armortype to 1
		endif
	else
		set armortype to 1
	endif
endif


SetFunctionValue armortype

End