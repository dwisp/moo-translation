Maskar's Oblivion Overhaul.esp
0x43DBFF
MOOUpdateMoragTongMoveDeadFunctionScript
Scn MOOUpdateMoragTongMoveDeadFunctionScript

ref me
short i

Begin Function {  }

set i to ar_Size MOO.moragtong

while i > 0
	set i to i - 1
	let me := MOO.moragtong[i]

	if me.GetTimeDead > 24
		me.MoveTo MOOCellMarker
		me.Resurrect
		me.Disable
	endif
loop

End