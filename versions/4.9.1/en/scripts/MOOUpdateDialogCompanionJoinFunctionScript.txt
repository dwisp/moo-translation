Maskar's Oblivion Overhaul.esp
0x497FFE
MOOUpdateDialogCompanionJoinFunctionScript
Scn MOOUpdateDialogCompanionJoinFunctionScript

ref me

short i
short mysize
ref myadventurer

short penaltylevel ; penalty based on level difference
short penaltytotal ; penalty based on amount of companions
short penaltyconfidence ; penalty based on confidence

short mydisposition
short mylevel
short myconfidence

short dialogcompanionjoin

Begin Function { me }

; LEVEL PENALTY
set mylevel to me.GetLevel
if mylevel > player.GetLevel
	set penaltylevel to ( mylevel - player.GetLevel ) * MOO.ini_companion_penalty_level
endif

; NPCS WITH LOWER CONFIDENCE ARE MORE INTERESTED TO JOIN PARTY
set myconfidence to me.GetAv Confidence
if myconfidence < 25
	set penaltyconfidence to MOO.ini_companion_penalty_confidence_verylow
elseif myconfidence < 50
	set penaltyconfidence to MOO.ini_companion_penalty_confidence_low
elseif myconfidence < 75
	set penaltyconfidence to MOO.ini_companion_penalty_confidence_average
elseif myconfidence < 99
	set penaltyconfidence to MOO.ini_companion_penalty_confidence_high
else
	set penaltyconfidence to MOO.ini_companion_penalty_confidence_veryhigh
endif

; COMPANION AMOUNT PENALTY
set penaltytotal to penaltytotal + MOO.totalcompanions * MOO.ini_companion_penalty_total

; DISPOSITION
set mydisposition to me.GetDisposition player

; CHECK CAN JOIN
if penaltylevel + penaltytotal + penaltyconfidence + MOO.ini_companion_disposition_min <= mydisposition
	set dialogcompanionjoin to 1
endif

if MOO.ini_debug
	printc "[MOO] Companion penaltylevel: %.0f penaltytotal: %.0f penaltyconfidence: %.0f mydisposition: %.0f dialogcompanionjoin: %.0f" penaltylevel penaltytotal penaltyconfidence mydisposition dialogcompanionjoin
endif

SetFunctionValue dialogcompanionjoin

End