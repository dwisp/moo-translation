Maskar's Oblivion Overhaul.esp
0x518845
MOOOutpostSetCurrentLocationTunnelGateFunctionScript
Scn MOOOutpostSetCurrentLocationTunnelGateFunctionScript

short mydigger
short myx
short myy

short maxgates

Begin Function { mydigger myx myy }

let maxgates := ( Ar_Size MOO.dungeongates ) - 1

; TEMP SETTINGS TO CREATE LINKED GATE EVENT ( UP TO 3 OBJECTS )
;short currentgatedigger
;short currentgateplatex
;short currentgateplatey
;short currentgateactivatorx
;short currentgateactivatory
;short currentgatex
;short currentgatey
;short currentgatestatus ; bits 1 (key), 2 (activator), 3 (gate)

; INIT
if mydigger != MOO.currentgatedigger
	set MOO.currentgatedigger to mydigger
	set MOO.currentgatestatus to 0
endif

; UPDATE STATUS


; ADD GATE
if ( MOO.currentgatestatus == 0 && GetRandomPercent < MOO.ini_outpost_tunnel_gate ) || MOO.currentgatestatus

if eval MOO.dungeonmap[myx][myy] == 5 || MOO.dungeonmap[myx][myy] == 10
if eval MOO.dungeonmapevent[myx][myy] == 0

	if eval MOO.currentgatestatus & 2 == 0 ; add activator
		set MOO.currentgatestatus to MOO.currentgatestatus + 2
		set MOO.currentgateactivatorx to myx
		set MOO.currentgateactivatory to myy

		; DO NOT USE THIS TILE -> RESERVED
		let MOO.dungeonmapevent[myx][myy] := MOO.id_dungeon_reserved

	elseif eval MOO.currentgatestatus & 4 == 0 ; add gate
		set MOO.currentgatestatus to MOO.currentgatestatus + 4
		set MOO.currentgatex to myx
		set MOO.currentgatey to myy

		; DO NOT USE THIS TILE -> RESERVED
		let MOO.dungeonmapevent[myx][myy] := MOO.id_dungeon_reserved
	endif

endif
endif

endif


; CREATE GATE EVENT
if eval MOO.currentgatestatus & 6 == 6 ; found activator and gate ; optionally also key
	if eval MOO.dungeonmapevent[MOO.currentgateactivatorx][MOO.currentgateactivatory] == MOO.id_dungeon_reserved
	if eval MOO.dungeonmapevent[MOO.currentgatex][MOO.currentgatey] == MOO.id_dungeon_reserved

	if eval MOO.dungeonmap[MOO.currentgateactivatorx][MOO.currentgateactivatory] == 5 || MOO.dungeonmap[MOO.currentgateactivatorx][MOO.currentgateactivatory] == 10
	if MOO.currentdungeongate < maxgates

		Call MOOOutpostSetCurrentLocationTunnelGateAssignFunctionScript

	endif
	endif

	endif
	endif

	set MOO.currentgatestatus to 0

endif


End