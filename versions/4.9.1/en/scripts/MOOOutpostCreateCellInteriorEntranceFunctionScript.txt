Maskar's Oblivion Overhaul.esp
0x50B1E5
MOOOutpostCreateCellInteriorEntranceFunctionScript
Scn MOOOutpostCreateCellInteriorEntranceFunctionScript

float dungeonheight
ref myplacer

short myx
short myy
short mydirection
short i

short myentrancetile
ref mydoor1
ref mydoor2
short myrand

short myheight ; always starts at 0

short mydigger

Begin Function { dungeonheight myplacer }

; PICK RANDOM LOCATION
set myx to Rand 1 15.99
set myy to Rand 1 15.99

; PICK DIRECTION
set mydirection to 1 ; up


; xxxx xxxx xxxx
; special room tunnel

; tunnel + room
; tunnel + special ( load door )
; tunnel + stairs
; tunnel + ramp

; xxxx 0000 xxxx

; [][stairs][ramp][door]

; PICK START LOCATION

; CREATE EXIT TILE
set myheight to 0 ; base height is adjusted later on
let myentrancetile := 256 + 5 + ( 4 << 4 ) ; doorway + tunnel direction + door direction

let MOO.dungeonmap[myx][myy] := myentrancetile
let MOO.dungeonmapheight[myx][myy] := myheight

; CREATE DOOR + MARKER
set mydoor2 to Call MOOOutpostCreateCellInteriorEntranceDoorFunctionScript myx myy dungeonheight myplacer
Call MOOOutpostCreateCellInteriorEntranceMarkerFunctionScript mydoor1 mydoor2 myplacer ; mydoor1 == 0 as it's already created

; CREATE STAIRS TILE
set myheight to myheight - 128 ; tunnel digger is lowered

set myy to myy + 1
let MOO.dungeonmap[myx][myy] := 4 + 1024 ; tunnel down + stairs ; down because stairs go down
let MOO.dungeonmapheight[myx][myy] := myheight


; CREATE TUNNEL TILE
set myy to myy + 1
let MOO.dungeonmap[myx][myy] := 4 ; tunnel up
let MOO.dungeonmapheight[myx][myy] := myheight


; CREATE DIGGERS
; 0 forward
; 1 forward
; 2	forward
; 3	forward

; 4	left
; 5	right
; 6	left + forward
; 7	left + right
; 8	right + forward
; 9	left + right + forward

set myrand to Rand 0 7.999

; FORWARD
if myrand < 4 || myrand == 6 || myrand == 8 || myrand == 9
	set MOO.tunneldiggers to MOO.tunneldiggers + 1
	let MOO.tunnelx[MOO.tunneldiggers] := myx
	let MOO.tunnely[MOO.tunneldiggers] := myy
	let MOO.tunneldirection[MOO.tunneldiggers] := 1
endif

; LEFT
if myrand == 4 || myrand == 6 || myrand == 7 || myrand == 9
	set MOO.tunneldiggers to MOO.tunneldiggers + 1
	let MOO.tunnelx[MOO.tunneldiggers] := myx
	let MOO.tunnely[MOO.tunneldiggers] := myy
	let MOO.tunneldirection[MOO.tunneldiggers] := 2
endif

; RIGHT
if myrand == 5 || myrand == 7 || myrand == 8 || myrand == 9
	set MOO.tunneldiggers to MOO.tunneldiggers + 1
	let MOO.tunnelx[MOO.tunneldiggers] := myx
	let MOO.tunnely[MOO.tunneldiggers] := myy
	let MOO.tunneldirection[MOO.tunneldiggers] := 8
endif


End