Maskar's Oblivion Overhaul.esp
0x1C06E4
MOOOnMagicApplyFunctionScript
Scn MOOOnMagicApplyFunctionScript

ref magicitem
ref caster

ref me

Begin Function { magicitem caster }

if ( MagicItemHasEffect CUDI magicitem ) == 0
	return
endif

if MOO.ini_disease_groups == 0
	return
endif

set me to GetSelf

if caster.IsActor
if me.GetItemCount MOOTokenDisease == 0

	Call MOOCureDiseaseFunctionScript magicitem caster me
	me.AddItemNS MOOTokenDisease 1

endif
endif

End