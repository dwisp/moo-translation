Maskar's Oblivion Overhaul.esp
0x4F3E2C
MOOPlaceWorldSpaceReplaceRockWithHarpyNestFunctionScript
Scn MOOPlaceWorldSpaceReplaceRockWithHarpyNestFunctionScript

ref me

ref mynest
float myx
float myy

float myscale
float myradius
float mysize

short harpytype ; 0, 1, 2 pale, normal, dark

Begin Function { me }

; CHECK ANGLE
if Call MOOPlaceObjectOnTerrainCheckAngleFunctionScript me 200 25
	return
endif

; CHECK ABOVE WATER
if ( me.GetPos z ) < ( me.GetParentCellWaterHeight + 500 )
	return
endif

; CHECK SCALE
if me.GetScale < 1
	set myscale to me.GetScale
	set myradius to me.GetBoundingRadius
	set mysize to me.GetEditorSize
	return
endif

; CHECK BLOCKED
if Call MOOHelperCheckAreaBlockedFunctionScript me 400 ; 400 == range
	return
endif

set mynest to Call MOOPlaceWorldSpaceReplaceRockWithHarpyNestGetNestFunctionScript me

if mynest == 0
	return
endif

set myx to me.GetPos x
set myy to me.GetPos y

; xpos ypos mymarker myobject myradius zposmod zanglemod xylock zlock

Call MOOPlaceObjectOnTerrainFunctionScript myx myy me mynest 200 0 0 0 0

; harpies are added through nest

if MOO.ini_debug
	printc "[MOO] Placing harpy nest."
endif

SetFunctionValue 1

End