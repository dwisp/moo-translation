Maskar's Oblivion Overhaul.esp
0x428C65
MOORuneUpdateScript
Scn MOORuneUpdateScript

ref me

ref myspell1 ; attributes
ref myspell2 ; skills

ref myclass

Begin Function { me }

if me
if IsFormValid me
if me.IsActor

	set myspell1 to Call MOORuneGetSpellFunctionScript me MOOAbRuneAttributes

	if myspell1
		set myspell2 to Call MOORuneGetSpellFunctionScript me MOOAbRuneSkills

	elseif me.GetItemCount MOOTokenRune == 0

		set myspell1 to MOOAbRuneAttributes
		set myspell1 to CloneForm myspell1
		me.AddSpellNS myspell1

		set myspell2 to MOOAbRuneSkills
		set myspell2 to CloneForm myspell2
		me.AddSpellNS myspell2

	endif

	if me.GetItemCount MOOTokenRune == 0
		me.AddItemNS 	MOOTokenRune 1
	endif

endif
endif
endif

End