Maskar's Oblivion Overhaul.esp
0x53C8DA
MOOHelperCheckAreaBlockedFunctionScript
Scn MOOHelperCheckAreaBlockedFunctionScript

ref myplacer
short myrange

short found

Begin Function { myplacer myrange }

; check for statics, activators, trees and flora nearby

set found to Call MOOHelperCheckAreaBlockedObjectTypeFunctionScript myplacer myrange 18 ; activator
if found == 0
	set found to Call MOOHelperCheckAreaBlockedObjectTypeFunctionScript myplacer myrange 23 ; container
endif
if found == 0
	set found to Call MOOHelperCheckAreaBlockedObjectTypeFunctionScript myplacer myrange 24 ; door
endif
if found == 0
	set found to Call MOOHelperCheckAreaBlockedObjectTypeFunctionScript myplacer myrange 28 ; stat
endif
if found == 0
	set found to Call MOOHelperCheckAreaBlockedObjectTypeFunctionScript myplacer myrange 30 ; tree
endif
if found == 0
	set found to Call MOOHelperCheckAreaBlockedObjectTypeFunctionScript myplacer myrange 31 ; flora
endif
if found == 0
	set found to Call MOOHelperCheckAreaBlockedObjectTypeFunctionScript myplacer myrange 32 ; furniture
endif


SetFunctionValue found

End