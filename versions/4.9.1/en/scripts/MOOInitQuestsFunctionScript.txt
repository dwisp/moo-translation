Maskar's Oblivion Overhaul.esp
0x225AD4
MOOInitQuestsFunctionScript
Scn MOOInitQuestsFunctionScript

short questtype

Begin Function {  }

; region 15 and 24 are skipped as they have no npcs

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 10 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 11 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 12 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 13 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 14 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 20 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 21 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 22 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 23 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 25 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 26 questtype

set questtype to Call MOOGetQuestTypeFunctionScript
Call MOOInitQuestFunctionScript 27 questtype

End