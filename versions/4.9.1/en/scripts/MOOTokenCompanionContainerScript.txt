Maskar's Oblivion Overhaul.esp
0x4934D7
MOOTokenCompanionContainerScript
Scn MOOTokenCompanionContainerScript

ref me
short mystatus
ref mytorch

Begin Gamemode

set me to GetContainer
if me
	if mystatus == 0
		set mystatus to 1

		Call MOOHelperClearOwnerFunctionScript me me ; fix ownership if owned by companion - can happen through removeallitems

		me.ToggleActorsAI
		Call MOORemoveAllItemsScript me MOOCellChestStorage
		MOOCellChestStorage.Activate player
	elseif mystatus == 1
		set mystatus to 2
		MOOCellChestStorage.RemoveAllItems me 1
	elseif mystatus == 2
		if me.IsActorUsingATorch
			set mytorch to me.GetEquippedObject 14
			if me
				me.RemoveItem mytorch 1
				me.AddItem mytorch 1
				me.EquipItem mytorch
			endif
		else
			me.AddItem MOOTorchNoSound 1
			me.EquipItem MOOTorchNoSound
			me.RemoveItem MOOTorchNoSound 1
		endif

		; update dialog
		set MOOAI.dialogcompanionrepair to Call MOOUpdateDialogCompanionRepairFunctionScript me

		set mytorch to me.GetEquippedObject 14
		if mytorch
			me.UnequipItemSilent mytorch
		endif

		me.ToggleActorsAI
		RemoveMe
	endif
endif

End