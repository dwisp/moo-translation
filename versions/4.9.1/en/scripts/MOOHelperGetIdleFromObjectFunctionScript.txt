Maskar's Oblivion Overhaul.esp
0x573545
MOOHelperGetIdleFromObjectFunctionScript
Scn MOOHelperGetIdleFromObjectFunctionScript

ref myitem

ref myidle

Begin Function { myitem }

if IsIngredient myitem

			if ( CompareModelPath "\IngredAmbrosia.NIF" myitem ) || myitem == Ambrosia
				set myidle to MOOIdleEatAmbrosia
			elseif ( CompareModelPath "\Apple01.NIF" myitem ) || myitem == Apple
				set myidle to MOOIdleEatApple
			elseif ( CompareModelPath "\Beef.NIF" myitem ) || myitem == Beef
				set myidle to MOOIdleEatBeef
			elseif ( CompareModelPath "\Blackberry01.NIF" myitem ) || myitem == Blackberry
				set myidle to MOOIdleEatBlackberry
			elseif ( CompareModelPath "\IngredBoarMeat.NIF" myitem ) || myitem == BoarMeat
				set myidle to MOOIdleEatBoarMeat
			elseif ( CompareModelPath "\Bread01.NIF" myitem ) || myitem == Breadloaf
				set myidle to MOOIdleEatBreadloaf
			elseif ( CompareModelPath "\Carrot01.NIF" myitem ) || myitem == Carrot
				set myidle to MOOIdleEatCarrot
			elseif ( CompareModelPath "\Cheese01.NIF" myitem ) || myitem == CheeseWedge
				set myidle to MOOIdleEatCheeseWedge
			elseif ( CompareModelPath "\ingredcheesewheel.NIF" myitem ) || myitem == CheeseWheel
				set myidle to MOOIdleEatCheeseWheel
			elseif ( CompareModelPath "Corn01.NIF" myitem ) || myitem == Corn
				set myidle to MOOIdleEatCorn
			elseif ( CompareModelPath "\IngredCrabmeat.NIF" myitem ) || myitem == CrabMeat
				set myidle to MOOIdleEatCrabMeat
			elseif ( CompareModelPath "\Grapes01.NIF" myitem ) || myitem == Grapes
				set myidle to MOOIdleEatGrapes
			elseif ( CompareModelPath "\IngredHam.NIF" myitem ) || myitem == Ham
				set myidle to MOOIdleEatHam
			elseif ( CompareModelPath "\ingredServantsPie.NIF" myitem ) || myitem == HouseServantPie
				set myidle to MOOIdleEatHouseServantPie
			elseif ( CompareModelPath "\IngredLeek01.NIF" myitem ) || myitem == Leek
				set myidle to MOOIdleEatLeek
			elseif ( CompareModelPath "\Lettuce01.NIF" myitem ) || myitem == Lettuce
				set myidle to MOOIdleEatLettuce
			elseif ( CompareModelPath "\ingredLichor.NIF" myitem ) || myitem == Lichor
				set myidle to MOOIdleEatLichor
			elseif ( CompareModelPath "\ingredMutton.NIF" myitem ) || myitem == Mutton
				set myidle to MOOIdleEatMutton
			elseif ( CompareModelPath "\ingredonion.NIF" myitem ) || myitem == Onion
				set myidle to MOOIdleEatOnion
			elseif ( CompareModelPath "\orange01.NIF" myitem ) || myitem == Orange
				set myidle to MOOIdleEatOrange
			elseif ( CompareModelPath "\ingredpear.NIF" myitem ) || myitem == Pear
				set myidle to MOOIdleEatPear
			elseif ( CompareModelPath "\Potato01.NIF" myitem ) || myitem == Potato
				set myidle to MOOIdleEatPotato
			elseif ( CompareModelPath "\Pumpkin01.NIF" myitem ) || myitem == Pumpkin
				set myidle to MOOIdleEatPumpkin
			elseif ( CompareModelPath "\IngredRadish01.NIF" myitem ) || myitem == Radish
				set myidle to MOOIdleEatRadish
			elseif ( CompareModelPath "\IngredRice01.NIF" myitem ) || myitem == Rice
				set myidle to MOOIdleEatRice
			elseif ( CompareModelPath "\Strawberry01.NIF" myitem ) || myitem == Strawberry
				set myidle to MOOIdleEatStrawberry
			elseif ( CompareModelPath "\IngredSweetCake.NIF" myitem ) || myitem == Sweetcake
				set myidle to MOOIdleEatSweetcake
			elseif ( CompareModelPath "\IngredSweetRoll.NIF" myitem ) || myitem == Sweetroll
				set myidle to MOOIdleEatSweetroll
			elseif ( CompareModelPath "\IngredTobacco01.NIF" myitem ) || myitem == Tobacco
				set myidle to MOOIdleEatTobacco
			elseif ( CompareModelPath "\Tomatoe01.NIF" myitem ) || myitem == Tomato
				set myidle to MOOIdleEatTomato
			elseif ( CompareModelPath "\IngredVenisonSteak.NIF" myitem ) || myitem == Venison
				set myidle to MOOIdleEatVenison
			elseif ( CompareModelPath "\IngredWatermelon01.NIF" myitem ) || myitem == Watermelon
				set myidle to MOOIdleEatWatermelon
			elseif ( CompareModelPath "\IngredWheat01.NIF" myitem ) || myitem == WheatGrain
				set myidle to MOOIdleEatWheat

			elseif ( CompareModelPath "\SE04Felldew.NIF" myitem )
				set myidle to MOOIdleEatFelldew
			elseif ( CompareModelPath "\AlocasiaFruit.NIF" myitem )
				set myidle to MOOIdleEatAlocasiaFruit
			elseif ( CompareModelPath "\SEMasterBloomCore.NIF" myitem )
				set myidle to MOOIdleEatMasterBloomCore
			elseif ( CompareModelPath "\SESmokedBaliwogLeg.NIF" myitem )
				set myidle to MOOIdleEatSmokedBaliwogLeg

			elseif ( CompareModelPath "\huntersmeat.nif" myitem ) || ( CompareModelPath "\ingredpredatorsteak.nif" myitem )
				set myidle to MOOIdleEatvenison

			else
				set myidle to MOOIdleEatGeneric
			endif

else
			if ( CompareModelPath "\BPN\Canteen.nif" myitem )
				set myidle to MOOIdleDrinkCanteen
			elseif ( CompareModelPath "\BPN\CoffeeMug.nif" myitem )
				set myidle to MOOIdleDrinkCoffeeMug

			elseif ( CompareModelPath "\Clutter\AleBottle.NIF" myitem ) || myitem == DrinkAle
				set myidle to MOOIdleDrinkAle
			elseif ( CompareModelPath "\Clutter\BeerBottle.NIF" myitem ) || myitem == DrinkBeer
				set myidle to MOOIdleDrinkBeer
			elseif ( CompareModelPath "\Clutter\MeadBottle.NIF" myitem ) || myitem == DrinkMead
				set myidle to MOOIdleDrinkMead
			elseif ( CompareModelPath "\Clutter\winebottle01.NIF" myitem ) || myitem == DrinkWine0Cheap
				set myidle to MOOIdleDrinkWineBottle01
			elseif ( CompareModelPath "\Clutter\winebottle02.NIF" myitem ) || myitem == DrinkWine1SurilieGood || myitem == DrinkWine3SurilieBetter || myitem == DrinkWine5SurilieBest
				set myidle to MOOIdleDrinkWineBottle02
			elseif ( CompareModelPath "\Clutter\winebottle03.NIF" myitem ) || myitem == DrinkWine2TamikaGood || myitem == DrinkWine4TamikaBetter || myitem == DrinkWine6TamikaBest
				set myidle to MOOIdleDrinkWineBottle03
			else
				set myidle to MOOIdleDrinkGeneric
			endif
endif



SetFunctionValue myidle

End