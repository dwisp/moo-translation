Maskar's Oblivion Overhaul.esp
0x2C6754
MOOInitContainerRockFunctionScript
Scn MOOInitContainerRockFunctionScript

ref me
short metalfound

ref base
float myscale

ref placedrock
ref myrock
short seed
short gemcode

short seed1
short seed2
short seed3
short seed4
short seed5

short direction

Begin Function { me metalfound }

if Call MOOInitContainerRockExistsFunctionScript me
	return
endif

if MOO.ini_convert_rocks_mods == 0
if MOO.cellseed > 16777215 && GetPlayerInSEWorld == 0
	return
endif
endif

set base to me.GetBaseObject

set seed to ( ( ( me.GetPos x ) + ( me.GetPos y ) + ( me.GetPos z ) ) / 70 ) + MOO.cellseed + MOO.gameseed

if metalfound ; there's a gold or silver vein nearby
	if MOO.gameseed
		let seed := seed & 31
	else
		let seed := seed & 63
	endif
else
	let seed := seed & 127
endif

if seed > 31
	set direction to 1
endif

if seed == 1 || seed == 60
	set gemcode to 1
elseif seed == 3 || seed == 58
	set gemcode to 2
elseif seed == 5 || seed == 56
	set gemcode to 3
elseif seed == 7 || seed == 54
	set gemcode to 4
elseif seed == 9 || seed == 52
	set gemcode to 5
elseif seed == 32 || seed == 33 || seed == 34
	set gemcode to 6
elseif seed == 22 || seed == 28 || seed == 44 || seed == 46
	set gemcode to 7
endif

; no veins in very small rocks
if me.GetScale < 1
	set gemcode to 0
endif

if eval MOO.cellseed & 1 == 1
	set seed1 to 1
endif
if eval MOO.cellseed & 2 == 2
	set seed2 to 1
endif
if eval MOO.cellseed & 4 == 4
	set seed3 to 1
endif
if eval MOO.cellseed & 8 == 8
	set seed4 to 1
endif
if eval MOO.cellseed & 16 == 16
	set seed5 to 1
endif


if direction
	if seed1 == 0 && gemcode == 1
		set gemcode to 2
	endif
	if seed2 == 0 && gemcode == 2
		set gemcode to 3
	endif
	if seed3 == 0 && gemcode == 3
		set gemcode to 4
	endif
	if seed4 == 0 && gemcode == 4
		set gemcode to 5
	endif
	if seed5 == 0 && gemcode == 5
		set gemcode to 6
	endif
else
	if seed5 == 0 && gemcode == 5
		set gemcode to 4
	endif
	if seed4 == 0 && gemcode == 4
		set gemcode to 3
	endif
	if seed3 == 0 && gemcode == 3
		set gemcode to 2
	endif
	if seed2 == 0 && gemcode == 2
		set gemcode to 1
	endif
	if seed1 == 0 && gemcode == 1
		set gemcode to 6
	endif
endif


; less gems near cities
if MOO.safetyzone == 1
if direction
	if gemcode >= 2 && gemcode <= 5
		set gemcode to 6

	elseif gemcode == 1
		set gemcode to 7
	endif
endif	
endif


if gemcode
	if MOO.ini_debug
		printc "[MOO] Vein added with seed: %.0f - %.0f %.0f %.0f %.0f %.0f gemcode: %.0f" seed seed1 seed2 seed3 seed4 seed5 gemcode
	endif

	set myrock to Call MOOGetVeinFromRockFunctionScript base gemcode
endif

if myrock

	set myscale to me.GetScale
	set placedrock to me.PlaceAtMe myrock
	placedrock.SetScaleEX myscale
	me.Disable

	; add items from vein to dynamic cell loot list
	Call MOOAddItemsToCellListFunctionScript placedrock

endif

End