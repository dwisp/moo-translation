Maskar's Oblivion Overhaul.esp
0x577328
MOOEventMushroomsTreeOutsideBorderPlaceMushroomFunctionScript
Scn MOOEventMushroomsTreeOutsideBorderPlaceMushroomFunctionScript

ref mytarget
ref me1
ref me2
ref me3
ref me4
ref me5
ref me6

short myseed

float refx
float refy
float myx
float myy

ref myplant

Begin Function { mytarget me1 me2 me3 me4 me5 me6 }

; PLACE 1 PLANT AT 10/15 chance

; PICK RANDOM LOCATION AROUND TREE
set myx to Rand -400 400
if myx > -150 && myx < 150
	if GetRandomPercent < 50
		set myy to Rand -400 -150
	else
		set myy to Rand 150 400
	endif
else
	set myy to Rand -400 400
endif


set refx to myx + mytarget.GetPos x
set refy to myy + mytarget.GetPos y



; GET RANDOM ( 1 - 15 )

set myseed to Call MOOEventMushroomGetSeedFunctionscript mytarget

if myseed == 0
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me1 32 0 0 0 0
elseif myseed == 1
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me2 32 0 0 0 0
elseif myseed == 2
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me3 32 0 0 0 0
elseif myseed == 3
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me4 32  0 0 0 0
elseif myseed == 4
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me5 32  0 0 0 0
elseif myseed == 5
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me6 32  0 0 0 0
elseif myseed == 6
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant01 32 0 0 1 0
elseif myseed == 7
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant02 32 0 0 1 0
elseif myseed == 8
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant03 32 0 0 1 0
elseif myseed == 9
	set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant04 32 0 0 1 0
endif

SetFunctionValue myplant

End