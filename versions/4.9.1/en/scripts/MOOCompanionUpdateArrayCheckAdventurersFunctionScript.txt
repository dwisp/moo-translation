Maskar's Oblivion Overhaul.esp
0x547AD0
MOOCompanionUpdateArrayCheckAdventurersFunctionScript
Scn MOOCompanionUpdateArrayCheckAdventurersFunctionScript

ref me
short i
short mysize
short mygold

Begin Function {  }

; CREATE NEW COMPANIONS ARRAY

set MOO.totalcompanions to 0
set mysize to Ar_Size MOO.adventurers

set i to 0
while i < mysize

	; GET ADVENTURER
	let me := MOO.adventurers[i]

	if me
	if IsFormValid me
	if me.GetDead == 0
	if me.GetFactionRank MOOCompanionFaction > 0

		Call MOOCompanionUpdateArrayAddAdventurerFunctionScript me

	endif
	endif
	endif
	endif

	set i to i + 1
loop

End