Maskar's Oblivion Overhaul.esp
0x541AF2
MOOOnEquipTreasureMapFunctionScript
Scn MOOOnEquipTreasureMapFunctionScript

ref myitem

short mapnumber

float myx
float myy
float myz
ref myworldspace

Begin Function { myitem }

set mapnumber to GetObjectCharge myitem

let myx := MOO.maplistsx[mapnumber]
let myy := MOO.maplistsy[mapnumber]

printc "[MOO] Map %.0f found with coordinates ( %.0f, %.0f )." mapnumber myx myy

if player.IsInInterior == 0
	set myworldspace to player.GetParentWorldSpace
	if myworldspace == Tamriel

		set myz to GetTerrainHeight myx myy

		MOOCellMarkerTreasureMap.Enable
		Call MOOPlaceSpawnSpotFunctionScript MOOCellMarkerTreasureMap myx myy myz 1
	endif
endif

End