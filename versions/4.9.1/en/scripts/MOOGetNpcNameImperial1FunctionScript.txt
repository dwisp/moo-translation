Maskar's Oblivion Overhaul.esp
0x22FEF1
MOOGetNpcNameImperial1FunctionScript
Scn MOOGetNpcNameImperial1FunctionScript

short male

short i

string_var n1

Begin Function { male }

; male

if male
set i to Rand 0 146
if i == 0
	let n1 := "Aebondeius"
elseif i == 1
	let n1 := "Afer"
elseif i == 2
	let n1 := "Albecius"
elseif i == 3
	let n1 := "Allian"
elseif i == 4
	let n1 := "Antonius"
elseif i == 5
	let n1 := "Apelles"
elseif i == 6
	let n1 := "Ariulcabor"
elseif i == 7
	let n1 := "Arius"
elseif i == 8
	let n1 := "Arlowe"
elseif i == 9
	let n1 := "Arrinis"
elseif i == 10
	let n1 := "Astius"
elseif i == 11
	let n1 := "Attelivupis"
elseif i == 12
	let n1 := "Audenian"
elseif i == 13
	let n1 := "Augurius"
elseif i == 14
	let n1 := "Aunius"
elseif i == 15
	let n1 := "Bacola"
elseif i == 16
	let n1 := "Baro"
elseif i == 17
	let n1 := "Benunius"
elseif i == 18
	let n1 := "Burcanius"
elseif i == 19
	let n1 := "Caccian"
elseif i == 20
	let n1 := "Caius"
elseif i == 21
	let n1 := "Calvario"
elseif i == 22
	let n1 := "Calvus"
elseif i == 23
	let n1 := "Canctunian"
elseif i == 24
	let n1 := "Capiton"
elseif i == 25
	let n1 := "Cassius"
elseif i == 26
	let n1 := "Carnius"
elseif i == 27
	let n1 := "Cavortius"
elseif i == 28
	let n1 := "Cidius"
elseif i == 29
	let n1 := "Cimber"
elseif i == 30
	let n1 := "Clagius"
elseif i == 31
	let n1 := "Clasomo"
elseif i == 32
	let n1 := "Clecentor"
elseif i == 33
	let n1 := "Clibergus"
elseif i == 34
	let n1 := "Cocistian"
elseif i == 35
	let n1 := "Codus"
elseif i == 36
	let n1 := "Coirtene"
elseif i == 37
	let n1 := "Constans"
elseif i == 38
	let n1 := "Corrudus"
elseif i == 39
	let n1 := "Crassius"
elseif i == 40
	let n1 := "Crito"
elseif i == 41
	let n1 := "Crottus"
elseif i == 42
	let n1 := "Crulius"
elseif i == 43
	let n1 := "Cunius"
elseif i == 44
	let n1 := "Darius"
elseif i == 45
	let n1 := "Depusanis"
elseif i == 46
	let n1 := "Drusus"
elseif i == 47
	let n1 := "Dunius"
elseif i == 48
	let n1 := "Durus"
elseif i == 49
	let n1 := "Duvianus"
elseif i == 50
	let n1 := "Ertius"
elseif i == 51
	let n1 := "Esdrufus"
elseif i == 52
	let n1 := "Falco"
elseif i == 53
	let n1 := "Falx"
elseif i == 54
	let n1 := "Fandus"
elseif i == 55
	let n1 := "Frinnius"
elseif i == 56
	let n1 := "Furius"
elseif i == 57
	let n1 := "Ganciele"
elseif i == 58
	let n1 := "Gergio"
elseif i == 59
	let n1 := "Glallian"
elseif i == 60
	let n1 := "Granius"
elseif i == 61
	let n1 := "Gratian"
elseif i == 62
	let n1 := "Gualtierus"
elseif i == 63
	let n1 := "Hasphat"
elseif i == 64
	let n1 := "Ignatius"
elseif i == 65
	let n1 := "Ildogesto"
elseif i == 66
	let n1 := "Ilnori"
elseif i == 67
	let n1 := "Iratian"
elseif i == 68
	let n1 := "Iulus"
elseif i == 69
	let n1 := "Jonus"
elseif i == 70
	let n1 := "Larisus"
elseif i == 71
	let n1 := "Larrius"
elseif i == 72
	let n1 := "Letreius"
elseif i == 73
	let n1 := "Levus"
elseif i == 74
	let n1 := "Linus"
elseif i == 75
	let n1 := "Lucan"
elseif i == 76
	let n1 := "Lucretinaus"
elseif i == 77
	let n1 := "Lurius"
elseif i == 78
	let n1 := "Luspinian"
elseif i == 79
	let n1 := "Luven"
elseif i == 80
	let n1 := "Malpenix"
elseif i == 81
	let n1 := "Manilian"
elseif i == 82
	let n1 := "Marsus"
elseif i == 83
	let n1 := "Masque"
elseif i == 84
	let n1 := "Matus"
elseif i == 85
	let n1 := "Mecilvus"
elseif i == 86
	let n1 := "Miles"
elseif i == 87
	let n1 := "Molvirian"
elseif i == 88
	let n1 := "Moris"
elseif i == 89
	let n1 := "Murberius"
elseif i == 90
	let n1 := "Murudius"
elseif i == 91
	let n1 := "Namanian"
elseif i == 92
	let n1 := "Naspis"
elseif i == 93
	let n1 := "Natalinus"
elseif i == 94
	let n1 := "Nirtunus"
elseif i == 95
	let n1 := "Nitterius"
elseif i == 96
	let n1 := "Nolus"
elseif i == 97
	let n1 := "Oritius"
elseif i == 98
	let n1 := "Percius"
elseif i == 99
	let n1 := "Pilper"
elseif i == 100
	let n1 := "Pilus"
elseif i == 101
	let n1 := "Plitinius"
elseif i == 102
	let n1 := "Processus"
elseif i == 103
	let n1 := "Procyon"
elseif i == 104
	let n1 := "Pustula"
elseif i == 105
	let n1 := "Raxle"
elseif i == 106
	let n1 := "Reberio"
elseif i == 107
	let n1 := "Rufinus"
elseif i == 108
	let n1 := "Sabinus"
elseif i == 109
	let n1 := "Saenus"
elseif i == 110
	let n1 := "Saprius"
elseif i == 111
	let n1 := "Sauleius"
elseif i == 112
	let n1 := "Scelian"
elseif i == 113
	let n1 := "Segunivus"
elseif i == 114
	let n1 := "Seliulus"
elseif i == 115
	let n1 := "Sellus"
elseif i == 116
	let n1 := "Senilias"
elseif i == 117
	let n1 := "Silius"
elseif i == 118
	let n1 := "Sirollus"
elseif i == 119
	let n1 := "Sitialius"
elseif i == 120
	let n1 := "Somutis"
elseif i == 121
	let n1 := "Stentus"
elseif i == 122
	let n1 := "Stlubo"
elseif i == 123
	let n1 := "Strillian"
elseif i == 124
	let n1 := "Surus"
elseif i == 125
	let n1 := "Synnolian"
elseif i == 126
	let n1 := "Tappius"
elseif i == 127
	let n1 := "Tasellis"
elseif i == 128
	let n1 := "Ticemius"
elseif i == 129
	let n1 := "Tienius"
elseif i == 130
	let n1 := "Tininnus"
elseif i == 131
	let n1 := "Trebonius"
elseif i == 132
	let n1 := "Turedus"
elseif i == 133
	let n1 := "Tyronius"
elseif i == 134
	let n1 := "Vala"
elseif i == 135
	let n1 := "Valgus"
elseif i == 136
	let n1 := "Valvius"
elseif i == 137
	let n1 := "Vantustius"
elseif i == 138
	let n1 := "Varian"
elseif i == 139
	let n1 := "Varus"
elseif i == 140
	let n1 := "Vasesius"
elseif i == 141
	let n1 := "Vertilvius"
elseif i == 142
	let n1 := "Vinnus"
elseif i == 143
	let n1 := "Vodunius"
elseif i == 144
	let n1 := "Wulf"
else
	let n1 := "Zeno"
endif
endif

; female

if male == 0
set i to Rand 0 67
if i == 0
	let n1 := "Adraria"
elseif i == 1
	let n1 := "Agrippina"
elseif i == 2
	let n1 := "Ahetotis"
elseif i == 3
	let n1 := "Alcedonia"
elseif i == 4
	let n1 := "Apronia"
elseif i == 5
	let n1 := "Arenara"
elseif i == 6
	let n1 := "Audania"
elseif i == 7
	let n1 := "Blatta"
elseif i == 8
	let n1 := "Cania"
elseif i == 9
	let n1 := "Canodia"
elseif i == 10
	let n1 := "Catia"
elseif i == 11
	let n1 := "Cinda"
elseif i == 12
	let n1 := "Cinia"
elseif i == 13
	let n1 := "Clilias"
elseif i == 14
	let n1 := "Coventina"
elseif i == 15
	let n1 := "Craetia"
elseif i == 16
	let n1 := "Eloe"
elseif i == 17
	let n1 := "Emelia"
elseif i == 18
	let n1 := "Eponis"
elseif i == 19
	let n1 := "Ettiene"
elseif i == 20
	let n1 := "Fallaise"
elseif i == 21
	let n1 := "Fammana"
elseif i == 22
	let n1 := "Fentula"
elseif i == 23
	let n1 := "Flacassia"
elseif i == 24
	let n1 := "Flaenia"
elseif i == 25
	let n1 := "Fralvia"
elseif i == 26
	let n1 := "Fruscia"
elseif i == 27
	let n1 := "Gaea"
elseif i == 28
	let n1 := "Germia"
elseif i == 29
	let n1 := "Horulia"
elseif i == 30
	let n1 := "Idonea"
elseif i == 31
	let n1 := "Isobel"
elseif i == 32
	let n1 := "Ladia"
elseif i == 33
	let n1 := "Lalaine"
elseif i == 34
	let n1 := "Lalatia"
elseif i == 35
	let n1 := "Larienna"
elseif i == 36
	let n1 := "Lassinia"
elseif i == 37
	let n1 := "Laurina"
elseif i == 38
	let n1 := "Leone"
elseif i == 39
	let n1 := "Livia"
elseif i == 40
	let n1 := "Lunia"
elseif i == 41
	let n1 := "Maesa"
elseif i == 42
	let n1 := "Marara"
elseif i == 43
	let n1 := "Moria"
elseif i == 44
	let n1 := "Nebia"
elseif i == 45
	let n1 := "Pallia"
elseif i == 46
	let n1 := "Pania"
elseif i == 47
	let n1 := "Pelena"
elseif i == 48
	let n1 := "Pellecia"
elseif i == 49
	let n1 := "Peregrina"
elseif i == 50
	let n1 := "Pritia"
elseif i == 51
	let n1 := "Raesa"
elseif i == 52
	let n1 := "Ruccia"
elseif i == 53
	let n1 := "Sabrina"
elseif i == 54
	let n1 := "Sarmosia"
elseif i == 55
	let n1 := "Selvia"
elseif i == 56
	let n1 := "Severa"
elseif i == 57
	let n1 := "Severia"
elseif i == 58
	let n1 := "Solea"
elseif i == 59
	let n1 := "Syloria"
elseif i == 60
	let n1 := "Thraccenia"
elseif i == 61
	let n1 := "Vala"
elseif i == 62
	let n1 := "Vianis"
elseif i == 63
	let n1 := "Viatrix"
elseif i == 64
	let n1 := "Viccia"
elseif i == 65
	let n1 := "Vilbia"
else
	let n1 := "Volrina"
endif
endif

SetFunctionValue n1

sv_Destruct n1

End