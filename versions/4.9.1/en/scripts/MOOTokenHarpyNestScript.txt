Maskar's Oblivion Overhaul.esp
0x50B1E6
MOOTokenHarpyNestScript
Scn MOOTokenHarpyNestScript

ref me

short harpytype
short init

ref harpy
ref harpylist
ref harpylistS

Begin Gamemode

set me to GetContainer
if me == 0
	return
endif

if init
	RemoveMe
	return
endif

set harpytype to Call MOOPlaceWorldSpaceReplaceRockWithHarpyNestGetHarpyFunctionScript me.GetBaseObject

;	me.PlaceAtMe MOOHarpyPale1		harpy
;	me.PlaceAtMe MOOHarpyPale2		harpy mage
;	me.PlaceAtMe MOOHarpyPale3		harpy matriarch
;
;	me.PlaceAtMe MOOHarpyDark1		harpy mage
;	me.PlaceAtMe MOOHarpyDark2		harpy matriarch
;	me.PlaceAtMe MOOHarpyDark3		harpy matriarch
;
;	me.PlaceAtMe MOOHarpy1				harpy
;	me.PlaceAtMe MOOHarpy2				harpy
;	me.PlaceAtMe MOOHarpy3				harpy mage

if harpytype == 0
	set harpy to MOOHarpyPale1
	set harpylist to MOOLL2HarpyPale
	set harpylistS to MOOLL0HarpyPaleS
elseif harpytype == 1
	set harpy to MOOHarpy1
	set harpylist to MOOLL2Harpy
	set harpylistS to MOOLL0HarpyS
else
	set harpy to MOOHarpyDark1
	set harpylist to MOOLL2HarpyDark
	set harpylistS to MOOLL0HarpyDarkS
endif


if MOO.ini_levelscaling_creature_enabled
	if MOO.safetyzone == 1
		me.PlaceAtMe harpy
	elseif MOO.safetyzone == 2
		me.PlaceAtMe harpylist
	else
		me.PlaceAtMe harpylistS
		me.PlaceAtMe harpylistS
		me.PlaceAtMe harpylistS
	endif
else
	if MOO.safetyzone == 1
		me.PlaceAtMe harpy
	elseif MOO.safetyzone == 2
		me.PlaceAtMe harpylistS
		me.PlaceAtMe harpylistS
	else
		me.PlaceAtMe harpylistS
		me.PlaceAtMe harpylistS
		me.PlaceAtMe harpylistS
	endif
endif

set init to 1

End