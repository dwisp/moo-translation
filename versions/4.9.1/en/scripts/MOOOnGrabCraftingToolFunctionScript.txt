Maskar's Oblivion Overhaul.esp
0x36820F
MOOOnGrabCraftingToolFunctionScript
Scn MOOOnGrabCraftingToolFunctionScript

ref mytarget

short craftid
ref me
ref mydeed
float myx
float myy
float myz

Begin Function { mytarget }

set craftid to Call MOOCraftGetCraftIdFunctionScript mytarget.GetBaseObject
let mydeed := MOO.toolsdeed[craftid]

if mydeed == 0
	return
endif

if mytarget.GetScript == MOOTrapPressurePlateScript
	if mytarget.GetDestroyed
		message "You try to pick up the item, but it falls apart in your hands."
		PlaySound UIArmorWeaponRepairBreak
		mytarget.Disable
	else
		message "The trap is live and can no longer be picked up!"
	endif
	return
endif

; house
if mytarget.CompareModelPath "MOO\Crafting\House\"
	message "You can't move a house!"
	return
endif

; GIVE BACK DEED
PlaySound CSkinLarge
player.AddItem mydeed 1

; furniture only
if mytarget.CompareModelPath "MOO\Crafting\Large\" == 0
	; for normal furniture
	if mytarget.IsContainer
		mytarget.RemoveAllItems playerref
	endif
	mytarget.Disable
	return
endif


set myx to mytarget.GetPos x
set myy to mytarget.GetPos y
set myz to mytarget.GetPos z

set me to GetFirstRef 23 1 ; container
while me
	if me.GetDisabled == 0
	if me.CompareModelPath "MOO\Crafting\Large\"
	if ( myx == me.GetPos x ) && ( myy == me.GetPos y ) && ( myz == me.GetPos z )

		me.RemoveAllItems playerref
;;;		me.MoveTo MOOCellUnusedMarker
		me.Disable

	endif
	endif
	endif

	set me to GetNextRef
loop

set me to GetFirstRef 18 1 ; activator
while me
	if me.GetDisabled == 0
	if me.CompareModelPath "MOO\Crafting\Large\"
	if ( myx == me.GetPos x ) && ( myy == me.GetPos y ) && ( myz == me.GetPos z )

		me.MoveTo MOOCellMarker
		me.Disable

	endif
	endif
	endif

	set me to GetNextRef
loop

End