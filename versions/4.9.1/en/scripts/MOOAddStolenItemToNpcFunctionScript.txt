Maskar's Oblivion Overhaul.esp
0x26F407
MOOAddStolenItemToNpcFunctionScript
Scn MOOAddStolenItemToNpcFunctionScript

ref me
ref stolenitem

Begin Function { me stolenitem }

if GetRandomPercent < 3
if ( me.HasSpell MOOAbFactionTokenSpell ) || ( me.GetInFaction BanditFaction ) || ( me.GetInFaction MarauderFaction )

	me.AddItemNS stolenitem 1

	if MOO.ini_debug
		printc "[MOO] Added %n (stolen) to %n." stolenitem me
	endif

	let MOO.queststatusregions[MOO.myregion] := 1
	let MOO.questtargetrefregions[MOO.myregion] := me.GetParentCell ; cell as ref
	set stolenitem to 0

endif
endif

End