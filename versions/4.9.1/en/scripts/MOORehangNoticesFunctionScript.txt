Maskar's Oblivion Overhaul.esp
0x20AC63
MOORehangNoticesFunctionScript
Scn MOORehangNoticesFunctionScript

ref noticeboard
ref notice1
ref notice2
ref notice3

short boardcode
float boardheight

float myrand
float boardmin
float boardmax

Begin Function { noticeboard notice1 notice2 notice3 }

set boardcode to Call MOOGetBoardCodeFunctionScript noticeboard
set boardheight to noticeboard.GetPos z

set MOO.rehangnoticeboards to MOO.rehangnoticeboards - boardcode

if eval MOO.updatenoticeboards & boardcode != boardcode
	set MOO.updatenoticeboards to MOO.updatenoticeboards + boardcode
endif

set boardmin to boardheight + 25
set boardmax to boardheight + 50

set myrand to Rand boardmin boardmax
notice1.setpos z myrand
set myrand to Rand 175 185
notice1.setangle z myrand

set myrand to Rand boardmin boardmax
notice2.setpos z myrand
set myrand to Rand 175 185
notice2.setangle z myrand

set myrand to Rand boardmin boardmax
notice3.setpos z myrand
set myrand to Rand 175 185
notice3.setangle z myrand

End