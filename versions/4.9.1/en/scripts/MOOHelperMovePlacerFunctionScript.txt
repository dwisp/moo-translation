Maskar's Oblivion Overhaul.esp
0x517A93
MOOHelperMovePlacerFunctionScript
Scn MOOHelperMovePlacerFunctionScript

ref myplacer
float myx
float myy
float myz
float myanglez

short turns

float placeranglez

Begin Function { myplacer myx myy myz myanglez }

; MOVE PLACER
set placeranglez to myplacer.GetAngle z

set turns to placeranglez / 90
while turns
	if myx == 0
		if myy == 0
			; do nothing
		elseif myy > 0
			set myx to myy
			set myy to 0
		else
			set myx to myy
			set myy to 0
		endif

	elseif myx > 0
		if myy == 0
			set myy to myx * -1
			set myx to 0
		elseif myy > 0
			set myy to myy * -1
		else
			set myx to myx * -1
		endif

	else
		if myy == 0
			set myy to myx * -1
			set myx to 0
		elseif myy > 0
			set myx to myx * -1
		else
			set myy to myy * -1
		endif
	endif
	set turns to turns - 1
loop


set myx to myx + myplacer.GetPos x
set myy to myy + myplacer.GetPos y
set myz to myz + myplacer.GetPos z

set myanglez to myanglez + placeranglez
myplacer.SetPos x myx
myplacer.SetPos y myy
myplacer.SetPos z myz
myplacer.SetAngle z myanglez

End