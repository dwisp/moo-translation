Maskar's Oblivion Overhaul.esp
0x0231BA
MOOOnAttackByPlayerFunctionScript
Scn MOOOnAttackByPlayerFunctionScript

ref attacker
ref myspell

Begin Function { attacker }

if player.IsCasting
	set myspell to GetPlayerSpell

	if myspell == MOOSpellFeignDeath
		Call MOOFeignDeathFunctionScript
	endif

endif

End