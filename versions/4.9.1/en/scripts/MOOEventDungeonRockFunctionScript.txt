Maskar's Oblivion Overhaul.esp
0x50736B
MOOEventDungeonRockFunctionScript
Scn MOOEventDungeonRockFunctionScript

ref myplacer

ref mycell
ref me

short isplaced

Begin Function { myplacer }

; CYCLE THROUGH NEARBY ROCKS
MOOCellPlacerCreature.MoveTo myplacer
set mycell to MOOCellPlacerCreature.GetParentCell
if mycell == 0
	return
endif

set me to GetFirstRefInCell mycell 28

while me

	if Call MOOEventDungeonRockFoundFunctionScript me.GetBaseObject

		set isplaced to Call MOOPlaceWorldSpaceReplaceRockWithDungeonFunctionScript me

		if isplaced
;			Call MOOPlaceObjectOnTerrainClearTerrainWithRadiusFunctionScript myplacer 500
			break
		endif
	endif

	set me to GetNextRef
loop

SetFunctionValue isplaced

End


