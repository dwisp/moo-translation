Maskar's Oblivion Overhaul.esp
0x478592
MOOHealFixScript
Scn MOOHealFixScript

ref me
ref target
short iscaster

ref mycombattarget
ref myAI

Begin Function { me target iscaster }

; CASTER HEALS
if iscaster == -1

	if target.GetCurrentAIProcedure == 24 ; do nothing
		if target.IsInCombat
			set mycombattarget to target.GetCombatTarget
			target.Evp
			if mycombattarget
				target.StartCombat mycombattarget
			endif
		else
			target.Evp
		endif
	endif

; CASTER FAILS TO HEAL
else
	me.Evp
endif

; FIX COMPANION AI
if MOOHeal.myspell
if ( IsClonedForm me ) && ( me.GetInFaction MOOCompanionFaction )
	me.Evp
	Call MOOResetAdventurerAIFunctionScript me
endif
endif

End