Maskar's Oblivion Overhaul.esp
0x37EC92
MOOCraftGetCraftTypeFunctionScript
Scn MOOCraftGetCraftTypeFunctionScript

ref myitem
short i ; start
short j ; end

short mytype

Begin _Function { myitem i j }

while i < j
	if eval CompareModelPath $MOO.craftingfilename[i] myitem
		let mytype := MOO.craftingtype[i]
	endif
	set i to i + 1
loop

SetFunctionValue mytype

End