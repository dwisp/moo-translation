Maskar's Oblivion Overhaul.esp
0x55FAEB
MOOCompanionUpdateActionsFindRefNeededContainerCheckContentsFunctionScript
Scn MOOCompanionUpdateActionsFindRefNeededContainerCheckContentsFunctionScript

ref myactor
ref mycontainer

ref myitem

ref qualifyingcontainer

Begin Function { myactor mycontainer }

foreach myitem <- mycontainer
	if Call MOOCompanionUpdateActionsFindRefNeededCheckItemFunctionScript myactor myitem
		set qualifyingcontainer to mycontainer
		break
	endif
loop

SetFunctionValue qualifyingcontainer

End