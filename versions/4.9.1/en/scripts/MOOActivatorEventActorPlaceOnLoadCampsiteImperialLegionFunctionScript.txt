Maskar's Oblivion Overhaul.esp
0x559B16
MOOActivatorEventActorPlaceOnLoadCampsiteImperialLegionFunctionScript
Scn MOOActivatorEventActorPlaceOnLoadCampsiteImperialLegionFunctionScript

ref me
short mytype ; 0 forester
short myseed

ref myboss
ref mygrunt

Begin Function { me mytype myseed }

set myboss to MOOLL0ImperialLegionForester
set mygrunt to MOOLL0ImperialLegionForester


; BOSS
if me.GetBaseObject == MOOMarkerEventActorCampsiteBoss
	me.PlaceAtMe myboss

; GRUNT
else
	me.PlaceAtMe mygrunt
endif

End