Maskar's Oblivion Overhaul.esp
0x1305BA
MOOObjectWarglaiveBackScript
scn MOOObjectWarglaiveBackScript

ref me
short doOnce

Begin MenuMode 1002

	if ( doOnce == 2 )
		RemoveMe
		Return
	endif

	Set me to GetContainer
	if me == 0
		return
	endif

	if ( me.GetEquipped MOOWeapWarglaiveBack == 1 )
		if ( me.GetEquipped MOOArmorWarglaiveShield != 1 )
			me.EquipItemNS MOOWeapWarglaive
			Set doOnce to 2
		else
			me.EquipItemNS MOOWeapWarglaive
			me.EquipItemNS MOOArmorWarglaiveShield
			Set doOnce to 2
		endif
	else
		RemoveMe
	endif

End

Begin GameMode

	if ( doOnce == 2 )
		RemoveMe
		Return
	endif

	Set me to GetContainer
	if me == 0
		return
	endif

	if ( me.GetEquipped MOOWeapWarglaiveBack == 1 )
		if ( me.GetEquipped MOOArmorWarglaiveShield != 1 )
			me.EquipItemNS MOOWeapWarglaive
			Set doOnce to 2
		elseif ( me.IsWeaponOut == 1 )
			me.EquipItemNS MOOWeapWarglaive
			me.EquipItemNS MOOArmorWarglaiveShield
			Set doOnce to 2
		endif
	else
		RemoveMe
	endif

End