Maskar's Oblivion Overhaul.esp
0x42C36F
MOOTrapDeleteDisabledTrapsFunctionScript
Scn MOOTrapDeleteDisabledTrapsFunctionScript

ref me

Begin Function {  }

set me to GetFirstRef 18 4 ; activator
while me

	if me.GetDisabled
	if me.GetScript == MOOTrapPressurePlateScript
		me.DeleteReference
	endif
	endif

	set me to GetNextRef
loop

End