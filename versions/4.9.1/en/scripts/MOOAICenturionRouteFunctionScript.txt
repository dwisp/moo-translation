Maskar's Oblivion Overhaul.esp
0x48B9A6
MOOAICenturionRouteFunctionScript
Scn MOOAICenturionRouteFunctionScript

short enemystrength

short requiredguards

Begin Function { enemystrength }

if enemystrength >= 6
	set requiredguards to 3
elseif enemystrength >= 3
	set requiredguards to 2
elseif enemystrength >= 1
	set requiredguards to 1
endif


SetFunctionValue requiredguards

End
