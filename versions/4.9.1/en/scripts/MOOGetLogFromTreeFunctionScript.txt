Maskar's Oblivion Overhaul.esp
0x3599C0
MOOGetLogFromTreeFunctionScript
Scn MOOGetLogFromTreeFunctionScript

ref mytarget

ref mytree

Begin Function { mytarget }

if mytarget.CompareModelPath "\dtree"
	set mytree to MOOLogdtreebark01
elseif mytarget.CompareModelPath "\maniatree"
	set mytree to MOOLogmtreebark01
elseif mytarget.CompareModelPath "\treeenglishholly"
	set mytree to MOOLogshrubenglishhollybark
elseif mytarget.CompareModelPath "\treeblacklocust"
	set mytree to MOOLogtreeblacklocustbark
elseif mytarget.CompareModelPath "\treecamoranparadise"
	set mytree to MOOLogtreecamoranparadisebark
elseif mytarget.CompareModelPath "\treecottonwood"
	set mytree to MOOLogtreecottonwoodbark
elseif mytarget.CompareModelPath "\treecpginkgo"
	set mytree to MOOLogtreecpginkgobark
elseif mytarget.CompareModelPath "\treecpsnowgum"
	set mytree to MOOLogtreecpsnowgumbark
elseif mytarget.CompareModelPath "\treecpswampcypress"
	set mytree to MOOLogtreecpswampcypressbark
elseif mytarget.CompareModelPath "\treedeodar"
	set mytree to MOOLogtreedeodarbark
elseif mytarget.CompareModelPath "\treedogwood"
	set mytree to MOOLogtreedogwoodbark
elseif mytarget.CompareModelPath "\treeeasthemlock"
	set mytree to MOOLogtreeeasthemlockbark
elseif mytarget.CompareModelPath "\treeenglishoak"
	if mytarget.CompareModelPath "fa.spt"
		set mytree to MOOLogtreeenglishoakbarklichen
	else
		set mytree to MOOLogtreeenglishoakbark
	endif
elseif mytarget.CompareModelPath "\treegcsycamore"
	set mytree to MOOLogtreegcsycamorebark
elseif mytarget.CompareModelPath "\treeginkgo"
	set mytree to MOOLogtreeginkgobark
elseif mytarget.CompareModelPath "\treegreypoplar"
	set mytree to MOOLogtreegreypoplarbark
elseif mytarget.CompareModelPath "\treeironwood"
	set mytree to MOOLogtreeironwoodbark
elseif mytarget.CompareModelPath "\treejuniper"
	set mytree to MOOLogtreejuniperbark
elseif mytarget.CompareModelPath "\treequakingaspen"
	set mytree to MOOLogtreequakingaspenbark
elseif mytarget.CompareModelPath "\treeredwoodlarge"
	set mytree to MOOLogtreeredwoodbark
elseif mytarget.CompareModelPath "\treescotchpine"
	set mytree to MOOLogtreescotchpinebark
elseif mytarget.CompareModelPath "\treesilverbirch"
	set mytree to MOOLogtreesilverbirchbark
elseif mytarget.CompareModelPath "\treesnowgum"
	set mytree to MOOLogtreesnowgumbark
elseif mytarget.CompareModelPath "\treejapanesemaple" || mytarget.CompareModelPath "\treesugarmaple"
	set mytree to MOOLogtreesugarmaplebark
elseif mytarget.CompareModelPath "\treeswampcypressforestfg08"
	set mytree to MOOLogtreeswampcypressbarkfg08
elseif mytarget.CompareModelPath "\treeswampcypress"
	set mytree to MOOLogtreeswampcypressbark
elseif mytarget.CompareModelPath "\treeweepingwillow"
	set mytree to MOOLogtreeweepingwillowbark
elseif mytarget.CompareModelPath "\treewhitepine"
	if mytarget.CompareModelPath "fa.spt"
		set mytree to MOOLogtreewhitepinebarklichen				
	else
		set mytree to MOOLogtreewhitepinebark
	endif
elseif mytarget.CompareModelPath "\treewillowoak"
	set mytree to MOOLogtreewillowoakbark
elseif mytarget.CompareModelPath "\treeyew"
	set mytree to MOOLogtreeyewbark
endif

if mytarget.CompareModelPath "small.spt" || mytarget.CompareModelPath "youngfa.spt" || mytarget.CompareModelPath "youngsu.spt" || mytarget.CompareModelPath "young.spt"
	set mytree to 0
endif

SetFunctionValue mytree

End