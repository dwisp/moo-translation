Maskar's Oblivion Overhaul.esp
0x593E99
MOOGetGameLoadedMagicEffectsFunctionScript
Scn MOOGetGameLoadedMagicEffectsFunctionScript

ref mysound

Begin Function {  }

set mysound to MOOSoundEffect
SetMagicEffectCastingSound mysound SEFF
SetMagicEffectBoltSound mysound SEFF
SetMagicEffectHitSound mysound SEFF
SetMagicEffectAreaSound mysound SEFF
set mysound to MOOLightNone
SetMagicEffectLight mysound SEFF

if MOO.ini_magic_summonzombie
	set mysound to MOOLL2RegionSummonZombie1
	SetMagicEffectUsedObject mysound ZZOM
	set mysound to MOOLL2RegionSummonZombie2
	SetMagicEffectUsedObject mysound ZHDZ
endif

if MOO.ini_magic_summonskeleton
	set mysound to MOOLL2RegionSummonSkeleton1
	SetMagicEffectUsedObject mysound ZSKE
	set mysound to MOOLL2RegionSummonSkeleton2
	SetMagicEffectUsedObject mysound ZSKA
	set mysound to MOOLL2RegionSummonSkeleton3
	SetMagicEffectUsedObject mysound ZSKH
	set mysound to MOOLL2RegionSummonSkeleton4
	SetMagicEffectUsedObject mysound ZSKC
endif

if MOO.ini_magic_summondremora
	set mysound to MOOLL0SummonDremora1Caitiff
	SetMagicEffectUsedObject mysound ZDRE

	set mysound to MOOLL0SummonDremora5Markynaz
	SetMagicEffectUsedObject mysound ZDRL
endif

if MOO.ini_magic_summonscamp
	set mysound to MOOLL0SummonScamp
	SetMagicEffectUsedObject mysound ZSCA
endif
if MOO.ini_magic_summonclannfear
	set mysound to MOOLL0SummonClannfear
	SetMagicEffectUsedObject mysound ZCLA
endif
if MOO.ini_magic_summondaedroth
	set mysound to MOOLL0SummonDaedroth
	SetMagicEffectUsedObject mysound ZDAE
endif

if MOO.ini_magic_summonbear
	set mysound to MOOLL2RegionSummonBear
	SetMagicEffectUsedObject mysound Z005
endif

; skeleton shaders
if MOO.ini_skeleton_shaders
	RemoveAllEffectItems ShaderSkeletonChampion
	RemoveAllEffectItems ShaderSkeletonGuardian
	RemoveAllEffectItems ShaderSkeletonHero
	CopyAllEffectItems MOOShaderSkeletonChampion ShaderSkeletonChampion
	CopyAllEffectItems MOOShaderSkeletonGuardian ShaderSkeletonGuardian
	CopyAllEffectItems MOOShaderSkeletonHero ShaderSkeletonHero
endif

End