Maskar's Oblivion Overhaul.esp
0x3BE18F
MOOGetHandScytheTypeFunctionScript
Scn MOOGetHandScytheTypeFunctionScript

ref me

ref myweapon

short type

Begin Function { me }

set myweapon to me.GetEquippedObject 16
if myweapon
if CompareModelPath "\handscythe" myweapon
	set type to 1
endif
endif

SetFunctionValue type

End