Maskar's Oblivion Overhaul.esp
0x4C83F0
MOOOnEquipGetBroomTypeFunctionScript
Scn MOOOnEquipGetBroomTypeFunctionScript

ref myitem

short type

Begin Function { myitem }

if GetObjectType myitem == 27 ; misc
	if CompareModelPath "\broomfarm01.nif" myitem
		set type to 1
	elseif CompareModelPath "\broomlower01.nif" myitem
		set type to 2
	endif
endif

SetFunctionValue type

End