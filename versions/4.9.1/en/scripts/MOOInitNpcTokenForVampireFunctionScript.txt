Maskar's Oblivion Overhaul.esp
0x4BBDD3
MOOInitNpcTokenForVampireFunctionScript
Scn MOOInitNpcTokenForVampireFunctionScript

ref me

Begin Function { me }

if me.GetItemCount MOOTokenVampire == 0

	me.AddItemNS MOOTokenVampire 1

	if me.GetAV Confidence >= 100 ; boss

		if me.GetFactionRank MOOVampireFaction == 1 ; ancient boss
			me.AddSpellNS MOOAbHealth5Master ; 400
		else ; normal boss
			me.AddSpellNS MOOAbHealth2Apprentice ; 160
		endif

	else ; grunt

		if me.GetFactionRank MOOVampireFaction == 1 ; ancient grunt
			me.AddSpellNS MOOAbHealth3Journeyman ; 240
		else ; normal grunt
			me.AddSpellNS MOOAbHealth1Novice ; 80
		endif

	endif
endif

End