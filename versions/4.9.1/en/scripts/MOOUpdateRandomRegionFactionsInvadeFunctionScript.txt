Maskar's Oblivion Overhaul.esp
0x3D8AC5
MOOUpdateRandomRegionFactionsInvadeFunctionScript
Scn MOOUpdateRandomRegionFactionsInvadeFunctionScript

short myfaction
short myregion
short totalinvaders

short totallinks
short totalfactions

short i
short j

short destinationrating
short destinationregion

short myrating
short enemyrating

short regionto
short backupregion

Begin Function { myfaction myregion totalinvaders }

set destinationrating to -1000

set totallinks to ar_Size MOO.regionlinkfrom
set totalfactions to -1 + ar_Size MOO.regionalfactions

set i to 0
while i < totallinks

	let regionto := MOO.regionlinkto[i]

	if eval MOO.regioncontrol[myfaction][regionto] >= 100
		let backupregion := MOO.regionlinkto[i]

	elseif eval MOO.regionlinkfrom[i] == myregion

		; check all factions in region -> determine rating

		set enemyrating to 0
		let myrating := MOO.regioncontrol[myfaction][regionto]
		set j to 0
		while j < totalfactions

			set j to j + 1

			if myfaction != j
				if eval MOO.regioncontrol[j][regionto] > enemyrating
					let enemyrating := MOO.regioncontrol[j][regionto]
				endif
			endif
		loop

		if myrating - enemyrating > destinationrating
			set destinationrating to myrating - enemyrating
			set destinationregion to regionto
		endif

	endif

	set i to i + 1
loop


; move to destination region
; sometimes move invaders to safe region - possibly to travel through

if backupregion
if GetRandomPercent < 50
	set destinationregion to backupregion
endif
endif

if destinationregion
	let MOO.regioncontrol[myfaction][destinationregion] := MOO.regioncontrol[myfaction][destinationregion] + totalinvaders

	if MOO.ini_debug
		printc "[MOO] Regional combat: %.0f invaders of type %.0f moved to region %.0f ( rating: %.0f )." totalinvaders myfaction destinationregion destinationrating
	endif
endif

End