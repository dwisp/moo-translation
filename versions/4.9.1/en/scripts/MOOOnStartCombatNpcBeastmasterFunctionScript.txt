Maskar's Oblivion Overhaul.esp
0x42D11B
MOOOnStartCombatNpcBeastmasterFunctionScript
Scn MOOOnStartCombatNpcBeastmasterFunctionScript

ref attacker
ref target

ref me
ref ally
ref mylevel

Begin Function { attacker target }

set me to GetFirstRef 36 4 ; creatures
while me

	if me.GetDisabled == 0
	if me.GetDead == 0
	if me.IsInCombat == 0
	if me.GetInFaction PlayerFaction == 0
	if me.GetDisposition attacker > 90 && me.GetDisposition target < 50

		if me.CompareModelPath "MOO\Lizard\" || me.CompareModelPath "\Boar\" || me.CompareModelPath "\Gnarl\" || me.CompareModelPath "\Bear\"
			if ally == 0
				set ally to me
			elseif me.GetLevel > ally.GetLevel
				set ally to me
			endif

		elseif me.CompareModelPath "\MountainLion\" || me.CompareModelPath "\Wasp\" || me.CompareModelPath "\Dog\"
			if ally == 0
				set ally to me
			elseif me.GetLevel > ally.GetLevel
				set ally to me
			endif

		endif

	endif
	endif
	endif
	endif
	endif

	set me to GetNextRef
loop


if ally
	if MOO.ini_debug
		printc "[MOO] %n is called to help %n attack %n." ally attacker target
	endif

;	attacker.Cast MOOSpellWhistleNpc attacker
	attacker.PlaySound3D MOONpcWhistle

	ally.StartCombat target
endif

End