Maskar's Oblivion Overhaul.esp
0x479A14
MOOObjectBackpackStaticScript
Scn MOOObjectBackpackStaticScript

ref me

Begin OnEquip

set me to GetContainer

if me.GetItemCount MOOTokenAmulet == 0
	me.AddItemNS MOOTokenAmulet 1
endif

End


Begin OnUnequip

set me to GetContainer

me.RemoveItemNS MOOTokenAmulet 1

End