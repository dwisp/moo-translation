Maskar's Oblivion Overhaul.esp
0x556B0E
MOOOnActivateMarkerDoorFunctionScript
Scn MOOOnActivateMarkerDoorFunctionScript

ref mystatic
ref mydoor1

ref myplacer

ref mybase

float myx
float myy
float myz
float myanglez
short myrange

ref mymarker1
ref mydoor2
ref mymarker2

ref mydoor3
ref mydoor4
ref mymarker3
ref mymarker4

Begin Function { mystatic mydoor1 }

; TEMP
set myplacer to MOOCellPlacer3

set mybase to mystatic.GetBaseObject

; CHECK AVAILABLE CELLS
if MOO.currentcell >= MOO.totalcells
	return
endif

; GET MARKER1
set myrange to 200
set myx to mydoor1.GetPos x
set myy to mydoor1.GetPos y
set myz to mydoor1.GetPos z
set myanglez to mydoor1.GetAngle z
set mymarker1 to Call MOOPlaceWorldSpaceDungeonDoorMarkerFunctionScript mydoor1 myx myy myrange myanglez

; CREATE INTERIOR
Call MOOOnActivateMarkerDoorCreateInteriorFunctionScript mybase

set mydoor2 to MOO.tempref1
set mymarker2 to MOO.tempref2

; second interior door
set mydoor4 to MOO.tempref3
set mymarker4 to MOO.tempref4

; LINK DOORS
mydoor1.SetDoorTeleport mymarker2
mydoor2.SetDoorTeleport mymarker1

; SECOND ENTRANCE
if mybase == MOOStaticCraftOtherHouse02
	set mydoor3 to mydoor1.PlaceAtMe MOODoorHouse02

	myplacer.MoveTo mydoor1
	Call MOOMoveMarkerFunctionScript myplacer myplacer 232 (-650) (-90)
	set mymarker3 to myplacer.placeatme XMarker

	mydoor3.SetDoorTeleport mymarker4
	mydoor4.SetDoorTeleport mymarker3
endif

; LIGHT FIXER
mydoor2.PlaceAtMe MOODungeonCellReset

End