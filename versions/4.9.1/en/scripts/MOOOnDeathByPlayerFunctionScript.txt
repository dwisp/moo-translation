Maskar's Oblivion Overhaul.esp
0x31443C
MOOOnDeathByPlayerFunctionScript
Scn MOOOnDeathByPlayerFunctionScript

ref target
ref attacker

ref me
short i

Begin Function { target attacker }

; UPDATE RANKS
Call MOOOnDeathByPlayerRanksFunctionScript target

; MORAG TONG
if target.GetInFaction MOOMoragTongFaction
	Call MOOOnDeathByPlayerMoragTongLeaderFunctionScript target attacker
endif

; TEACH TAMED CREATURE
set i to ar_Size MOOTame.followers

while i > 0
	set i to i - 1
	let me := MOOTame.followers[i]
	if me
	if me.GetDistance player < 1000
	if me.GetItemCount MOOTokenCreatureGuarding
		Call MOOOnDeathByFollowerFunctionScript target me 0
	endif
	endif
	endif
loop

End