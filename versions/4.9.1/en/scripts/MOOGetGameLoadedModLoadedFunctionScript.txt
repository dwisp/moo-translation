Maskar's Oblivion Overhaul.esp
0x4BF477
MOOGetGameLoadedModLoadedFunctionScript
Scn MOOGetGameLoadedModLoadedFunctionScript

ref tempref
short found

Begin Function {  }

set tempref to GetFormFromMod "Oblivion.esm" "06A88D"
if tempref
if IsFormValid tempref
if tempref.GetObjectType == 28
	set found to 1
endif
endif
endif

if ( IsModLoaded "DLCShiveringIsles.esp" ) && found
	set MOO.SEloaded to 1
else
	set MOO.SEloaded to 0
	set MOO.ini_add_notice_SE to 0
endif
if ( IsModLoaded "Oblivion XP.esp" )
	set MOO.XPloaded to 1
else
	set MOO.XPloaded to 0
endif
if ( IsModLoaded "Better Cities .esp" )
	set MOO.BCloaded to 1
else
	set MOO.BCloaded to 0
endif
if ( IsModLoaded "Harvest [Flora].esp" )
	set MOO.HFloaded to 1
else
	set MOO.HFloaded to 0
endif
if ( IsModLoaded "Cobl Main.esm" )
	set MOO.COBLloaded to 1
else
	set MOO.COBLloaded to 0
endif

End