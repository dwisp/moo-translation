Maskar's Oblivion Overhaul.esp
0x382A15
MOOAddRecipiesFunctionScript
Scn MOOAddRecipiesFunctionScript

ref myobject

Begin Function {  }

if IsModLoaded $MOO.ini_object_filename == 0
	Call MOOAddRecipiesClearFunctionScript
	return
endif

set myobject to GetFormFromMod $MOO.ini_object_filename $MOO.ini_object_target_id
if myobject == 0
	Call MOOAddRecipiesClearFunctionScript
	return
endif
if IsFormValid myobject == 0
	Call MOOAddRecipiesClearFunctionScript
	return
endif

; CHECK FOR LIST
if ( GetObjectType myobject == 43 )
	set myobject to GetNthLevItem 0 myobject ; pick first from list
	if myobject == 0
		Call MOOAddRecipiesClearFunctionScript
		return
	endif
	if IsFormValid myobject == 0
		Call MOOAddRecipiesClearFunctionScript
		return
	endif
endif

let MOO.recipiesgroup[MOO.totalrecipies] := MOO.ini_object_type
let MOO.recipiesmod[MOO.totalrecipies] := GetModIndex $MOO.ini_object_filename
let MOO.recipiestarget[MOO.totalrecipies] := myobject

let MOO.recipiessource[0][MOO.totalrecipies] := MOO.ini_object_source1_id
let MOO.recipiessource[1][MOO.totalrecipies] := MOO.ini_object_source2_id
let MOO.recipiessource[2][MOO.totalrecipies] := MOO.ini_object_source3_id
let MOO.recipiessource[3][MOO.totalrecipies] := MOO.ini_object_source4_id
let MOO.recipiessourceweight[0][MOO.totalrecipies] := MOO.ini_object_source1_weight
let MOO.recipiessourceweight[1][MOO.totalrecipies] := MOO.ini_object_source2_weight
let MOO.recipiessourceweight[2][MOO.totalrecipies] := MOO.ini_object_source3_weight
let MOO.recipiessourceweight[3][MOO.totalrecipies] := MOO.ini_object_source4_weight


if MOO.ini_debug == 2
	printc "[MOO] Added: %n (type: %.0f) - %.0f %.2f / %.0f %.2f / %.0f %.2f / %.0f %.2f" myobject MOO.ini_object_type MOO.ini_object_source1_id MOO.ini_object_source1_weight MOO.ini_object_source2_id MOO.ini_object_source2_weight MOO.ini_object_source3_id MOO.ini_object_source3_weight MOO.ini_object_source4_id MOO.ini_object_source4_weight
endif

set MOO.totalrecipies to MOO.totalrecipies + 1

Call MOOAddRecipiesClearFunctionScript

End