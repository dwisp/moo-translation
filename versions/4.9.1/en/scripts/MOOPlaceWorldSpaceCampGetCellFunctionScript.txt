Maskar's Oblivion Overhaul.esp
0x4FB2A7
MOOPlaceWorldSpaceCampGetCellFunctionScript
Scn MOOPlaceWorldSpaceCampGetCellFunctionScript

short i

ref mycell

Begin Function {  }

set i to Rand 1 30
if i == 1
	set mycell to MOOCellCamp01
elseif i == 2
	set mycell to MOOCellCamp02
elseif i == 3
	set mycell to MOOCellCamp03
elseif i == 4
	set mycell to MOOCellCamp04
elseif i == 5
	set mycell to MOOCellCamp05
elseif i == 6
	set mycell to MOOCellCamp06
elseif i == 7
	set mycell to MOOCellCamp07
elseif i == 8
	set mycell to MOOCellCamp08
elseif i == 9
	set mycell to MOOCellCamp09
elseif i == 10
	set mycell to MOOCellCamp10
elseif i == 11
	set mycell to MOOCellCamp11
elseif i == 12
	set mycell to MOOCellCamp12
elseif i == 13
	set mycell to MOOCellCamp13
elseif i == 14
	set mycell to MOOCellCamp14
elseif i == 15
	set mycell to MOOCellCamp15
elseif i == 16
	set mycell to MOOCellCamp16
elseif i == 17
	set mycell to MOOCellCamp17
elseif i == 18
	set mycell to MOOCellCamp18
elseif i == 19
	set mycell to MOOCellCamp19
elseif i == 20
	set mycell to MOOCellCamp20
elseif i == 21
	set mycell to MOOCellCamp21
elseif i == 22
	set mycell to MOOCellCamp22
elseif i == 23
	set mycell to MOOCellCamp23
elseif i == 24
	set mycell to MOOCellCamp24
elseif i == 25
	set mycell to MOOCellCamp25
elseif i == 26
	set mycell to MOOCellCamp26
elseif i == 27
	set mycell to MOOCellCamp27
elseif i == 28
	set mycell to MOOCellCamp28
else
	set mycell to MOOCellCamp29
endif

SetFunctionValue mycell

End