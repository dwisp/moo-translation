Maskar's Oblivion Overhaul.esp
0x2A36B3
MOOFixFactionFunctionScript
Scn MOOFixFactionFunctionScript

ref me
ref targetfaction

ref myfaction
short i
short factionreaction1
short factionreaction2

Begin Function { me targetfaction }

let MOO.tempitem := GetFactions me

set i to ar_Size MOO.tempitem

while i > 0
	set i to i - 1

	let myfaction := MOO.tempitem[i]
	if myfaction

		if myfaction != CreatureFaction

			set factionreaction1 to GetFactionReaction CreatureFaction myfaction
			set factionreaction2 to GetFactionReaction myfaction CreatureFaction

			if factionreaction1 != factionreaction2
				if MOO.ini_debug
					printc "[MOO] Added %n to CreatureFaction (mod: %.0f)." myfaction factionreaction2
				endif
				SetFactionReaction CreatureFaction myfaction factionreaction2
			endif

		endif
	endif
loop

End