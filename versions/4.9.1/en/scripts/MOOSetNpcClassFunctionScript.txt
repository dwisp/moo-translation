Maskar's Oblivion Overhaul.esp
0x23281E
MOOSetNpcClassFunctionScript
Scn MOOSetNpcClassFunctionScript

short npctype

ref myclass

Begin Function { npctype }

;		-1 adventurer
;		0 escaped prisoner (escaped)
;		1 horse thief (stealing a horse)
;		2 murderer (killing) -> maybe bandit/marauder type leaders
;		3 thief (stealing)
;		4 vampire (attacking people in sleep)
;		5 mythical dawn agent (helping daedra)
;		6 grave robber
;		7 dark brotherhood assassin (contract killer)
;		8 skooma dealer

if npctype == -1
	set myclass to Call MOOGetNpcClassFunctionScript -1
elseif npctype == 0
	set myclass to Call MOOGetNpcClassFunctionScript -1
elseif npctype == 1
	set myclass to Call MOOGetNpcClassFunctionScript -1
elseif npctype == 2
	set myclass to Call MOOGetNpcClassFunctionScript 18 ; Heavy Armor
elseif npctype == 3
	set myclass to Call MOOGetNpcClassFunctionScript 30 ; Security
elseif npctype == 4
	set myclass to Call MOOGetNpcClassFunctionScript -1
elseif npctype == 5
	set myclass to Call MOOGetNpcClassFunctionScript -1
elseif npctype == 6
	set myclass to Call MOOGetNpcClassFunctionScript -1 ; Undead when Conjuration
elseif npctype == 7
	set myclass to Call MOOGetNpcClassFunctionScript 27 ; LightArmor
else
	set myclass to Call MOOGetNpcClassFunctionScript 19 ; Alchemy
endif


SetFunctionValue myclass

End