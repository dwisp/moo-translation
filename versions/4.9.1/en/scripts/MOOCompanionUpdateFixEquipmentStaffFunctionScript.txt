Maskar's Oblivion Overhaul.esp
0x549D0B
MOOCompanionUpdateFixEquipmentStaffFunctionScript
Scn MOOCompanionUpdateFixEquipmentStaffFunctionScript

ref me
ref myitem

ref myenchantment
float myenchantmentcost
float mychargecurrent
short myusescurrent

ref betteritem

ref unwanteditem

Begin Function { me myitem }

set myenchantment to GetEnchantment myitem
if myenchantment == 0
	set unwanteditem to myitem
else

	set myenchantmentcost to GetEnchantmentCost myenchantment
	set mychargecurrent to me.GetEquippedCurrentCharge 16

	if myenchantmentcost
		set myusescurrent to mychargecurrent / myenchantmentcost
	endif

	if myenchantmentcost == 0 || myusescurrent == 0
		set unwanteditem to myitem
	endif
endif

if unwanteditem
	set betteritem to Call MOOCompanionUpdateFixEquipmentStaffGetReplacementFunctionScript me
	if betteritem
		me.EquipItem betteritem
	else
		me.UnequipItem unwanteditem
	endif
endif

SetFunctionValue unwanteditem

End