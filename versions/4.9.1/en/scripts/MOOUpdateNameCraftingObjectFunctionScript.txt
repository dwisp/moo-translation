Maskar's Oblivion Overhaul.esp
0x3AC166
MOOUpdateNameCraftingObjectFunctionScript
Scn MOOUpdateNameCraftingObjectFunctionScript

ref me
short mytype

string_var mystring

Begin Function { me mytype }

let mystring := Call MOOInitCraftToolsNameFunctionScript mytype
SetName $mystring me

sv_Destruct mystring

End