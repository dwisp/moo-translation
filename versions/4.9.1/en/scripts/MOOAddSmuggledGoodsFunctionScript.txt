Maskar's Oblivion Overhaul.esp
0x4225D6
MOOAddSmuggledGoodsFunctionScript
Scn MOOAddSmuggledGoodsFunctionScript

ref me

short i

Begin Function { me }

if GetRandomPercent < 25
	me.AddItemNS MOOSkoomaPipe 1
endif

if GetRandomPercent < 50
	set i to Rand 0 3
	me.AddItemNS PotionSkooma i
endif
if GetRandomPercent < 50
	set i to Rand 0 3
	me.AddItemNS MOOMWMoonSugar i
endif
if GetRandomPercent < 50
	set i to Rand 0 3
	me.AddItemNS Nightshade i
endif
if GetRandomPercent < 50
	me.AddItemNS MortarPestle 1

	if GetRandomPercent < 25
		me.AddItemNS Alembic 1
	endif
	if GetRandomPercent < 25
		me.AddItemNS Calcinator 1
	endif
	if GetRandomPercent < 25
		me.AddItemNS Retort 1
	endif
endif

End