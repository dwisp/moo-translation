Maskar's Oblivion Overhaul.esp
0x272AA9
MOODiseaseAddDiseaseToPlayerFunctionScript
Scn MOODiseaseAddDiseaseToPlayerFunctionScript

ref me

short diseasecode

ref mydisease

Begin Function { me }

if GetGodMode == 0
if GetRandomPercent >= ( me.GetDistance player / MOO.ini_disease_distance ) * 100
if GetRandomPercent >= player.GetAV ResistDisease

	set mydisease to 0
	set diseasecode to me.GetItemCount MOOTokenDiseaseCode
	if diseasecode > 0 && diseasecode < 26
		let mydisease := MOO.diseases[diseasecode]
	elseif me.HasEffectShader creatureEffectZombieDread
		set mydisease to DisZombieDread
	endif

	if mydisease
		if player.HasSpell mydisease
			set mydisease to 0
		else
			messageex "You have contracted %n" mydisease
			player.AddSpellNS mydisease
			ModPCMiscStat 26 1
		endif
	endif

endif
endif
endif

SetFunctionValue mydisease

End