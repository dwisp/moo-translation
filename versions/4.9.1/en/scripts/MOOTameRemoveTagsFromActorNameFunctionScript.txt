Maskar's Oblivion Overhaul.esp
0x31E87C
MOOTameRemoveTagsFromActorNameFunctionScript
Scn MOOTameRemoveTagsFromActorNameFunctionScript

ref me

string_var myname

Begin Function { me }

let myname := Call MOOTameRemoveTagsFromNameFunctionScript me.GetName

me.SetNameEx "%z" myname

sv_Destruct myname

End