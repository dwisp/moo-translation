Maskar's Oblivion Overhaul.esp
0x48C07B
MOOAICentrionUpdateFunctionScript
Scn MOOAICentrionUpdateFunctionScript

short totalguards
short routeguards
short myroute

Begin Function { totalguards routeguards myroute }

while routeguards
	if totalguards == 1
		set MOOAI.centurion1job to myroute
	elseif totalguards == 2
		set MOOAI.centurion2job to myroute
	elseif totalguards == 3
		set MOOAI.centurion3job to myroute
	elseif totalguards == 4
		set MOOAI.centurion4job to myroute
	elseif totalguards == 5
		set MOOAI.centurion5job to myroute
	elseif totalguards == 6
		set MOOAI.centurion6job to myroute
	endif
	set totalguards to totalguards - 1
	set routeguards to routeguards - 1
loop

SetFunctionValue totalguards

End