Maskar's Oblivion Overhaul.esp
0x50736E
MOOEventMushroomsTreeFunctionScript
Scn MOOEventMushroomsTreeFunctionScript

ref myplacer

ref me
ref mycell
ref myref
float refx
float refy
float refz

float mydistance

short myseed
ref myplant

ref me1
ref me2
ref me3
ref me4
ref me5
ref me6
ref myactorplacer
short mushroomtype
short isplaced

Begin Function { myplacer }

; CHECK ALLOWED
if MOO.myregion != 15 && MOO.myregion != 22 && MOO.myregion != 11 ; valenwood ; west weald west ; skyrim
	return
elseif MOO.safetyzone <= 1
	return
endif

MOOCellPlacerCreature.MoveTo myplacer
set mycell to MOOCellPlacerCreature.GetParentCell
if mycell == 0
	return
endif

; GET MUSHROOMS
set mushroomtype to Call MOOEventMushroomGetTypeFunctionScript

if mushroomtype == 0
	set me1 to MOOFloraMushroom01
	set me2 to MOOFloraMushroom02
	set me3 to MOOFloraMushroom03
	set me4 to MOOFloraMushroom04
	set me5 to MOOFloraMushroom05
	set me6 to MOOFloraMushroom06

elseif mushroomtype == 1
	set me1 to MOOFloraMushroom01B
	set me2 to MOOFloraMushroom02B
	set me3 to MOOFloraMushroom03B
	set me4 to MOOFloraMushroom04B
	set me5 to MOOFloraMushroom05B
	set me6 to MOOFloraMushroom06B

elseif mushroomtype == 2
	set me1 to MOOFloraMushroom01C
	set me2 to MOOFloraMushroom02C
	set me3 to MOOFloraMushroom03C
	set me4 to MOOFloraMushroom04C
	set me5 to MOOFloraMushroom05C
	set me6 to MOOFloraMushroom06C

else
	set me1 to MOOFloraMushroom01D
	set me2 to MOOFloraMushroom02D
	set me3 to MOOFloraMushroom03D
	set me4 to MOOFloraMushroom04D
	set me5 to MOOFloraMushroom05D
	set me6 to MOOFloraMushroom06D

endif


; REPLACE TREE WITH MUSHROOMS
set me to GetFirstRefInCell mycell 30

while me

	if me.GetDisabled == 0
	if me.GetEditorSize < 500
	if GetRandomPercent < 50

	set refx to me.GetPos x
	set refy to me.GetPos y

	set myseed to Call MOOEventMushroomGetSeedFunctionscript me

	if myseed == 0
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me1 32 0 0 0 0
	elseif myseed == 1
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me2 32 0 0 0 0
	elseif myseed == 2
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me3 32 0 0 0 0
	elseif myseed == 3
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me4 32  0 0 0 0
	elseif myseed == 4
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me5 32  0 0 0 0
	elseif myseed == 5
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker me6 32  0 0 0 0
	elseif myseed == 6
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant01 32 0 0 1 0
	elseif myseed == 7
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant02 32 0 0 1 0
	elseif myseed == 8
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant03 32 0 0 1 0
	elseif myseed == 9
		set myplant to Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOFloraMushroomPlant04 32 0 0 1 0
	endif

	; PLACE MUSHROOM CREATURE
	if myplant
		set isplaced to 1
		Call MOOActivatorEventActorPlaceOnLoadFloraFunctionScript myplant
		me.Disable
	endif

	endif
	endif
	endif

	set me to GetNextRef
loop


; PLACE MARKER TO PLACE MUSHROOMS
if isplaced

	; PLACE MUSHROOM CREATURE PLACER
	set refx to myplacer.GetPos x
	set refy to myplacer.GetPos y
	Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOActivatorEventActorFlora 32 0 0 0 0

endif

End