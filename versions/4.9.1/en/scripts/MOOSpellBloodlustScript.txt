Maskar's Oblivion Overhaul.esp
0x42EC7C
MOOSpellBloodlustScript
Scn MOOSpellBloodlustScript

ref me

Begin ScriptEffectStart

set me to GetSelf
if ( me.GetInFaction MOOOrsiniumFaction ) && ( me.GetDead == 0 )
	StopMagicShaderVisuals MOOeffectBloodlust
	PlayMagicShaderVisuals MOOeffectBloodlust 30
	me.PlaySound3d MOOSoundBloodlust
endif

End