Maskar's Oblivion Overhaul.esp
0x2261AB
MOOGetEnWeapFunctionScript
Scn MOOGetEnWeapFunctionScript

short i

ref ench

Begin Function {  }

set i to Rand 0 127.99

if i == 0
	set ench to EnWeapAbsorbAgility08
elseif i == 1
	set ench to EnWeapAbsorbEndurance08
elseif i == 2
	set ench to EnWeapAbsorbFatigue15
elseif i == 3
	set ench to EnWeapAbsorbHealth10
elseif i == 4
	set ench to EnWeapAbsorbIntelligence08
elseif i == 5
	set ench to EnWeapAbsorbLuck08
elseif i == 6
	set ench to EnWeapAbsorbMagicka10
elseif i == 7
	set ench to EnWeapAbsorbMagicka20
elseif i == 8
	set ench to EnWeapAbsorbSpeed08
elseif i == 9
	set ench to EnWeapAbsorbStrength08
elseif i == 10
	set ench to EnWeapAbsorbWillpower08

elseif i == 11
	set ench to EnWeapArenaShimmerthorn
elseif i == 12
	set ench to EnWeapAxeofHazards
elseif i == 13
	set ench to EnWeapAxeofIcyDarkness
elseif i == 14
	set ench to EnWeapBladeofFieryLight
elseif i == 15
	set ench to EnWeapBlizzardBow
elseif i == 16
	set ench to EnWeapBoreal
elseif i == 17
	set ench to EnWeapBurden10
elseif i == 18
	set ench to EnWeapBurden20
elseif i == 19
	set ench to EnWeapBurden30
elseif i == 20
	set ench to EnWeapBurden50
elseif i == 21
	set ench to EnWeapDaggerofPacification

elseif i == 22
	set ench to EnWeapDamageAgility02
elseif i == 23
	set ench to EnWeapDamageAgility03
elseif i == 24
	set ench to EnWeapDamageEndurance02
elseif i == 25
	set ench to EnWeapDamageEndurance03
elseif i == 26
	set ench to EnWeapDamageFatigue20
elseif i == 27
	set ench to EnWeapDamageFatigue30
elseif i == 28
	set ench to EnWeapDamageHealth08
elseif i == 29
	set ench to EnWeapDamageIntelligence02
elseif i == 30
	set ench to EnWeapDamageIntelligence03
elseif i == 31
	set ench to EnWeapDamageLuck02
elseif i == 32
	set ench to EnWeapDamageLuck03
elseif i == 33
	set ench to EnWeapDamageMagicka20
elseif i == 34
	set ench to EnWeapDamageMagicka25
elseif i == 35
	set ench to EnWeapDamageMagicka30
elseif i == 36
	set ench to EnWeapDamagePersonality02
elseif i == 37
	set ench to EnWeapDamagePersonality03
elseif i == 38
	set ench to EnWeapDamageSpeed02
elseif i == 39
	set ench to EnWeapDamageSpeed03
elseif i == 40
	set ench to EnWeapDamageStrength02
elseif i == 41
	set ench to EnWeapDamageStrength03
elseif i == 42
	set ench to EnWeapDamageWillpower02
elseif i == 43
	set ench to EnWeapDamageWillpower03

elseif i == 44
	set ench to EnWeapDAMalacathVolendrung
elseif i == 45
	set ench to EnWeapDAMephalaEbonyBlade
elseif i == 46
	set ench to EnWeapDefiler
elseif i == 47
	set ench to EnWeapDemoralize25
elseif i == 48
	set ench to EnWeapDemoralize35
elseif i == 49
	set ench to EnWeapDemoralize45
elseif i == 50
	set ench to EnWeapDisintegrateArmor20
elseif i == 51
	set ench to EnWeapDisintegrateArmor40
elseif i == 52
	set ench to EnWeapDisintegrateArmor60
elseif i == 53
	set ench to EnWeapDisintegrateArmor80
elseif i == 54
	set ench to EnWeapDisintegrateWeapon40
elseif i == 55
	set ench to EnWeapDisintegrateWeapon60
elseif i == 56
	set ench to EnWeapDisintegrateWeapon80
elseif i == 57
	set ench to EnWeapDispel25
elseif i == 58
	set ench to EnWeapDragonsBow

elseif i == 59
	set ench to EnWeapDrainAgility10
elseif i == 60
	set ench to EnWeapDrainBlock10
elseif i == 61
	set ench to EnWeapDrainBlock15
elseif i == 62
	set ench to EnWeapDrainBlock20
elseif i == 63
	set ench to EnWeapDrainBlock30
elseif i == 64
	set ench to EnWeapDrainEndurance10
elseif i == 65
	set ench to EnWeapDrainFatigue30
elseif i == 66
	set ench to EnWeapDrainHealth15
elseif i == 67
	set ench to EnWeapDrainIntelligence10
elseif i == 68
	set ench to EnWeapDrainLuck10
elseif i == 69
	set ench to EnWeapDrainMagicka30
elseif i == 70
	set ench to EnWeapDrainMagicka40
elseif i == 71
	set ench to EnWeapDrainSpeed10
elseif i == 72
	set ench to EnWeapDrainStrength10
elseif i == 73
	set ench to EnWeapDrainWillpower10

elseif i == 74
	set ench to EnWeapFireDamage05Ember
elseif i == 75
	set ench to EnWeapFireDamage10Flame
elseif i == 76
	set ench to EnWeapFireDamage15Blaze
elseif i == 77
	set ench to EnWeapFireDamage20Scorch
elseif i == 78
	set ench to EnWeapFireDamage2Light
elseif i == 79
	set ench to EnWeapFireDamage3Light
elseif i == 80
	set ench to EnWeapFireDamage4Light
elseif i == 81
	set ench to EnWeapFireDamage5Light

elseif i == 82
	set ench to EnWeapFrostDamage05Shiver
elseif i == 83
	set ench to EnWeapFrostDamage10Freeze
elseif i == 84
	set ench to EnWeapFrostDamage15Polar
elseif i == 85
	set ench to EnWeapFrostDamage20Blizzard

elseif i == 86
	set ench to EnWeapGhostAxe
elseif i == 87
	set ench to EnWeapGladiatorsSword
elseif i == 88
	set ench to EnWeapHammerofUnholyTerror
elseif i == 89
	set ench to EnWeapImmolator
elseif i == 90
	set ench to EnWeapLight
elseif i == 91
	set ench to EnWeapMaceofLegion
elseif i == 92
	set ench to EnWeapMaceofSunfire
elseif i == 93
	set ench to EnWeapMagesMace
elseif i == 94
	set ench to EnWeapNakedAxe
elseif i == 95
	set ench to EnWeapParalyze1
elseif i == 96
	set ench to EnWeapParalyze2
elseif i == 97
	set ench to EnWeapParalyzeJink
elseif i == 98
	set ench to EnWeapPounder
elseif i == 99
	set ench to EnWeapRedwave01
elseif i == 100
	set ench to EnWeapRedwave05
elseif i == 101
	set ench to EnWeapRedwave10
elseif i == 102
	set ench to EnWeapRedwave15
elseif i == 103
	set ench to EnWeapRedwave20
elseif i == 104
	set ench to EnWeapRedwave25
elseif i == 105
	set ench to EnWeapRedwave30

elseif i == 106
	set ench to EnWeapShockDamage05Spark
elseif i == 107
	set ench to EnWeapShockDamage10Bolt
elseif i == 108
	set ench to EnWeapShockDamage15Lightning
elseif i == 109
	set ench to EnWeapShockDamage20Storm

elseif i == 110
	set ench to EnWeapSilence15
elseif i == 111
	set ench to EnWeapSilence20
elseif i == 112
	set ench to EnWeapSilence25

elseif i == 113
	set ench to EnWeapSoultrap05
elseif i == 114
	set ench to EnWeapSoultrap07
elseif i == 115
	set ench to EnWeapSoultrap10
elseif i == 116
	set ench to EnWeapSoultrap15

elseif i == 117
	set ench to EnWeapStormBow
elseif i == 118
	set ench to EnWeapSwordofSubmission
elseif i == 119
	set ench to EnWeapSwordofWounding
elseif i == 120
	set ench to EnWeapTempest
elseif i == 121
	set ench to EnWeapThievesDagger

elseif i == 122
	set ench to EnWeapTurnundead20
elseif i == 123
	set ench to EnWeapTurnundead25
elseif i == 124
	set ench to EnWeapTurnundead30
elseif i == 125
	set ench to EnWeapTurnundead35
elseif i == 126
	set ench to EnWeapVampiresFang
else
	set ench to EnWeapVoltag
endif

SetFunctionValue ench

End