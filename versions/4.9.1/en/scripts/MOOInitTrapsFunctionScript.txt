Maskar's Oblivion Overhaul.esp
0x071C3D
MOOInitTrapsFunctionScript
Scn MOOInitTrapsFunctionScript

ref nullref

Begin Function {  }

let MOO.traps[0] := nullref
let MOO.traps[1] := MOOTrapFire01
let MOO.traps[2] := MOOTrapFrost01
let MOO.traps[3] := MOOTrapShock01
let MOO.traps[4] := MOOTrapPoison01
let MOO.traps[5] := MOOTrapFire02
let MOO.traps[6] := MOOTrapFrost02
let MOO.traps[7] := MOOTrapShock02
let MOO.traps[8] := MOOTrapPoison02
let MOO.traps[9] := MOOTrapFire03
let MOO.traps[10] := MOOTrapFrost03
let MOO.traps[11] := MOOTrapShock03
let MOO.traps[12] := MOOTrapPoison03
let MOO.traps[13] := MOOTrapFire04
let MOO.traps[14] := MOOTrapFrost04
let MOO.traps[15] := MOOTrapShock04
let MOO.traps[16] := MOOTrapPoison04

let MOO.trapspells[0] := nullref
let MOO.trapspells[1] := MOOSpellTrapFire01
let MOO.trapspells[2] := MOOSpellTrapFrost01
let MOO.trapspells[3] := MOOSpellTrapShock01
let MOO.trapspells[4] := MOOSpellTrapPoison01
let MOO.trapspells[5] := MOOSpellTrapFire02
let MOO.trapspells[6] := MOOSpellTrapFrost02
let MOO.trapspells[7] := MOOSpellTrapShock02
let MOO.trapspells[8] := MOOSpellTrapPoison02
let MOO.trapspells[9] := MOOSpellTrapFire03
let MOO.trapspells[10] := MOOSpellTrapFrost03
let MOO.trapspells[11] := MOOSpellTrapShock03
let MOO.trapspells[12] := MOOSpellTrapPoison03
let MOO.trapspells[13] := MOOSpellTrapFire04
let MOO.trapspells[14] := MOOSpellTrapFrost04
let MOO.trapspells[15] := MOOSpellTrapShock04
let MOO.trapspells[16] := MOOSpellTrapPoison04

let MOO.trappotions[0] := nullref
let MOO.trappotions[1] := MOOPotionFire1
let MOO.trappotions[2] := MOOPotionFrost1
let MOO.trappotions[3] := MOOPotionShock1
let MOO.trappotions[4] := MOOPotionToxic1
let MOO.trappotions[5] := MOOPotionFire2
let MOO.trappotions[6] := MOOPotionFrost2
let MOO.trappotions[7] := MOOPotionShock2
let MOO.trappotions[8] := MOOPotionToxic2
let MOO.trappotions[9] := MOOPotionFire3
let MOO.trappotions[10] := MOOPotionFrost3
let MOO.trappotions[11] := MOOPotionShock3
let MOO.trappotions[12] := MOOPotionToxic3
let MOO.trappotions[13] := MOOPotionFire4
let MOO.trappotions[14] := MOOPotionFrost4
let MOO.trappotions[15] := MOOPotionShock4
let MOO.trappotions[16] := MOOPotionToxic4

let MOO.trappressureplate[0] := nullref
let MOO.trappressureplate[1] := MOOPressurePlateFire01
let MOO.trappressureplate[2] := MOOPressurePlateFrost01
let MOO.trappressureplate[3] := MOOPressurePlateShock01
let MOO.trappressureplate[4] := MOOPressurePlatePoison01
let MOO.trappressureplate[5] := MOOPressurePlateFire02
let MOO.trappressureplate[6] := MOOPressurePlateFrost02
let MOO.trappressureplate[7] := MOOPressurePlateShock02
let MOO.trappressureplate[8] := MOOPressurePlatePoison02
let MOO.trappressureplate[9] := MOOPressurePlateFire03
let MOO.trappressureplate[10] := MOOPressurePlateFrost03
let MOO.trappressureplate[11] := MOOPressurePlateShock03
let MOO.trappressureplate[12] := MOOPressurePlatePoison03
let MOO.trappressureplate[13] := MOOPressurePlateFire04
let MOO.trappressureplate[14] := MOOPressurePlateFrost04
let MOO.trappressureplate[15] := MOOPressurePlateShock04
let MOO.trappressureplate[16] := MOOPressurePlatePoison04

End