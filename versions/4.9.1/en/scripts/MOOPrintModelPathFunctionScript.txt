Maskar's Oblivion Overhaul.esp
0x191DA6
MOOPrintModelPathFunctionScript
Scn MOOPrintModelPathFunctionScript

ref myitem

short myid
string_var mystring

Begin Function { myitem }

printc "-------------------------------------------------------------"

printc "[MOO] object: %n" myitem

; MOD
set myid to GetSourceModIndex myitem
let mystring := GetNthModName myid
if myid == 255
	let mystring := "Created ingame"
endif	
printc "[MOO] mod: %z" mystring

; FORMID
let mystring := GetFormIDString myitem
sv_Erase mystring 0 2
printc "[MOO] formid: %z" mystring

; FILEPATH
if ( IsArmor myitem ) || ( IsClothing myitem )
	let mystring := GetBipedModelPath 0 myitem
else
	let mystring := GetModelPath myitem
endif
printc "[MOO] filename: %z" mystring

printc "-------------------------------------------------------------"

sv_Destruct mystring

End