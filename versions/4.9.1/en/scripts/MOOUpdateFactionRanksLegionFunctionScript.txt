Maskar's Oblivion Overhaul.esp
0x45F26F
MOOUpdateFactionRanksLegionFunctionScript
Scn MOOUpdateFactionRanksLegionFunctionScript

short rankmod

short currentrank
short newrank
short factionfamelegion
short kickplayer

Begin Function { rankmod }

let factionfamelegion := MOO.factionfame[0] + rankmod

if factionfamelegion < 0
	set kickplayer to 1
	let factionfamelegion := 0
elseif factionfamelegion > 2000000
	let factionfamelegion := 2000000
endif

let MOO.factionfame[0] := factionfamelegion


; PC can join legion
if factionfamelegion < MOO.ini_pc_legion_rating_rank0
	set MOO.joinlegion to 0
else
	set MOO.joinlegion to 1
endif

if player.GetFactionRank ImperialLegion < 0
	return
endif


; UPDATE FACTION RANK

set currentrank to player.GetFactionRank MOOLegionFaction

if factionfamelegion < MOO.ini_pc_legion_rating_rank0
	set newrank to 0
elseif factionfamelegion < MOO.ini_pc_legion_rating_rank1
	set newrank to 0
elseif factionfamelegion < MOO.ini_pc_legion_rating_rank2
	set newrank to 1
elseif factionfamelegion < MOO.ini_pc_legion_rating_rank3
	set newrank to 2
elseif factionfamelegion < MOO.ini_pc_legion_rating_rank4
	set newrank to 3
elseif factionfamelegion < MOO.ini_pc_legion_rating_rank5
	set newrank to 4
elseif factionfamelegion < MOO.ini_pc_legion_rating_rank6
	set newrank to 5
else
	set newrank to 6
endif


if kickplayer
	player.SetFactionRank ImperialLegion -1
	player.SetFactionRank MOOLegionFaction -1
	message "You have been removed as a member of the Imperial Legion."
	return
endif


if newrank != currentrank

	player.SetFactionRank MOOLegionFaction newrank

	if currentrank == -1
		message "You are now a member of the Imperial Legion."

		player.AddItem LegionCuirass 1
		player.AddItem LegionBoots 1
		player.AddItem LegionGauntlets 1
		player.AddItem LegionGreaves 1
		player.AddItem LegionHelmetOld 1
		player.AddItem LegionShield 1
		player.AddItem WeapSilverLongsword 1

		; reset regional faction ranks
		let MOO.factionfame[1] := 0
		let MOO.factionfame[2] := 0
		let MOO.factionfame[3] := 0
		let MOO.factionfame[4] := 0
		let MOO.factionfame[5] := 0
		let MOO.factionfame[6] := 0
		let MOO.factionfame[7] := 0
	endif
endif

End