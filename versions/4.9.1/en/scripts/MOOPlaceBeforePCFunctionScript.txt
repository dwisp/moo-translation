Maskar's Oblivion Overhaul.esp
0x27F84B
MOOPlaceBeforePCFunctionScript
Scn MOOPlaceBeforePCFunctionScript

ref myobject
float mydistance
float myheight

ref myref

ref myitem
float itemdistance
short itemheight
short anglefixed
float anglex
float angley
float anglez
float cameraz

float myposx
float myposy
float myposz

float myheight1
float myheight2
float myheight3
float myheight4

float myangle
float myangle2

float campsitex
float campsitey
float campsitez


Begin Function { myobject mydistance myheight }

set myitem to myobject
set itemdistance to mydistance
set itemheight to myheight

set myitem to MOOCellMarker.PlaceAtMe myitem

; == position ==

set myangle to ( player.getAngle z ) - cameraz
set myangle2 to ( player.getAngle z ) + 90

set campsitex to ( player.getpos x ) + ( sin myangle ) * itemdistance
set campsitey to ( player.getpos y ) + ( cos myangle ) * itemdistance

set myposy to campsitey
set myposx to campsitex - 20
set myheight1 to GetTerrainHeight myposx myposy
set myposx to campsitex + 20
set myheight2 to GetTerrainHeight myposx myposy
set myposx to campsitex
set myposy to campsitey - 20
set myheight3 to GetTerrainHeight myposx myposy
set myposy to campsitey + 20
set myheight4 to GetTerrainHeight myposx myposy

set campsitez to ( myheight1 + myheight2 + myheight3 + myheight4 ) / 4

set myposx to campsitex - ( player.getpos x )
set myposy to campsitey - ( player.getpos y )
set myposz to campsitez - ( player.getpos z ) + itemheight

myitem.MoveTo player myposx myposy myposz

; == angle ==

set myangle to myheight3 - myheight4 + anglex
myitem.SetAngle x myangle

set myangle to myheight2 - myheight1 + angley
myitem.SetAngle y myangle

set myangle to myangle2 + anglez - 90
myitem.SetAngle z myangle

SetFunctionValue myitem

End