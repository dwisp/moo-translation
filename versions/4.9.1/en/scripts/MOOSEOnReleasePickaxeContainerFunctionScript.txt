Maskar's Oblivion Overhaul.esp
0x2C4508
MOOSEOnReleasePickaxeContainerFunctionScript
Scn MOOSEOnReleasePickaxeContainerFunctionScript

ref me

ref myore

Begin Function { me }

if me == SEMadnessRockDeposit
	set myore to SEMadnessOre
elseif me == SEAmberColumn01 || me == SEAmberColumn02 || me == SEAmberRootHanging01 || me == SEAmberRootHanging02 || me == SEAmberTrunk01 || me == SEAmberTrunk02
	set myore to SEAmber	
elseif ( me.CompareModelPath "\MadnessRock" ) || ( me.CompareModelPath "XMSHarvestMadness" )
	set myore to SEMadnessOre
elseif ( me.CompareModelPath "\Amber" ) || ( me.CompareModelPath "XMSHarvestAmber" )
	set myore to SEAmber	
endif

SetFunctionValue myore

End