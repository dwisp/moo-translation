Maskar's Oblivion Overhaul.esp
0x30C176
MOOTameFeedActorFunctionScript
Scn MOOTameFeedActorFunctionScript

ref me

short creaturetype
short foodtype
ref myitem
ref myitembase
short foodcount

short success

Begin Function { me }

; type: 0 none 1 meat 2 other 3 both 4 rain water 5 gold
set creaturetype to Call MOOTameGetTamableFunctionScript me 2

if creaturetype == 5 ; feed gold
	set foodcount to me.GetLevel * 25
	if player.GetItemCount Gold001 >= foodcount
		player.RemoveItemNS Gold001 foodcount
		set success to 1
	endif

else
	foreach myitem <- playerref

		if myitem.IsIngredient
		if myitem.IsQuestItem == 0
		if myitem.GetBaseObject != MS39Nirnroot
			set myitembase to myitem.GetBaseObject
			set foodtype to Call MOOTameGetFoodTypeFunctionScript myitem
			if foodtype == 1 ; meat
				if creaturetype == 1 || creaturetype == 3
					player.RemoveItemNS myitembase 1
					set success to 1
					break
				endif
			elseif foodtype == 2 ; other
				if creaturetype == 2 || creaturetype == 3
				if myitem.IsFood
					player.RemoveItemNS myitembase 1
					set success to 1
					break
				endif
				endif
			endif
		endif
		endif
		endif

	loop
endif

SetFunctionValue success

End