Maskar's Oblivion Overhaul.esp
0x201607
MOOUpdateCreatureRegionsDaedraFunctionScript
Scn MOOUpdateCreatureRegionsDaedraFunctionScript

short i
short region
ref mapmarker
ref mygate
short totalgates

Begin Function {  }

set i to 10
while i <= 27
	if eval MOO.creatureregions[i] == 1
		let MOO.creatureregions[i] := 0
	endif
	let MOO.gateregions[i] := 0
	set i to i + 1
loop

if MOO.markersloaded == 0
if GameDaysPassed >= 2
	let MOO.oblivionmarkers := GetMapMarkers 2 11
endif
endif

set i to ar_Size MOO.oblivionmarkers

if i < 99
	set MOO.markersloaded to 0
	return
endif

if MOO.markersloaded == 0
	set MOO.markersloaded to 1
	if MOO.ini_debug
		printc "[MOO] Oblivion gate map markers loaded."
	endif
endif

while i > 0
	set i to i - 1

	let mapmarker := MOO.oblivionmarkers[i]
	if mapmarker

		set mygate to mapmarker.GetParentRef
		if mygate
		if mygate.GetDisabled == 0
		if mygate.IsOblivionGate
		if mygate.GetDestroyed == 0

			let region := Call MOOGetRegionFunctionScript mygate
			if region > 0
				let MOO.creatureregions[region] := 1 ; Daedra
				let MOO.gateregions[region] := MOO.gateregions[region] + 1
				set totalgates to totalgates + 1
			endif

		endif
		endif
		endif
		endif

	endif
loop

if MOO.ini_debug
	printc "[MOO] Total open Oblivion gates: %.0f" totalgates
endif

End