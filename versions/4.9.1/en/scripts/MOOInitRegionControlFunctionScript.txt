Maskar's Oblivion Overhaul.esp
0x3D8AC4
MOOInitRegionControlFunctionScript
Scn MOOInitRegionControlFunctionScript

Begin Function {  }

let MOO.regionalfactions[1] := MOOOrsiniumFaction
let MOO.regionalfactions[2] := MOOHammerfellFaction
let MOO.regionalfactions[3] := MOOSkyrimFaction
let MOO.regionalfactions[4] := MOOMorrowindFaction
let MOO.regionalfactions[5] := MOOBlackMarshFaction
let MOO.regionalfactions[6] := MOOElsweyrFaction
let MOO.regionalfactions[7] := MOOValenwoodFaction

let MOO.regionalfactionstart[0] := 0
let MOO.regionalfactionstart[1] := MOO.ini_npc_invasion_region_orsinium ; 10
let MOO.regionalfactionstart[2] := MOO.ini_npc_invasion_region_hammerfell ; 10
let MOO.regionalfactionstart[3] := MOO.ini_npc_invasion_region_skyrim ; 11
let MOO.regionalfactionstart[4] := MOO.ini_npc_invasion_region_morrowind ; 12
let MOO.regionalfactionstart[5] := MOO.ini_npc_invasion_region_blackmarsh ; 13
let MOO.regionalfactionstart[6] := MOO.ini_npc_invasion_region_elsweyr ; 14
let MOO.regionalfactionstart[7] := MOO.ini_npc_invasion_region_valenwood ; 15

let MOO.regionalfactiongrowth[0] := 0
let MOO.regionalfactiongrowth[1] := MOO.ini_npc_invasion_growth_orsinium
let MOO.regionalfactiongrowth[2] := MOO.ini_npc_invasion_growth_hammerfell
let MOO.regionalfactiongrowth[3] := MOO.ini_npc_invasion_growth_skyrim
let MOO.regionalfactiongrowth[4] := MOO.ini_npc_invasion_growth_morrowind
let MOO.regionalfactiongrowth[5] := MOO.ini_npc_invasion_growth_blackmarsh
let MOO.regionalfactiongrowth[6] := MOO.ini_npc_invasion_growth_elsweyr
let MOO.regionalfactiongrowth[7] := MOO.ini_npc_invasion_growth_valenwood

let MOO.regionalgruntlist[1] := MOOLL2BanditOrc
let MOO.regionalgruntlist[2] := MOOLL2BanditRedguard
let MOO.regionalgruntlist[3] := MOOLL2BanditNord
let MOO.regionalgruntlist[4] := MOOLL2BanditDunmer
let MOO.regionalgruntlist[5] := MOOLL2BanditArgonian
let MOO.regionalgruntlist[6] := MOOLL2BanditKhajiit
let MOO.regionalgruntlist[7] := MOOLL2BanditBosmer

let MOO.regionalbosslist[1] := MOOLL0BanditBossOrc
let MOO.regionalbosslist[2] := MOOLL0BanditBossRedguard
let MOO.regionalbosslist[3] := MOOLL0BanditBossNord
let MOO.regionalbosslist[4] := MOOLL0BanditBossDunmer
let MOO.regionalbosslist[5] := MOOLL0BanditBossArgonian
let MOO.regionalbosslist[6] := MOOLL0BanditBossKhajiit
let MOO.regionalbosslist[7] := MOOLL0BanditBossBosmer


; Hammerfell
let MOO.regionlinkfrom[0] := 10
let MOO.regionlinkto[0] := 11
let MOO.regionlinkfrom[1] := 10
let MOO.regionlinkto[1] := 20
let MOO.regionlinkfrom[2] := 10
let MOO.regionlinkto[2] := 22
let MOO.regionlinkfrom[3] := 10
let MOO.regionlinkto[3] := 23

; Skyrim
let MOO.regionlinkfrom[4] := 11
let MOO.regionlinkto[4] := 10
let MOO.regionlinkfrom[5] := 11
let MOO.regionlinkto[5] := 12
let MOO.regionlinkfrom[6] := 11
let MOO.regionlinkto[6] := 20
let MOO.regionlinkfrom[7] := 11
let MOO.regionlinkto[7] := 26

; Morrowind
let MOO.regionlinkfrom[8] := 12
let MOO.regionlinkto[8] := 11
let MOO.regionlinkfrom[9] := 12
let MOO.regionlinkto[9] := 13
let MOO.regionlinkfrom[10] := 12
let MOO.regionlinkto[10] := 25
let MOO.regionlinkfrom[11] := 12
let MOO.regionlinkto[11] := 26

; Black Marsh
let MOO.regionlinkfrom[12] := 13
let MOO.regionlinkto[12] := 12
let MOO.regionlinkfrom[13] := 13
let MOO.regionlinkto[13] := 14
let MOO.regionlinkfrom[14] := 13
let MOO.regionlinkto[14] := 24
let MOO.regionlinkfrom[15] := 13
let MOO.regionlinkto[15] := 25

; Elsweyr
let MOO.regionlinkfrom[16] := 14
let MOO.regionlinkto[16] := 13
let MOO.regionlinkfrom[17] := 14
let MOO.regionlinkto[17] := 15
let MOO.regionlinkfrom[18] := 14
let MOO.regionlinkto[18] := 21
let MOO.regionlinkfrom[19] := 14
let MOO.regionlinkto[19] := 22
let MOO.regionlinkfrom[20] := 14
let MOO.regionlinkto[20] := 24
let MOO.regionlinkfrom[21] := 14
let MOO.regionlinkto[21] := 25

; Valenwood
let MOO.regionlinkfrom[22] := 15
let MOO.regionlinkto[22] := 14
let MOO.regionlinkfrom[23] := 15
let MOO.regionlinkto[23] := 21
let MOO.regionlinkfrom[24] := 15
let MOO.regionlinkto[24] := 22
let MOO.regionlinkfrom[25] := 15
let MOO.regionlinkto[25] := 23

; Great Forest
let MOO.regionlinkfrom[26] := 20
let MOO.regionlinkto[26] := 10
let MOO.regionlinkfrom[27] := 20
let MOO.regionlinkto[27] := 11
let MOO.regionlinkfrom[28] := 20
let MOO.regionlinkto[28] := 21
let MOO.regionlinkfrom[29] := 20
let MOO.regionlinkto[29] := 22
let MOO.regionlinkfrom[30] := 20
let MOO.regionlinkto[30] := 26
let MOO.regionlinkfrom[31] := 20
let MOO.regionlinkto[31] := 27

; West Weald East
let MOO.regionlinkfrom[32] := 21
let MOO.regionlinkto[32] := 14
let MOO.regionlinkfrom[33] := 21
let MOO.regionlinkto[33] := 15
let MOO.regionlinkfrom[34] := 21
let MOO.regionlinkto[34] := 22
let MOO.regionlinkfrom[35] := 21
let MOO.regionlinkto[35] := 25
let MOO.regionlinkfrom[36] := 21
let MOO.regionlinkto[36] := 26
let MOO.regionlinkfrom[37] := 21
let MOO.regionlinkto[37] := 27

; West Weald West
let MOO.regionlinkfrom[38] := 22
let MOO.regionlinkto[38] := 10
let MOO.regionlinkfrom[39] := 22
let MOO.regionlinkto[39] := 14
let MOO.regionlinkfrom[40] := 22
let MOO.regionlinkto[40] := 15
let MOO.regionlinkfrom[41] := 22
let MOO.regionlinkto[41] := 20
let MOO.regionlinkfrom[42] := 22
let MOO.regionlinkto[42] := 21
let MOO.regionlinkfrom[43] := 22
let MOO.regionlinkto[43] := 23

; Gold Coast
let MOO.regionlinkfrom[44] := 23
let MOO.regionlinkto[44] := 10
let MOO.regionlinkfrom[45] := 23
let MOO.regionlinkto[45] := 15
let MOO.regionlinkfrom[46] := 23
let MOO.regionlinkto[46] := 22

; Topal Bay
let MOO.regionlinkfrom[47] := 24
let MOO.regionlinkto[47] := 13
let MOO.regionlinkfrom[48] := 24
let MOO.regionlinkto[48] := 14

; Niben Bay
let MOO.regionlinkfrom[49] := 25
let MOO.regionlinkto[49] := 12
let MOO.regionlinkfrom[50] := 25
let MOO.regionlinkto[50] := 13
let MOO.regionlinkfrom[51] := 25
let MOO.regionlinkto[51] := 14
let MOO.regionlinkfrom[52] := 25
let MOO.regionlinkto[52] := 21
let MOO.regionlinkfrom[53] := 25
let MOO.regionlinkto[53] := 26

; Nibenay Basin
let MOO.regionlinkfrom[54] := 26
let MOO.regionlinkto[54] := 11
let MOO.regionlinkfrom[55] := 26
let MOO.regionlinkto[55] := 12
let MOO.regionlinkfrom[56] := 26
let MOO.regionlinkto[56] := 20
let MOO.regionlinkfrom[57] := 26
let MOO.regionlinkto[57] := 21
let MOO.regionlinkfrom[58] := 26
let MOO.regionlinkto[58] := 25
let MOO.regionlinkfrom[59] := 26
let MOO.regionlinkto[59] := 27

; Imperial City
let MOO.regionlinkfrom[60] := 27
let MOO.regionlinkto[60] := 20
let MOO.regionlinkfrom[61] := 27
let MOO.regionlinkto[61] := 21
let MOO.regionlinkfrom[62] := 27
let MOO.regionlinkto[62] := 26

End