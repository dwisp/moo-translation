Maskar's Oblivion Overhaul.esp
0x541AF0
MOOOnEquipBookFunctionScript
Scn MOOOnEquipBookFunctionScript

ref me
ref myitem

ref leveledlist
short found

Begin Function { me myitem }

; BOOK WRITER KIT
if myitem == MOOBookKit
	Call MOOAddBookKitScript me myitem
	return
endif

; ONLY WHEN DEBUG MODE == 2
if MOO.ini_debug < 2
	return
endif

if GetSourceModIndex myitem != MOO.MOOid
	return
endif

; TREASURE MAP
if Call MOOHelperIsInListFunctionScript myitem MOOLL0Map1Plain
	set found to 1
elseif Call MOOHelperIsInListFunctionScript myitem MOOLL0Map2Expert
	set found to 1
elseif Call MOOHelperIsInListFunctionScript myitem MOOLL0Map3Adept
	set found to 1
elseif Call MOOHelperIsInListFunctionScript myitem MOOLL0Map4Clever
	set found to 1
elseif Call MOOHelperIsInListFunctionScript myitem MOOLL0Map5Devious
	set found to 1
endif

if found
	Call MOOOnEquipTreasureMapFunctionScript myitem
endif

End