Maskar's Oblivion Overhaul.esp
0x0A1275
MOOReadBookFunctionScript
Scn MOOReadBookFunctionScript

ref mybook
ref myspell

Begin Function { mybook }

set myspell to Call MOOGetSpellfromBookFunctionScript mybook
if myspell
if player.HasSpell myspell == 0
	player.AddSpellNS myspell
	message "You have learned a new spell."
endif
endif

End