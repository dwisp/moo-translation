Maskar's Oblivion Overhaul.esp
0x2E912D
MOOUpdateLockFunctionScript
Scn MOOUpdateLockFunctionScript

ref me

float mydifficulty
short myquality
short mylocationtype
short mylocationseed

Begin Function { me }

; location type - zone 1, zone 2, zone 3, owned (1-4)
; quality - low, average, high

if GetSourceModIndex me != 0
	return
endif

if MOO.ini_locks_updated == 0
	return
endif

if MOO.myregion < 10
	return
endif

set myquality to Call MOOGetQualityObjectFunctionScript me
if myquality == 0
	return
endif

set mylocationtype to Call MOOGetLocationTypeObjectFunctionScript me
set mylocationseed to Call MOOGetLocationSeedObjectFunctionScript me

if myquality == 1
	set mydifficulty to MOO.ini_locks_low
elseif myquality == 2
	set mydifficulty to MOO.ini_locks_average
	if MOO.safetyzone == 3
		set mylocationseed to mylocationseed * 1.4
	endif
else
	set mydifficulty to MOO.ini_locks_high
	if MOO.safetyzone == 2
		set mylocationseed to mylocationseed * 1.4
	elseif MOO.safetyzone == 3
		set mylocationseed to mylocationseed * 1.8
	endif
endif

if mylocationtype == 2
	set mydifficulty to mydifficulty + MOO.ini_locks_nearcity
elseif mylocationtype == 2
	set mydifficulty to mydifficulty + MOO.ini_locks_outskirts
elseif mylocationtype == 3
	set mydifficulty to mydifficulty + MOO.ini_locks_remote
elseif mylocationtype == 4 ; owned
	set mydifficulty to mydifficulty + MOO.ini_locks_owned
endif

set mydifficulty to mydifficulty - ( mylocationseed * MOO.ini_locks_variable )

if mydifficulty < 1
	set mydifficulty to 1
elseif mydifficulty > 99
	set mydifficulty to 99
endif

me.Lock mydifficulty

End