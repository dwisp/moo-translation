Maskar's Oblivion Overhaul.esp
0x241F6F
MOOSetNpcMagicIllusionFunctionScript
Scn MOOSetNpcMagicIllusionFunctionScript

ref me
short elementaltype
short creaturetype

Begin Function { me elementaltype creaturetype }

;		20: Alteration
;		21: Conjuration
;		22: Destruction
;		23: Illusion
;		24: Mysticism
;		25: Restoration

if me.GetAVC 23 == 100
	me.AddSpellNS StandardSilence5Master
	me.AddSpellNS StandardInvisibility5Master
	me.AddSpellNS StandardParalyze5Master

elseif me.GetAVC 23 >= 75
	me.AddSpellNS StandardSilence4Expert
	me.AddSpellNS StandardInvisibility4Expert
	me.AddSpellNS StandardParalyze4Expert

else
	me.AddSpellNS StandardSilence3Journeyman
	me.AddSpellNS StandardInvisibility3Journeyman
	me.AddSpellNS StandardParalyze3Journeyman

endif

;StandardSilence3Journeyman
;StandardSilence4Expert
;StandardSilence5Master

;StandardInvisibility3Journeyman
;StandardInvisibility4Expert
;StandardInvisibility5Master

;StandardParalyze3Journeyman
;StandardParalyze4Expert
;StandardParalyze5Master

End