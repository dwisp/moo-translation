Maskar's Oblivion Overhaul.esp
0x513625
MOOInitDungeonEventsFunctionScript
Scn MOOInitDungeonEventsFunctionScript

Begin Function {  }

; DUNGEON EVENT IDS
set MOO.id_dungeon_door to 1

set MOO.id_dungeon_teleporter_blue to 2
set MOO.id_dungeon_teleporter_marker to 3
set MOO.id_dungeon_teleporter_red to 4

set MOO.id_dungeon_gate to 5
set MOO.id_dungeon_lever to 6
set MOO.id_dungeon_plate to 7
set MOO.id_dungeon_actor to 8

set MOO.id_dungeon_keyhole_iron to 9
set MOO.id_dungeon_keyhole_bronze to 10
set MOO.id_dungeon_keyhole_silver to 11
set MOO.id_dungeon_keyhole_gold to 12

set MOO.id_dungeon_alcove to 13
set MOO.id_dungeon_alcove_secret to 14
set MOO.id_dungeon_empty to 15

set MOO.id_dungeon_treasure_bronze to 16
set MOO.id_dungeon_treasure_iron to 17
set MOO.id_dungeon_treasure_silver to 18
set MOO.id_dungeon_treasure_gold to 19

set MOO.id_dungeon_reserved to 20 ; currently only used for plates and keyholes


; TELEPORTERS + TARGETS
let MOO.dungeonteleporters[1] := MOODungeonTeleporterBlue01
let MOO.dungeontargets[1] := MOODungeonTeleporterTarget01

let MOO.dungeonteleporters[2] := MOODungeonTeleporterBlue02
let MOO.dungeontargets[2] := MOODungeonTeleporterTarget02

let MOO.dungeonteleporters[3] := MOODungeonTeleporterBlue03
let MOO.dungeontargets[3] := MOODungeonTeleporterTarget03

let MOO.dungeonteleporters[4] := MOODungeonTeleporterBlue04
let MOO.dungeontargets[4] := MOODungeonTeleporterTarget04

let MOO.dungeonteleporters[5] := MOODungeonTeleporterBlue05
let MOO.dungeontargets[5] := MOODungeonTeleporterTarget05

let MOO.dungeonteleporters[6] := MOODungeonTeleporterBlue06
let MOO.dungeontargets[6] := MOODungeonTeleporterTarget06

let MOO.dungeonteleporters[7] := MOODungeonTeleporterBlue07
let MOO.dungeontargets[7] := MOODungeonTeleporterTarget07

let MOO.dungeonteleporters[8] := MOODungeonTeleporterBlue08
let MOO.dungeontargets[8] := MOODungeonTeleporterTarget08

let MOO.dungeonteleporters[9] := MOODungeonTeleporterBlue09
let MOO.dungeontargets[9] := MOODungeonTeleporterTarget09

let MOO.dungeonteleporters[10] := MOODungeonTeleporterBlue10
let MOO.dungeontargets[10] := MOODungeonTeleporterTarget10


; GATES + LEVERS
let MOO.dungeongates[1] := MOODungeonGate01
let MOO.dungeonlevers[1] := MOODungeonLever01
let MOO.dungeonkeyholebronze[1] := MOODungeonKeyholeBronze01
let MOO.dungeonkeyholeiron[1] := MOODungeonKeyholeIron01
let MOO.dungeonkeyholesilver[1] := MOODungeonKeyholeSilver01
let MOO.dungeonkeyholegold[1] := MOODungeonKeyholeGold01
let MOO.dungeonplates[1] := MOODungeonPlate01
let MOO.dungeonactors[1] := MOODungeonTeleporterGreen01

let MOO.dungeongates[2] := MOODungeonGate02
let MOO.dungeonlevers[2] := MOODungeonLever02
let MOO.dungeonkeyholebronze[2] := MOODungeonKeyholeBronze02
let MOO.dungeonkeyholeiron[2] := MOODungeonKeyholeIron02
let MOO.dungeonkeyholesilver[2] := MOODungeonKeyholeSilver02
let MOO.dungeonkeyholegold[2] := MOODungeonKeyholeGold02
let MOO.dungeonplates[2] := MOODungeonPlate02
let MOO.dungeonactors[2] := MOODungeonTeleporterGreen02

let MOO.dungeongates[3] := MOODungeonGate03
let MOO.dungeonlevers[3] := MOODungeonLever03
let MOO.dungeonkeyholebronze[3] := MOODungeonKeyholeBronze03
let MOO.dungeonkeyholeiron[3] := MOODungeonKeyholeIron03
let MOO.dungeonkeyholesilver[3] := MOODungeonKeyholeSilver03
let MOO.dungeonkeyholegold[3] := MOODungeonKeyholeGold03
let MOO.dungeonplates[3] := MOODungeonPlate03
let MOO.dungeonactors[3] := MOODungeonTeleporterGreen03

let MOO.dungeongates[4] := MOODungeonGate04
let MOO.dungeonlevers[4] := MOODungeonLever04
let MOO.dungeonkeyholebronze[4] := MOODungeonKeyholeBronze04
let MOO.dungeonkeyholeiron[4] := MOODungeonKeyholeIron04
let MOO.dungeonkeyholesilver[4] := MOODungeonKeyholeSilver04
let MOO.dungeonkeyholegold[4] := MOODungeonKeyholeGold04
let MOO.dungeonplates[4] := MOODungeonPlate04
let MOO.dungeonactors[4] := MOODungeonTeleporterGreen04

let MOO.dungeongates[5] := MOODungeonGate05
let MOO.dungeonlevers[5] := MOODungeonLever05
let MOO.dungeonkeyholebronze[5] := MOODungeonKeyholeBronze05
let MOO.dungeonkeyholeiron[5] := MOODungeonKeyholeIron05
let MOO.dungeonkeyholesilver[5] := MOODungeonKeyholeSilver05
let MOO.dungeonkeyholegold[5] := MOODungeonKeyholeGold05
let MOO.dungeonplates[5] := MOODungeonPlate05
let MOO.dungeonactors[5] := MOODungeonTeleporterGreen05

let MOO.dungeongates[6] := MOODungeonGate06
let MOO.dungeonlevers[6] := MOODungeonLever06
let MOO.dungeonkeyholebronze[6] := MOODungeonKeyholeBronze06
let MOO.dungeonkeyholeiron[6] := MOODungeonKeyholeIron06
let MOO.dungeonkeyholesilver[6] := MOODungeonKeyholeSilver06
let MOO.dungeonkeyholegold[6] := MOODungeonKeyholeGold06
let MOO.dungeonplates[6] := MOODungeonPlate06
let MOO.dungeonactors[6] := MOODungeonTeleporterGreen06

let MOO.dungeongates[7] := MOODungeonGate07
let MOO.dungeonlevers[7] := MOODungeonLever07
let MOO.dungeonkeyholebronze[7] := MOODungeonKeyholeBronze07
let MOO.dungeonkeyholeiron[7] := MOODungeonKeyholeIron07
let MOO.dungeonkeyholesilver[7] := MOODungeonKeyholeSilver07
let MOO.dungeonkeyholegold[7] := MOODungeonKeyholeGold07
let MOO.dungeonplates[7] := MOODungeonPlate07
let MOO.dungeonactors[7] := MOODungeonTeleporterGreen07

let MOO.dungeongates[8] := MOODungeonGate08
let MOO.dungeonlevers[8] := MOODungeonLever08
let MOO.dungeonkeyholebronze[8] := MOODungeonKeyholeBronze08
let MOO.dungeonkeyholeiron[8] := MOODungeonKeyholeIron08
let MOO.dungeonkeyholesilver[8] := MOODungeonKeyholeSilver08
let MOO.dungeonkeyholegold[8] := MOODungeonKeyholeGold08
let MOO.dungeonplates[8] := MOODungeonPlate08
let MOO.dungeonactors[8] := MOODungeonTeleporterGreen08

let MOO.dungeongates[9] := MOODungeonGate09
let MOO.dungeonlevers[9] := MOODungeonLever09
let MOO.dungeonkeyholebronze[9] := MOODungeonKeyholeBronze09
let MOO.dungeonkeyholeiron[9] := MOODungeonKeyholeIron09
let MOO.dungeonkeyholesilver[9] := MOODungeonKeyholeSilver09
let MOO.dungeonkeyholegold[9] := MOODungeonKeyholeGold09
let MOO.dungeonplates[9] := MOODungeonPlate09
let MOO.dungeonactors[9] := MOODungeonTeleporterGreen09

let MOO.dungeongates[10] := MOODungeonGate10
let MOO.dungeonlevers[10] := MOODungeonLever10
let MOO.dungeonkeyholebronze[10] := MOODungeonKeyholeBronze10
let MOO.dungeonkeyholeiron[10] := MOODungeonKeyholeIron10
let MOO.dungeonkeyholesilver[10] := MOODungeonKeyholeSilver10
let MOO.dungeonkeyholegold[10] := MOODungeonKeyholeGold10
let MOO.dungeonplates[10] := MOODungeonPlate10
let MOO.dungeonactors[10] := MOODungeonTeleporterGreen10

let MOO.dungeongates[11] := MOODungeonGate11
let MOO.dungeonlevers[11] := MOODungeonLever11
let MOO.dungeonkeyholebronze[11] := MOODungeonKeyholeBronze11
let MOO.dungeonkeyholeiron[11] := MOODungeonKeyholeIron11
let MOO.dungeonkeyholesilver[11] := MOODungeonKeyholeSilver11
let MOO.dungeonkeyholegold[11] := MOODungeonKeyholeGold11
let MOO.dungeonplates[11] := MOODungeonPlate11
let MOO.dungeonactors[11] := MOODungeonTeleporterGreen11

let MOO.dungeongates[12] := MOODungeonGate12
let MOO.dungeonlevers[12] := MOODungeonLever12
let MOO.dungeonkeyholebronze[12] := MOODungeonKeyholeBronze12
let MOO.dungeonkeyholeiron[12] := MOODungeonKeyholeIron12
let MOO.dungeonkeyholesilver[12] := MOODungeonKeyholeSilver12
let MOO.dungeonkeyholegold[12] := MOODungeonKeyholeGold12
let MOO.dungeonplates[12] := MOODungeonPlate12
let MOO.dungeonactors[12] := MOODungeonTeleporterGreen12

let MOO.dungeongates[13] := MOODungeonGate13
let MOO.dungeonlevers[13] := MOODungeonLever13
let MOO.dungeonkeyholebronze[13] := MOODungeonKeyholeBronze13
let MOO.dungeonkeyholeiron[13] := MOODungeonKeyholeIron13
let MOO.dungeonkeyholesilver[13] := MOODungeonKeyholeSilver13
let MOO.dungeonkeyholegold[13] := MOODungeonKeyholeGold13
let MOO.dungeonplates[13] := MOODungeonPlate13
let MOO.dungeonactors[13] := MOODungeonTeleporterGreen13

let MOO.dungeongates[14] := MOODungeonGate14
let MOO.dungeonlevers[14] := MOODungeonLever14
let MOO.dungeonkeyholebronze[14] := MOODungeonKeyholeBronze14
let MOO.dungeonkeyholeiron[14] := MOODungeonKeyholeIron14
let MOO.dungeonkeyholesilver[14] := MOODungeonKeyholeSilver14
let MOO.dungeonkeyholegold[14] := MOODungeonKeyholeGold14
let MOO.dungeonplates[14] := MOODungeonPlate14
let MOO.dungeonactors[14] := MOODungeonTeleporterGreen14

let MOO.dungeongates[15] := MOODungeonGate15
let MOO.dungeonlevers[15] := MOODungeonLever15
let MOO.dungeonkeyholebronze[15] := MOODungeonKeyholeBronze15
let MOO.dungeonkeyholeiron[15] := MOODungeonKeyholeIron15
let MOO.dungeonkeyholesilver[15] := MOODungeonKeyholeSilver15
let MOO.dungeonkeyholegold[15] := MOODungeonKeyholeGold15
let MOO.dungeonplates[15] := MOODungeonPlate15
let MOO.dungeonactors[15] := MOODungeonTeleporterGreen15

End