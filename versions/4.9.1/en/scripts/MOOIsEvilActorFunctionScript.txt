Maskar's Oblivion Overhaul.esp
0x1F194B
MOOIsEvilActorFunctionScript
Scn MOOIsEvilActorFunctionScript

ref me

ref myfaction
short i

short evil

Begin Function { me }

set evil to 0

set i to me.GetNumFactions
while i > 0
	set i to i - 1
	set myfaction to me.GetNthFaction i
	if IsFactionEvil myfaction
		set evil to 1
	endif
loop

SetFunctionValue evil

End
