Maskar's Oblivion Overhaul.esp
0x2507E9
MOOOverrideNotice2InvasionFunctionScript
Scn MOOOverrideNotice2InvasionFunctionScript

ref sourcenotice
ref targetnotice

string_var mystring

Begin Function { sourcenotice targetnotice }

set mystring to GetModelPath sourcenotice
targetnotice.SetModelPath $mystring

set mystring to GetName sourcenotice
targetnotice.SetNameEx $mystring

set mystring to GetDescription sourcenotice
targetnotice.SetDescription $mystring

targetnotice.Update3d

sv_Destruct mystring

End