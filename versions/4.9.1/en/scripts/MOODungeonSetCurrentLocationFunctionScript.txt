Maskar's Oblivion Overhaul.esp
0x4C0224
MOODungeonSetCurrentLocationFunctionScript
Scn MOODungeonSetCurrentLocationFunctionScript

short mydigger
short mydirection

short myx
short myy

Begin Function { mydigger mydirection }

; GET OLD LOCATION
let myx := MOO.roomx[mydigger]
let myy := MOO.roomy[mydigger]

; GET NEW LOCATION
if mydirection == 1 ; move up
	set myy to myy + 1
elseif mydirection == 2 ; move right
	set myx to myx + 1
elseif mydirection == 4 ; move down
	set myy to myy - 1
else ; move left
	set myx to myx - 1
endif

; UPDATE MAP
Call MOODungeonUpdateMapDiggerFunctionScript 1 myx myy

; UPDATE DIGGER
let MOO.roomx[mydigger] := myx
let MOO.roomy[mydigger] := myy

End