Maskar's Oblivion Overhaul.esp
0x5757D3
MOOOnActivateCompanionShowInfoFunctionScript
Scn MOOOnActivateCompanionShowInfoFunctionScript

ref me

short mylevel
ref myclass

string_var mystring1
string_var mystring2

short myconfidence
short myresponsibility

Begin Function { me }

set mylevel to me.GetLevel
set myclass to me.GetClass
set myconfidence to me.GetAV confidence
set myresponsibility to me.GetAV responsibility


if myconfidence < 25
	let mystring1 := "cowardly"
elseif myconfidence < 50
	let mystring1 := "cautious"
elseif myconfidence < 75
	let mystring1 := "bold"
elseif myconfidence < 100
	let mystring1 := "brave"
else
	let mystring1 := "fearless"
endif

if myresponsibility < 25
	let mystring2 := "irresponsible"
elseif myresponsibility < 50
	let mystring2 := "undependable"
elseif myresponsibility < 75
	let mystring2 := "open-minded"
elseif myresponsibility < 100
	let mystring2 := "decent"
else
	let mystring2 := "trustworthy"
endif


MessageEx "%n is a %z and %z level %.0f %n." me mystring1 mystring2 mylevel myclass

sv_Destruct mystring1
sv_Destruct mystring2

End