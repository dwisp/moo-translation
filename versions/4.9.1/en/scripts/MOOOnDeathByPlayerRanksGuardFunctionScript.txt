Maskar's Oblivion Overhaul.esp
0x43E2D6
MOOOnDeathByPlayerRanksGuardFunctionScript
Scn MOOOnDeathByPlayerRanksGuardFunctionScript

ref target

short i

Begin Function { target }

; REGIONAL FACTIONS (1-7) - ONLY WHEN NOT IN IMPERIAL LEGION

if player.GetFactionRank MOOLegionFaction == -1

	; KILLING GUARD GIVES RATING TO REGIONAL FACTIONS
	set i to ar_Size MOO.factionfame
	while i > 1
		set i to i - 1

		let MOO.factionfame[i] := MOO.factionfame[i] + 5
		if eval MOO.factionfame[i] > MOO.ini_pc_invasion_rating_rank4
			let MOO.factionfame[i] := MOO.ini_pc_invasion_rating_rank4
		endif
	loop

endif


; IMPERIAL FACTION (0)
Call MOOUpdateFactionRanksLegionFunctionScript (-2500)

if MOO.ini_debug == 2
	ar_Dump MOO.factionfame
endif


End