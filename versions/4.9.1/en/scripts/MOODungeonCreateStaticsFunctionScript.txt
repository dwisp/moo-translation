Maskar's Oblivion Overhaul.esp
0x4C1D76
MOODungeonCreateStaticsFunctionScript
Scn MOODungeonCreateStaticsFunctionScript

short myx
short myy
short myz
short mapx
short mapy
short mapz
ref mystatic

short myrand
short myid

ref mycell

Begin Function { myz }

set myy to 0
while myy < 30

	set myx to 0
	while myx < 30

		let myid := MOO.dungeonmap[myx][myy]
		if myid

		set mystatic to Call MOODungeonGetTileFunctionScript myid
		if mystatic

			Call MOODungeonCreateAmbientFunctionScript mystatic myx myy myz
			Call MOODungeonCreateClutterFunctionScript mystatic myx myy myz
			Call MOODungeonCreateLightFunctionScript mystatic myx myy myz

			set mapx to myx * 512
			set mapy to myy * 512
			set mapz to myz

			MOOCellPlacer.setpos x mapx
			MOOCellPlacer.setpos y mapy
			MOOCellPlacer.setpos z mapz

			MOOCellPlacer.setangle x 0
			MOOCellPlacer.setangle y 0
			MOOCellPlacer.setangle z 0

			Call MOODungeonUpdateTileOffsetFunctionScript myx myy

			MOOCellPlacer.PlaceAtMe mystatic
		endif

		endif

		set myx to myx + 1
	loop

	set myy to myy + 1
loop

SetFunctionValue mycell

End