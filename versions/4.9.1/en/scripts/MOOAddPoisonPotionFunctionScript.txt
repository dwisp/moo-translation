Maskar's Oblivion Overhaul.esp
0x144DE0
MOOAddPoisonPotionFunctionScript
Scn MOOAddPoisonPotionFunctionScript

ref mypotion1
ref mypotion2
ref mypotion3

Begin Function { mypotion1 mypotion2 mypotion3 }

if ( GetRandomPercent < MOO.ini_trap_poison )

	if MOO.ini_trap_poison < 100
		PlaySound MOOSoundTrapPotion
	else
		PlaySound TRPTripwireSnap
	endif

	if mypotion1
		player.additemns mypotion1 1
	endif
	if mypotion2
		player.additemns mypotion2 1
	endif
	if mypotion3
		player.additemns mypotion3 1
	endif

else
	PlaySound TRPTripwireSnap
endif

set MOO.mypotion1 to 0
set MOO.mypotion2 to 0
set MOO.mypotion3 to 0

End