Maskar's Oblivion Overhaul.esp
0x262DEE
MOOTokenDiseaseShaderScript
Scn MOOTokenDiseaseShaderScript

ref me
ref shader
short i

Begin Gamemode

if i == MOONotice.i && shader
	return
endif
set i to MOONotice.i


set me to GetContainer
if me == 0
	return
endif

if me.GetDead
	me.StopMagicShaderVisuals MOOEffectDiseaseMild
	me.StopMagicShaderVisuals MOOEffectDiseaseSerious
	me.StopMagicShaderVisuals MOOEffectDiseaseAcute
	return
endif

if shader == 0
	me.StopMagicShaderVisuals MOOEffectDiseaseMild
	me.StopMagicShaderVisuals MOOEffectDiseaseSerious
	me.StopMagicShaderVisuals MOOEffectDiseaseAcute

	set me to GetSelf
	if me.GetItemCount MOOTokenDiseaseCode >= 20
		set shader to MOOEffectDiseaseAcute
	elseif me.GetItemCount MOOTokenDiseaseCode >= 12
		set shader to MOOEffectDiseaseSerious
	elseif me.GetItemCount MOOTokenDiseaseCode >= 1
		set shader to MOOEffectDiseaseMild
	endif

	me.RemoveItem MOOTokenDiseaseCheck 1
endif

if me.HasEffectShader shader == 0
	me.PlayMagicShaderVisuals shader
endif

End