Maskar's Oblivion Overhaul.esp
0x3D8AC7
MOOOnDeathFactionNpcFunctionScript
Scn MOOOnDeathFactionNpcFunctionScript

ref target
ref attacker

short totalinvaders
short i
ref myfaction
ref tempfaction
short myrank
short tempfame
short factionreaction
short j

short regionstrengthmod
short enemymod
short friendlymod

Begin Function { target attacker }

set i to -1 + ar_Size MOO.regionalfactions
while i > 0
	let myfaction := MOO.regionalfactions[i]
	if target.GetInfaction myfaction
		break
	endif
	set i to i - 1
loop

if i == 0
	return
endif

; UPDATE REGION STRENGTH
; scout (1) grunt (2) boss (3) champion (4) guard/leader (5)

set myrank to target.GetFactionRank myfaction

if myrank == 4 ; leader
	set regionstrengthmod to 0 ; unused
	set enemymod to -2500
	set friendlymod to 25
elseif MOO.ini_npc_invasion_kill_strength
	set regionstrengthmod to myrank + 1
	set enemymod to -100
	set friendlymod to myrank + 1
else
	set regionstrengthmod to -1
	set enemymod to -100
	set friendlymod to myrank + 1
endif

; LOWER REGION STRENGTH
if myrank == 4 ; leader
	set j to 0
	while j < 28
		let MOO.regioncontrol[i][j] := MOO.regioncontrol[i][j] - ( MOO.regioncontrol[i][j] * ( MOO.ini_npc_invasion_leader / 100 ) )
		set j to j + 1
	loop

	if attacker == player
		MessageEx "You have killed the %n." target
	else
		MessageEx "The %n has been killed." target
	endif

else ; normal faction npcs
	let totalinvaders := MOO.regioncontrol[i][MOO.myregion] + regionstrengthmod
	if totalinvaders < 0
		set totalinvaders to 0
	endif
	let MOO.regioncontrol[i][MOO.myregion] := totalinvaders
	if MOO.ini_debug
		printc "[MOO] Total remaining invaders %.0f type %.0f." totalinvaders i
	endif
endif

; NO RANK WHEN IN IMPERIAL LEGION
if attacker == player
if player.GetFactionRank MOOLegionFaction == -1

	; REGIONAL FACTIONS (1-7)
	set j to ar_Size MOO.factionfame
	while j > 1
		set j to j - 1

		let tempfaction := MOO.regionalfactions[j]
		let tempfame := MOO.factionfame[j]
		set factionreaction to GetFactionReaction tempfaction myfaction
		if factionreaction >= 0
			let MOO.factionfame[j] := MOO.factionfame[j] + enemymod
		else
			let MOO.factionfame[j] := MOO.factionfame[j] + friendlymod
		endif

		; can now wear faction clothing
		if eval ( tempfame < MOO.ini_pc_invasion_rating_rank0 ) && ( MOO.factionfame[j] >= MOO.ini_pc_invasion_rating_rank0 )
			messageex "Your disposition to the %n has increased." tempfaction
		endif

		if eval MOO.factionfame[i] < 0
			let MOO.factionfame[i] := 0
		elseif eval MOO.factionfame[i] > MOO.ini_pc_invasion_rating_rank4
			let MOO.factionfame[i] := MOO.ini_pc_invasion_rating_rank4
		endif
	loop
endif
endif

; make sure you're still allowed to wear any cape/cloaks
Call MOOUpdateFactionStatusFunctionScript


if MOO.ini_debug == 2
	ar_Dump MOO.factionfame
endif


; update bandit lists base on remaining amount of invaders in region
Call MOOUpdateCurrentRegionBanditsFunctionScript

; update wilderness for decrease faction npcs
Call MOOUpdateCurrentRegionWildernessFunctionScript

End