Maskar's Oblivion Overhaul.esp
0x2A8FA2
MOOAIWantedFunctionScript
Scn MOOAIWantedFunctionScript

short myregion
ref mymarker
ref mypackage

ref me
ref mycell
ref mydoor
short dungeontype

Begin Function { myregion mymarker mypackage }

if eval MOO.questtyperegions[myregion] == 2
	let me := MOO.questtargetrefregions[myregion]
	if IsFormValid me
	if me.GetDead == 0 && me.IsInCombat == 0

		set mycell to 0

		if mymarker.GetInCell MOOCell
			if me.IsInInterior
				set mycell to me.GetParentCell
			else
				set dungeontype to Call MOOAIGetDungeonTypeFunctionScript me
				set mycell to Call MOOGetCellFunctionScript myregion dungeontype
			endif

		elseif me.IsInInterior && me.GetInSameCell mymarker

			if ( GetRandomPercent < 97 ) || ( MOO.ini_wantednpc_travel == 0 ) || ( me.GetInFaction VampireFaction )
				set mycell to me.GetParentCell
			else
				set dungeontype to Call MOOAIGetDungeonTypeFunctionScript me
				set mycell to Call MOOGetCellFunctionScript myregion dungeontype
				if MOO.ini_debug
					printc "[MOO] %n is traveling to %n" me mycell
				endif
			endif

		else
			if GetRandomPercent >= 97
				set dungeontype to Call MOOAIGetDungeonTypeFunctionScript me
				set mycell to Call MOOGetCellFunctionScript myregion dungeontype
				if MOO.ini_debug
					printc "[MOO] %n is traveling to %n" me mycell
				endif
			endif
		endif

		if mycell
			set mydoor to Call MOOGetWantedNpcMarkerFunctionScript mycell
			mymarker.MoveTo mydoor
		endif

		me.AddScriptPackage mypackage

	endif
	endif
endif

End