Maskar's Oblivion Overhaul.esp
0x593E9C
MOOOnEquipImbuementToolImbueGetItemNewFunctionScript
Scn MOOOnEquipImbuementToolImbueGetItemNewFunctionScript

ref mylist
ref myitem

short i
short templevel
short mylevel

ref mynewitem

Begin Function { mylist myitem }

; GET MOST SUITABLE ITEM FROM LIST BASED ON LEVEL
set i to GetNumLevitems mylist
while i
	set i to i - 1

	set templevel to GetNthLevItemLevel i mylist
	if templevel <= player.GetLevel
	if templevel > mylevel
		set mylevel to templevel
		set mynewitem to GetNthLevItem i mylist
	endif
	endif

loop

SetFunctionValue mynewitem

End