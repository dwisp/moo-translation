Maskar's Oblivion Overhaul.esp
0x239602
MOOSetNpcAIFunctionScript
Scn MOOSetNpcAIFunctionScript

ref me
short npctype

short i

Begin Function { me npctype }

me.AddScriptPackage MOOAIWanted

; aggression
; confidence
; energy level
; responsibility

; update aggression for wanted npcs in public places
me.SetAV Aggression 100

if npctype == 4 ; vampire
	set i to 100
else
	set i to 75 + ( me.GetLevel * 0.4 ) 
	if i > 100
		set i to 100
	endif
endif

me.SetAV Confidence i

End