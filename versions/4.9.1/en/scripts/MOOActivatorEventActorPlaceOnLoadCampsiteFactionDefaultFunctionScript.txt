Maskar's Oblivion Overhaul.esp
0x559B19
MOOActivatorEventActorPlaceOnLoadCampsiteFactionDefaultFunctionScript
Scn MOOActivatorEventActorPlaceOnLoadCampsiteFactionDefaultFunctionScript

ref me
short myfactionid

ref myboss
ref mygrunt

Begin Function { me myfactionid }

; MOOOrsiniumFaction
if myfactionid == 1
	set myboss to MOOLL0BanditBossOrc
	set mygrunt to MOOLL2BanditOrc

; MOOHammerfellFaction
elseif myfactionid == 2
	set myboss to MOOLL0BanditBossRedguard
	set mygrunt to MOOLL2BanditRedguard

; MOOSkyrimFaction
elseif myfactionid == 3
	set myboss to MOOLL0BanditBossNord
	set mygrunt to MOOLL2BanditNord

; MOOMorrowindFaction
elseif myfactionid == 4
	set myboss to MOOLL0BanditBossDunmer
	set mygrunt to MOOLL2BanditDunmer

; MOOBlackMarshFaction
elseif myfactionid == 5
	set myboss to MOOLL0BanditBossArgonian
	set mygrunt to MOOLL2BanditArgonian

; MOOElsweyrFaction
elseif myfactionid == 6
	set myboss to MOOLL0BanditBossKhajiit
	set mygrunt to MOOLL2BanditKhajiit

; MOOValenwoodFaction
elseif myfactionid == 7
	set myboss to MOOLL0BanditBossBosmer
	set mygrunt to MOOLL2BanditBosmer

else
	set myboss to MOOLL0BanditBoss
	set mygrunt to MOOLL2BanditGrunt
endif


; NOT EVERY CAMPSITE HAS A BOSS
if GetRandomPercent < 65
	set myboss to mygrunt
endif


; BOSS
if me.GetBaseObject == MOOMarkerEventActorCampsiteBoss
	me.PlaceAtMe myboss

; GRUNT
else
	me.PlaceAtMe mygrunt
endif

End