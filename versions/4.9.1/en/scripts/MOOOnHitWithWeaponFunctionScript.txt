Maskar's Oblivion Overhaul.esp
0x189BE0
MOOOnHitWithWeaponFunctionScript
Scn MOOOnHitWithWeaponFunctionScript

ref me
ref myweapon

short weapontype

Begin Function { me myweapon }

if me.IsActor == 0
	return
endif

if MOO.ini_damage_flat
	set MOO.onhitweapon to myweapon
endif

if MOO.ini_weapon_slayer
	set weapontype to Call MOOSlayerGetWeaponTypeFunctionScript myweapon
	if weapontype
		Call MOOOnHitWithWeaponSlayerFunctionScript me myweapon weapontype
	endif
endif

End