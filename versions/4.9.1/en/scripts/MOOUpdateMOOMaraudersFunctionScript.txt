Maskar's Oblivion Overhaul.esp
0x43F080
MOOUpdateMOOMaraudersFunctionScript
Scn MOOUpdateMOOMaraudersFunctionScript

Begin Function {  }

Call MOOReplaceListFunctionScript LL1MarauderBattlemage100 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderBattlemage25 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderBattlemage50 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderBattlemage75 MOOLL2RegionMarauder

Call MOOReplaceListFunctionScript LL1MarauderMelee100 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderMelee25 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderMelee50 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderMelee75 MOOLL2RegionMarauder

Call MOOReplaceListFunctionScript LL1MarauderMissile100 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderMissile25 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderMissile50 MOOLL2RegionMarauder
Call MOOReplaceListFunctionScript LL1MarauderMissile75 MOOLL2RegionMarauder

Call MOOReplaceListFunctionScript LL1MarauderBossLvl100 MOOLL2RegionMarauderBoss

End