Maskar's Oblivion Overhaul.esp
0x50BF95
MOOGetGameLoadedOutpostFunctionScript
Scn MOOGetGameLoadedOutpostFunctionScript

ref nullref

Begin Function {  }

; check MOOGetGameLoadedDungeonMapFunctionScript for more arrays

if eval ( ar_Size MOO.outposttileid ) != -1
	return
endif

; BIT 2048 used for gauntlet

; NAMES

let MOO.outpostnames := ar_Construct Array
let MOO.outposttype := ar_Construct Array
let MOO.outpostdifficulty := ar_Construct Array

let MOO.outpostnames[0] := "Abandoned Outpost"
let MOO.outposttype[0] := 0
let MOO.outpostdifficulty[0] := 0

let MOO.outpostnames[1] := "Infested Outpost"
let MOO.outposttype[1] := 0
let MOO.outpostdifficulty[1] := 0

let MOO.outpostnames[2] := "Scavengers Outpost"
let MOO.outposttype[2] := 0
let MOO.outpostdifficulty[2] := 1

let MOO.outpostnames[3] := "Outpost of Predators"
let MOO.outposttype[3] := 0
let MOO.outpostdifficulty[3] := 2

let MOO.outpostnames[4] := "Forgotten Outpost"
let MOO.outposttype[4] := 1
let MOO.outpostdifficulty[4] := 0

let MOO.outpostnames[5] := "Enchanted Outpost"
let MOO.outposttype[5] := 1
let MOO.outpostdifficulty[5] := 1

let MOO.outpostnames[6] := "Outpost of Legends"
let MOO.outposttype[6] := 1
let MOO.outpostdifficulty[6] := 2

let MOO.outpostnames[7] := "Spooky Outpost"
let MOO.outposttype[7] := 2
let MOO.outpostdifficulty[7] := 0

let MOO.outpostnames[8] := "Haunted Outpost"
let MOO.outposttype[8] := 2
let MOO.outpostdifficulty[8] := 1

let MOO.outpostnames[9] := "Outpost of Horrors"
let MOO.outposttype[9] := 2
let MOO.outpostdifficulty[9] := 2


; ACTOR LISTS

let MOO.outpostactorboss := ar_Construct Array
let MOO.outpostactorgrunt := ar_Construct Array

; INIT
let MOO.outpostactorboss[0] := nullref
let MOO.outpostactorgrunt[0] := nullref

; ANIMALS

; IRON
let MOO.outpostactorboss[1] := MOOLL0GauntletAnimalIron01Boss
let MOO.outpostactorgrunt[1] := MOOLL0GauntletAnimalIron01Grunt
let MOO.outpostactorboss[2] := MOOLL0GauntletAnimalIron02Boss
let MOO.outpostactorgrunt[2] := MOOLL0GauntletAnimalIron02Grunt
let MOO.outpostactorboss[3] := MOOLL0GauntletAnimalIron03Boss
let MOO.outpostactorgrunt[3] := MOOLL0GauntletAnimalIron03Grunt
let MOO.outpostactorboss[4] := MOOLL0GauntletAnimalIron04Boss
let MOO.outpostactorgrunt[4] := MOOLL0GauntletAnimalIron04Grunt

; BRONZE
let MOO.outpostactorboss[5] := MOOLL0GauntletAnimalBronze01Boss
let MOO.outpostactorgrunt[5] := MOOLL0GauntletAnimalBronze01Grunt
let MOO.outpostactorboss[6] := MOOLL0GauntletAnimalBronze02Boss
let MOO.outpostactorgrunt[6] := MOOLL0GauntletAnimalBronze02Grunt
let MOO.outpostactorboss[7] := MOOLL0GauntletAnimalBronze03Boss
let MOO.outpostactorgrunt[7] := MOOLL0GauntletAnimalBronze03Grunt
let MOO.outpostactorboss[8] := MOOLL0GauntletAnimalBronze04Boss
let MOO.outpostactorgrunt[8] := MOOLL0GauntletAnimalBronze04Grunt

; SILVER
let MOO.outpostactorboss[9] := MOOLL0GauntletAnimalSilver01Boss
let MOO.outpostactorgrunt[9] := MOOLL0GauntletAnimalSilver01Grunt
let MOO.outpostactorboss[10] := MOOLL0GauntletAnimalSilver02Boss
let MOO.outpostactorgrunt[10] := MOOLL0GauntletAnimalSilver02Grunt
let MOO.outpostactorboss[11] := MOOLL0GauntletAnimalSilver03Boss
let MOO.outpostactorgrunt[11] := MOOLL0GauntletAnimalSilver03Grunt
let MOO.outpostactorboss[12] := MOOLL0GauntletAnimalSilver04Boss
let MOO.outpostactorgrunt[12] := MOOLL0GauntletAnimalSilver04Grunt

; GOLD
let MOO.outpostactorboss[13] := MOOLL0GauntletAnimalGold01Boss
let MOO.outpostactorgrunt[13] := MOOLL0GauntletAnimalGold01Grunt
let MOO.outpostactorboss[14] := MOOLL0GauntletAnimalGold02Boss
let MOO.outpostactorgrunt[14] := MOOLL0GauntletAnimalGold02Grunt
let MOO.outpostactorboss[15] := MOOLL0GauntletAnimalGold03Boss
let MOO.outpostactorgrunt[15] := MOOLL0GauntletAnimalGold03Grunt
let MOO.outpostactorboss[16] := MOOLL0GauntletAnimalGold04Boss
let MOO.outpostactorgrunt[16] := MOOLL0GauntletAnimalGold04Grunt


; MYTHICAL

; IRON
let MOO.outpostactorboss[17] := MOOLL0GauntletMythicalIron01Boss
let MOO.outpostactorgrunt[17] := MOOLL0GauntletMythicalIron01Grunt
let MOO.outpostactorboss[18] := MOOLL0GauntletMythicalIron02Boss
let MOO.outpostactorgrunt[18] := MOOLL0GauntletMythicalIron02Grunt
let MOO.outpostactorboss[19] := MOOLL0GauntletMythicalIron03Boss
let MOO.outpostactorgrunt[19] := MOOLL0GauntletMythicalIron03Grunt
let MOO.outpostactorboss[20] := MOOLL0GauntletMythicalIron04Boss
let MOO.outpostactorgrunt[20] := MOOLL0GauntletMythicalIron04Grunt

; BRONZE
let MOO.outpostactorboss[21] := MOOLL0GauntletMythicalBronze01Boss
let MOO.outpostactorgrunt[21] := MOOLL0GauntletMythicalBronze01Grunt
let MOO.outpostactorboss[22] := MOOLL0GauntletMythicalBronze02Boss
let MOO.outpostactorgrunt[22] := MOOLL0GauntletMythicalBronze02Grunt
let MOO.outpostactorboss[23] := MOOLL0GauntletMythicalBronze03Boss
let MOO.outpostactorgrunt[23] := MOOLL0GauntletMythicalBronze03Grunt
let MOO.outpostactorboss[24] := MOOLL0GauntletMythicalBronze04Boss
let MOO.outpostactorgrunt[24] := MOOLL0GauntletMythicalBronze04Grunt

; SILVER
let MOO.outpostactorboss[25] := MOOLL0GauntletMythicalSilver01Boss
let MOO.outpostactorgrunt[25] := MOOLL0GauntletMythicalSilver01Grunt
let MOO.outpostactorboss[26] := MOOLL0GauntletMythicalSilver02Boss
let MOO.outpostactorgrunt[26] := MOOLL0GauntletMythicalSilver02Grunt
let MOO.outpostactorboss[27] := MOOLL0GauntletMythicalSilver03Boss
let MOO.outpostactorgrunt[27] := MOOLL0GauntletMythicalSilver03Grunt
let MOO.outpostactorboss[28] := MOOLL0GauntletMythicalSilver04Boss
let MOO.outpostactorgrunt[28] := MOOLL0GauntletMythicalSilver04Grunt

; GOLD
let MOO.outpostactorboss[29] := MOOLL0GauntletMythicalGold01Boss
let MOO.outpostactorgrunt[29] := MOOLL0GauntletMythicalGold01Grunt
let MOO.outpostactorboss[30] := MOOLL0GauntletMythicalGold02Boss
let MOO.outpostactorgrunt[30] := MOOLL0GauntletMythicalGold02Grunt
let MOO.outpostactorboss[31] := MOOLL0GauntletMythicalGold03Boss
let MOO.outpostactorgrunt[31] := MOOLL0GauntletMythicalGold03Grunt
let MOO.outpostactorboss[32] := MOOLL0GauntletMythicalGold04Boss
let MOO.outpostactorgrunt[32] := MOOLL0GauntletMythicalGold04Grunt


; UNDEAD

; IRON
let MOO.outpostactorboss[33] := MOOLL0GauntletUndeadIron01Boss
let MOO.outpostactorgrunt[33] := MOOLL0GauntletUndeadIron01Grunt
let MOO.outpostactorboss[34] := MOOLL0GauntletUndeadIron02Boss
let MOO.outpostactorgrunt[34] := MOOLL0GauntletUndeadIron02Grunt
let MOO.outpostactorboss[35] := MOOLL0GauntletUndeadIron03Boss
let MOO.outpostactorgrunt[35] := MOOLL0GauntletUndeadIron03Grunt
let MOO.outpostactorboss[36] := MOOLL0GauntletUndeadIron04Boss
let MOO.outpostactorgrunt[36] := MOOLL0GauntletUndeadIron04Grunt

; BRONZE
let MOO.outpostactorboss[37] := MOOLL0GauntletUndeadBronze01Boss
let MOO.outpostactorgrunt[37] := MOOLL0GauntletUndeadBronze01Grunt
let MOO.outpostactorboss[38] := MOOLL0GauntletUndeadBronze02Boss
let MOO.outpostactorgrunt[38] := MOOLL0GauntletUndeadBronze02Grunt
let MOO.outpostactorboss[39] := MOOLL0GauntletUndeadBronze03Boss
let MOO.outpostactorgrunt[39] := MOOLL0GauntletUndeadBronze03Grunt
let MOO.outpostactorboss[40] := MOOLL0GauntletUndeadBronze04Boss
let MOO.outpostactorgrunt[40] := MOOLL0GauntletUndeadBronze04Grunt

; SILVER
let MOO.outpostactorboss[41] := MOOLL0GauntletUndeadSilver01Boss
let MOO.outpostactorgrunt[41] := MOOLL0GauntletUndeadSilver01Grunt
let MOO.outpostactorboss[42] := MOOLL0GauntletUndeadSilver02Boss
let MOO.outpostactorgrunt[42] := MOOLL0GauntletUndeadSilver02Grunt
let MOO.outpostactorboss[43] := MOOLL0GauntletUndeadSilver03Boss
let MOO.outpostactorgrunt[43] := MOOLL0GauntletUndeadSilver03Grunt
let MOO.outpostactorboss[44] := MOOLL0GauntletUndeadSilver04Boss
let MOO.outpostactorgrunt[44] := MOOLL0GauntletUndeadSilver04Grunt

; GOLD
let MOO.outpostactorboss[45] := MOOLL0GauntletUndeadGold01Boss
let MOO.outpostactorgrunt[45] := MOOLL0GauntletUndeadGold01Grunt
let MOO.outpostactorboss[46] := MOOLL0GauntletUndeadGold02Boss
let MOO.outpostactorgrunt[46] := MOOLL0GauntletUndeadGold02Grunt
let MOO.outpostactorboss[47] := MOOLL0GauntletUndeadGold03Boss
let MOO.outpostactorgrunt[47] := MOOLL0GauntletUndeadGold03Grunt
let MOO.outpostactorboss[48] := MOOLL0GauntletUndeadGold04Boss
let MOO.outpostactorgrunt[48] := MOOLL0GauntletUndeadGold04Grunt


; TILES

let MOO.outposttileid := ar_Construct Map ; key to statics
let MOO.outposttileangle := ar_Construct Map
let MOO.outposttileheight := ar_Construct Map

let MOO.outpoststatic := ar_Construct Array
let MOO.outpoststaticchance := ar_Construct Array


; TILES

; TILES: TUNNELS
; 1
let MOO.outposttileid[1] := 0
let MOO.outposttileangle[1] := 0
let MOO.outposttileheight[1] := 0
; 2
let MOO.outposttileid[2] := 0
let MOO.outposttileangle[2] := 90
let MOO.outposttileheight[2] := 0
; 4
let MOO.outposttileid[4] := 0
let MOO.outposttileangle[4] := 180
let MOO.outposttileheight[4] := 0
; 8
let MOO.outposttileid[8] := 0
let MOO.outposttileangle[8] := 270
let MOO.outposttileheight[8] := 0


; 5
let MOO.outposttileid[5] := 1
let MOO.outposttileangle[5] := 0
let MOO.outposttileheight[5] := 0
; 10
let MOO.outposttileid[10] := 1
let MOO.outposttileangle[10] := 90
let MOO.outposttileheight[10] := 0


; 3
let MOO.outposttileid[3] := 7
let MOO.outposttileangle[3] := 0
let MOO.outposttileheight[3] := 0
; 6
let MOO.outposttileid[6] := 7
let MOO.outposttileangle[6] := 90
let MOO.outposttileheight[6] := 0
; 12
let MOO.outposttileid[12] := 7
let MOO.outposttileangle[12] := 180
let MOO.outposttileheight[12] := 0
; 9
let MOO.outposttileid[9] := 7
let MOO.outposttileangle[9] := 270
let MOO.outposttileheight[9] := 0


; 7
let MOO.outposttileid[7] := 10
let MOO.outposttileangle[7] := 0
let MOO.outposttileheight[7] := 0
; 14
let MOO.outposttileid[14] := 10
let MOO.outposttileangle[14] := 90
let MOO.outposttileheight[14] := 0
; 13
let MOO.outposttileid[13] := 10
let MOO.outposttileangle[13] := 180
let MOO.outposttileheight[13] := 0
; 11
let MOO.outposttileid[11] := 10
let MOO.outposttileangle[11] := 270
let MOO.outposttileheight[11] := 0


; 15
let MOO.outposttileid[15] := 13
let MOO.outposttileangle[15] := 0
let MOO.outposttileheight[15] := 0


; TILES: ROOMS

; CORNER

; 1 << 4
let MOO.outposttileid[1 << 4] := 15
let MOO.outposttileangle[1 << 4] := 0
let MOO.outposttileheight[1 << 4] := 0

; 2 << 4
let MOO.outposttileid[2 << 4] := 15
let MOO.outposttileangle[2 << 4] := 90
let MOO.outposttileheight[2 << 4] := 0

; 4 << 4
let MOO.outposttileid[4 << 4] := 15
let MOO.outposttileangle[4 << 4] := 180
let MOO.outposttileheight[4 << 4] := 0

; 8 << 4
let MOO.outposttileid[8 << 4] := 15
let MOO.outposttileangle[8 << 4] := 270
let MOO.outposttileheight[8 << 4] := 0


; WALL

; 3 << 4
let MOO.outposttileid[3 << 4] := 18
let MOO.outposttileangle[3 << 4] := 0
let MOO.outposttileheight[3 << 4] := 0

; 6 << 4
let MOO.outposttileid[6 << 4] := 18
let MOO.outposttileangle[6 << 4] := 90
let MOO.outposttileheight[6 << 4] := 0

; 12 << 4
let MOO.outposttileid[12 << 4] := 18
let MOO.outposttileangle[12 << 4] := 180
let MOO.outposttileheight[12 << 4] := 0

; 9 << 4
let MOO.outposttileid[9 << 4] := 18
let MOO.outposttileangle[9 << 4] := 270
let MOO.outposttileheight[9 << 4] := 0


; EMPTY SPACE

; 15 << 4
let MOO.outposttileid[15 << 4] := 21
let MOO.outposttileangle[15 << 4] := 0
let MOO.outposttileheight[15 << 4] := 0


; TILES: OTHER

; TUNNEL + ROOM
; 4 + ( 3 << 4 )
let MOO.outposttileid[1 + ( 3 << 4 )] := 24
let MOO.outposttileangle[1 + ( 3 << 4 )] := 0
let MOO.outposttileheight[1 + ( 3 << 4 )] := 0

; 8 + ( 6 << 4 )
let MOO.outposttileid[2 + ( 6 << 4 )] := 24
let MOO.outposttileangle[2 + ( 6 << 4 )] := 90
let MOO.outposttileheight[2 + ( 6 << 4 )] := 0

; 1 + ( 12 << 4 )
let MOO.outposttileid[4 + ( 12 << 4 )] := 24
let MOO.outposttileangle[4 + ( 12 << 4 )] := 180
let MOO.outposttileheight[4 + ( 12 << 4 )] := 0

; 2 + ( 9 << 4 )
let MOO.outposttileid[8 + ( 9 << 4 )] := 24
let MOO.outposttileangle[8 + ( 9 << 4 )] := 270
let MOO.outposttileheight[8 + ( 9 << 4 )] := 0


; TUNNEL + DOOR
; BOTTOM -> TOP
let MOO.outposttileid[256 + 5 + ( 1 << 4 )] := 25
let MOO.outposttileangle[256 + 5 + ( 1 << 4 )] := 0
let MOO.outposttileheight[256 + 5 + ( 1 << 4 )] := 0

; LEFT -> RIGHT
let MOO.outposttileid[256 + 10 + ( 2 << 4 )] := 25
let MOO.outposttileangle[256 + 10 + ( 2 << 4 )] := 90
let MOO.outposttileheight[256 + 10 + ( 2 << 4 )] := 0

; TOP -> BOTTOM
let MOO.outposttileid[256 + 5 + ( 4 << 4 )] := 25
let MOO.outposttileangle[256 + 5 + ( 4 << 4 )] := 180
let MOO.outposttileheight[256 + 5 + ( 4 << 4 )] := 0

; RIGHT -> LEFT
let MOO.outposttileid[256 + 10 + ( 8 << 4 )] := 25
let MOO.outposttileangle[256 + 10 + ( 8 << 4 )] := 270
let MOO.outposttileheight[256 + 10 + ( 8 << 4 )] := 0

; CORNER + DOOR FROM LEFT
; LEFT -> TOP
let MOO.outposttileid[256 + 9 + ( 1 << 4 )] := 26
let MOO.outposttileangle[256 + 9 + ( 1 << 4 )] := 0
let MOO.outposttileheight[256 + 9 + ( 1 << 4 )] := 0

; TOP -> RIGHT
let MOO.outposttileid[256 + 3 + ( 2 << 4 )] := 26
let MOO.outposttileangle[256 + 3 + ( 2 << 4 )] := 90
let MOO.outposttileheight[256 + 3 + ( 2 << 4 )] := 0

; RIGHT -> BOTTOM
let MOO.outposttileid[256 + 6 + ( 4 << 4 )] := 26
let MOO.outposttileangle[256 + 6 + ( 4 << 4 )] := 180
let MOO.outposttileheight[256 + 6 + ( 4 << 4 )] := 0

; BOTTOM -> LEFT
let MOO.outposttileid[256 + 12 + ( 8 << 4 )] := 26
let MOO.outposttileangle[256 + 12 + ( 8 << 4 )] := 270
let MOO.outposttileheight[256 + 12 + ( 8 << 4 )] := 0

; CORNER + DOOR FROM RIGHT
; RIGHT -> TOP
let MOO.outposttileid[256 + 3 + ( 1 << 4 )] := 27
let MOO.outposttileangle[256 + 3 + ( 1 << 4 )] := 0
let MOO.outposttileheight[256 + 3 + ( 1 << 4 )] := 0

; BOTTOM -> RIGHT
let MOO.outposttileid[256 + 6 + ( 2 << 4 )] := 27
let MOO.outposttileangle[256 + 6 + ( 2 << 4 )] := 90
let MOO.outposttileheight[256 + 6 + ( 2 << 4 )] := 0

; LEFT -> BOTTOM
let MOO.outposttileid[256 + 12 + ( 4 << 4 )] := 27
let MOO.outposttileangle[256 + 12 + ( 4 << 4 )] := 180
let MOO.outposttileheight[256 + 12 + ( 4 << 4 )] := 0

; TOP -> LEFT
let MOO.outposttileid[256 + 9 + ( 8 << 4 )] := 27
let MOO.outposttileangle[256 + 9 + ( 8 << 4 )] := 270
let MOO.outposttileheight[256 + 9 + ( 8 << 4 )] := 0


; TUNNEL + RAMP
; 1 + 512 
let MOO.outposttileid[1 + 512] := 28
let MOO.outposttileangle[1 + 512] := 0
let MOO.outposttileheight[1 + 512] := 0

; 2 + 512 
let MOO.outposttileid[2 + 512] := 28
let MOO.outposttileangle[2 + 512] := 90
let MOO.outposttileheight[2 + 512] := 0

; 4 + 512 
let MOO.outposttileid[4 + 512] := 28
let MOO.outposttileangle[4 + 512] := 180
let MOO.outposttileheight[4 + 512] := 0

; 8 + 512 
let MOO.outposttileid[8 + 512] := 28
let MOO.outposttileangle[8 + 512] := 270
let MOO.outposttileheight[8 + 512] := 0


; TUNNEL + STAIRS
; 1 + 1024 
let MOO.outposttileid[1 + 1024] := 29
let MOO.outposttileangle[1 + 1024] := 0
let MOO.outposttileheight[1 + 1024] := 0

; 2 + 1024 
let MOO.outposttileid[2 + 1024] := 29
let MOO.outposttileangle[2 + 1024] := 90
let MOO.outposttileheight[2 + 1024] := 0

; 4 + 1024 
let MOO.outposttileid[4 + 1024] := 29
let MOO.outposttileangle[4 + 1024] := 180
let MOO.outposttileheight[4 + 1024] := 0

; 8 + 1024 
let MOO.outposttileid[8 + 1024] := 29
let MOO.outposttileangle[8 + 1024] := 270
let MOO.outposttileheight[8 + 1024] := 0




; STATICS

; TUNNEL
; 1
let MOO.outpoststatic[0] := MOOStaticOutpostTunnel1way01
let MOO.outpoststaticchance[0] := 100

; 2
let MOO.outpoststatic[1] := MOOStaticOutpostTunnel2way01
let MOO.outpoststaticchance[1] := 25
let MOO.outpoststatic[2] := MOOStaticOutpostTunnel2way02
let MOO.outpoststaticchance[2] := 25
let MOO.outpoststatic[3] := MOOStaticOutpostTunnel2way03
let MOO.outpoststaticchance[3] := 25
let MOO.outpoststatic[4] := MOOStaticOutpostTunnel2way04
let MOO.outpoststaticchance[4] := 100

; TEMP DISABLED - LONG TILES
let MOO.outpoststatic[5] := MOOStaticOutpostTunnel2way05
let MOO.outpoststaticchance[5] := 16
let MOO.outpoststatic[6] := MOOStaticOutpostTunnel2way06
let MOO.outpoststaticchance[6] := 100

let MOO.outpoststatic[7] := MOOStaticOutpostTunnelCorner01
let MOO.outpoststaticchance[7] := 33
let MOO.outpoststatic[8] := MOOStaticOutpostTunnelCorner02
let MOO.outpoststaticchance[8] := 33
let MOO.outpoststatic[9] := MOOStaticOutpostTunnelCorner03
let MOO.outpoststaticchance[9] := 100

; 3
let MOO.outpoststatic[10] := MOOStaticOutpostTunnel3way01
let MOO.outpoststaticchance[10] := 33
let MOO.outpoststatic[11] := MOOStaticOutpostTunnel3way02
let MOO.outpoststaticchance[11] := 33
let MOO.outpoststatic[12] := MOOStaticOutpostTunnel3way03
let MOO.outpoststaticchance[12] := 100

; 4
let MOO.outpoststatic[13] := MOOStaticOutpostTunnel4way01
let MOO.outpoststaticchance[13] := 50
let MOO.outpoststatic[14] := MOOStaticOutpostTunnel4way02
let MOO.outpoststaticchance[14] := 100


; ROOM
; 1 << 4
let MOO.outpoststatic[15] := MOOStaticOutpostRoomCorner01
let MOO.outpoststaticchance[15] := 33
let MOO.outpoststatic[16] := MOOStaticOutpostRoomCorner02
let MOO.outpoststaticchance[16] := 33
let MOO.outpoststatic[17] := MOOStaticOutpostRoomCorner03
let MOO.outpoststaticchance[17] := 100

; 3 << 4
let MOO.outpoststatic[18] := MOOStaticOutpostRoomWall01
let MOO.outpoststaticchance[18] := 33
let MOO.outpoststatic[19] := MOOStaticOutpostRoomWall02
let MOO.outpoststaticchance[19] := 33
let MOO.outpoststatic[20] := MOOStaticOutpostRoomWall03
let MOO.outpoststaticchance[20] := 100

; 15 << 4
let MOO.outpoststatic[21] := MOOStaticOutpostRoomFloor01
let MOO.outpoststaticchance[21] := 33
let MOO.outpoststatic[22] := MOOStaticOutpostRoomFloor02
let MOO.outpoststaticchance[22] := 33
let MOO.outpoststatic[23] := MOOStaticOutpostRoomFloor03
let MOO.outpoststaticchance[23] := 100


; TUNNEL + ROOM
; 1 + 1 << 4
let MOO.outpoststatic[24] := MOOStaticOutpostRoomEntry01
let MOO.outpoststaticchance[24] := 100

; TUNNEL + DOOR FROM BACK
; 1 + 256
let MOO.outpoststatic[25] := MOOStaticOutpostTunnelExit01
let MOO.outpoststaticchance[25] := 100

; TUNNEL + DOOR FROM LEFT
; 3 + 256
let MOO.outpoststatic[26] := MOOStaticOutpostTunnelExitCornerLeft01
let MOO.outpoststaticchance[26] := 100

; TUNNEL + DOOR FROM RIGHT
; 3 + 256 + 1 << 4
let MOO.outpoststatic[27] := MOOStaticOutpostTunnelExitCornerRight01
let MOO.outpoststaticchance[27] := 100

; TUNNEL + RAMP
; 1 + 512 
let MOO.outpoststatic[28] := MOOStaticOutpostTunnelRamp01
let MOO.outpoststaticchance[28] := 100

; TUNNEL + STAIRS
; 1 + 1024
let MOO.outpoststatic[29] := MOOStaticOutpostTunnelStairs01
let MOO.outpoststaticchance[29] := 50
let MOO.outpoststatic[30] := MOOStaticOutpostTunnelStairs02
let MOO.outpoststaticchance[30] := 100

End