Maskar's Oblivion Overhaul.esp
0x4E028D
MOOMushroomSpore02Script
ScriptName MOOMushroomSpore02Script

;ref thisRef
;ref atOpponent

begin ScriptEffectStart

	;set atOpponent to getActionRef					;// opponent - eg.: player	
	;set thisRef to getSelf 								;// the mushroom
	MOOCellSporeMushroomPod02.moveto player
	MOOCellSporeMushroomPod02.enable

end

begin ScriptEffectFinish

	MOOCellSporeMushroomPod02.disable

end

