Maskar's Oblivion Overhaul.esp
0x4D5E80
MOOAddTameablesFunctionScript
Scn MOOAddTameablesFunctionScript

Begin Function {  }

if MOO.ini_debug == 2
	printc "[MOO] Added: %z (food: %.0f spells: %.0f doors: %.0f slots: %.0f)" MOO.ini_object_folder MOO.ini_object_food MOO.ini_object_spells MOO.ini_object_doors MOO.ini_object_slots
endif

let MOO.petsfolder[MOO.totalpets] := MOO.ini_object_folder
let MOO.petsfilename[MOO.totalpets] := MOO.ini_object_filename
let MOO.petsfood[MOO.totalpets] := MOO.ini_object_food
let MOO.petsspells[MOO.totalpets] := MOO.ini_object_spells
let MOO.petsdoors[MOO.totalpets] := MOO.ini_object_doors
let MOO.petsslots[MOO.totalpets] := MOO.ini_object_slots

set MOO.totalpets to MOO.totalpets + 1

let MOO.ini_object_filename := ""

End