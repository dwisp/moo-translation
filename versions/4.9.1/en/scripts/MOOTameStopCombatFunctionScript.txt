Maskar's Oblivion Overhaul.esp
0x3158DE
MOOTameStopCombatFunctionScript
Scn MOOTameStopCombatFunctionScript

ref mypet

ref me
short evilme

Begin Function { mypet }

mypet.StopCombat

set me to GetFirstRef 69 4
while me

	set evilme to Call MOOIsEvilActorFunctionScript me

	if me.GetCombatTarget == mypet
	if me != mypet && evilme == 0
		me.ModDisposition mypet 100
		mypet.ModDisposition me 100
		me.StopCombat
	endif
	endif

	set me to GetNextRef
loop

End