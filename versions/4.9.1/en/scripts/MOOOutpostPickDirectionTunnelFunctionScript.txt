Maskar's Oblivion Overhaul.esp
0x50BF98
MOOOutpostPickDirectionTunnelFunctionScript
Scn MOOOutpostPickDirectionTunnelFunctionScript

short mydigger

short myrand

short mydirection

Begin Function { mydigger }

; DIGGER
; MOO.dungeonx[myx]
; MOO.dungeony[myy]
; MOO.dungeondirection[mydigger] ; 1, 2, 4, 8 == top, left, bottom, right

let mydirection := MOO.tunneldirection[mydigger]

; digger is disabled
if mydirection == 0
	return
endif

set myrand to GetRandomPercent
if myrand < MOO.ini_outpost_tunnel_corner
	; turn left
	let mydirection := mydirection << 1
	if mydirection == 16
		set mydirection to 1
	endif

elseif myrand < MOO.ini_outpost_tunnel_corner * 2
	; turn right
	let mydirection := mydirection >> 1
	if mydirection == 0
		set mydirection to 8
	endif

; 22

; STOP ONLY WHEN ENOUGH TILES HAVE BEEN PLACED
elseif ( myrand < ( MOO.ini_outpost_tunnel_corner * 2 ) + MOO.ini_outpost_tunnel_distance ) && ( MOO.currenttilestotal > MOO.ini_outpost_tiles_total_min )

	; stop
	set mydirection to 0

endif

SetFunctionValue mydirection

End

