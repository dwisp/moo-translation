Maskar's Oblivion Overhaul.esp
0x399B22
MOOFixLockLevelFunctionScript
Scn MOOFixLockLevelFunctionScript

ref me
float mylocklevel
float newlocklevel

ref mykey
ref mylinkeddoor

Begin Function { me }

; GET KEY
set mykey to me.GetOpenKey
if mykey == 0
if me.IsLoadDoor
	set mylinkeddoor to me.GetLinkedDoor
	set mykey to mylinkeddoor.GetOpenKey
endif
endif

; NO KEY FOUND
if mykey == 0
	if me.GetLockLevel == 100
		; can't be needing key without a key existing
		me.Lock 20
	endif
	return
endif

; KEY FOUND
me.Lock 100
if me.GetLockLevel < 100
	me.Lock mylocklevel
	set newlocklevel to mylocklevel - ( me.GetLockLevel - mylocklevel )	
	me.Lock newlocklevel
endif

End