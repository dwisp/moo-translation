Maskar's Oblivion Overhaul.esp
0x423384
MOOSEUpdateBrightnessStreetlightFunctionScript
scn MOOSEUpdateBrightnessStreetlightFunctionScript

short i

Begin Function {  }

set i to 300 + MOO.ini_streetlight_brightness
SetLightRadius i SECrucibleStreet300

set i to 512 + MOO.ini_streetlight_brightness
SetLightRadius i SECrucibleStreet512

set i to 600 + MOO.ini_streetlight_brightness
SetLightRadius i SECrucibleStreet600

set i to 768 + MOO.ini_streetlight_brightness
SetLightRadius i SECrucibleStreet768

End