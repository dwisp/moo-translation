Maskar's Oblivion Overhaul.esp
0x50811F
MOOPlaceWorldSpaceObjectOnTargetOutsideBorderFunctionScript
Scn MOOPlaceWorldSpaceObjectOnTargetOutsideBorderFunctionScript

ref myplacer

short totaltrees
short isplaced
ref mycell
short mychance

Begin Function { myplacer }

; GET CELL
set mycell to myplacer.GetParentCell
if mycell == 0
	return
endif

; CHECK
if myplacer.GetPos z == 0
	return
endif

; SKIP IF NO TREES ARE FOUND -> LANDSCAPE BECOMES UGLY WHEN TOO FAR AWAY
if MOO.ini_event_placement_empty == 0
	set totaltrees to GetNumRefsInCell mycell 30
	if totaltrees == 0
		return
	endif
endif

; CAMPSITE OUTSIDE BORDER
if GetRandomPercent < MOO.ini_event_remote_campsite
	set isplaced to Call MOOEventCampsiteOutsideBorderFunctionScript myplacer

	if isplaced
		set MOO.totalevents to MOO.totalevents + 1
		set MOO.totalremotecampsite to MOO.totalremotecampsite + 1
	endif
endif

; HOUSE OUTSIDE BORDER
if isplaced == 0
	set mychance to MOO.ini_event_remote_house + ( MOO.totalfailedhouse * MOO.ini_event_fail_bonus )
	if GetRandomPercent < mychance
		set isplaced to Call MOOEventHouseOutsideBorderFunctionScript myplacer

		if isplaced
			set MOO.totalevents to MOO.totalevents + 1
			set MOO.totalremotehouse to MOO.totalremotehouse + 1
			set MOO.totalfailedhouse to 0
		else
			set MOO.totalfailedhouse to MOO.totalfailedhouse + 1
		endif
	endif
endif

; DUNGEON OUTSIDE BORDER
if isplaced == 0
if GetRandomPercent < MOO.ini_event_remote_dungeon
	set isplaced to Call MOOEventDungeonOutsideBorderFunctionScript myplacer

	if isplaced
		set MOO.totalevents to MOO.totalevents + 1
		set MOO.totalremotedungeon to MOO.totalremotedungeon + 1
	endif
endif
endif

; HARPY NEST OUTSIDE BORDER
if isplaced == 0
if GetRandomPercent < MOO.ini_event_remote_harpynest
	set isplaced to Call MOOEventHarpyNestOutsideBorderFunctionScript myplacer

	if isplaced
		set MOO.totalevents to MOO.totalevents + 1
		set MOO.totalremoteharpynest to MOO.totalremoteharpynest + 1
	endif
endif
endif

; ACTOR OUTSIDE BORDER
if isplaced == 0
if GetRandomPercent < MOO.ini_event_remote_actor
	set isplaced to Call MOOEventActorOutsideBorderFunctionScript myplacer

	if isplaced
		set MOO.totalevents to MOO.totalevents + 1
		set MOO.totalremoteactor to MOO.totalremoteactor + 1
	endif
endif
endif

; PLACE PLANTS AROUND TREES
set isplaced to Call MOOEventPlantTreeOutsideBorderFunctionScript myplacer

End