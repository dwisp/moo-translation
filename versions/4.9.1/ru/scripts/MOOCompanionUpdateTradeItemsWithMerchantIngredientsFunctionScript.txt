Maskar's Oblivion Overhaul.esp
0x547AD7
MOOCompanionUpdateTradeItemsWithMerchantIngredientsFunctionScript
Scn MOOCompanionUpdateTradeItemsWithMerchantIngredientsFunctionScript

ref mycompanion
ref mymerchant
ref mycontainer
short i

ref myitem
short myingredients
short merchantingredients
short miningredients
short maxingredients
short ingredienttype

short sellingredients
short buyingredients

short busy

Begin Function { mycompanion mymerchant mycontainer i }

set miningredients to 10
set maxingredients to 250
set ingredienttype to 25

set busy to Call MOOCompanionUpdateTradeItemsWithMerchantMagicEffectItemsFunctionScript mycompanion mymerchant i miningredients maxingredients ingredienttype

SetFunctionValue busy

End