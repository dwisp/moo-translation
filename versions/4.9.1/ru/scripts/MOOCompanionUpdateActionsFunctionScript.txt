Maskar's Oblivion Overhaul.esp
0x53EB0F
MOOCompanionUpdateActionsFunctionScript
Scn MOOCompanionUpdateActionsFunctionScript

ref me
short i

ref mypackage

Begin Function { me i }

; WAIT FOR FIND PACKAGE TO COMPLETE / FAIL
if eval MOO.companionsbusy[i]
	Call MOOCompanionUpdateActionsBusyFunctionScript me i
endif

if me.IsInCombat
	Call MOOCompanionUpdateActionsInCombatFunctionScript me
	return
endif


if me.GetCurrentAIPackage == 0 ; explore / find

	if me.GetItemCount MOOTokenCompanionFollow
	if me.GetDistance player > MOO.ini_companion_busy_distance || player.IsInCombat || player.IsSneaking
		me.AddScriptPackage MOOAIFollowerFollow
		return
	endif
	endif

	; GIVE UP
	if me.GetCurrentAIProcedure != 2 ; acquire ; == 7 wander
		if eval MOO.companionsbusy[i] > MOO.ini_companion_busy_min
			set mypackage to Call MOOCompanionUpdateActionsFindRefFunctionScript me i
			if mypackage
				me.AddScriptPackage mypackage
			endif
		endif

		if mypackage == 0
			if me.GetItemCount MOOTokenCompanionFollow
				me.AddScriptPackage MOOAIFollowerFollow
			else
				me.AddScriptPackage MOOAIFollowerStay
			endif
		endif
	endif

; NEW ACTION
elseif me.IsInCombat == 0 && me.GetDistance player < 500 && me.IsSneaking == 0

	; CHECK FOR CORPSE CHECKING
	Call MOOCompanionUpdateActionsCorpseCheckFunctionScript me

	; DON'T DO ANYTHING FOR X SECONDS
	if eval MOO.companionsbusy[i] > 0
		return
	endif

	; REPAIR + RECHARGE + MAKE POTIONS + SHOP + FIND + NEEDS
	if GetRandomPercent < 33
		Call MOOCompanionUpdateActionsMaintenanceFunctionScript me i
	endif

endif

End