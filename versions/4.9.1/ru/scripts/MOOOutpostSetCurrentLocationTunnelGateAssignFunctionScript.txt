Maskar's Oblivion Overhaul.esp
0x51B199
MOOOutpostSetCurrentLocationTunnelGateAssignFunctionScript
Scn MOOOutpostSetCurrentLocationTunnelGateAssignFunctionScript

short mykey
short myactivator ; lever, keyhole

Begin Function {  }

; LEVER
if eval MOO.currentgatestatus & 1 == 0
	set myactivator to MOO.id_dungeon_lever

; KEYHOLE
else
	set myactivator to Call MOOOutpostSetCurrentLocationTunnelGateGetKeyholeFunctionScript
	set mykey to MOO.id_dungeon_actor
endif


; CHECK IF GATE EVENT CAN STILL BE PLACED
if eval MOO.dungeonmapevent[MOO.currentgateplatex][MOO.currentgateplatey] == MOO.id_dungeon_reserved || MOO.currentgatestatus & 1 == 0 ; no key
if eval MOO.dungeonmapevent[MOO.currentgateactivatorx][MOO.currentgateactivatory] == MOO.id_dungeon_reserved
if eval MOO.dungeonmapevent[MOO.currentgatex][MOO.currentgatey] == MOO.id_dungeon_reserved

	; ASSIGN GATE
	set MOO.currentdungeongate to MOO.currentdungeongate + 1

	; PLACE KEY EVENT
	if mykey

		; ACTORS
		Call MOOOutpostSetCurrentLocationTunnelGateAssignActorsFunctionScript mykey myactivator

		; PLATE
		let MOO.dungeonmapevent[MOO.currentgateplatex][MOO.currentgateplatey] := MOO.id_dungeon_plate + ( MOO.currentdungeongate << 5 )

		; STORE DIFFICULTY
		let MOO.dungeoneventdifficulties[MOO.currentdungeongate] := myactivator ; MOO.id_dungeon_keyhole_bronze, etc.
	endif

	; PLACE ACTIVATOR EVENT
	let MOO.dungeonmapevent[MOO.currentgateactivatorx][MOO.currentgateactivatory] := myactivator + ( MOO.currentdungeongate << 5 )

	; PLACE GATE EVENT
	let MOO.dungeonmapevent[MOO.currentgatex][MOO.currentgatey] := MOO.id_dungeon_gate + ( MOO.currentdungeongate << 5 )

endif
endif
endif

End