Maskar's Oblivion Overhaul.esp
0x57C5A3
MOOHelperGetNameNpcRegionFunctionScript
Scn MOOHelperGetNameNpcRegionFunctionScript

String_var myname

short i
short j

short male

Begin Function {  }

set male to Rand 0 1.999

if MOO.myregion == 10 ; Hammerfell
	set j to Rand 0 2.999
	if j == 0
		set i to 7 ; Orc
	elseif j == 1
		set i to 8 ; Redguard
	else
		set i to 1 ; Breton
	endif

elseif MOO.myregion == 11 ; Skyrim
	set i to 6 ; Nord

elseif MOO.myregion == 12 ; Morrowind
	set i to 2 ; DarkElf ; Dunmer

elseif MOO.myregion == 13 ; Black Marsh
	set i to 0 ; Argonian

elseif MOO.myregion == 14 ; Elsweyr
	set i to 5 ; Khajiit

elseif MOO.myregion == 15 ; Valenwood
	set i to 9 ; Woodelf ; Bosmer

elseif MOO.myregion == 23 ; Gold Coast
	set i to 3 ; HighElf ; Altmer

elseif MOO.myregion == 24 ; Topal Bay ; skip
	set i to 0 ; Argonian

else
	set i to Rand 0 9.999
endif


if i == 0 ; Argonian
	let myname := Call MOOGetNpcNameArgonianFunctionScript male
elseif i == 1 ; Breton
	let myname := Call MOOGetNpcNameBretonFunctionScript male
elseif i == 2 ; DarkElf ; Dunmer
	let myname := Call MOOGetNpcNameDunmerFunctionScript male
elseif i == 3 ; HighElf ; Altmer
	let myname := Call MOOGetNpcNameAltmerFunctionScript male
elseif i == 4 ; Imperial
	let myname := Call MOOGetNpcNameImperialFunctionScript male
elseif i == 5 ; Khajiit
	let myname := Call MOOGetNpcNameKhajiitFunctionScript male
elseif i == 6 ; Nord
	let myname := Call MOOGetNpcNameNordFunctionScript male
elseif i == 7 ; Orc
	let myname := Call MOOGetNpcNameOrcFunctionScript male
elseif i == 8 ; Redguard
	let myname := Call MOOGetNpcNameRedguardFunctionScript male
else ; Woodelf ; Bosmer
	let myname := Call MOOGetNpcNameBosmerFunctionScript male
endif

SetFunctionValue myname

sv_Destruct myname

End