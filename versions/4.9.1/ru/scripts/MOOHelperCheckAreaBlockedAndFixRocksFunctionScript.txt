Maskar's Oblivion Overhaul.esp
0x4B49B8
MOOHelperCheckAreaBlockedAndFixRocksFunctionScript
Scn MOOHelperCheckAreaBlockedAndFixRocksFunctionScript

ref myplacer
short myrange

ref mycell
ref me

float myx
float myy
float myz
float terrainz

short deleterock
short found

Begin Function { myplacer myrange }

set mycell to myplacer.GetParentCell

set me to GetFirstRefInCell mycell 28 1 ; stat
while me
	if me != myplacer
	if me.GetDistance myplacer < myrange
		if me.CompareModelPath "Rocks\"
			set myx to me.GetPos x
			set myy to me.GetPos y
			set myz to me.GetPos z
			set terrainz to GetTerrainHeight myx myy
			if terrainz < myz - 100 ; rock can fly a little
				set found to 1
				break
			else
				set deleterock to 1
			endif

		else
			set found to 1
			break
		endif
	endif
	endif

	set me to GetNextRef
loop

; ROCKS FOUND - BUT CAN BE DISABLED - CHECK IF LOCKED
if found == 0 && deleterock
	set me to GetFirstRefInCell mycell 28 1 ; stat
	while me
		if me != myplacer
		if me.GetDistance myplacer < myrange
			if me.CompareModelPath "Rocks\"
				me.Disable
				if me.GetDisabled == 0
					set found to 1
					break
				endif
			endif
		endif
		endif

		set me to GetNextRef
	loop
endif

SetFunctionValue found

End