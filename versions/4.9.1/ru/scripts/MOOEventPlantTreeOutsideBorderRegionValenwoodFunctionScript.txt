Maskar's Oblivion Overhaul.esp
0x5779FD
MOOEventPlantTreeOutsideBorderRegionValenwoodFunctionScript
Scn MOOEventPlantTreeOutsideBorderRegionValenwoodFunctionScript

ref myplacer

ref me
ref mycell
ref myref
float refx
float refy
float refz

float mydistance

ref myplant

ref me1
ref me2
ref me3
ref me4
ref me5
ref me6
ref myactorplacer
short mushroomtype
short isplaced

Begin Function { myplacer }

;MOOCellPlacerCreature.MoveTo myplacer
;set mycell to MOOCellPlacerCreature.GetParentCell
;if mycell == 0
;	return
;endif

set mycell to myplacer.GetParentCell
if mycell == 0
	return
endif

; GET MUSHROOMS
set mushroomtype to Call MOOEventMushroomGetTypeFunctionScript

if mushroomtype == 0
	set me1 to MOOFloraMushroom01
	set me2 to MOOFloraMushroom02
	set me3 to MOOFloraMushroom03
	set me4 to MOOFloraMushroom04
	set me5 to MOOFloraMushroom05
	set me6 to MOOFloraMushroom06

elseif mushroomtype == 1
	set me1 to MOOFloraMushroom01B
	set me2 to MOOFloraMushroom02B
	set me3 to MOOFloraMushroom03B
	set me4 to MOOFloraMushroom04B
	set me5 to MOOFloraMushroom05B
	set me6 to MOOFloraMushroom06B

elseif mushroomtype == 2
	set me1 to MOOFloraMushroom01C
	set me2 to MOOFloraMushroom02C
	set me3 to MOOFloraMushroom03C
	set me4 to MOOFloraMushroom04C
	set me5 to MOOFloraMushroom05C
	set me6 to MOOFloraMushroom06C

else
	set me1 to MOOFloraMushroom01D
	set me2 to MOOFloraMushroom02D
	set me3 to MOOFloraMushroom03D
	set me4 to MOOFloraMushroom04D
	set me5 to MOOFloraMushroom05D
	set me6 to MOOFloraMushroom06D

endif


; PLACE MUSHROOMS
set me to GetFirstRefInCell mycell 30

while me

	if me.GetDisabled == 0
	if GetRandomPercent < 50

		set myplant to Call MOOEventMushroomsTreeOutsideBorderPlaceMushroomFunctionScript me me1 me2 me3 me4 me5 me6

		; PLACE MUSHROOM CREATURE ON MUSHROOM ( NOT TREE )
		if myplant
			set isplaced to 1
			Call MOOActivatorEventActorPlaceOnLoadFloraFunctionScript myplant
		endif

	endif
	endif

	set me to GetNextRef
loop


; PLACE MARKER TO PLACE MUSHROOM CREATURES ( ON MUSHROOM )
if isplaced

	set refx to myplant.GetPos x
	set refy to myplant.GetPos y
	Call MOOPlaceObjectOnTerrainFunctionScript refx refy DrakeloweMapMarker MOOActivatorEventActorFlora 32 0 0 0 0

endif

End