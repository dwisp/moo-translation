Maskar's Oblivion Overhaul.esp
0x3A9F2A
MOOObjectFoodFunctionScript
Scn MOOObjectFoodFunctionScript

ref me
float currentday
short mystatus
ref myitem
ref newitem

Begin OnAdd

if mystatus == 0
	set myitem to MOOCraft.lastcrafted
	set currentday to GameDaysPassed
	set mystatus to 1
endif

End


Begin Gamemode

if currentday > GameDaysPassed - 1
	return
endif
if mystatus == 0 || GetContainer == 0
	return
endif

if mystatus == 1
	set me to GetContainer
	set newitem to Call MOOCraftGetFoodAfterTimeFunctionScript myitem
	me.AddItemNS newitem 1
elseif mystatus == 5
	RemoveMe
endif

set mystatus to mystatus + 1

End