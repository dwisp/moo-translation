Maskar's Oblivion Overhaul.esp
0x38D50B
MOOCraftInitMenuRecipiesFunctionScript
Scn MOOCraftInitMenuRecipiesFunctionScript

short i
short j
short k

short myid
float myweight

short tempamount
short maxitems

Begin Function {  }

set i to 0
set j to 0

set MOOCraft.mytypecount to 0

while i < MOO.totalrecipies

	if eval MOO.recipiesgroup[i] == MOOCraft.mytype

		set MOOCraft.mytypecount to MOOCraft.mytypecount + 1

		set maxitems to 999
		set j to 0
		while j < 4
			let myid := MOO.recipiessource[j][i]
			if myid
				let myweight := MOO.recipiessourceweight[j][i]
				if myweight
					let tempamount := MOOCraft.myresources[myid] / myweight
					if tempamount < maxitems
						set maxitems to tempamount
					endif
				endif
			endif
			set j to j + 1
		loop

		if maxitems
			let MOOCraft.recipiesentry[k] := i
			let MOOCraft.recipiesamount[k] := maxitems
			set k to k + 1
		endif

	endif

	set i to i + 1

loop

End