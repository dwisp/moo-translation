Maskar's Oblivion Overhaul.esp
0x4CDCBC
MOODungeonCreateLightFunctionScript
Scn MOODungeonCreateLightFunctionScript

ref mystatic
short myx
short myy
short myz

short mytile

short mapx
short mapy
short mapz

short modx
short mody
short modz

short i

short mytile
ref mylight

Begin Function { mystatic myx myy myz }

let mytile := MOO.dungeonmap[myx][myy]
set i to 64 + 16

if eval mytile == ( 4 << 4 ) + ( 1 << 8 ) ; NW
	set mylight to CaveDayLightAmbHalf400

	set modx to 128
	set mody to 128
	set modz to 256

elseif eval mytile == ( 8 << 4 ) + ( 1 << 8 ) ; NE
	set mylight to CaveDayLightAmbHalf400

	set modx to 128
	set mody to 128
	set modz to 256

elseif eval mytile == ( 1 << 4 ) + ( 1 << 8 ) ; SE
	set mylight to CaveDayLightAmbHalf400

	set modx to 128
	set mody to 128
	set modz to 256

elseif eval mytile == ( 2 << 4 ) + ( 1 << 8 ) ; SW
	set mylight to CaveDayLightAmbHalf400

	set modx to 128
	set mody to 128
	set modz to 256

endif

if mylight == 0
	return
endif

set mapx to ( myx * 512 ) + modx
set mapy to ( myy * 512 ) + mody
set mapz to myz + modz

MOOCellPlacer.setpos x mapx
MOOCellPlacer.setpos y mapy
MOOCellPlacer.setpos z mapz

MOOCellPlacer.PlaceAtMe mylight

End

