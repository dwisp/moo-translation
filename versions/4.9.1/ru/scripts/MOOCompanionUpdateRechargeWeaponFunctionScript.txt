Maskar's Oblivion Overhaul.esp
0x543D53
MOOCompanionUpdateRechargeWeaponFunctionScript
Scn MOOCompanionUpdateRechargeWeaponFunctionScript

ref me

ref myitem
ref myenchantment

short mychargemax
short mychargecurrent
short mychargemod

short myenchantmentcost

short myusesmax
short myusescurrent

ref mysoulgem

short busy

Begin Function { me }

; GET ENCHANTMENT
set myitem to me.GetEquippedObject 16
if myitem
	set myenchantment to GetEnchantment myitem
endif
if myenchantment == 0
	return
endif

; GET USES
set myenchantmentcost to GetEnchantmentCost myenchantment
set mychargecurrent to me.GetEquippedCurrentCharge 16
set mychargemax to GetObjectCharge myitem

if myenchantmentcost
	set myusescurrent to mychargecurrent / myenchantmentcost
	set myusesmax to mychargemax / myenchantmentcost
endif

if myusescurrent > 0 || myusesmax <= 0
	return
endif

; GET SOUL GEM
set mychargemod to mychargemax - mychargecurrent
set mysoulgem to Call MOOCompanionUpdateRechargeWeaponGetSoulGemFunctionScript me mychargemod

; RECHARGE WEAPON
if mysoulgem
	set busy to Call MOOCompanionUpdateRechargeWeaponRechargeFunctionScript me mysoulgem mychargemod
endif

SetFunctionValue busy

End