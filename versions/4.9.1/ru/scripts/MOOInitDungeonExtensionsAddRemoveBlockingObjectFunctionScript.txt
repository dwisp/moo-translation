Maskar's Oblivion Overhaul.esp
0x59A512
MOOInitDungeonExtensionsAddRemoveBlockingObjectFunctionScript
Scn MOOInitDungeonExtensionsAddRemoveBlockingObjectFunctionScript

ref me
short mytype

short i
string_var mypath

Begin Function { me mytype }

set i to MOO.totaldungeonobstacles
while i
	set i to i - 1

	let mypath := MOO.dungeonobstacles[i]

	if me.CompareModelPath $mypath
	if eval MOO.dungeonobstaclestype[i] == mytype
		me.Disable
	endif
	endif
loop

sv_Destruct mypath

End
