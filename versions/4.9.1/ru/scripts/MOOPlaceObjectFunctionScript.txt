Maskar's Oblivion Overhaul.esp
0x21B6E7
MOOPlaceObjectFunctionScript
Scn MOOPlaceObjectFunctionScript

float xpos
float ypos
float zpos
float xangle
float yangle
float zangle
ref marker
ref object

ref me

Begin Function { xpos ypos zpos xangle yangle zangle marker object }

set me to MOOCellPlacer
me.MoveTo marker

me.setpos x xpos
me.setpos y ypos
me.setpos z zpos
me.setangle x xangle
me.setangle y yangle
me.setangle z zangle
me.PlaceAtMe object

End