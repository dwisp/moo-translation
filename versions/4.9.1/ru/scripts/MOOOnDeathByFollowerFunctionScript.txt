Maskar's Oblivion Overhaul.esp
0x302B21
MOOOnDeathByFollowerFunctionScript
Scn MOOOnDeathByFollowerFunctionScript

ref target
ref attacker
short killingblow ; follower did killing blow == more xp

short myxp
short gainedxp
short myhappiness
short targetlevel
short i

Begin Function { target attacker killingblow }

set i to attacker.GetItemCount MOOTokenCreatureTame

let myxp := MOOTame.followersXP[i]

let myhappiness := MOOTame.followershappiness[i]
set targetlevel to target.GetLevel

set gainedxp to ( targetlevel * ( myhappiness + 1 ) * MOO.ini_follower_skillgain )
if killingblow == 0
	set gainedxp to gainedxp * MOO.ini_follower_indirectXP / 100
endif
set myxp to myxp - gainedxp

; level up
if myxp < 0

	Call MOOTameIncreaseLevelFunctionScript attacker

	messageex "%n становится сильнее!" attacker

	attacker.AddItemNS MOOTokenCreatureXP 1
	set myxp to Call MOOTameGetRequiredXPFunctionScript attacker

	; update abilities/spells
	Call MOOTameUpdateAbilitiesFunctionScript attacker
endif

let MOOTame.followersXP[i] := myxp

if MOO.ini_debug
	printc "[MOO] %n has gained %.0f experience ( experience to next level: %.0f )." attacker gainedxp myxp
endif

End