Maskar's Oblivion Overhaul.esp
0x48B9A4
MOOInitCenturionMoveFunctionScript
Scn MOOInitCenturionMoveFunctionScript

ref me
ref myhorse

ref myitem

Begin Function { me myhorse }

myhorse.MoveTo MOOCellMarkerHorseCenturion
me.MoveTo MOOCellMarkerHorseCenturion

set myitem to CalcLeveledItem MOOLL0ArmorLegionCenturionCuirass 99
if me.GetItemCount myitem == 0
	me.AddItemNS myitem 1
	me.EquipItemNS myitem
endif
set myitem to CalcLeveledItem MOOLL0ArmorLegionCenturionGreaves 99
if me.GetItemCount myitem == 0
	me.AddItemNS myitem 1
	me.EquipItemNS myitem
endif

End