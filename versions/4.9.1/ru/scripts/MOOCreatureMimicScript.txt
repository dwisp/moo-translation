Maskar's Oblivion Overhaul.esp
0x06A7F6
MOOCreatureMimicScript
scn MOOCreatureMimicScript

short loaded
ref me
ref myidle

Begin Gamemode

if loaded
	return
endif

set me to GetSelf
set myidle to MOOIdleMimicGetUp
me.PlayIdle myidle 1

set loaded to 1

End


Begin OnReset

set loaded to 0
set me to GetSelf
me.Resurrect
me.MoveTo MOOCellMarker
me.Disable

End