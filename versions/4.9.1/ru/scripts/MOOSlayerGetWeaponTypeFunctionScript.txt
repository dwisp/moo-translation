Maskar's Oblivion Overhaul.esp
0x191DA5
MOOSlayerGetWeaponTypeFunctionScript
Scn MOOSlayerGetWeaponTypeFunctionScript

ref myweapon

short weapontype
short i
string_var weaponname
string_var iniweaponname

Begin Function { myweapon }

let weaponname := GetModelPath myweapon

set i to MOO.totalweapons - 1
while i >= 0
	let iniweaponname := MOO.weaponsfilename[i]

	if ( sv_Count $iniweaponname weaponname > 0 )
		let weapontype := MOO.weaponstype[i]
		break
	endif
	set i to i - 1
loop

sv_Destruct weaponname
sv_Destruct iniweaponname

SetFunctionValue weapontype

End