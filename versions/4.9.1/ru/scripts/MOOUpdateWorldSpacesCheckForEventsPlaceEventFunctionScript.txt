Maskar's Oblivion Overhaul.esp
0x58EC98
MOOUpdateWorldSpacesCheckForEventsPlaceEventFunctionScript
Scn MOOUpdateWorldSpacesCheckForEventsPlaceEventFunctionScript

float myx
float myy

float myz

short isplaced

Begin Function { myx myy }

; PLACE EVENTS BASED ON ARRAY
let myx := myx + MOO.worldspacesx1[MOO.currentworldspaceid]
let myy := myy + MOO.worldspacesy1[MOO.currentworldspaceid]

set myx to ( myx * 4096 ) + 2048
set myy to ( myy * 4096 ) + 2048
set myz to GetTerrainHeight myx myy

; MOVE PLACER
MOOCellPlacer.MoveTo player

MOOCellPlacer.setpos x myx
MOOCellPlacer.setpos y myy
MOOCellPlacer.setpos z myz

MOOCellPlacerEvent.MoveTo MOOCellPlacer

; PLACE OBJECTS
set isplaced to Call MOOPlaceWorldSpaceObjectOnTargetFunctionScript MOOCellPlacerEvent

SetFunctionValue isplaced

End