Maskar's Oblivion Overhaul.esp
0x409EBC
MOOContainerVaultScript
Scn MOOContainerVaultScript

ref mycontainer


ref mycell
ref myobject

Begin OnActivate player

if MOO.COBLloaded
	set mycontainer to GetFormFromMod "Cobl Main.esm" "0016A3"
else
	set mycontainer to MOOCellContainerVaultMain
endif

if IsFormValid mycontainer
	mycontainer.activate player 1
endif

End