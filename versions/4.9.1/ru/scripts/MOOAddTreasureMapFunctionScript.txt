Maskar's Oblivion Overhaul.esp
0x27B390
MOOAddTreasureMapFunctionScript
Scn MOOAddTreasureMapFunctionScript

ref me
ref mymap
short i
float randmin
float randmax

Begin Function { me }

if MOO.ini_treasure_map == 0
	return
endif
if me.GetCreatureType == 1 || me.GetInFaction DremoraFaction ; Deadra/Dremora have no maps
	return
endif
if MOO.myregion < 10
	return
endif
if ( me.GetAV Intelligence < 30 ) && ( me.CreatureUsesWeaponAndShield == 0 )
	return
endif

if me.GetCreatureType == -1
	if me.GetLevel >= 20
		set i to 5
	elseif me.GetLevel >= 15
		set i to 4
	elseif me.GetLevel >= 10
		set i to 3
	elseif me.GetLevel >= 5
		set i to 2
	else
		set i to 1
	endif
else
	set i to me.GetActorSoulLevel
endif

set randmin to i - 2
if randmin < 1
	set randmin to 1
endif
set randmax to i + 0.99

set i to Rand randmin randmax
if i == 1
	set mymap to MOOLL0Map1Plain
elseif i == 2
	set mymap to MOOLL0Map2Expert
elseif i == 3
	set mymap to MOOLL0Map3Adept
elseif i == 4
	set mymap to MOOLL0Map4Clever
elseif i == 5
	set mymap to MOOLL0Map5Devious
endif

if mymap
	set mymap to CalcLeveledItem mymap 99
	me.AddItemNS mymap 1

	if MOO.ini_debug
		printc "[MOO] Adding %n to %n" mymap me
	endif
endif

End