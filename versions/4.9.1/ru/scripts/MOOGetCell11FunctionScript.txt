Maskar's Oblivion Overhaul.esp
0x244F42
MOOGetCell11FunctionScript
Scn MOOGetCell11FunctionScript

short dungeontype ; 0 default 1 vampire 2 necromancer 3 conjurer

short i

ref mycell

Begin Function { dungeontype }

if dungeontype == 1 ; vampire

	set i to Rand 0 2
	if i == 0
		set mycell to Ninendava
	else
		set mycell to GuttedMine
	endif

elseif dungeontype == 2 ; necromancer

	set i to Rand 0 3
	if i == 0
		set mycell to EchoCave
	elseif i == 1
		set mycell to EchoCave02
	else
		set mycell to EchoCave03
	endif

else ; default

	set i to Rand 0 3
	if i == 0
		set mycell to CapstoneCave
	elseif i == 1
		set mycell to CapstoneCave02
	else
		set mycell to BorealStoneCave
	endif

endif


SetFunctionValue mycell

End