Maskar's Oblivion Overhaul.esp
0x50BF97
MOOOutpostCheckDirectionAllowedTunnelFunctionScript
Scn MOOOutpostCheckDirectionAllowedTunnelFunctionScript

short mydigger
short mydirection

short mytile

short myx
short myy
short oldx
short oldy

short dungeonsize

short allowed ; -1 (out of bounds, but can have door added), 0 (no), 1 (yes)
short adddoor ; 1 == set allowed to -1

Begin Function { mydigger mydirection }

set dungeonsize to ar_Size MOO.dungeonmap
set allowed to 1

; GET NEW LOCATION
let oldx := MOO.tunnelx[mydigger]
let oldy := MOO.tunnely[mydigger]

; DIGGER IS DISABLED
if mydirection == 0
	set allowed to 0
endif

set myx to oldx
set myy to oldy

if mydirection == 1 ; move up
	set myy to myy + 1
elseif mydirection == 2 ; move right
	set myx to myx + 1
elseif mydirection == 4 ; move down
	set myy to myy - 1
else ; move left
	set myx to myx - 1
endif

; OUT OF BOUNDS
if myx < 0 || myx >= dungeonsize || myy < 0 || myy >= dungeonsize

	; ONLY DOOR WHEN MINIMUM TOTAL TILES REACHED
	; ONLY WHEN NO EVENT ON TILE ( LIKE TELEPORTER MARKER )
	if MOO.currenttilestotal >= MOO.ini_outpost_tiles_total_min
	if eval MOO.dungeonmapevent[oldx][oldy] == 0
		set adddoor to 1
	endif
	endif

	set allowed to 0
endif

; DIFFERENT HEIGHTS
if allowed
if eval MOO.dungeonmap[oldx][oldy] <= 15
if eval MOO.dungeonmap[myx][myy] <= 15 && MOO.dungeonmap[myx][myy] > 0
if eval MOO.dungeonmapheight[oldx][oldy] != MOO.dungeonmapheight[myx][myy]
	set allowed to 0
endif
endif
endif
endif

; CONNECTING TO EXISTING TILE
if allowed
let mytile := MOO.dungeonmap[myx][myy]
if mytile
	; NOT TUNNEL
	if mytile > 15
		set allowed to 0

	; DUNGEON TOO SMALL
	elseif MOO.currenttilestotal < MOO.ini_outpost_tiles_total_min
		set allowed to 0
	endif
endif
endif

; DO NOT CONNECT TO TILES WITH EVENTS
if allowed
if eval MOO.dungeonmapevent[myx][myy] != 0
	set allowed to 0
endif
endif

; MAX TILES - ADD MORE IF NEEDED
if MOO.currenttilestotal > MOO.ini_outpost_tiles_total_max
	if Call MOOOutpostCheckDirectionAllowedTunnelDoneFunctionScript oldx oldy
		set allowed to 0
	else
		set MOO.currenttilestotal to MOO.ini_outpost_tiles_total_max - MOO.ini_outpost_tunnel_new_extended
	endif
endif

; SPECIAL ALLOWED -> OUT OF BOUNDS - BUT CAN ADD DOOR
if adddoor
	set allowed to -1
endif

SetFunctionValue allowed

End