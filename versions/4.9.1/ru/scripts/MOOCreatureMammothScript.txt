Maskar's Oblivion Overhaul.esp
0x0B64E6
MOOCreatureMammothScript
Scn MOOCreatureMammothScript

ref mytarget
ref myidle
ref me

Begin Gamemode

if GetCombatTarget == mytarget
	return
endif

set mytarget to GetCombatTarget
if mytarget
	set me to GetFirstRef 69 1
	while me
		if me.GetDisabled == 0
		if me.GetDead == 0
		if me != player
		if me.GetInFaction MOOMammothFaction
		if GetDistance me < MOO.ini_mammoth_distance
			me.StartCombat mytarget
		endif
		endif
		endif
		endif
		endif

		set me to GetNextRef
	loop
endif

End
