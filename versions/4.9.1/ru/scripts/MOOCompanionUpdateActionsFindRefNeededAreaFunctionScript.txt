Maskar's Oblivion Overhaul.esp
0x55FAC7
MOOCompanionUpdateActionsFindRefNeededAreaFunctionScript
Scn MOOCompanionUpdateActionsFindRefNeededAreaFunctionScript

ref myactor

ref me
ref mybase

ref tempitem
float tempdistance
ref myitem
float mydistance

Begin Function { myactor }

set mydistance to 99999999

set me to GetFirstRef 70 1 ; inventory items
While me

	; SKIP ARROW PROJECTILE ( TO AVOID CRASHES )
	if me.GetProjectileType != 0

	; CHECK QUALIFYING
	if Call MOOCompanionUpdateActionsFindRefNeededCheckItemFunctionScript myactor me

		set tempitem to me
		set tempdistance to me.GetDistance myactor

		; CHECK DISTANCE
		if tempdistance <= mydistance
			set myitem to tempitem
			set mydistance to tempdistance
		endif
	endif
	endif

	set me to GetNextRef
Loop

SetFunctionValue myitem

End