Maskar's Oblivion Overhaul.esp
0x25C767
MOOUpdateFleeingCreaturesFunctionScript
Scn MOOUpdateFleeingCreaturesFunctionScript

ref attacker
ref target

ref me

Begin Function { attacker target }

set me to GetFirstRef 69 2
while me

	if me != attacker
	if ( me.GetDisabled == 0 ) && ( me.GetDead == 0 )
	if ( me.GetDisposition attacker == 100 ) && ( me.GetDisposition target < 50 )
	if me.GetDistance attacker < MOO.ini_npc_distance
	if eval ( me.GetInFaction CreatureFaction == 0 ) || ( me.GetModelPath == attacker.GetModelPath )
	if me.GetAV Intelligence <= 30
	if me.GetItemCount MOOTokenDiseaseNoDamage == 0

		me.StartCombat target

		if MOO.ini_ability_climb_npc
			attacker.AddItem MOOTokenCombatNpc 1
		else
			attacker.AddItem MOOTokenCombatNpcNoClimb 1
		endif

		if GetRandomPercent < 50
			me.AddItem MOOTokenFlee 1

			if MOO.ini_debug
				printc "[MOO] %n fleeing -> pulled by diseased ally." me
			endif
		endif

	endif
	endif
	endif
	endif
	endif
	endif
	endif

	set me to GetNextRef
loop

End