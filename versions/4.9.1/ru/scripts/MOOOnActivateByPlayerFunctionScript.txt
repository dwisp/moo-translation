Maskar's Oblivion Overhaul.esp
0x071C40
MOOOnActivateByPlayerFunctionScript
Scn MOOOnActivateByPlayerFunctionScript

ref me
ref myitem

ref mykey
short crafttype

ref mymapmarker
ref mytarget
ref mylinkeddoor

Begin Function { myitem me }

; BPN compatibility requirement
RunScriptLine "set MOO.BPNversion to BPN.currentversion"

if myitem

if MOO.ini_debug
	Call MOOPrintModelPathFunctionScript myitem.GetBaseObject
endif

if GetScript myitem == MOOObjectMarkerScript
	; MARKER
	Call MOOOnActivateMarkerFunctionScript myitem

else
	; CRAFT TOOL
	set crafttype to Call MOOCraftGetCraftTypeFunctionScript myitem 0 MOO.startresources
	if crafttype
		Call MOOOnActivateToolFunctionScript myitem crafttype
	endif
endif


if myitem.IsActor

	; dump factions
	if MOO.ini_debug == 2
		Call MOODumpFactionsFunctionScript myitem
	endif

	; UPDATE DIALOGUE TOPICS
	Call MOOUpdateDialogFunctionScript myitem

	; GUARD - REWARD
	if myitem.IsGuard
		Call MOOUpdateDialogGuardFunctionScript

	; MISSING PET
	elseif myitem.GetItemCount MOOTokenCreatureMissing
		Call MOOOnActivateMissingPetFunctionScript myitem

	; CRAFTING ONLY
	elseif MOO.ini_ability_crafting

		; SHEEP
		if myitem.CompareModelPath "\Sheep\Skeleton.nif"
			Call MOOOnActivateSheepFunctionScript myitem

		; CHICKEN
		elseif myitem.CompareModelPath "\Chicken\Skeleton.nif"
			Call MOOOnActivateChickenFunctionScript myitem

		; COW
		elseif myitem.CompareModelPath "\Cow\Skeleton.nif"
			if MOO.BPNversion < 640
				Call MOOOnActivateCowFunctionScript myitem
			endif

		endif
	endif

elseif myitem.IsContainer
	if myitem.GetLocked

		set mykey to myitem.GetOpenKey
		if mykey
		if player.GetItemCount mykey

			if GetScript mykey == KurtTestBookScript
				set MOO.mykey to mykey
			endif

			if MOO.ini_debug
				printc "[MOO] Unlocked with key. Traps removed."
			endif
			Call MOORemoveTrapsContainerFunctionScript myitem
		endif
		endif
	endif

elseif myitem.IsDoor
	if myitem.IsLoadDoor
		if myitem.GetLocked
			set MOO.mycell to 0
		endif

		set mylinkeddoor to myitem.GetLinkedDoor

		if myitem.IsInInterior == 0
			set MOO.redteleportermarker to mylinkeddoor
		endif

		if mylinkeddoor.IsDoor == 0
			Call MOOOnActivateDoorHouseFunctionScript myitem
		endif

		set MOO.mymapmarker to Call MOOGetMapMarkerForObjectFunctionScript myitem.GetLinkedDoor

		Call MOOGetCurrentDungeonFunctionScript myitem.GetLinkedDoor
		Call MOOGetCurrentRegionFunctionScript myitem.GetLinkedDoor
		Call MOOGetCurrentDungeonLevelFunctionScript myitem.GetLinkedDoor
	endif

elseif myitem.IsBook
	Call MOOOnActivateBookFunctionScript myitem

elseif myitem.IsActivator
	if myitem.CompareModelPath "Oblivion\Sigil\SigilStone01.NIF"
		Call MOOOnActivateSigilStoneFunctionScript
	endif

endif

endif

End