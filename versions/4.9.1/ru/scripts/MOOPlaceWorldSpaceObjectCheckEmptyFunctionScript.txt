Maskar's Oblivion Overhaul.esp
0x4F6E34
MOOPlaceWorldSpaceObjectCheckEmptyFunctionScript
Scn MOOPlaceWorldSpaceObjectCheckEmptyFunctionScript

ref myplacer

ref mycell

short totalstatics
short totaldoors

float myx
float myy
float myz
float waterheight

short fail

Begin Function { myplacer }

; CHECK CELL == 0
set mycell to myplacer.GetParentCell
if mycell == 0
	set fail to 1
endif

; CHECK CELL HAS WATER
if myplacer.ParentCellHasWater
	set myx to myplacer.GetPos x
	set myy to myplacer.GetPos y
	set myz to GetTerrainHeight myx myy
	set waterheight to myplacer.GetParentCellWaterHeight

	if waterheight > myz
		set fail to 1
	endif
endif

; CHECK STATICS AND DOORS
set totalstatics to GetNumRefsInCell mycell 28 0
set totaldoors to GetNumRefsInCell mycell 24 0 

if totalstatics != 0 || totaldoors != 0
	set fail to 1
endif


SetFunctionValue fail

End