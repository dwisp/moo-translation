Maskar's Oblivion Overhaul.esp
0x50FD25
MOOInitCellDungeonFunctionScript
Scn MOOInitCellDungeonFunctionScript

ref myplacer

Begin Function { myplacer }

; CHECK IF IN OUTPOST AND NEEDS CELLS ADDING
if Call MOOInitCellDungeonOutpostRequiredFunctionScript
	Call MOOInitCellDungeonOutpostCreateFunctionScript myplacer
	return ; can't be in cave and outpost at the same time
endif

; CHECK IF IN CAVE AND NEEDS CELLS ADDING
if MOO.ini_dungeon_extensions
if Call MOOInitDungeonExtensionRequiredFunctionScript
	Call MOOInitDungeonExtensionsFunctionScript
endif
endif

End