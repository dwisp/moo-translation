Maskar's Oblivion Overhaul.esp
0x50736C
MOOEventHarpyNestRockFunctionScript
Scn MOOEventHarpyNestRockFunctionScript

ref myplacer

ref mycell
ref me

short isplaced

Begin Function { myplacer }

; CYCLE THROUGH NEARBY ROCKS
MOOCellPlacerCreature.MoveTo myplacer
set mycell to MOOCellPlacerCreature.GetParentCell
if mycell == 0
	return
endif

set me to GetFirstRefInCell mycell 28

while me
	if me.GetDisabled == 0
	if me.CompareModelPath "Rocks\"
	if Call MOOEventHarpyNestRockFoundFunctionScript me.GetBaseObject

		set isplaced to Call MOOPlaceWorldSpaceReplaceRockWithHarpyNestFunctionScript me

		if isplaced
			break
		endif
	endif
	endif
	endif

	set me to GetNextRef
loop

SetFunctionValue isplaced

End