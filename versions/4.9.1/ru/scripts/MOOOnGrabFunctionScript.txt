Maskar's Oblivion Overhaul.esp
0x346CE5
MOOOnGrabFunctionScript
Scn MOOOnGrabFunctionScript

ref mytarget

Begin Function {  }

set mytarget to GetCrossHairRef

if mytarget == 0
	return
endif

; COMMAND FOLLOWER
if mytarget.IsActor
	if mytarget.GetItemCount MOOTokenCreatureTame
		set MOOMenu.show to 1 ; show taming menu
		set MOOMenu.mytarget to mytarget
		StartQuest MOOMenu
	endif

	return
endif

; PICK UP CRAFTING TOOL
if mytarget.GetScript != MOOObjectDeedScript ; do not grab deeds
	if Call MOOCraftGetCraftIdFunctionScript mytarget.GetBaseObject
		Call MOOOnGrabCraftingToolFunctionScript mytarget

	elseif mytarget.IsFlora
		Call MOOOnGrabFloraFunctionScript mytarget
	endif
endif

End