Maskar's Oblivion Overhaul.esp
0x22EA72
MOOGetNpcNameKhajiitFunctionScript
Scn MOOGetNpcNameKhajiitFunctionScript

short male

short i
string_var prefix
string_var suffix

string_var name

Begin Function { male }

set i to Rand 0 15
if male
	if i == 0
		let prefix := "���'"
	elseif i == 1
		let prefix := "��''"
	elseif i == 2
		let prefix := "�''"
	elseif i == 3
		let prefix := "�'"
	elseif i == 4
		let prefix := "��''"
	elseif i == 5
		let prefix := "�'"
	elseif i == 6
		let prefix := "��'"
	elseif i == 7
		let prefix := "��'"
	elseif i == 8
		let prefix := "�'"
	else
		let prefix := "�''"
	endif
else
	if i == 0
		let prefix := "���"
	elseif i == 1
		let prefix := "��"
	elseif i == 2
		let prefix := "����"
	elseif i == 3
		let prefix := "���"
	elseif i == 4
		let prefix := "���"
	elseif i == 5
		let prefix := "���"
	elseif i == 6
		let prefix := "���"
	elseif i == 7
		let prefix := "���"
	elseif i == 8
		let prefix := "���"
	elseif i == 9
		let prefix := "���"
	elseif i == 10
		let prefix := "���"
	elseif i == 11
		let prefix := "����"
	elseif i == 12
		let prefix := "���"
	elseif i == 13
		let prefix := "���"
	else
		let prefix := "��"
	endif
endif

if male
	set i to Rand 0 16
	if i == 0
		let suffix := "���"
	elseif i == 1
		let suffix := "���"
	elseif i == 2
		let suffix := "�����"
	elseif i == 3
		let suffix := "�����"
	elseif i == 4
		let suffix := "������"
	elseif i == 5
		let suffix := "������"
	elseif i == 6
		let suffix := "�����"
	elseif i == 7
		let suffix := "������"
	elseif i == 8
		let suffix := "�����"
	elseif i == 9
		let suffix := "����"
	elseif i == 10
		let suffix := "�����"
	elseif i == 11
		let suffix := "����"
	elseif i == 12
		let suffix := "�����"
	elseif i == 13
		let suffix := "�����"
	elseif i == 14
		let suffix := "�����"
	else
		let suffix := "����"
	endif
else
	set i to Rand 0 15
	if i == 0
		let suffix := "����"
	elseif i == 1
		let suffix := "��"
	elseif i == 2
		let suffix := "���"
	elseif i == 3
		let suffix := "���"
	elseif i == 4
		let suffix := "���"
	elseif i == 5
		let suffix := "���"
	elseif i == 6
		let suffix := "���"
	elseif i == 7
		let suffix := "����"
	elseif i == 8
		let suffix := "�����"
	elseif i == 9
		let suffix := "����"
	elseif i == 10
		let suffix := "���"
	elseif i == 11
		let suffix := "����"
	elseif i == 12
		let suffix := "����"
	elseif i == 13
		let suffix := "����"
	else
		let suffix := "���"
	endif
endif

let name := $prefix + $suffix

set i to Rand 0 14
if i == 0
	let prefix := "�������"
elseif i == 1
	let prefix := "�������"
elseif i == 2
	let prefix := "����������"
elseif i == 3
	let prefix := "�������"
elseif i == 4
	let prefix := "�����"
elseif i == 5
	let prefix := "�����"
elseif i == 6
	let prefix := "��������"
elseif i == 7
	let prefix := "�������"
elseif i == 8
	let prefix := "��������"
elseif i == 9
	let prefix := "�������"
elseif i == 10
	let prefix := "�������"
elseif i == 11
	let prefix := "���������"
elseif i == 12
	let prefix := "�����"
else
	let prefix := "������"
endif

set i to Rand 0 15
if i == 0
	let suffix := "�������"
elseif i == 1
	let suffix := "��������"
elseif i == 2
	let suffix := "�������������"
elseif i == 3
	let suffix := "������"
elseif i == 4
	let suffix := "��������"
elseif i == 5
	let suffix := "��������"
elseif i == 6
	let suffix := "��������"
elseif i == 7
	let suffix := "���������"
elseif i == 8
	let suffix := "�������"
elseif i == 9
	let suffix := "����������"
elseif i == 10
	let suffix := "�������"
elseif i == 11
	let suffix := "��������"
elseif i == 12
	let suffix := "�������"
elseif i == 13
	let suffix := "�������"
else
	let suffix := "������"
endif

if male == 1
	let name := name + " " + prefix + "�� " + suffix + "�"
else
	let name := name + " " + prefix + "�� " + suffix + "��"
endif

SetFunctionValue name

sv_Destruct prefix
sv_Destruct suffix
sv_Destruct name

End