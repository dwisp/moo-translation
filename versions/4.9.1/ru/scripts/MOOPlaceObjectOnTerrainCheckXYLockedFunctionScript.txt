Maskar's Oblivion Overhaul.esp
0x4F973C
MOOPlaceObjectOnTerrainCheckXYLockedFunctionScript
Scn MOOPlaceObjectOnTerrainCheckXYLockedFunctionScript

ref me

short xylocked

Begin Function { me }

if me == CookingPot || me == CookingFirePlank01 || me == FireOpenSmall256
	set xylocked to 1
endif

SetFunctionValue xylocked


End