Maskar's Oblivion Overhaul.esp
0x241F6D
MOOSetNpcMagicConjurationFunctionScript
Scn MOOSetNpcMagicConjurationFunctionScript

ref me
short elementaltype
short creaturetype

ref myclass

Begin Function { me elementaltype creaturetype }

;		20: Alteration
;		21: Conjuration
;		22: Destruction
;		23: Illusion
;		24: Mysticism
;		25: Restoration

;  12: Armorer
;  13: Athletics
;  14: Blade
;  15: Block
;  16: Blunt
;  18: HeavyArmor
;  28: Marksman

set myclass to me.GetClass

if IsMajorC 18 myclass ; heavy armor
	if me.GetAVC 18 >= 75
		me.AddSpellNS StandardBoundArmor4ExpertTest

		if IsMajorC 15
			me.AddSpellNS StandardBoundArmorShieldExpert
		endif

	elseif me.GetAVC 18 >= 50
		me.AddSpellNS StandardBoundArmor3JourneymanTest
		me.AddSpellNS StandardBoundArmorCuriassJourneyman

	elseif me.GetAVC 18 >= 25
		me.AddSpellNS StandardBoundArmor2ApprenticeTest
		me.AddSpellNS StandardBoundArmorGreavesApprentice

	else
		me.AddSpellNS StandardBoundArmor1NoviceTest
		me.AddSpellNS StandardBoundArmorBootsNovice
		me.AddSpellNS StandardBoundArmorGauntletsNovice
		me.AddSpellNS StandardBoundArmorHelmetNovice
	endif
endif

;  12: Armorer
;  13: Athletics
;  14: Blade
;  15: Block
;  16: Blunt
;  18: HeavyArmor
;  28: Marksman

if IsMajorC 28 myclass ; marksman
	me.AddSpellNS StandardBoundBowJourneyman
elseif ( IsMajorC 15 myclass ) ; block
	if IsMajorC 16 myclass
		me.AddSpellNS StandardBoundMaceJourneyman
	else
		me.AddSpellNS StandardBoundDaggerNovice
	endif
else
	if IsMajorC 16 myclass
		me.AddSpellNS StandardBoundAxeApprentice
	else
		me.AddSpellNS StandardBoundSwordExpert
	endif
endif

if creaturetype == 0

	me.AddSpellNS AbBirthsignNecromancer

	if me.GetAVC 21 == 100
		me.AddSpellNS StandardSummonWraithGloomMaster
		me.AddSpellNS StandardSummonLichMaster
	elseif me.GetAVC 21 >= 75
		me.AddSpellNS StandardSummonSkeletonChampionExpert
		me.AddSpellNS StandardSummonSkeletonHeroExpert
		me.AddSpellNS StandardSummonWraithFadedExpert
	elseif me.GetAVC 21 >= 50
		me.AddSpellNS StandardSummonSkeletonGuardianJourneyman
		me.AddSpellNS StandardSummonZombieHeadlessJourneyman
	else
		me.AddSpellNS StandardSummonGhostApprentice
		me.AddSpellNS StandardSummonSkeletonApprentice
		me.AddSpellNS StandardSummonZombieApprentice
	endif

else

	me.AddSpellNS AbBirthsignConjurer

	if me.GetAVC 21 == 100
		if elementaltype == 2
			me.AddSpellNS StandardSummonAtronachStormMaster
		else
			me.AddSpellNS StandardSummonDremoraLordMaster
			me.AddSpellNS StandardSummonXivilaiMaster
		endif
	elseif me.GetAVC 21 >= 75
		if elementaltype == 1
			me.AddSpellNS StandardSummonAtronachFrostExpert
		else
			me.AddSpellNS StandardSummonClannfearExpert
			me.AddSpellNS StandardSummonDaedrothExpert
			me.AddSpellNS StandardSummonSpiderDaedraExpert
		endif
	elseif me.GetAVC 21 >= 50
		if elementaltype == 0
			me.AddSpellNS StandardSummonAtronachFlameJourneyman
		else
			me.AddSpellNS StandardSummonDremoraJourneyman
		endif
	else
		me.AddSpellNS StandardSummonScampApprentice
	endif
endif

End