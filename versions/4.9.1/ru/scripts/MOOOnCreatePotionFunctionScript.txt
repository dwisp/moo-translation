Maskar's Oblivion Overhaul.esp
0x26196C
MOOOnCreatePotionFunctionScript
Scn MOOOnCreatePotionFunctionScript

ref mypotion
short newbase

ref addedpotion

Begin Function { mypotion newbase }

if MOO.ini_disease_groups
if GetMagicItemEffectCount mypotion == 1
if MagicItemHasEffect CUDI mypotion

	if player.GetAV Alchemy == 100
		set addedpotion to MOOPotionCraftCureDiseaseAcute
	elseif player.GetAV Alchemy >= 75
		set addedpotion to MOOPotionCraftCureDiseaseSerious
	else
		set addedpotion to MOOPotionCraftCureDiseaseMild
	endif

	player.AddItemNS addedpotion 1

endif
endif
endif

; also run to adjust potion weight
set MOOPotion.mypotion to mypotion
set MOOPotion.addedpotion to addedpotion
StartQuest MOOPotion

End