Maskar's Oblivion Overhaul.esp
0x2FCB79
MOOMenuScript
Scn MOOMenuScript

float fQuestDelayTime

short show
short menu
ref mytarget

short mylevel
; short myxp
float myhealth
float temp
string_var stringname
string_var stringlevel
string_var stringhappiness

string_var line1
string_var line2

string_var inputtext

string_var linepart1
string_var linepart2
short startpos
short length

short button
short followercode

short i
ref follower1
ref follower2
ref follower3
ref follower4
ref follower5
ref follower6
ref follower7
ref follower8

; info
short tamecode
short elementtype
short foodtype
short combattype
short difficultytype 
ref mydisease
short myXP
short levelXP
short myconfidence
short learnspells
short opendoors

string_var stringconfidence
string_var stringfoodtype
string_var stringelementtype
string_var stringcombattype
string_var stringopendoors
string_var stringlearnspells
string_var stringdifficultytype 

Begin Gamemode

if show

	set menu to show
	set show to 0

	if menu == 1 ; taming

		set myhealth to mytarget.GetMaxAV Health
		set foodtype to Call MOOTameGetTamableFunctionScript mytarget 2

		; LINE 1
		let linepart1 := Call MOOTameRemoveTagsFromNameFunctionScript mytarget.GetName
		let line1 := linepart1

		; LINE 2
		set mylevel to mytarget.GetLevel

		let linepart2 := Call MOOGetFollowerHappinessFunctionScript mytarget

		let line2 := "������� " + $mytarget.GetLevel + " - " + linepart2

		set length to ( ( sv_length line1 ) - ( sv_length line2 ) ) / 1.7
		if length < 0
			set length to length * -1
			while length
				sv_Insert " " line1
				set length to length - 1
			loop
		else
			while length
				sv_Insert " " line2
				set length to length - 1
			loop
		endif

		; if pet is diseased
		set mydisease to Call MOOTameGetDiseaseFunctionScript mytarget
		if mydisease != MOOSpellNoname
			messageex "%z, ������, ������." linepart1
		endif

		if MOO.ini_follower_info
			if foodtype == 5 ; mimic
				messageboxex "%z%r%z|������|���������|����������|������ ���|��������|���������|������� ������|��������� �� ����|����������|������" line1 line2
			else
				messageboxex "%z%r%z|������|���������|����������|������ ���|��������|���������|������� ������|��������� �� ����|����������|������" line1 line2
			endif
		else
			if foodtype == 5 ; mimic
				messageboxex "%z%r%z|������|���������|����������|������ ���|��������|���������|������� ������|��������� �� ����|������" line1 line2
			else
				messageboxex "%z%r%z|������|���������|����������|������ ���|��������|���������|������� ������|��������� �� ����|������" line1 line2
			endif
		endif

	elseif menu == 2 ; taming - name
		if IsTextInputInUse == 0
			OpenTextInput "������: |�������!" 0 MOO.ini_follower_name
		endif

	elseif menu == 10 ; tracking - main

		Call MOOTameUpdateTrackablesFunctionScript

		let follower1 := MOOTame.trackables[1]
		let follower2 := MOOTame.trackables[2]
		let follower3 := MOOTame.trackables[3]
		let follower4 := MOOTame.trackables[4]
		let follower5 := MOOTame.trackables[5]
		let follower6 := MOOTame.trackables[6]
		let follower7 := MOOTame.trackables[7]
		let follower8 := MOOTame.trackables[8]

		set i to MOOTame.totaltrackables
		if i == 0
			messagebox "�� ���� ���������� ��� �� ������� ������ ���������."

		elseif MOOTame.trackedactor
			if i == 1
				messageboxex "���� ����� ���������?|%n|���������� ������������ %n" follower1 MOOTame.trackedactor
			elseif i == 2
				messageboxex "���� ����� ���������?|%n|%n|���������� ������������ %n" follower1 follower2 MOOTame.trackedactor
			elseif i == 3
				messageboxex "���� ����� ���������?|%n|%n|%n|���������� ������������ %n" follower1 follower2 follower3 MOOTame.trackedactor
			elseif i == 4
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|���������� ������������ %n" follower1 follower2 follower3 follower4 MOOTame.trackedactor
			elseif i == 5
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|���������� ������������ %n" follower1 follower2 follower3 follower4 follower5 MOOTame.trackedactor
			elseif i == 6
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|%n|���������� ������������ %n" follower1 follower2 follower3 follower4 follower5 follower6 MOOTame.trackedactor
			elseif i == 7
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|%n|%n|���������� ������������ %n" follower1 follower2 follower3 follower4 follower5 follower6 follower7 MOOTame.trackedactor
			else
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|%n|%n|%n|���������� ������������ %n" follower1 follower2 follower3 follower4 follower5 follower6 follower7 follower8 MOOTame.trackedactor
			endif

		else
			if i == 1
				messageboxex "���� ����� ���������?|%n|������" follower1
			elseif i == 2
				messageboxex "���� ����� ���������?|%n|%n|������" follower1 follower2
			elseif i == 3
				messageboxex "���� ����� ���������?|%n|%n|%n|������" follower1 follower2 follower3
			elseif i == 4
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|������" follower1 follower2 follower3 follower4
			elseif i == 5
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|������" follower1 follower2 follower3 follower4 follower5
			elseif i == 6
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|%n|������" follower1 follower2 follower3 follower4 follower5 follower6
			elseif i == 7
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|%n|%n|������" follower1 follower2 follower3 follower4 follower5 follower6 follower7
			else
				messageboxex "���� ����� ���������?|%n|%n|%n|%n|%n|%n|%n|%n|������" follower1 follower2 follower3 follower4 follower5 follower6 follower7 follower8
			endif
		endif

	endif
endif

set button to GetButtonPressed
if button == -1
	return
endif

;printc "%.0f" button

if menu == 1 ; main taming menu

		; name
		; follow
		; stop
		; stay
		; guard
		; eat
		; drop / open container
		; release
		; info
		; cancel

	if button == 0
		set show to 2
		return

	elseif button == 1 ; FOLLOW
		messageex "%z ������ ������� �� ����." linepart1
		if mytarget.GetBoundingRadius < 43
			mytarget.AddScriptPackage MOOAIFollowerFollowSmall ; small pets like cats
		elseif mytarget.GetBoundingRadius < 200
			mytarget.AddScriptPackage MOOAIFollowerFollow
		else
			mytarget.AddScriptPackage MOOAIFollowerFollowLarge ; more distance
		endif

	elseif button == 2 ; STOP
		if mytarget.IsInCombat
			Call MOOTameStopCombatFunctionScript mytarget
		endif
		mytarget.AddScriptPackage MOOAIFollowerWander

		if mytarget.GetItemCount MOOTokenCreatureGuarding
			mytarget.RemoveItemNS MOOTokenCreatureGuarding 1
		endif

		if mytarget.GetItemCount MOOTokenCreatureNamed
			mytarget.SetName $linepart1
		else
			mytarget.SetNameEx "%z (�������)" linepart1
		endif

	elseif button == 3 ; STAY
		messageex "%z ����� ������ �����." linepart1
		mytarget.AddScriptPackage MOOAIFollowerStay

	elseif button == 4 ; GUARD
		if mytarget.GetItemCount MOOTokenCreatureGuarding == 0
			mytarget.AddItemNS MOOTokenCreatureGuarding 1
		endif
		mytarget.SetNameEx "%z (��������)" linepart1

	elseif button == 5 ; EAT
		if mytarget.GetItemCount MOOTokenCreatureUnconscious
			messageex "%z ��� ��������." linepart1

		elseif mytarget.IsInCombat
			messageex "%z ������ � ���." linepart1

		elseif Call MOOTameFeedActorFunctionScript mytarget

			if foodtype == 5
				messageex "� ������������ %z �������." linepart1
				PlaySound ITMGoldDown
			else
				messageex "� ������������ %z." linepart1
			endif
			set followercode to mytarget.GetItemCount MOOTokenCreatureTame
			let MOOTame.followershappiness[followercode] := 9

			set temp to myhealth * MOO.ini_healing_food / 100
			mytarget.ModAV2 Health temp

		else
			message "� ���� ��� ���������� ���."
		endif

	elseif button == 6 ; DROP / OPEN CONTAINER
		if mytarget.GetItemCount MOOTokenCreatureUnconscious
			messageex "%z ��� ��������." linepart1
		else
			if foodtype == 5
				Call MOOMessageCarryWeightFunctionScript mytarget 1

				mytarget.RemoveAllItems MOOCellChestStorage
				mytarget.AddItemNS MOOTokenCreatureMimic 1

				MOOCellChestStorage.Activate Player
			else
				Call MOOTameDropItemsFunctionScript mytarget
			endif
		endif

	elseif button == 7 ; RELEASE
		set menu to 3
		messageboxex "����� ��������� %z �� ����?|��|���" linepart1
		return

	elseif button == 8 ; INFO / CANCEL

	if MOO.ini_follower_info

	set tamecode to mytarget.GetItemCount MOOTokenCreatureTame
	set elementtype to mytarget.GetItemCount MOOTokenCreatureElement

	set combattype to Call MOOTameGetTamableFunctionScript mytarget 3
	set difficultytype to Call MOOTameGetTamableFunctionScript mytarget 5

	set mydisease to Call MOOTameGetDiseaseFunctionScript mytarget

	set levelXP to Call MOOTameGetRequiredXPFunctionScript mytarget
	let myXP := levelXP - MOOTame.FollowersXP[tamecode]

	set myconfidence to mytarget.GetAV Confidence

	if combattype == 2
		set learnspells to 1
	else
		set learnspells to 0
	endif

	if mytarget.GetAV Intelligence < 30
		set opendoors to 0
	else
		set opendoors to 1
	endif

	if elementtype == 1
		let stringelementtype := "�����"
	elseif elementtype == 2
		let stringelementtype := "�����"
	elseif elementtype == 3
		let stringelementtype := "��-��"
	elseif elementtype == 4
		let stringelementtype := "����"
	else
		let stringelementtype := "���"
	endif

	if combattype == 1
		let stringcombattype := "������"
	elseif combattype == 2
		let stringcombattype := "����������"
	else
		let stringcombattype := "�������"
	endif

	if foodtype == 1
		let stringfoodtype := "������"
	elseif foodtype == 2
		let stringfoodtype := "����������"
	elseif foodtype == 3
		let stringfoodtype := "��������"
	elseif foodtype == 4
		let stringfoodtype := "������� ����"
	elseif foodtype == 5
		let stringfoodtype := "������"
	else
		let stringfoodtype := "���"
	endif

	if myconfidence >= 100
		let stringconfidence := "��. �������"
	elseif myconfidence >= 75
		let stringconfidence := "�������"
	elseif myconfidence >= 50
		let stringconfidence := "�������"
	elseif myconfidence >= 25
		let stringconfidence := "���������"
	else
		let stringconfidence := "��. ���������"
	endif

	if learnspells
		let stringlearnspells := "��"
	else
		let stringlearnspells := "���"
	endif

	if opendoors
		let stringopendoors := "��"
	else
		let stringopendoors := "���"
	endif

	if difficultytype == 1
		let stringdifficultytype := "������"
	elseif difficultytype == 2
		let stringdifficultytype := "�������"
	elseif difficultytype == 3
		let stringdifficultytype := "�������"
	else
		let stringdifficultytype := "�����������"
	endif

	messageboxex "                ���������� %r                                     %r ��������: %z %r ����: %.0f / %.0f %r ��� ��������: %z %r ������� �������: %z %r ����� ���: %z %r �������: %n %r ������ � ���������: %z %r ���� ����������: %z %r ��������� ����������: %z" stringconfidence myXP levelXP stringfoodtype stringelementtype stringcombattype mydisease stringopendoors stringlearnspells stringdifficultytype

	endif

	elseif button == 9 ; CANCEL
		; do nothing
	endif

elseif menu == 2

	if button == 0 ; user has finished

		set inputtext to GetInputText
		if mytarget.GetItemCount MOOTokenCreatureGuarding
			mytarget.SetNameEx "%z (��������)" inputtext
		else
			mytarget.SetNameEx "%z" inputtext
		endif

		if mytarget.GetItemCount MOOTokenCreatureNamed == 0
			mytarget.AddItemNS MOOTokenCreatureNamed 1
		endif

		CloseTextInput
	endif

elseif menu == 3 ; release menu

	if button == 0 ; yes
		messageex "%z ������, ��� ��� ������� ���� �����!" linepart1
		Call MOOTameReleaseFollowerFunctionScript mytarget
	endif

elseif menu == 10

	; button 0 to 8 -> last is cancel or stop tracking
	; MOOTame.totaltrackables -> +1 == cancel

	if button == MOOTame.totaltrackables
		if MOOTame.trackedactor ; stop tracking (else cancel)
			MOOCellFollowerMarker.MoveTo MOOCellMarker
			set MOOTame.trackedactor to 0

			if GetActiveQuest == MOOTame
				ClearActiveQuest
			endif
		endif

	elseif button < MOOTame.totaltrackables
		let MOOTame.trackedactor := MOOTame.trackables[button + 1]
		SetStageDate MOOTame 10 0 GameDay GameMonth GameYear
		SetActiveQuest MOOTame
	endif

	Call MOOUpdateQuestTrackingTextFunctionScript

endif

set menu to 0
StopQuest MOOMenu

End


Begin Menumode

if menu == 2
	UpdateTextInput
endif

End
;<CSEBlock>
;<CSECaretPos> 94 </CSECaretPos>
;</CSEBlock>