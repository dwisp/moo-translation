Maskar's Oblivion Overhaul.esp
0x50C670
MOOOutpostCreateRoomFunctionScript
Scn MOOOutpostCreateRoomFunctionScript

short roomx1
short roomy1
short sizex
short sizey
short myx
short myy

short roomx2
short roomy2

short myheight

short i
short j

Begin Function { roomx1 roomy1 sizex sizey myx myy }

set roomx2 to roomx1 + sizex - 1
set roomy2 to roomy1 + sizey - 1


let myheight := MOO.dungeonmapheight[myx][myy]

; create room

; 4		12		8
; 6		15		9
; 2		3		1

; CREATE MIDDLE
set i to 1
while i < sizey - 1

	set j to 1
	while j < sizex - 1
		let MOO.dungeonmap[roomx1 + j][roomy1 + i] := 15 << 4
		let MOO.dungeonmapheight[roomx1 + j][roomy1 + i] := myheight

		set j to j + 1
	loop

	set i to i + 1
loop

; CREATE BORDERS
set i to 0
while i < sizey

	; LEFT
	let MOO.dungeonmap[roomx1][roomy1 + i] := 6 << 4
	let MOO.dungeonmapheight[roomx1][roomy1 + i] := myheight

	; RIGHT
	let MOO.dungeonmap[roomx2][roomy1 + i] := 9 << 4
	let MOO.dungeonmapheight[roomx2][roomy1 + i] := myheight

	set i to i + 1
loop

set i to 0
while i < sizex

	; BOTTOM
	let MOO.dungeonmap[roomx1 + i][roomy1] := 3 << 4
	let MOO.dungeonmapheight[roomx1 + i][roomy1] := myheight

	; TOP
	let MOO.dungeonmap[roomx1 + i][roomy2] := 12 << 4
	let MOO.dungeonmapheight[roomx1 + i][roomy2] := myheight

	set i to i + 1
loop



; CREATE CORNERS ( 2, 1, 4, 8 )
; SW
let MOO.dungeonmap[roomx1][roomy1] := 2 << 4
let MOO.dungeonmapheight[roomx1][roomy1] :=myheight
; SE
let MOO.dungeonmap[roomx2][roomy1] := 1 << 4
let MOO.dungeonmapheight[roomx2][roomy1] := myheight
; NW
let MOO.dungeonmap[roomx1][roomy2] := 4 << 4
let MOO.dungeonmapheight[roomx1][roomy2] := myheight
; NE
let MOO.dungeonmap[roomx2][roomy2] := 8 << 4
let MOO.dungeonmapheight[roomx2][roomy2] := myheight


End