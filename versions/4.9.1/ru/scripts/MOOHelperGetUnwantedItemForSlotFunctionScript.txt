Maskar's Oblivion Overhaul.esp
0x549636
MOOHelperGetUnwantedItemForSlotFunctionScript
Scn MOOHelperGetUnwantedItemForSlotFunctionScript

ref me
ref mymerchant
short myslot

short mymerchantbuyall

ref myitem
ref myclass

short myskillblock
short myskillheavy
short myskilllight
short myskillblade
short myskillblunt
short myskillmarksman
short myskillmagic

ref mybase
short myrating
ref tempitem
short temprating

short myweapontype
short calculaterating

short myitemslot 

ref unwanteditem

Begin Function { me mymerchant myslot }

; INIT
set myclass to me.GetClass

if IsMajorC 15 myclass
	set myskillblock to 1
endif
if IsMajorC 18 myclass
	set myskillheavy to 1
endif
if IsMajorC 27 myclass
	set myskilllight to 1
endif
if IsMajorC 14 myclass
	set myskillblade to 1
endif
if IsMajorC 16 myclass
	set myskillblunt to 1
endif
if IsMajorC 28 myclass
	set myskillmarksman to 1
endif
if GetClassSpecialization myclass == 1
	set myskillmagic to 1
endif

; BUY STOLEN ITEMS
if mymerchant.GetAV Responsibility < 30
	set mymerchantbuyall to 1
endif


foreach myitem <- me
	if myitem.IsArmor || myitem.IsWeapon || myitem.IsAmmo
	if myitem.IsScripted == 0 && myitem.IsQuestItem == 0 && myitem.IsPlayable2
	if myitem.GetFullGoldValue > 0
	if ( myitem.GetOwner == 0 || myitem.GetOwner == me.GetBaseObject ) || mymerchantbuyall
	if myitem.GetEnchantment == 0

	set myitemslot to myitem.GetEquipmentSlot
	if eval myitemslot & myslot == myslot

	; EQUIPPED ITEMS CAN BE UNWANTED IF NOT HAVING SKILL
	if myitem.IsEquipped == 0

		set mybase to myitem.GetBaseObject

		; ARMOR
		if myitem.IsArmor

			; HEAVY
			if myitem.GetArmorType && myskillheavy == 0
				set unwanteditem to mybase
				break

			; LIGHT
			elseif myitem.GetArmorType == 0 && myskilllight == 0
				set unwanteditem to mybase
				break

			; CHECK RATING
			else
				set calculaterating to 1
			endif

		; AMMO
		elseif myitem.IsAmmo
			if myskillmarksman
				set calculaterating to 1
			else
				set unwanteditem to mybase
			endif

		; WEAPON
		elseif myitem.IsWeapon

			set myweapontype to myitem.GetWeaponType

			; BLADE
			if ( myweapontype == 0 || myweapontype == 1 ) && myskillblade == 0
				set unwanteditem to mybase

			; BLUNT
			elseif ( myweapontype == 2 || myweapontype == 3 ) && myskillblunt == 0
				set unwanteditem to mybase

			; BOW
			elseif myweapontype == 5 && myskillmarksman == 0
				set unwanteditem to mybase

			; STAFF
			elseif myweapontype == 4 && ( myskillmagic == 0 || myitem.GetEnchantment == 0 )
				set unwanteditem to mybase

			; CHECK RATING
			else
				set calculaterating to 1
			endif
		endif


		; CALCULATE RATING
		if unwanteditem
			break
		elseif calculaterating
			set calculaterating to 0

			; DO NOT CHECK ENCHANTED ITEMS ( ONLY STAVES ABOVE )
			set myrating to Call MOOHelperGetEquipmentRatingFunctionScript mybase

			if tempitem == 0
				set tempitem to mybase
				set temprating to myrating
			elseif myrating > temprating
				set unwanteditem to tempitem
				break
			elseif myrating < temprating
				set unwanteditem to mybase
				break
			endif
		endif

	endif
	endif

	endif
	endif
	endif
	endif
	endif
loop


SetFunctionValue unwanteditem

End