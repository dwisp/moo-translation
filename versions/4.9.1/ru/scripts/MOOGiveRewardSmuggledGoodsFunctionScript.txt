Maskar's Oblivion Overhaul.esp
0x422CAF
MOOGiveRewardSmuggledGoodsFunctionScript
Scn MOOGiveRewardSmuggledGoodsFunctionScript

short totalgoods
short totalgold

short rankmod

Begin Function {  }

set totalgoods to player.GetItemCount MOOSmuggledGoods
set totalgold to totalgoods * MOO.ini_reward_smuggledgoods
player.RemoveItemNS MOOSmuggledGoods totalgoods
player.AddItem Gold001 totalgold

if MOO.XPloaded
	Call MOOXPSmuggledGoodsFunctionScript totalgoods
endif

; IMPERIAL LEGION RANK
set rankmod to totalgoods * 2
Call MOOUpdateFactionRanksLegionFunctionScript rankmod

End