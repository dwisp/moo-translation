Maskar's Oblivion Overhaul.esp
0x311A4B
MOOTameLowerHappinessFunctionScript
Scn MOOTameLowerHappinessFunctionScript

ref me

short i

Begin Function { me }

set i to me.GetItemCount MOOTokenCreatureTame

let MOOTame.followershappiness[i] := MOOTame.followershappiness[i] - 1

if eval MOOTame.followershappiness[i] < 0
	let MOOTame.followershappiness[i] := 0
endif

if MOO.ini_debug
	printc "[MOO] %n has become less happy." me
endif

End