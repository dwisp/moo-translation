Maskar's Oblivion Overhaul.esp
0x50CD46
MOOOutpostCreateRoomAtPositionFunctionScript
Scn MOOOutpostCreateRoomAtPositionFunctionScript

short oldx
short oldy
short mydirection
short addkey

short myx
short myy

short myrand
float randmax

short roomx
short roomy
short sizex
short sizey

short maxoptionsx
short maxoptionsy
short pickedoptionx
short pickedoptiony

short allowed

Begin Function { oldx oldy mydirection addkey }

; INIT
set myx to oldx
set myy to oldy

; DETERMINE SIZE
set sizex to Rand 3 6.999
set sizey to Rand 3 6.999

set maxoptionsx to sizex - 2
set maxoptionsy to sizey - 2

set randmax to maxoptionsx + 0.999
set pickedoptionx to Rand 1 maxoptionsx
set randmax to maxoptionsy + 0.999
set pickedoptiony to Rand 1 maxoptionsy


; ADD CORRIDOR IF KEY
if addkey
	if mydirection == 1
		set myy to myy + 1
	elseif mydirection == 2
		set myx to myx + 1
	elseif mydirection == 4
		set myy to myy - 1
	else ; 8
		set myx to myx - 1
	endif
endif


if mydirection == 1
	set roomy to myy + 1
	set roomx to myx - pickedoptionx

elseif mydirection == 2
	set roomx to myx + 1
	set roomy to myy - pickedoptiony

elseif mydirection == 4
	set roomy to myy - sizey
	set roomx to myx - pickedoptionx

else ; 8
	set roomx to myx - sizex
	set roomy to myy - pickedoptiony

endif

; CHECK IF ROOM FITS
; CHECK IF STORAGE ROOM AND NO EVENT IN DOORWAY - OR -
; CHECK IF GAUNTLET ROOM AND PLATE TILE EMPTY

if Call MOOOutpostCreateRoomCheckFitFunctionScript roomx roomy sizex sizey
if eval ( addkey == 0 && MOO.dungeonmapevent[myx][myy] == 0 ) || ( addkey && MOO.dungeonmap[myx][myy] == 0 )

	set allowed to 1

	; ADD CORRIDOR FOR POSSIBLE GAUNTLET
	if addkey
		if mydirection == 1 || mydirection == 4
			let MOO.dungeonmap[myx][myy] := 5
		else
			let MOO.dungeonmap[myx][myy] := 10
		endif

		let MOO.dungeonmapheight[myx][myy] := MOO.dungeonmapheight[oldx][oldy]
		let MOO.dungeonmap[oldx][oldy] := MOO.dungeonmap[oldx][oldy] | mydirection

		; CREATE KEY FOR GATE
		Call MOOOutpostCreateRoomAddKeyFunctionScript myx myy roomx roomy sizex sizey
	endif

	; CREATE ROOM
	Call MOOOutpostCreateRoomFunctionScript roomx roomy sizex sizey myx myy

	; CREATE ENTRANCE
	Call MOOOutpostCreateRoomEntranceFunctionScript myx myy mydirection

endif
endif


SetFunctionValue allowed

End