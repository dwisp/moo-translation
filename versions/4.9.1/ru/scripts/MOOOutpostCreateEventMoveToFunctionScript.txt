Maskar's Oblivion Overhaul.esp
0x518844
MOOOutpostCreateEventMoveToFunctionScript
Scn MOOOutpostCreateEventMoveToFunctionScript

ref myplacer
ref mystatic
short myeventid
short myx
short myy

ref myplacedobject

Begin Function { myplacer mystatic myeventid myx myy }

; LEVER
if myeventid == MOO.id_dungeon_lever
	set myplacedobject to Call MOOHelperMovePlacerAndPlaceStaticFunctionScript myplacer mystatic 114 0 100 0 0 0 ; x y z anglex angley anglez

; KEYHOLE - BRONZE, IRON, SILVER, GOLD
elseif myeventid == MOO.id_dungeon_keyhole_bronze || myeventid == MOO.id_dungeon_keyhole_iron || myeventid == MOO.id_dungeon_keyhole_silver || myeventid == MOO.id_dungeon_keyhole_gold
	set myplacedobject to Call MOOHelperMovePlacerAndPlaceStaticFunctionScript myplacer mystatic 111 0 100 0 0 0

; TELEPORTERS
elseif myeventid == MOO.id_dungeon_teleporter_blue || myeventid == MOO.id_dungeon_teleporter_red || myeventid == MOO.id_dungeon_actor
	set myplacedobject to Call MOOHelperMovePlacerAndPlaceStaticFunctionScript myplacer mystatic 0 0 128 0 0 0

; TREASURE
elseif myeventid == MOO.id_dungeon_treasure_bronze || myeventid == MOO.id_dungeon_treasure_iron || myeventid == MOO.id_dungeon_treasure_silver || myeventid == MOO.id_dungeon_treasure_gold
	set myplacedobject to Call MOOHelperMovePlacerAndPlaceStaticFunctionScript myplacer mystatic 0 0 50 0 0 0

; EVERYTHING ELSE
else
	set myplacedobject to myplacer.PlaceAtMe mystatic
endif

SetFunctionValue myplacedobject

End