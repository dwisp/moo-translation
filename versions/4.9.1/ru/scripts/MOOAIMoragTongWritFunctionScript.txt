Maskar's Oblivion Overhaul.esp
0x5A2701
MOOAIMoragTongWritFunctionScript
Scn MOOAIMoragTongWritFunctionScript

ref mytarget

Begin Function {  }

; SELECT TARGET
set mytarget to Call MOOAIMoragTongWritGetTargetFunctionScript
if mytarget == 0
	return
endif

set MOO.writtarget to mytarget

let MOO.writname := mytarget.GetName
set MOO.writreward to 0

; ADD WRIT
player.AddItem MOOWritofExecutionPlayer 1

; ADD TOKEN TO TARGET
mytarget.AddItemNS MOOTokenMoragTongTarget 1

; UPDATE WRIT
Call MOOInitWritofExecutionFunctionScript

End