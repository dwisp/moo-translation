Maskar's Oblivion Overhaul.esp
0x225AD2
MOOUpdateQuestRegionsFunctionScript
Scn MOOUpdateQuestRegionsFunctionScript

; set MOO.myregion to 10 ; Hammerfell
; set MOO.myregion to 11 ; Skyrim	
; set MOO.myregion to 12 ; Morrowind
; set MOO.myregion to 13 ; Black Marsh
; set MOO.myregion to 14 ; Elsweyr
; set MOO.myregion to 15 ; Valenwood ; skip
; set MOO.myregion to 20 ; Great Forest (1+2)
; set MOO.myregion to 21 ; West Weald (East)
; set MOO.myregion to 22 ; West Weald (West)
; set MOO.myregion to 23 ; Gold Coast
; set MOO.myregion to 24 ; Topal Bay ; skip
; set MOO.myregion to 25 ; Niban Bay
; set MOO.myregion to 26 ; Niban Basin
; set MOO.myregion to 27 ; Imperial City

; MOO.creatureregions[MOO.myregion] ; 1 Daedra 2 Goblins 3 Undead 4 Undead 5 Gnarls
; MOO.questtyperegions[MOO.myregion] ; 1 Stolen 2 Wanted 3 Lost 4 Missing
; MOO.queststatusregions[MOO.myregion] ; 0 created 1 placed in-game 2 dead/picked up 
; MOO.questtargetregions[MOO.myregion] ; object of npc/item
; MOO.questtargetrefregions[MOO.myregion] ; reference of npc or container (for item -> npc or chest type)
; MOO.questdayregions[MOO.myregion] ; GameDaysPassed -> when cell was last reset

short i
short questtype
ref mytarget
float mydays

Begin Function {  }

if ( GetNumRefsInCell Vilverin 24 0 ) == 0
	set MOO.mygameday to -1
	return
endif

set i to 10
while i < 27

	if eval ( MOO.questtyperegions[i] == 1 ) || ( MOO.questtyperegions[i] == 3 ) ; stolen or lost item

		if eval ( MOO.questdayregions[i] <= GameDaysPassed )
			set questtype to Call MOOGetQuestTypeFunctionScript
			Call MOOInitQuestFunctionScript i questtype

			if MOO.ini_debug
				printc "[MOO] Quest target reset for region: %.0f" i
			endif

		elseif eval MOO.queststatusregions[i] == 1

			set mydays to Rand MOO.ini_notice_item_place_min MOO.ini_notice_item_place_max

			if MOO.ini_debug
				printc "[MOO] Quest item was placed -> %.1f days to go before reset region: %.0f" mydays i
			endif

			let MOO.queststatusregions[i] := 2
			let MOO.questdayregions[i] := GameDaysPassed + mydays

		elseif eval MOO.queststatusregions[i] == 3

			set mydays to Rand MOO.ini_notice_item_found_min MOO.ini_notice_item_found_max

			if MOO.ini_debug
				printc "[MOO] Quest item was found -> %.1f days to go before reset region: %.0f" mydays i
			endif

			let MOO.queststatusregions[i] := 4
			let MOO.questdayregions[i] := GameDaysPassed + mydays
		endif

	elseif eval MOO.questtyperegions[i] == 2 ; wanted npc

		let mytarget := MOO.questtargetrefregions[i]

		if ( IsFormValid mytarget == 0 )

			set mydays to Rand MOO.ini_notice_npc_kill_min MOO.ini_notice_npc_kill_max

			if MOO.ini_debug
				printc "[MOO] Quest npc does no longer exist -> %.1f days to go before reset region: %.0f" mydays i
			endif

			let MOO.queststatusregions[i] := 2
			let MOO.questdayregions[i] := GameDaysPassed + mydays

		elseif eval ( MOO.questdayregions[i] <= GameDaysPassed )

			if MOO.ini_debug
				printc "[MOO] Quest npc is declared lost -> reset region: %.0f" i
			endif

			set questtype to Call MOOGetQuestTypeFunctionScript
			Call MOOInitQuestFunctionScript i questtype

		elseif eval ( mytarget.GetDead ) && ( MOO.queststatusregions[i] == 1 )

			set mydays to Rand MOO.ini_notice_npc_kill_min MOO.ini_notice_npc_kill_max

			if MOO.ini_debug
				printc "[MOO] Quest npc is dead -> %.1f days to go before reset region: %.0f" mydays i
			endif

			let MOO.queststatusregions[i] := 2
			let MOO.questdayregions[i] := GameDaysPassed + mydays
		endif

	elseif eval MOO.questtyperegions[i] == 4 ; missing actor (not yet added)

		if MOO.ini_debug
			printc "[MOO] Quest npc (not yet added type) -> reset region: %.0f" i
		endif

		set questtype to Call MOOGetQuestTypeFunctionScript
		Call MOOInitQuestFunctionScript i questtype

	elseif eval ( MOO.questtyperegions[i] == 0 ) ; nothing is happening

		if eval ( MOO.questdayregions[i] <= GameDaysPassed )

			set questtype to Call MOOGetQuestTypeFunctionScript
			Call MOOInitQuestFunctionScript i questtype

		endif
	endif


	set i to i + 1
	if i == 15
		set i to 20
	elseif i == 24
		set i to 25
	endif
loop

; update notices based on new quests
Call MOONoticeFillNoticesFunctionScript

End