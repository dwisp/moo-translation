Maskar's Oblivion Overhaul.esp
0x48D4FA
MOOSetNpcAIAdventurerFunctionScript
Scn MOOSetNpcAIAdventurerFunctionScript

ref me
short myrand
short myrandmin
short myrandmax

short i
ref myclass
short mymod

Begin Function { me }

; aggression
; confidence
; energy level
; responsibility

; update aggression for wanted npcs in public places
me.SetAV Aggression 5

; CONFIDENCE
set myrand to Rand MOO.ini_companion_confidence_min MOO.ini_companion_confidence_max
set myrand to myrand + ( me.GetLevel * MOO.ini_companion_confidence_level )
if myrand > 100
	set myrand to 100
endif
me.SetAV Confidence myrand

; RESPONSIBILITY

set myclass to me.GetClass
set i to Ar_Size MOO.npcclassesmod
while i
	set i to i - 1
	if eval MOO.npcclasses[i] == myclass
		let mymod := MOO.npcclassesmod[i]
	endif
loop

if mymod == 0
	set myrandmin to 0
	set myrandmax to 100
elseif mymod > 0
	set myrandmin to mymod
	set myrandmax to 110
elseif mymod < 0
	set myrandmin to 0
	set myrandmax to 100 + mymod
endif

set myrand to Rand myrandmin myrandmax
if myrand < 0
	set myrand to 0
elseif myrand > 100
	set myrand to 100
endif
me.SetAV Responsibility myrand

if MOO.ini_debug
	printc "[MOO] %n has class %n and a responsibility of %.0f." me myclass myrand
endif

End