Maskar's Oblivion Overhaul.esp
0x0730C9
MOOActivatorTrapPoison02Script
Scn MOOActivatorTrapPoison02Script

float time
short hits

Begin Gamemode

if MOO.togglepoison02
	set MOO.togglepoison02 to 0
	set hits to 9
	set time to 0
	PlaySound TRPGasRelease
	return
endif

if hits <= 0
	return
endif

set time to time + GetSecondsPassed

if time >= 1.7
	set hits to hits - 1
	set time to 0
	Call MOOTrapHitFunctionScript -18
endif

if hits <= 0
	MoveTo MOOCellMarker
endif

End