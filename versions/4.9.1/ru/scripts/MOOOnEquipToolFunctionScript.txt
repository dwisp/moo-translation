Maskar's Oblivion Overhaul.esp
0x368213
MOOOnEquipToolFunctionScript
Scn MOOOnEquipToolFunctionScript

short crafttype

Begin Function { crafttype }

if crafttype == MOO.id_SewingKit
	Call MOOOnEquipSewingKitFunctionScript
elseif crafttype == MOO.id_TinkersTools
	Call MOOOnEquipTinkersToolsFunctionScript
elseif crafttype == MOO.id_FletchingJig
	Call MOOOnEquipFletchingJigFunctionScript
elseif crafttype == MOO.id_Scissors
	Call MOOOnEquipScissorsFunctionScript
elseif crafttype == MOO.id_GrainGrinder
	Call MOOOnEquipGrainGrinderFunctionScript
elseif crafttype == MOO.id_RollingPin
	Call MOOOnEquipRollingPinFunctionScript
elseif crafttype == MOO.id_Skillet
	Call MOOOnEquipSkilletFunctionScript
elseif crafttype == MOO.id_Quill
	Call MOOOnEquipQuillFunctionScript
endif

End