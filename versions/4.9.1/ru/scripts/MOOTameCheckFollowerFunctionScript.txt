Maskar's Oblivion Overhaul.esp
0x31AAFD
MOOTameCheckFollowerFunctionScript
Scn MOOTameCheckFollowerFunctionScript

ref me
short i

short myhappiness
float modhealth

Begin Function { me i }

if IsFormValid me
	if me.GetDead || me.Getdisabled
		Call MOOTameReleaseFollowerFunctionScript me

	else
		if MOO.ini_follower_shader
			Call MOOTameUpdateShadersFunctionScript me
		endif

		let myhappiness := MOOTame.followershappiness[i] - MOOTame.modhappiness
		if myhappiness < 0
			set myhappiness to 0
		elseif myhappiness > 9
			set myhappiness to 9
		endif

		if me.GetItemCount MOOTokenCreatureUnconscious
			if me.IsAnimPlaying
				me.RemoveItemNS MOOTokenCreatureUnconscious 1
			endif

		elseif me.GetItemCount MOOTokenCreatureDrinksRainwater

			if me.ParentCellHasWater
			if ( me.getpos z ) < me.GetParentCellWaterHeight
				set myhappiness to 9
			endif
			endif

			if ( me.IsInInterior == 0 ) && ( IsRaining )
				set myhappiness to 9
			endif
		endif

		; HEALTH REGENERATION BASED ON HAPINESS
		set modhealth to ( ( me.GetMaxAV Health ) / 900 ) * myhappiness * MOO.ini_follower_healthgain
		me.ModAvMod Health damage modhealth

		let MOOTame.followershappiness[i] := myhappiness

		; SPRINT BASED ON DISTANCE TO PLAYER CHARACTER
		if me.GetItemCount MOOTokenCreatureSprint
			if me.GetDistance player < 750
				me.RemoveItemNS MOOTokenCreatureSprint 1
				me.RemoveSpellNS MOOSpellSprintCreature
			endif

		elseif me.IsRunning
			if me.GetDistance player > 1500
				me.AddItemNS MOOTokenCreatureSprint 1
				me.AddSpellNS MOOSpellSprintCreature
			endif
		endif
	endif

else
	let MOOTame.followers[i] := 0

endif

End