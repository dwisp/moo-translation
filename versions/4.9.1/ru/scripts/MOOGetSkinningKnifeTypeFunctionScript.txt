Maskar's Oblivion Overhaul.esp
0x349CBF
MOOGetSkinningKnifeTypeFunctionScript
Scn MOOGetSkinningKnifeTypeFunctionScript

ref me

ref myweapon
short type

Begin Function { me }

set myweapon to me.GetEquippedObject 16
if myweapon == MOOWeapSkinningKnife
	set type to 1
endif

SetFunctionValue type

End