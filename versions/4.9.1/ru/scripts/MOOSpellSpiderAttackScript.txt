Maskar's Oblivion Overhaul.esp
0x177624
MOOSpellSpiderAttackScript
Scn MOOSpellSpiderAttackScript

ref me
float mymagicka

Begin ScriptEffectStart

set mymagicka to ( GetAV Magicka ) * -1
ModAV2 Magicka mymagicka

End