Maskar's Oblivion Overhaul.esp
0x553A2E
MOODungeonCreateClutterPlaceRandomFunctionScript
Scn MOODungeonCreateClutterPlaceRandomFunctionScript

ref myclutter
float myz

short found

short fail

Begin Function { myclutter myz }

if myclutter == cobweb01 || myclutter == cobweb02 || myclutter == cobweb03 || myclutter == cobweb04 || myclutter == cobweb05 || myclutter == cobweb06 || myclutter == cobweb07
	set found to 1
elseif IsFlora myclutter
	set found to 1
elseif myclutter == BedrollSideEntry || myclutter == BedrollCrawlEntry || myclutter == Root03
	set found to 1
endif

if found
if GetRandomPercent < 25 || myz < 0 ; under water or random chance
	set fail to 1
endif
endif

SetFunctionValue fail

End