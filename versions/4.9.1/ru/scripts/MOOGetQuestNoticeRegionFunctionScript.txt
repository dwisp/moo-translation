Maskar's Oblivion Overhaul.esp
0x246AB3
MOOGetQuestNoticeRegionFunctionScript
Scn MOOGetQuestNoticeRegionFunctionScript

short myregion
short i

ref mynotice


Begin Function { myregion i }

if myregion == 0
	set mynotice to 0
	
elseif eval MOO.questtargetregions[myregion]
	if eval MOO.questtyperegions[myregion] == 1
		set mynotice to MOONoticeObjectStolen
	elseif eval MOO.questtyperegions[myregion] == 2
		set mynotice to MOONoticeObjectWanted
	elseif eval MOO.questtyperegions[myregion] == 3
		set mynotice to MOONoticeObjectLost
	endif
endif

if mynotice
	let MOONotice.noticestemp[i] := mynotice
	let MOONotice.regionstemp[i] := myregion
	set i to i + 1
endif

SetFunctionValue i

End