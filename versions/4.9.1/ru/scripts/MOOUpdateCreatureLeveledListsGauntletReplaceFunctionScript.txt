Maskar's Oblivion Overhaul.esp
0x52D87C
MOOUpdateCreatureLeveledListsGauntletReplaceFunctionScript
Scn MOOUpdateCreatureLeveledListsGauntletReplaceFunctionScript

ref mylist
ref myentry

Begin Function { mylist myentry }

ClearLeveledList mylist
AddToLeveledList mylist myentry 1 1

End