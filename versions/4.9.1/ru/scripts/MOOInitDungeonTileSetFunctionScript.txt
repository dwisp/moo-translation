Maskar's Oblivion Overhaul.esp
0x4CF13C
MOOInitDungeonTileSetFunctionScript
Scn MOOInitDungeonTileSetFunctionScript

short i

Begin Function {  }

; x 0 00 xxxx	normal tunnel tile ( + 0 )
; x 0 01 xxxx	tunnel: load door tile ( + 16 )
; x 0 10 xxxx	tunnel: special 1: narrow for door ( + 32 )

; x 1 00 xxxx	normal room tile ( + 64 )
; x 1 01 xxxx	room: load door tile -> ladder ( + 64 + 16 )
; x 1 10 xxxx	room: special 1: connection to tunnel ( + 64 + 32 )

; NEW

; [xxxx] [xxxx] [xxxx]
; [special] [room] [tunnel]


; =================================================================
; [0000] [0000] [xxxx]	normal tunnel tile
; =================================================================

; 1
let MOO.tilesetid[0] :=		1
let MOO.tilesetstatic[0] :=		CWHallEndCap01D
let MOO.tilesetchance[0] :=	100
let MOO.tilesetid[1] :=		2
let MOO.tilesetstatic[1] :=		CWHallEndCap01A
let MOO.tilesetchance[1] :=	100
let MOO.tilesetid[2] :=		4
let MOO.tilesetstatic[2] :=		CWHallEndCap01B
let MOO.tilesetchance[2] :=	100
let MOO.tilesetid[3] :=		8
let MOO.tilesetstatic[3] :=		CWHallEndCap01C
let MOO.tilesetchance[3] :=	100

; 2
let MOO.tilesetid[4] :=		3
let MOO.tilesetstatic[4] :=		CWHallTurn01D
let MOO.tilesetchance[4] :=		100
let MOO.tilesetid[5] :=		6
let MOO.tilesetstatic[5] :=		CWHallTurn01A
let MOO.tilesetchance[5] :=		100
let MOO.tilesetid[6] :=		9
let MOO.tilesetstatic[6] :=		CWHallTurn01C
let MOO.tilesetchance[6] :=		100
let MOO.tilesetid[7] :=		12
let MOO.tilesetstatic[7] :=		CWHallTurn01B
let MOO.tilesetchance[7] :=		100

let MOO.tilesetid[8] :=		5
let MOO.tilesetstatic[8] :=		CWHall01B
let MOO.tilesetchance[8] :=		100
let MOO.tilesetid[9] :=		10
let MOO.tilesetstatic[9] :=		CWHall01A
let MOO.tilesetchance[9] :=		100

; 3
let MOO.tilesetid[10] :=		7
let MOO.tilesetstatic[10] :=		CWHall3way01D
let MOO.tilesetchance[10] :=		100
let MOO.tilesetid[11] :=		11
let MOO.tilesetstatic[11] :=		CWHall3way01C
let MOO.tilesetchance[11] :=		100
let MOO.tilesetid[12] :=		13
let MOO.tilesetstatic[12] :=		CWHall3way01B
let MOO.tilesetchance[12] :=		100
let MOO.tilesetid[13] :=		14
let MOO.tilesetstatic[13] :=		CWHall3way01A
let MOO.tilesetchance[13] :=		100

; 4
let MOO.tilesetid[14] :=		15
let MOO.tilesetstatic[14] :=		MOOStaticDungeonCave4Way
let MOO.tilesetchance[14] :=	100


; =================================================================
; [0000] [xxxx] [0000]	normal room tile ( bits 4, 5, 6 and 7 )
; =================================================================

; 1
let MOO.tilesetid[15] :=		1 << 4
let MOO.tilesetstatic[15] :=		crmcornerinside01c
let MOO.tilesetchance[15] :=	50
let MOO.tilesetid[16] :=		1 << 4
let MOO.tilesetstatic[16] :=		crmcornerinside02c
let MOO.tilesetchance[16] :=	100
let MOO.tilesetid[17] :=		2 << 4
let MOO.tilesetstatic[17] :=		crmcornerinside01d
let MOO.tilesetchance[17] :=	50
let MOO.tilesetid[18] :=		2 << 4
let MOO.tilesetstatic[18] :=		crmcornerinside02d
let MOO.tilesetchance[18] :=	100
let MOO.tilesetid[19] :=		4 << 4
let MOO.tilesetstatic[19] :=		crmcornerinside01a
let MOO.tilesetchance[19] :=	50
let MOO.tilesetid[20] :=		4 << 4
let MOO.tilesetstatic[20] :=		crmcornerinside02a
let MOO.tilesetchance[20] :=	100
let MOO.tilesetid[21] :=		8 << 4
let MOO.tilesetstatic[21] :=		crmcornerinside01b
let MOO.tilesetchance[21] :=	50
let MOO.tilesetid[22] :=		8 << 4
let MOO.tilesetstatic[22] :=		crmcornerinside02b
let MOO.tilesetchance[22] :=	100


; 2
let MOO.tilesetid[23] :=		3 << 4
let MOO.tilesetstatic[23] :=		crmwall01c
let MOO.tilesetchance[23] :=	25
let MOO.tilesetid[24] :=		3 << 4
let MOO.tilesetstatic[24] :=		crmwall02c
let MOO.tilesetchance[24] :=	25
let MOO.tilesetid[25] :=		3 << 4
let MOO.tilesetstatic[25] :=		crmwall03c
let MOO.tilesetchance[25] :=	25
let MOO.tilesetid[26] :=		3 << 4
let MOO.tilesetstatic[26] :=		crmwall05c
let MOO.tilesetchance[26] :=	100

let MOO.tilesetid[27] :=		6 << 4
let MOO.tilesetstatic[27] :=		crmwall01d
let MOO.tilesetchance[27] :=	25
let MOO.tilesetid[28] :=		6 << 4
let MOO.tilesetstatic[28] :=		crmwall02d
let MOO.tilesetchance[28] :=	25
let MOO.tilesetid[29] :=		6 << 4
let MOO.tilesetstatic[29] :=		crmwall03d
let MOO.tilesetchance[29] :=	25
let MOO.tilesetid[30] :=		6 << 4
let MOO.tilesetstatic[30] :=		crmwall05d
let MOO.tilesetchance[30] :=	100

let MOO.tilesetid[31] :=		9 << 4
let MOO.tilesetstatic[31] :=		crmwall01b
let MOO.tilesetchance[31] :=	25
let MOO.tilesetid[32] :=		9 << 4
let MOO.tilesetstatic[32] :=		crmwall02b
let MOO.tilesetchance[32] :=	25
let MOO.tilesetid[33] :=		9 << 4
let MOO.tilesetstatic[33] :=		crmwall03b
let MOO.tilesetchance[33] :=	25
let MOO.tilesetid[34] :=		9 << 4
let MOO.tilesetstatic[34] :=		crmwall05b
let MOO.tilesetchance[34] :=	100

let MOO.tilesetid[35] :=		12 << 4
let MOO.tilesetstatic[35] :=		crmwall01a
let MOO.tilesetchance[35] :=	25
let MOO.tilesetid[36] :=		12 << 4
let MOO.tilesetstatic[36] :=		crmwall02a
let MOO.tilesetchance[36] :=	25
let MOO.tilesetid[37] :=		12 << 4
let MOO.tilesetstatic[37] :=		crmwall03a
let MOO.tilesetchance[37] :=	25
let MOO.tilesetid[38] :=		12 << 4
let MOO.tilesetstatic[38] :=		crmwall05a
let MOO.tilesetchance[38] :=	100

; NEW TILES
let MOO.tilesetid[39] :=		5 << 4
let MOO.tilesetstatic[39] :=		MOOcrmcornerinside01e
let MOO.tilesetchance[39] :=	100
let MOO.tilesetid[40] :=		10 << 4
let MOO.tilesetstatic[40] :=		MOOcrmcornerinside01f
let MOO.tilesetchance[40] :=	100

; 3
let MOO.tilesetid[41] :=		7 << 4
let MOO.tilesetstatic[41] :=		crmcorneroutside01d
let MOO.tilesetchance[41] :=	50
let MOO.tilesetid[42] :=		7 << 4
let MOO.tilesetstatic[42] :=		crmcorneroutside02d
let MOO.tilesetchance[42] :=	100

let MOO.tilesetid[43] :=		11 << 4
let MOO.tilesetstatic[43] :=		crmcorneroutside01c
let MOO.tilesetchance[43] :=	33
let MOO.tilesetid[44] :=		11 << 4
let MOO.tilesetstatic[44] :=		crmcorneroutside02c
let MOO.tilesetchance[44] :=	33
let MOO.tilesetid[45] :=		11 << 4
let MOO.tilesetstatic[45] :=		crmcorneroutside03c
let MOO.tilesetchance[45] :=	100

let MOO.tilesetid[46] :=		13 << 4
let MOO.tilesetstatic[46] :=		crmcorneroutside01b
let MOO.tilesetchance[46] :=	50
let MOO.tilesetid[47] :=		13 << 4
let MOO.tilesetstatic[47] :=		crmcorneroutside02b
let MOO.tilesetchance[47] :=	100

let MOO.tilesetid[48] :=		14 << 4
let MOO.tilesetstatic[48] :=		crmcorneroutside01a
let MOO.tilesetchance[48] :=	50
let MOO.tilesetid[49] :=		14 << 4
let MOO.tilesetstatic[49] :=		crmcorneroutside02a
let MOO.tilesetchance[49] :=	100

; 4
let MOO.tilesetid[50] :=		15 << 4
let MOO.tilesetstatic[50] :=		crmfloorstalag01a
let MOO.tilesetchance[50] :=	16
let MOO.tilesetid[51] :=		15 << 4
let MOO.tilesetstatic[51] :=		crmfloorstalag02a
let MOO.tilesetchance[51] :=	16
let MOO.tilesetid[52] :=		15 << 4
let MOO.tilesetstatic[52] :=		crmcolumn01a
let MOO.tilesetchance[52] :=	24
let MOO.tilesetid[53] :=		15 << 4
let MOO.tilesetstatic[53] :=		crmfloor01a
let MOO.tilesetchance[53] :=	100

; =================================================================
; [0000] [xxxx] [xxxx] room: special 1: connection to tunnel
; =================================================================

let MOO.tilesetid[54] :=		1 + ( 12 << 4 )
let MOO.tilesetstatic[54] :=		crmexitwide01a
let MOO.tilesetchance[54] :=	100
let MOO.tilesetid[55] :=		2 + ( 9 << 4 )
let MOO.tilesetstatic[55] :=		crmexitwide01b
let MOO.tilesetchance[55] :=	100
let MOO.tilesetid[56] :=		4 + ( 3 << 4 )
let MOO.tilesetstatic[56] :=		crmexitwide01c
let MOO.tilesetchance[56] :=	100
let MOO.tilesetid[57] :=		8 + ( 6 << 4 )
let MOO.tilesetstatic[57] :=		crmexitwide01d
let MOO.tilesetchance[57] :=	100

; =================================================================
; [xxxx] [xxxx] [0000] room: load door tile -> ladder
; =================================================================

let MOO.tilesetid[58] :=		( 4 << 4 ) + ( 1 << 8 )
let MOO.tilesetstatic[58] :=		crmcornerinladderexit01a
let MOO.tilesetchance[58] :=	100
let MOO.tilesetid[59] :=		( 8 << 4 ) + ( 1 << 8 )
let MOO.tilesetstatic[59] :=		crmcornerinladderexit01b
let MOO.tilesetchance[59] :=	100
let MOO.tilesetid[60] :=		( 1 << 4 ) + ( 1 << 8 )
let MOO.tilesetstatic[60] :=		crmcornerinladderexit01c
let MOO.tilesetchance[60] :=	100
let MOO.tilesetid[61] :=		( 2 << 4 ) + ( 1 << 8 )
let MOO.tilesetstatic[61] :=		crmcornerinladderexit01d
let MOO.tilesetchance[61] :=	100





return




; =================================================================
; x 0 01 xxxx	tunnel: load door tile ( + 16 )
; =================================================================
set i to 16

let MOO.tilesetid[15] :=		i + 10
let MOO.tilesetstatic[15] :=	cwhalldoor01a
let MOO.tilesetchance[15] :=	100
let MOO.tilesetid[16] :=		i + 5
let MOO.tilesetstatic[16] :=	cwhalldoor01b
let MOO.tilesetchance[16] :=	100

; =================================================================
; x 0 10 xxxx	tunnel: special 1: narrow for door ( + 32 )
; =================================================================
set i to 32

let MOO.tilesetid[17] :=		i + 2
let MOO.tilesetstatic[17] :=	cwentrance01a
let MOO.tilesetchance[17] :=	100
let MOO.tilesetid[18] :=		i + 4
let MOO.tilesetstatic[18] :=	cwentrance01b
let MOO.tilesetchance[18] :=	100
let MOO.tilesetid[19] :=		i + 8
let MOO.tilesetstatic[19] :=	cwentrance01c
let MOO.tilesetchance[19] :=	100
let MOO.tilesetid[20] :=		i + 1
let MOO.tilesetstatic[20] :=	cwentrance01d
let MOO.tilesetchance[20] :=	100







End