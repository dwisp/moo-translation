Maskar's Oblivion Overhaul.esp
0x31443B
MOOTameUpdateShadersFunctionScript
Scn MOOTameUpdateShadersFunctionScript

ref me

ref myeffect
ref effectitem
short mylevelups
short myelement
short i

Begin Function { me }

set mylevelups to me.GetItemCount MOOTokenCreatureXP
set myelement to me.GetItemCount MOOTokenCreatureElement

if myelement <= 0 || mylevelups <= 0
	return
endif

if mylevelups < 5
	set mylevelups to 0
elseif mylevelups < 10
	set mylevelups to 1
else
	set mylevelups to 2
endif

let myeffect := MOOTame.levelupeffects[mylevelups + ( 3 * ( myelement - 1 ) ) ]

if myeffect

	set i to ar_Size MOOTame.levelupeffects
	while i
		set i to i - 1
		let effectitem := MOOTame.levelupeffects[i]
		if myeffect != effectitem && me.HasEffectShader effectitem
			me.StopMagicShaderVisuals effectitem
		endif
	loop

	if me.HasEffectShader myeffect == 0
		me.PlayMagicShaderVisuals myeffect
	endif
endif

End