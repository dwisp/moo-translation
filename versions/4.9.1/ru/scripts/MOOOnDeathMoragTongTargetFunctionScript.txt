Maskar's Oblivion Overhaul.esp
0x5A5002
MOOOnDeathMoragTongTargetFunctionScript
Scn MOOOnDeathMoragTongTargetFunctionScript

ref target
ref attacker

Begin Function { target attacker }

target.RemoveItemNS MOOTokenMoragTongTarget 1

if target == MOO.writtarget
	set MOO.writreward to target.GetLevel * MOO.ini_pc_moragtong_writ_gold

	if attacker == player
		MessageEx "%n ����(�) �� �������� � ����������� ��������� ����� ����." target
	else
		MessageEx "%n ����(�)." target
	endif

	player.RemoveItemNS MOOWritofExecutionPlayer 1
	player.AddItemNS MOOWritCompleted 1

endif

End