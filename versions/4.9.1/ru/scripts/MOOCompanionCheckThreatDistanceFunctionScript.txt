Maskar's Oblivion Overhaul.esp
0x572E6D
MOOCompanionCheckThreatDistanceFunctionScript
Scn MOOCompanionCheckThreatDistanceFunctionScript

ref me

ref myactor
short i
short mylevel
float tempdistance

float mydistance

Begin Function { me }

set mylevel to me.GetLevel
set mydistance to 1000000

let MOO.tempitem := GetHighActors
set i to ar_Size MOO.tempitem
while i
	set i to i - 1
	let myactor := MOO.tempitem[i]

	if myactor.GetDead || myactor.GetDisabled
		continue

	elseif myactor.GetAV Aggression >= 80

		if myactor.GetAV Confidence >= 50
		if ( mylevel < myactor.GetLevel ) || ( mylevel / myactor.GetLevel < 1.5 )

			set tempdistance to myactor.GetDistance me
			if tempdistance < mydistance
				set mydistance to tempdistance
			endif

		endif
		endif
	endif
loop

SetFunctionValue mydistance

End