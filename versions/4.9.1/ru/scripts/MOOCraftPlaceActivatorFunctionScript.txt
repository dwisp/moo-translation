Maskar's Oblivion Overhaul.esp
0x34660D
MOOCraftPlaceActivatorFunctionScript
Scn MOOCraftPlaceActivatorFunctionScript

ref myactivator

short craftid
short mydistance
float myheight
float myrotation
short mylocation

Begin Function { myactivator }

set craftid to Call MOOCraftGetCraftIdFunctionScript myactivator

let mydistance := MOO.toolsdistance[craftid]
let myheight := MOO.toolsheight[craftid]
let myrotation := MOO.toolsrotation[craftid]
let mylocation := MOO.toolslocation[craftid]

Call MOOPlaceMarkerBeforePCFunctionScript myactivator mydistance myheight myrotation mylocation

End