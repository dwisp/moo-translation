Maskar's Oblivion Overhaul.esp
0x0AA5C4
MOODeleteHighLeveledItemFunctionScript
Scn MOODeleteHighLeveledItemFunctionScript

ref leveledlist

short arraysize
short i
ref myitem
short mycount

Begin Function { leveledlist }

set i to GetNumLevItems leveledlist
while i > 0
	set i to i - 1
	if ( GetNthLevItemLevel i leveledlist ) <= MOO.ini_levelscaling_equipment_grunt_max
	if ( GetNthLevItemLevel i leveledlist ) >= MOO.ini_levelscaling_equipment_grunt_min
		let MOO.leveleditem[arraysize] := GetNthLevItem i leveledlist
		let MOO.leveledcount[arraysize] := GetNthLevItemCount i leveledlist
		set arraysize to arraysize + 1
	endif
	endif
loop

ClearLeveledList leveledlist

set i to 0
while i != arraysize
	let myitem := MOO.leveleditem[i]
	let mycount := MOO.leveledcount[i]
	AddToLeveledList leveledlist myitem 1 mycount
	set i to i + 1
loop

End