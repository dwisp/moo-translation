Maskar's Oblivion Overhaul.esp
0x50E89E
MOOOutpostCreateCellLinkDoorsFunctionScript
Scn MOOOutpostCreateCellLinkDoorsFunctionScript

ref mydoor1
ref mydoor2
ref mymarker1
ref mymarker2

Begin Function {  }

set mydoor1 to MOO.tempdoor1
set mydoor2 to MOO.tempdoor2
set mymarker1 to MOO.tempmarker1
set mymarker2 to MOO.tempmarker2

mydoor1.SetDoorTeleport mymarker2
mydoor2.SetDoorTeleport mymarker1

End