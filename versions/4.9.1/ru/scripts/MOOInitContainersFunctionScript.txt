Maskar's Oblivion Overhaul.esp
0x1699C6
MOOInitContainersFunctionScript
Scn MOOInitContainersFunctionScript

ref me
ref itemowner

ref lostitem
short rockfound
short metalfound

ref oldscript

Begin Function {  }

if eval MOO.questtyperegions[MOO.myregion] == 3 ; lost item
if eval MOO.queststatusregions[MOO.myregion] == 0
	let lostitem := MOO.questtargetregions[MOO.myregion]
endif
endif

set me to GetFirstRef 23 1
while me

	if me.CompareModelPath "MOO\WorldObjects\"
		set rockfound to 1
		Call MOOAddItemsToCellListFunctionScript me
	elseif me.CompareModelPath "silver" || me.CompareModelPath "gold"
		set metalfound to 1
		Call MOOAddItemsToCellListFunctionScript me
		Call MOOInitContainerVeinFunctionScript me
	endif

	if me.GetItemCount MOOTokenContainerLoot
		set me to GetNextRef
		continue
	endif

	if me.GetDisabled
		Call MOORemoveContainerFunctionScript me
		set me to GetNextRef
		continue
	endif

	set itemowner to me.GetOwner
	if itemowner == 0
		set itemowner to me.GetParentCellOwner
	endif

	if itemowner == player.GetBaseObject
		set me to GetNextRef
		continue
	endif

	if me.GetLocked

		if me.GetLockLevel > 99
			Call MOOInitContainerLockedFunctionScript me

		elseif itemowner == 0

			Call MOOUpdateLockFunctionScript me

			if me.GetItemCount MOOTokenTrap == 0

				Call MOOTokenTrapAddTreasureFunctionScript me
				Call MOOTokenTrapAddTrapsFunctionScript me

				if lostitem
					set lostitem to Call MOOAddLostItemToContainerFunctionScript me lostitem 5
				endif

				if ( MOO.ini_game_difficulty == 2 && MOO.safetyzone == 3 ) || ( MOO.ini_game_difficulty == 3 && MOO.safetyzone > 1 )
					Call MOOInitContainerAddUnleveledLootFunctionScript me
				endif

			endif
		endif

	elseif itemowner == 0
		Call MOOAddLockToContainerFunctionScript me
	endif

	me.AddItemNS MOOTokenContainerLoot 1

	set me to GetNextRef
loop

if MOO.mycelltype != 2
	return
endif

; update flora
set me to GetFirstRef 31 1
while me
	Call MOOAddItemsToCellListFunctionScript me
	set me to GetNextRef
loop


; update rocks (static) to veins (containers)
if rockfound
	return
endif


ar_Erase MOO.temparray


if MOO.ini_add_veins == 0
	return
endif

set me to GetFirstRef 28 1
while me

	if me.GetDisabled == 0 || ( me.GetDisabled && rockfound == 0 )
	if me.GetParentRef == 0
		if me.CompareModelPath "\Caves\CRock0"
			Call MOOInitContainerRockFunctionScript me metalfound
		elseif me.CompareModelPath "\AyleidRuins\Interior\arrubblepile0"
			Call MOOInitContainerRubbleFunctionScript me
		endif
	endif
	endif

	set me to GetNextRef
loop

End