Maskar's Oblivion Overhaul.esp
0x4C83F1
MOOOnEquipBroomFunctionScript
Scn MOOOnEquipBroomFunctionScript

Begin Function {  }

Call MOOOnEquipBroomOnTypeFunctionScript 19 ; Apparatus
Call MOOOnEquipBroomOnTypeFunctionScript 20 ; Armor
Call MOOOnEquipBroomOnTypeFunctionScript 21 ; Book
Call MOOOnEquipBroomOnTypeFunctionScript 22 ; Clothing
Call MOOOnEquipBroomOnTypeFunctionScript 25 ; Ingredient
Call MOOOnEquipBroomOnTypeFunctionScript 27 ; Misc
Call MOOOnEquipBroomOnTypeFunctionScript 33 ; Weapon
Call MOOOnEquipBroomOnTypeFunctionScript 34 ; Ammo
Call MOOOnEquipBroomOnTypeFunctionScript 38 ; SoulGem
Call MOOOnEquipBroomOnTypeFunctionScript 39 ; Key
Call MOOOnEquipBroomOnTypeFunctionScript 40 ; AlchemyItem
Call MOOOnEquipBroomOnTypeFunctionScript 42 ; SigilStone

message "� ������� ������� ������."
PlaySound "NPCHumanHorseDismount"

End