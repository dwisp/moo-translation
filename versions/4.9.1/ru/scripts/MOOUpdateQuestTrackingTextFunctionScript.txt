Maskar's Oblivion Overhaul.esp
0x305422
MOOUpdateQuestTrackingTextFunctionScript
Scn MOOUpdateQuestTrackingTextFunctionScript

string_var mystring
ref me

Begin Function {  }

if MOOTame.trackedactor
	set me to MOOTame.trackedactor
	let mystring := "� ������ ������ � ���������� " + $me + "."

	SetStageText MOOTame 10 0 $mystring
	SetStage MOOTame 10
else
	SetStageText MOOTame 10 0 "� ������ ������ � ������ �� ����������."
endif

sv_Destruct mystring

End