Maskar's Oblivion Overhaul.esp
0x31445E
MOOTameUpdateAbility3FunctionScript
Scn MOOTameUpdateAbility3FunctionScript

ref me

short i
short mylevel
ref myspell
short myelement

Begin Function { me }

;3 buff
;- 1 fire shield					MOOSpell1SelfShieldFire
;- 2 frost shield					MOOSpell1SelfShieldFrost
;- 3 shock shield				MOOSpell1SelfShieldShock
;- 4 shield						MOOSpell1SelfShield
;- 5 reflect damage			MOOSpell1SelfReflectDamage
;- 6 reflect spells				MOOSpell1SelfReflectSpell
;- 7 invisibility					MOOSpell1SelfInvisibility
;- 8 heal							MOOSpell1SelfRestoreHealth
;- 9 light							MOOSpell1SelfLight

; if i == 5 || i == 11 || i == 17 || i == 23 || i == 29

; set i based on other spells

set myelement to me.GetItemCount MOOTokenCreatureElement

if myelement
	set i to Rand 4 9.99
	if i == 4
		set i to myelement
	endif
else
	set i to Rand 4 9.99
endif

set mylevel to me.GetLevel

if mylevel == 5
	if i == 1
		set myspell to MOOSpell1SelfShieldFire
	elseif i == 2
		set myspell to MOOSpell1SelfShieldFrost
	elseif i == 3
		set myspell to MOOSpell1SelfShieldShock
	elseif i == 4
		set myspell to MOOSpell1SelfShield
	elseif i == 5
		set myspell to MOOSpell1SelfReflectDamage
	elseif i == 6
		set myspell to MOOSpell1SelfReflectSpell
	elseif i == 7
		set myspell to MOOSpell1SelfInvisibility
	elseif i == 8
		set myspell to MOOSpell1SelfRestoreHealth
	elseif i == 9
		set myspell to MOOSpell1SelfLight
	endif
endif

if mylevel == 11
	if i == 1
		set myspell to MOOSpell2SelfShieldFire
	elseif i == 2
		set myspell to MOOSpell2SelfShieldFrost
	elseif i == 3
		set myspell to MOOSpell2SelfShieldShock
	elseif i == 4
		set myspell to MOOSpell2SelfShield
	elseif i == 5
		set myspell to MOOSpell2SelfReflectDamage
	elseif i == 6
		set myspell to MOOSpell2SelfReflectSpell
	elseif i == 7
		set myspell to MOOSpell2SelfInvisibility
	elseif i == 8
		set myspell to MOOSpell2SelfRestoreHealth
	elseif i == 9
		set myspell to MOOSpell2SelfLight
	endif
endif

if mylevel == 17
	if i == 1
		set myspell to MOOSpell3SelfShieldFire
	elseif i == 2
		set myspell to MOOSpell3SelfShieldFrost
	elseif i == 3
		set myspell to MOOSpell3SelfShieldShock
	elseif i == 4
		set myspell to MOOSpell3SelfShield
	elseif i == 5
		set myspell to MOOSpell3SelfReflectDamage
	elseif i == 6
		set myspell to MOOSpell3SelfReflectSpell
	elseif i == 7
		set myspell to MOOSpell3SelfInvisibility
	elseif i == 8
		set myspell to MOOSpell3SelfRestoreHealth
	elseif i == 9
		set myspell to MOOSpell3SelfLight
	endif
endif

if mylevel == 23
	if i == 1
		set myspell to MOOSpell4SelfShieldFire
	elseif i == 2
		set myspell to MOOSpell4SelfShieldFrost
	elseif i == 3
		set myspell to MOOSpell4SelfShieldShock
	elseif i == 4
		set myspell to MOOSpell4SelfShield
	elseif i == 5
		set myspell to MOOSpell4SelfReflectDamage
	elseif i == 6
		set myspell to MOOSpell4SelfReflectSpell
	elseif i == 7
		set myspell to MOOSpell4SelfInvisibility
	elseif i == 8
		set myspell to MOOSpell4SelfRestoreHealth
	elseif i == 9
		set myspell to MOOSpell4SelfLight
	endif
endif

if mylevel >= 29
	if i == 1
		set myspell to MOOSpell5SelfShieldFire
	elseif i == 2
		set myspell to MOOSpell5SelfShieldFrost
	elseif i == 3
		set myspell to MOOSpell5SelfShieldShock
	elseif i == 4
		set myspell to MOOSpell5SelfShield
	elseif i == 5
		set myspell to MOOSpell5SelfReflectDamage
	elseif i == 6
		set myspell to MOOSpell5SelfReflectSpell
	elseif i == 7
		set myspell to MOOSpell5SelfInvisibility
	elseif i == 8
		set myspell to MOOSpell5SelfRestoreHealth
	elseif i == 9
		set myspell to MOOSpell5SelfLight
	endif
endif


; set ability
if myspell
if i >= 1 && i < 4
	me.AddItemNS MOOTokenCreatureAbility3 1 ; element spell
else
	me.AddItemNS MOOTokenCreatureAbility3 2 ; other
endif
endif


SetFunctionValue myspell

End
