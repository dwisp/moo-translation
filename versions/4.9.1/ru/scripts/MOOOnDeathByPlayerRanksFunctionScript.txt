Maskar's Oblivion Overhaul.esp
0x45E4BF
MOOOnDeathByPlayerRanksFunctionScript
Scn MOOOnDeathByPlayerRanksFunctionScript

ref target

Begin Function { target }

; NOTE: REGIONAL FACTIONS WITH RANKS SKIPPED -> SEE ONDEATHFACTION


; PLAYER KILLED GUARD - UPDATE RANKS
if target.IsGuard
	Call MOOOnDeathByPlayerRanksGuardFunctionScript target
	return
endif

; PLAYER KILLED OTHER - UPDATE RANK
Call MOOOnDeathByPlayerRanksOtherFunctionScript target

End