Maskar's Oblivion Overhaul.esp
0x226F5D
MOOGetApparelCuirassFunctionScript
Scn MOOGetApparelCuirassFunctionScript

short i
ref baseitem

Begin Function {  }

set i to Rand 0 3.99

if i == 0
	set baseitem to ChainmailCuirass
elseif i == 1
	set baseitem to ElvenCuirass
elseif i == 2
	set baseitem to DwarvenCuirass
else
	set baseitem to OrcishCuirass
endif

SetFunctionValue baseitem

End