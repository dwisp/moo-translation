Maskar's Oblivion Overhaul.esp
0x593E9B
MOOOnEquipImbuementToolImbueGetItemFunctionScript
Scn MOOOnEquipImbuementToolImbueGetItemFunctionScript

ref myitem

ref mylist
short i
short j


ref mynewitem

Begin Function { myitem }

; FIND ITEM IN 

while i < MOO.totalimbuements

	let mylist := MOO.imbuementsid[i]

	set j to GetLevItemIndexByForm mylist myitem

	if j >= 0
		; GET NEW ITEM FROM LIST
		set mynewitem to Call MOOOnEquipImbuementToolImbueGetItemNewFunctionScript mylist myitem
		if mynewitem && mynewitem != myitem
			break
		endif
	endif

	set i to i + 1

loop

SetFunctionValue mynewitem

End