Maskar's Oblivion Overhaul.esp
0x4C689C
MOODungeonUpdateTileOffsetFunctionScript
Scn MOODungeonUpdateTileOffsetFunctionScript

short myx
short myy

short mapx
short mapy
short modx
short mody

short i
short mytile

Begin Function { myx myy }

let mytile := MOO.dungeonmap[myx][myy]

set modx to myx * 512
set mody to myy * 512


; =================================================================
; TUNNEL
; =================================================================

; CAPS
if mytile == 2
	set modx to modx + 256
elseif mytile == 4
	set mody to mody - 256
elseif mytile == 8
	set modx to modx - 256
elseif mytile == 1
	set mody to mody + 256
endif

; =================================================================
; ROOM
; =================================================================

; 1
if eval mytile == 4 << 4
	set modx to modx + 256
	set mody to mody - 256
elseif eval mytile == 8 << 4
	set modx to modx - 256
	set mody to mody - 256
elseif eval mytile == 1 << 4
	set modx to modx - 256
	set mody to mody + 256
elseif eval mytile == 2 << 4
	set modx to modx + 256
	set mody to mody + 256
endif

; 2
if eval mytile == 12 << 4
	set mody to mody - 256
elseif eval mytile == 9 << 4
	set modx to modx - 256
elseif eval mytile == 3 << 4
	set mody to mody + 256
elseif eval mytile == 6 << 4
	set modx to modx + 256
endif

; NEW TILES
if eval mytile == 5 << 4
	; do nothing
elseif eval mytile == 10 << 4
	; do nothing
endif

; 3
if eval mytile == 14 << 4
	set modx to modx + 256
	set mody to mody - 256
elseif eval mytile == 13 << 4
	set modx to modx - 256
	set mody to mody - 256
elseif eval mytile == 11 << 4
	set modx to modx - 256
	set mody to mody + 256
elseif eval mytile == 7 << 4
	set modx to modx + 256
	set mody to mody + 256
endif

; 4
if eval mytile == 15 << 4
endif

; =================================================================
; [0000] [xxxx] [xxxx] room: special 1: connection to tunnel
; =================================================================

if eval mytile == 1 + ( 12 << 4 )
	set mody to mody - 256
elseif eval mytile == 2 + ( 9 << 4 )
	set modx to modx - 256
elseif eval mytile == 4 + ( 3 << 4 )
	set mody to mody + 256
elseif eval mytile == 8 + ( 6 << 4 )
	set modx to modx + 256
endif

; =================================================================
; [xxxx] [xxxx] [0000] room: load door tile -> ladder
; =================================================================

if eval mytile == ( 4 << 4 ) + ( 1 << 8 )
	set modx to modx + 256
	set mody to mody - 256
elseif eval mytile == ( 8 << 4 ) + ( 1 << 8 )
	set modx to modx - 256
	set mody to mody - 256
elseif eval mytile == ( 1 << 4 ) + ( 1 << 8 )
	set modx to modx - 256
	set mody to mody + 256
elseif eval mytile == ( 2 << 4 ) + ( 1 << 8 )
	set modx to modx + 256
	set mody to mody + 256
endif



; UPDATE
MOOCellPlacer.setpos x modx
MOOCellPlacer.setpos y mody





End