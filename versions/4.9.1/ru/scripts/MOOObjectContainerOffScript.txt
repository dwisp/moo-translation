Maskar's Oblivion Overhaul.esp
0x16557B
MOOObjectContainerOffScript
Scn MOOObjectContainerOffScript

ref mycontainer
float zpos

Begin OnReset

set mycontainer to GetSelf

set zpos to mycontainer.GetPos z
if zpos < -1000000
	if MOO.ini_debug
		printc "[MOO] Moved container back into position (Mimics disabled)."
	endif
	set zpos to zpos + 10000000
	mycontainer.SetPos z zpos
	mycontainer.Update3d
endif

End