Maskar's Oblivion Overhaul.esp
0x0C16DB
MOOUpdateCurrentRegionFunctionScript
Scn MOOUpdateCurrentRegionFunctionScript

; bandits and marauders have dynamic spawns based on random invasions
; MOOUpdateCurrentRegionBanditsFunctionScript

ref mylist
ref mycrab
ref myskeleton
ref myconjurer
ref myconjurerboss
ref myvampire
ref myvampireboss
ref mygoblin
ref mynecromancer
ref mynecromancerboss
ref myzombie
ref mysummonzombie1
ref mysummonzombie2
ref mysummonskeleton1
ref mysummonskeleton2
ref mysummonskeleton3
ref mysummonskeleton4
ref mysummonbear
ref myguard
ref mymythicdawn

short seed
short gates

Begin Function {  }

set seed to MOO.myregion + GameYear + GameMonth - GameDay

if MOO.myregion <= 2 || MOO.myregion == 27 ; -1 Unknown 1 SE 2 Oblivion 27 IC
	if MOO.myregion == 2 ; Oblivion
		set mysummonzombie1 to MOOLL0RegionSummonZombie1Oblivion
		set mysummonzombie2 to MOOLL0RegionSummonZombie2Oblivion
		set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1Oblivion
		set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2Oblivion
		set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3Oblivion
		set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4Oblivion
	else
		set mysummonzombie1 to MOOLL0RegionSummonZombie1Default
		set mysummonzombie2 to MOOLL0RegionSummonZombie2Default
		set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1Default
		set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2Default
		set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3Default
		set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4Default
	endif

	set mysummonbear to MOOLL0RegionSummonBearDefault

	set mygoblin to MOOLL2Goblin
	set mycrab to MOOLL0RegionMudCrabUnknown
	set myzombie to MOOLL0RegionZombieDefault
	set myskeleton to MOOLL0RegionSkeletonDefault

	if MOO.myregion == 27
		set myvampire to MOOLL2RegionVampireIC
		set myvampireboss to MOOLL2RegionVampireIC
		set myguard to MOOLL2RegionGuardIC

		set myconjurer to MOOLL2RegionConjurerIC
		set myconjurerboss to MOOLL0RegionConjurerBossIC
		set mynecromancer to MOOLL2RegionNecromancerIC
		set mynecromancerboss to MOOLL0RegionNecromancerBossIC
	else
		set myvampire to MOOLL2RegionVampireDefault
		set myvampireboss to MOOLL2RegionVampireBossDefault
		set myguard to MOOLL2RegionGuardUnknown

		set myconjurer to MOOLL2RegionConjurerDefault
		set myconjurerboss to MOOLL0RegionConjurerBossDefault
		set mynecromancer to MOOLL2RegionNecromancerDefault
		set mynecromancerboss to MOOLL0RegionNecromancerBossDefault
	endif

elseif MOO.myregion == 10 ; Hammerfell
	set mysummonzombie1 to MOOLL0RegionSummonZombie1Hammerfell
	set mysummonzombie2 to MOOLL0RegionSummonZombie2Hammerfell
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1Hammerfell
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2Hammerfell
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3Hammerfell
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4Hammerfell

	set mysummonbear to MOOLL0RegionSummonBearHammerfell

	set myguard to MOOLL2RegionGuardHammerfell

	set mygoblin to MOOLL2GoblinB
	set mycrab to MOOLL0RegionMudCrabHammerfell
	set myzombie to MOOLL0RegionZombieHammerfell
	set myskeleton to MOOLL0RegionSkeletonHammerfell

	set myconjurer to MOOLL2RegionConjurerHammerfell
	set myconjurerboss to MOOLL0RegionConjurerBossHammerfell
	set mynecromancer to MOOLL2RegionNecromancerHammerfell
	set mynecromancerboss to MOOLL0RegionNecromancerBossHammerfell

	set myvampire to MOOLL2RegionVampireHammerfell
	set myvampireboss to MOOLL2RegionVampireBossHammerfell

elseif MOO.myregion == 11 ; Skyrim
	set mysummonzombie1 to MOOLL0RegionSummonZombie1Skyrim
	set mysummonzombie2 to MOOLL0RegionSummonZombie2Skyrim
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1Skyrim
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2Skyrim
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3Skyrim
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4Skyrim

	set mysummonbear to MOOLL0RegionSummonBearSkyrim

	set myguard to MOOLL2RegionGuardSkyrim

	set mygoblin to MOOLL2Goblin
	set mycrab to MOOLL0RegionMudCrabSkyrim
	set myzombie to MOOLL0RegionZombieSkyrim
	set myskeleton to MOOLL0RegionSkeletonSkyrim

	set myconjurer to MOOLL2RegionConjurerSkyrim
	set myconjurerboss to MOOLL0RegionConjurerBossSkyrim
	set mynecromancer to MOOLL2RegionNecromancerSkyrim
	set mynecromancerboss to MOOLL0RegionNecromancerBossSkyrim

	set myvampire to MOOLL2RegionVampireSkyrim
	set myvampireboss to MOOLL2RegionVampireBossSkyrim

elseif MOO.myregion == 12 ; Morrowind
	set mysummonzombie1 to MOOLL0RegionSummonZombie1Morrowind
	set mysummonzombie2 to MOOLL0RegionSummonZombie2Morrowind
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1Morrowind
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2Morrowind
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3Morrowind
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4Morrowind

	set mysummonbear to MOOLL0RegionSummonBearMorrowind

	set myguard to MOOLL2RegionGuardMorrowind

	set mygoblin to MOOLL2GoblinMW
	set mycrab to MOOLL0RegionMudCrabMorrowind
	set myzombie to MOOLL0RegionZombieMorrowind
	set myskeleton to MOOLL0RegionSkeletonMorrowind

	set myconjurer to MOOLL2RegionConjurerMorrowind
	set myconjurerboss to MOOLL0RegionConjurerBossMorrowind
	set mynecromancer to MOOLL2RegionNecromancerMorrowind
	set mynecromancerboss to MOOLL0RegionNecromancerBossMorrowind

	set myvampire to MOOLL2RegionVampireMorrowind
	set myvampireboss to MOOLL2RegionVampireBossMorrowind

elseif MOO.myregion == 13 ; Black Marsh
	set mysummonzombie1 to MOOLL0RegionSummonZombie1BlackMarsh
	set mysummonzombie2 to MOOLL0RegionSummonZombie2BlackMarsh
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1BlackMarsh
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2BlackMarsh
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3BlackMarsh
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4BlackMarsh

	set mysummonbear to MOOLL0RegionSummonBearBlackMarsh

	set myguard to MOOLL2RegionGuardBlackMarsh

	if eval seed & 1 == 1
		set mygoblin to MOOLL2GoblinB
	else
		set mygoblin to MOOLL2GoblinMW
	endif

	set mycrab to MOOLL0RegionMudCrabBlackMarsh
	set myzombie to MOOLL0RegionZombieBlackMarsh
	set myskeleton to MOOLL0RegionSkeletonBlackMarsh

	set myconjurer to MOOLL2RegionConjurerBlackMarsh
	set myconjurerboss to MOOLL0RegionConjurerBossBlackMarsh
	set mynecromancer to MOOLL2RegionNecromancerBlackMarsh
	set mynecromancerboss to MOOLL0RegionNecromancerBossBlackMarsh

	set myvampire to MOOLL2RegionVampireBlackMarsh
	set myvampireboss to MOOLL2RegionVampireBossBlackMarsh

elseif MOO.myregion == 14 ; Elsweyr
	set mysummonzombie1 to MOOLL0RegionSummonZombie1Elsweyr
	set mysummonzombie2 to MOOLL0RegionSummonZombie2Elsweyr
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1Elsweyr
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2Elsweyr
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3Elsweyr
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4Elsweyr

	set mysummonbear to MOOLL0RegionSummonBearElsweyr

	set myguard to MOOLL2RegionGuardElsweyr

	set mygoblin to MOOLL2GoblinB
	set mycrab to MOOLL0RegionMudCrabElsweyr
	set myzombie to MOOLL0RegionZombieElsweyr
	set myskeleton to MOOLL0RegionSkeletonElsweyr

	set myconjurer to MOOLL2RegionConjurerElsweyr
	set myconjurerboss to MOOLL0RegionConjurerBossElsweyr
	set mynecromancer to MOOLL2RegionNecromancerElsweyr
	set mynecromancerboss to MOOLL0RegionNecromancerBossElsweyr

	set myvampire to MOOLL2RegionVampireElsweyr
	set myvampireboss to MOOLL2RegionVampireBossElsweyr

elseif MOO.myregion == 15 ; Valenwood
	set mysummonzombie1 to MOOLL0RegionSummonZombie1Valenwood
	set mysummonzombie2 to MOOLL0RegionSummonZombie2Valenwood
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1Valenwood
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2Valenwood
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3Valenwood
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4Valenwood

	set mysummonbear to MOOLL0RegionSummonBearValenwood

	set myguard to MOOLL2RegionGuardValenwood

	set mygoblin to MOOLL2GoblinB
	set mycrab to MOOLL0RegionMudCrabValenwood
	set myzombie to MOOLL0RegionZombieValenwood
	set myskeleton to MOOLL0RegionSkeletonValenwood

		set myconjurer to MOOLL2RegionConjurerValenwood
		set myconjurerboss to MOOLL0RegionConjurerBossValenwood
		set mynecromancer to MOOLL2RegionNecromancerValenwood
		set mynecromancerboss to MOOLL0RegionNecromancerBossValenwood

	set myvampire to MOOLL2RegionVampireValenwood
	set myvampireboss to MOOLL2RegionVampireBossValenwood

elseif MOO.myregion == 20 ; Great Forest ================================
	set mysummonzombie1 to MOOLL0RegionSummonZombie1GreatForest
	set mysummonzombie2 to MOOLL0RegionSummonZombie2GreatForest
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1GreatForest
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2GreatForest
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3GreatForest
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4GreatForest

	set mysummonbear to MOOLL0RegionSummonBearGreatForest

	set myguard to MOOLL2RegionGuardGreatForest

	set mygoblin to MOOLL2Goblin
	set mycrab to MOOLL0RegionMudCrabGreatForest
	set myzombie to MOOLL0RegionZombieGreatForest
	set myskeleton to MOOLL0RegionSkeletonGreatForest

		set myconjurer to MOOLL2RegionConjurerGreatForest
		set myconjurerboss to MOOLL0RegionConjurerBossGreatForest
		set mynecromancer to MOOLL2RegionNecromancerGreatForest
		set mynecromancerboss to MOOLL0RegionNecromancerBossGreatForest

	set myvampire to MOOLL2RegionVampireGreatForest
	set myvampireboss to MOOLL2RegionVampireBossGreatForest

elseif MOO.myregion == 21 ; West Weald East
	set mysummonzombie1 to MOOLL0RegionSummonZombie1WestWealdEast
	set mysummonzombie2 to MOOLL0RegionSummonZombie2WestWealdEast
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1WestWealdEast
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2WestWealdEast
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3WestWealdEast
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4WestWealdEast

	set mysummonbear to MOOLL0RegionSummonBearWestWealdEast

	set myguard to MOOLL2RegionGuardWestWealdEast

	set mygoblin to MOOLL2GoblinB
	set mycrab to MOOLL0RegionMudCrabWestWealdEast
	set myzombie to MOOLL0RegionZombieWestWealdEast
	set myskeleton to MOOLL0RegionSkeletonWestWealdEast

		set myconjurer to MOOLL2RegionConjurerWestWealdEast
		set myconjurerboss to MOOLL0RegionConjurerBossWestWealdEast
		set mynecromancer to MOOLL2RegionNecromancerWestWealdEast
		set mynecromancerboss to MOOLL0RegionNecromancerBossWestWealdEast

	set myvampire to MOOLL2RegionVampireWestWealdEast
	set myvampireboss to MOOLL2RegionVampireBossWestWealdEast

elseif MOO.myregion == 22 ; West Weald West
	set mysummonzombie1 to MOOLL0RegionSummonZombie1WestWealdWest
	set mysummonzombie2 to MOOLL0RegionSummonZombie2WestWealdWest
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1WestWealdWest
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2WestWealdWest
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3WestWealdWest
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4WestWealdWest

	set mysummonbear to MOOLL0RegionSummonBearWestWealdWest

	set myguard to MOOLL2RegionGuardWestWealdWest

	set mygoblin to MOOLL2GoblinB
	set mycrab to MOOLL0RegionMudCrabWestWealdWest
	set myzombie to MOOLL0RegionZombieWestWealdWest
	set myskeleton to MOOLL0RegionSkeletonWestWealdWest

		set myconjurer to MOOLL2RegionConjurerWestWealdWest
		set myconjurerboss to MOOLL0RegionConjurerBossWestWealdWest
		set mynecromancer to MOOLL2RegionNecromancerWestWealdWest
		set mynecromancerboss to MOOLL0RegionNecromancerBossWestWealdWest

	set myvampire to MOOLL2RegionVampireWestWealdWest
	set myvampireboss to MOOLL2RegionVampireBossWestWealdWest

elseif MOO.myregion == 23 ; Gold Coast ================================
	set mysummonzombie1 to MOOLL0RegionSummonZombie1GoldCoast
	set mysummonzombie2 to MOOLL0RegionSummonZombie2GoldCoast
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1GoldCoast
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2GoldCoast
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3GoldCoast
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4GoldCoast

	set mysummonbear to MOOLL0RegionSummonBearGoldCoast

	set myguard to MOOLL2RegionGuardGoldCoast

	set mygoblin to MOOLL2GoblinB
	set mycrab to MOOLL0RegionMudCrabGoldCoast
	set myzombie to MOOLL0RegionZombieGoldCoast
	set myskeleton to MOOLL0RegionSkeletonGoldCoast

		set myconjurer to MOOLL2RegionConjurerGoldCoast
		set myconjurerboss to MOOLL0RegionConjurerBossGoldCoast
		set mynecromancer to MOOLL2RegionNecromancerGoldCoast
		set mynecromancerboss to MOOLL0RegionNecromancerBossGoldCoast

	set myvampire to MOOLL2RegionVampireGoldCoast
	set myvampireboss to MOOLL2RegionVampireBossGoldCoast

elseif MOO.myregion == 24 ; Topal Bay
	set mysummonzombie1 to MOOLL0RegionSummonZombie1TopalBay
	set mysummonzombie2 to MOOLL0RegionSummonZombie2TopalBay
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1TopalBay
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2TopalBay
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3TopalBay
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4TopalBay

	set mysummonbear to MOOLL0RegionSummonBearTopalBay

	set myguard to MOOLL2RegionGuardTopalBay

	set mygoblin to MOOLL2GoblinB
	set mycrab to MOOLL0RegionMudCrabTopalBay
	set myzombie to MOOLL0RegionZombieTopalBay
	set myskeleton to MOOLL0RegionSkeletonTopalBay

		set myconjurer to MOOLL2RegionConjurerTopalBay
		set myconjurerboss to MOOLL0RegionConjurerBossTopalBay
		set mynecromancer to MOOLL2RegionNecromancerTopalBay
		set mynecromancerboss to MOOLL0RegionNecromancerBossTopalBay

	set myvampire to MOOLL2RegionVampireTopalBay
	set myvampireboss to MOOLL2RegionVampireBossTopalBay

elseif MOO.myregion == 25 ; Niben Bay ================================
	set mysummonzombie1 to MOOLL0RegionSummonZombie1NibanBay
	set mysummonzombie2 to MOOLL0RegionSummonZombie2NibanBay
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1NibanBay
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2NibanBay
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3NibanBay
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4NibanBay

	set mysummonbear to MOOLL0RegionSummonBearNibanBay

	set myguard to MOOLL2RegionGuardNibanBay

	set mygoblin to MOOLL2Goblin
	set mycrab to MOOLL0RegionMudCrabNibanBay
	set myzombie to MOOLL0RegionZombieNibanBay
	set myskeleton to MOOLL0RegionSkeletonNibanBay

		set myconjurer to MOOLL2RegionConjurerNibanBay
		set myconjurerboss to MOOLL0RegionConjurerBossNibanBay
		set mynecromancer to MOOLL2RegionNecromancerNibanBay
		set mynecromancerboss to MOOLL0RegionNecromancerBossNibanBay

	set myvampire to MOOLL2RegionVampireNibanBay
	set myvampireboss to MOOLL2RegionVampireBossNibanBay

elseif MOO.myregion == 26 ; Nibenay Basin ================================
	set mysummonzombie1 to MOOLL0RegionSummonZombie1NibanBasin
	set mysummonzombie2 to MOOLL0RegionSummonZombie2NibanBasin
	set mysummonskeleton1 to MOOLL0RegionSummonSkeleton1NibanBasin
	set mysummonskeleton2 to MOOLL0RegionSummonSkeleton2NibanBasin
	set mysummonskeleton3 to MOOLL0RegionSummonSkeleton3NibanBasin
	set mysummonskeleton4 to MOOLL0RegionSummonSkeleton4NibanBasin

	set mysummonbear to MOOLL0RegionSummonBearNibanBasin

	set myguard to MOOLL2RegionGuardNibanBasin

	set mygoblin to MOOLL2Goblin
	set mycrab to MOOLL0RegionMudCrabNibanBasin
	set myzombie to MOOLL0RegionZombieNibanBasin
	set myskeleton to MOOLL0RegionSkeletonNibanBasin

	if eval seed & 4 == 4
		set myconjurer to MOOLL2RegionConjurerHammerfell
		set myconjurerboss to MOOLL0RegionConjurerBossHammerfell
	else
		set myconjurer to MOOLL2RegionConjurerMorrowind
		set myconjurerboss to MOOLL0RegionConjurerBossMorrowind

	endif
	if eval seed & 8 == 8
		set mynecromancer to MOOLL2RegionNecromancerHammerfell
		set mynecromancerboss to MOOLL0RegionNecromancerBossHammerfell
	else
		set mynecromancer to MOOLL2RegionNecromancerMorrowind
		set mynecromancerboss to MOOLL0RegionNecromancerBossMorrowind
	endif

	set myvampire to MOOLL2RegionVampireNibanBasin
	set myvampireboss to MOOLL2RegionVampireBossNibanBasin
endif


; bandits and marauder lists
Call MOOUpdateCurrentRegionBanditsFunctionScript


; summons

set mylist to MOOLL2RegionSummonZombie1
ClearLeveledList mylist
AddToLeveledList mylist mysummonzombie1 1 1

set mylist to MOOLL2RegionSummonZombie2
ClearLeveledList mylist
AddToLeveledList mylist mysummonzombie2 1 1

set mylist to MOOLL2RegionSummonSkeleton1
ClearLeveledList mylist
AddToLeveledList mylist mysummonskeleton1 1 1

set mylist to MOOLL2RegionSummonSkeleton2
ClearLeveledList mylist
AddToLeveledList mylist mysummonskeleton2 1 1

set mylist to MOOLL2RegionSummonSkeleton3
ClearLeveledList mylist
AddToLeveledList mylist mysummonskeleton3 1 1

set mylist to MOOLL2RegionSummonSkeleton4
ClearLeveledList mylist
AddToLeveledList mylist mysummonskeleton4 1 1

set mylist to MOOLL2RegionSummonBear
ClearLeveledList mylist
AddToLeveledList mylist mysummonbear 1 1

; other

set mylist to MOOLL2RegionMudCrab
ClearLeveledList mylist
AddToLeveledList mylist mycrab 1 1

set mylist to MOOLL2RegionZombie
ClearLeveledList mylist
AddToLeveledList mylist myzombie 1 1

set mylist to MOOLL2RegionUndeadBones
ClearLeveledList mylist
AddToLeveledList mylist myskeleton 1 1

set mylist to MOOLL2RegionConjurer
ClearLeveledList mylist
AddToLeveledList mylist myconjurer 1 1

set mylist to MOOLL2RegionConjurerBoss
ClearLeveledList mylist
AddToLeveledList mylist myconjurerboss 1 1

set mylist to MOOLL2RegionNecromancer
ClearLeveledList mylist
AddToLeveledList mylist mynecromancer 1 1

set mylist to MOOLL2RegionNecromancerBoss
ClearLeveledList mylist
AddToLeveledList mylist mynecromancerboss 1 1

set mylist to MOOLL2RegionVampire
ClearLeveledList mylist
AddToLeveledList mylist myvampire 1 1

set mylist to MOOLL2RegionVampireBoss
ClearLeveledList mylist
AddToLeveledList mylist myvampireboss 1 1

set mylist to MOOLL2RegionGoblin
ClearLeveledList mylist
AddToLeveledList mylist mygoblin 1 1

set mylist to MOOLL2RegionGuard
ClearLeveledList mylist
AddToLeveledList mylist myguard 1 1


; SPECIAL

let seed := MOO.creatureregions[MOO.myregion]
let gates := MOO.gateregions[MOO.myregion]

if gates
	; add mythic dawn to conjurer spawns based on amount of open gates
	if MOO.ini_add_mythicdawn
		set mymythicdawn to MOOLL2MythicDawn
		set mylist to MOOLL2RegionConjurer
		if gates == 1
			AddToLeveledList mylist myconjurer 1 1
			AddToLeveledList mylist myconjurer 1 1
			AddToLeveledList mylist mymythicdawn 1 1
		elseif gates == 2
			AddToLeveledList mylist mymythicdawn 1 1
		else
			AddToLeveledList mylist mymythicdawn 1 1
			AddToLeveledList mylist mymythicdawn 1 1
			AddToLeveledList mylist mymythicdawn 1 1
		endif
	endif
endif



; dungeon and wilderness specific spawns

Call MOOUpdateCurrentRegionWildernessFunctionScript
Call MOOUpdateCurrentRegionDoorFunctionScript ; dungeon specific encounters




if MOO.ini_debug
	Call MOOPrintRegionInfoFunctionScript seed gates
	return
endif

End