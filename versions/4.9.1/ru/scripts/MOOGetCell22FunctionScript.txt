Maskar's Oblivion Overhaul.esp
0x244F48
MOOGetCell22FunctionScript
Scn MOOGetCell22FunctionScript

short dungeontype ; 0 default 1 vampire 2 necromancer 3 conjurer

short i

ref mycell

Begin Function { dungeontype }

if dungeontype == 2 ; necromancer

	set i to Rand 0 6
	if i == 0
		set mycell to FortIstirus
	elseif i == 1
		set mycell to FortIstirus02
	elseif i == 2
		set mycell to FortIstirus03
	elseif i == 3
		set mycell to FortLinchal
	elseif i == 4
		set mycell to FortLinchal02
	else
		set mycell to FortLinchal03
	endif

else ; default

	set i to Rand 0 9
	if i == 0
		set mycell to DasekMoor
	elseif i == 1
		set mycell to DasekMoor02
	elseif i == 2
		set mycell to DasekMoor03
	elseif i == 3
		set mycell to BelletorsFolly
	elseif i == 4
		set mycell to BelletorsFolly02
	elseif i == 5
		set mycell to BelletorsFolly03
	elseif i == 6
		set mycell to CursedMine
	elseif i == 7
		set mycell to CursedMine02
	else
		set mycell to CursedMine03
	endif

endif


SetFunctionValue mycell

End