Maskar's Oblivion Overhaul.esp
0x51E179
MOOGetCurrentDungeonLevelFunctionScript
Scn MOOGetCurrentDungeonLevelFunctionScript

ref doormarker

ref mycell

Begin Function { doormarker }

set mycell to doormarker.GetParentCell
if mycell == 0
	return
endif
if GetSourceModIndex mycell == GetModIndex "Maskar's Oblivion Overhaul.esp"
	set MOO.currentdungeonlevel to Call MOOGetCurrentDungeonLevelMOOCellFunctionScript mycell
else
	set MOO.currentdungeonlevel to 1
endif


End