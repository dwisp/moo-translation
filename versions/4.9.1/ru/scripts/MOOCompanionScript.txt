Maskar's Oblivion Overhaul.esp
0x5203C5
MOOCompanionScript
Scn MOOCompanionScript

float fQuestDelayTime

ref me
short myrand
short i

Begin Gamemode

if MOO.totalcompanions <= 0
	return
endif

set i to 0
while i < MOO.totalcompanions

	let me := MOO.companions[i]

	if me
	if IsFormValid me

		; CHECK DISABLED
		if me.GetDisabled
			me.Enable
		endif

		set myrand to GetRandomPercent

		; SNEAK, ALERT OR TORCH
		if myrand < 50 ; always
			Call MOOCompanionUpdateAwarenessFunctionScript me

		; HEAL -> POTIONS
		elseif myrand < 75 ; always
			Call MOOCompanionUpdateHealthFunctionScript me
		endif

		; GENERAL ACTIONS
		if me.GetItemCount MOOTokenCompanionPayAttention == 0
			Call MOOCompanionUpdateActionsFunctionScript me i
		endif

	endif
	endif

	set i to i + 1

loop

End