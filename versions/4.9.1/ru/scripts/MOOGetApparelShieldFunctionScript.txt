Maskar's Oblivion Overhaul.esp
0x226F5B
MOOGetApparelShieldFunctionScript
Scn MOOGetApparelShieldFunctionScript

short i
ref baseitem

Begin Function {  }

set i to Rand 0 3.99

if i == 0
	set baseitem to ChainmailShield
elseif i == 1
	set baseitem to ElvenShield
elseif i == 2
	set baseitem to DwarvenShield
else
	set baseitem to OrcishShield
endif

SetFunctionValue baseitem

End