Maskar's Oblivion Overhaul.esp
0x27BAC9
MOOUpdateTreasureMapsFunctionScript
Scn MOOUpdateTreasureMapsFunctionScript

Begin Function {  }

if MOO.ini_treasure_map

	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap10Hammerfell "������������ �������"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap11Skyrim "���� ������"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap12Morrowind "���� �����"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap13BlackMarsh "������ ���"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap14Elsweyr "������� � ����������"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap15Valenwood "������� � ����������"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap20GreatForest "������� ���"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap21WestWealdEast "�������� �����"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap22WestWealdWest "������� � ����������"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap23GoldCoast "������� �����"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap24TopalBay "���������� �����"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap25NibanBay "����������� �����"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap26NibanBasin "����������� �������"
	Call MOOUpdateTreasureMapsTextFunctionScript MOOLL0RegionMap27IC "��������� �����"

endif

End