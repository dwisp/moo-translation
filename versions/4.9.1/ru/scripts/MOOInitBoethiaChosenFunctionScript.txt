Maskar's Oblivion Overhaul.esp
0x0F250F
MOOInitBoethiaChosenFunctionScript
Scn MOOInitBoethiaChosenFunctionScript

Begin Function {  }

if DABoethiaChosen01Ref.GetItemCount MOOTokenNpcLoot > 0
	if MOO.ini_debug
		printc "[MOO] Boethia's Chosen do not require new equipment."
	endif
	return
endif

DABoethiaChosen01Ref.AddItemNS MOOTokenNpcLoot 1

DABoethiaChosen01Ref.AddItemNS LL2NPCWeaponBossCombBowLvl100 1				; LL0NPCWeaponBowLvl100
DABoethiaChosen01Ref.AddItemNS LL2NPCWeaponBossCombArrow100 1				; LL1NPCArrowMagic100
DABoethiaChosen01Ref.AddItemNS LL2NPCArmorCombHeavyCuirass100 1				; LL0NPCArmorHeavyCuirass100

DABoethiaChosen02Ref.AddItemNS LL2NPCArmorCombLightCuirassLvl100 1				; LL0NPCArmor0MagicLightCuirassLvl100

DABoethiaChosen03Ref.AddItemNS LL2NPCWeaponBossCombBowLvl100 1				; LL0NPCWeapon0MagicBowLvl100
DABoethiaChosen03Ref.AddItemNS LL2NPCWeaponBossCombArrow100 1				; LL1NPCArrowMagic100
DABoethiaChosen03Ref.AddItemNS LL2NPCArmorCombLightCuirassLvl100 1				; LL0NPCArmorLightCuirass100

DABoethiaChosen04Ref.AddItemNS LL2NPCWeaponBossCombBluntLvl100 1			; LL1NPCWeaponBluntLvl100

DABoethiaChosen05Ref.AddItemNS LL2NPCWeaponBossCombBluntLvl100 1			; LL1NPCWeaponBluntLvl100
DABoethiaChosen05Ref.AddItemNS LL2NPCArmorCombHeavyBootsLvl100 1				; LL0NPCArmorHeavyBootsLvl100
DABoethiaChosen05Ref.AddItemNS LL2NPCArmorCombHeavyCuirassLvl100 1			; LL0NPCArmorHeavyCuirassLvl100

DABoethiaChosen06Ref.AddItemNS LL2NPCArmorCombLightBootsLvl100 1				; LL0NPCArmorLightBootsLvl100
DABoethiaChosen06Ref.AddItemNS LL2NPCArmorCombLightCuirassLvl100 1				; LL0NPCArmorLightCuirassLvl100
DABoethiaChosen06Ref.AddItemNS LL2NPCArmorCombLightGauntletsLvl100 1			; LL0NPCArmorLightGauntletsLvl100
DABoethiaChosen06Ref.AddItemNS LL2NPCArmorCombLightShieldLvl100 1				; LL0NPCArmorLightShieldLvl100

DABoethiaChosen07Ref.AddItemNS LL2NPCArmorCombHeavyCuirassLvl100 1			; LL0NPCArmor0MagicHeavyCuirassLvl100
DABoethiaChosen07Ref.AddItemNS LL2NPCArmorCombHeavyBootsLvl100 1				; LL0NPCArmorHeavyBootsLvl100
DABoethiaChosen07Ref.AddItemNS LL2NPCArmorCombHeavyShieldLvl100 1			; LL0NPCArmorHeavyShieldLvl100

DABoethiaChosen08Ref.AddItemNS LL2NPCArmorCombHeavyBootsLvl100 1				; LL0NPCArmorHeavyBootsLvl100
DABoethiaChosen08Ref.AddItemNS LL2NPCArmorCombHeavyCuirassLvl100 1			; LL0NPCArmorHeavyCuirassLvl100
DABoethiaChosen08Ref.AddItemNS LL2NPCArmorCombHeavyHelmetLvl100 1			; LL0NPCArmorHeavyHelmetLvl100

DABoethiaChosen10Ref.AddItemNS LL2NPCWeaponBossCombBowLvl100 1				; LL0NPCWeapon0MagicBowLvl100
DABoethiaChosen10Ref.AddItemNS LL2NPCWeaponBossCombArrow100 1				; LL1NPCArrowMagic100
DABoethiaChosen10Ref.AddItemNS LL2NPCArmorCombLightBoots100 1					; LL0NPCArmorLightBoots100
DABoethiaChosen10Ref.AddItemNS LL2NPCArmorCombLightCuirass100 1					; LL0NPCArmorLightCuirass100

if MOO.ini_debug
	printc "[MOO] Boethia's Chosen have received new equipment."
endif

End