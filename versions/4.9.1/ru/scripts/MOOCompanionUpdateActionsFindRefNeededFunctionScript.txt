Maskar's Oblivion Overhaul.esp
0x4B3C0D
MOOCompanionUpdateActionsFindRefNeededFunctionScript
Scn MOOCompanionUpdateActionsFindRefNeededFunctionScript

ref me

ref myitemarea
ref myitemactor
ref myitemcontainer
float mydistance

ref myitem

Begin Function { me }

; PICK CLOSEST BY AREA, BODY OR CONTAINER WITH USEFUL OBJECT

; currently skipped actor because this is done through corpse checking instead

set myitemarea to Call MOOCompanionUpdateActionsFindRefNeededAreaFunctionScript me
;set myitemactor to Call MOOCompanionUpdateActionsFindRefNeededActorFunctionScript me
set myitemcontainer to Call MOOCompanionUpdateActionsFindRefNeededContainerFunctionScript me

if myitemarea
	set mydistance to me.GetDistance myitemarea
	set myitem to myitemarea
endif

;if myitemactor
;if ( myitem == 0 ) || ( mydistance > me.GetDistance myitemactor )
;	set mydistance to me.GetDistance myitemactor
;	set myitem to myitemactor
;endif
;endif

if myitemcontainer
if ( myitem == 0 ) || ( mydistance > me.GetDistance myitemcontainer )
	set mydistance to me.GetDistance myitemcontainer
	set myitem to myitemcontainer
endif
endif

SetFunctionValue myitem

End