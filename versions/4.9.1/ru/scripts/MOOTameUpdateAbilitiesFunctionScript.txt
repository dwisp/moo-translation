Maskar's Oblivion Overhaul.esp
0x313692
MOOTameUpdateAbilitiesFunctionScript
Scn MOOTameUpdateAbilitiesFunctionScript

ref me

short ability1
short ability2
short ability3
short myelement

short levelups
short levelmod ; sqrt level
ref myspell
short i
short j
float myoldmagnitude
float mynewmagnitude
float myduration
short creaturetype

Begin Function { me }

set ability1 to me.GetItemCount MOOTokenCreatureAbility1
set ability2 to me.GetItemCount MOOTokenCreatureAbility2
set ability3 to me.GetItemCount MOOTokenCreatureAbility3

set myelement to me.GetItemCount MOOTokenCreatureElement

if MOO.ini_debug
	printc "[MOO] touch: %.0f target: %.0f self: %.0f ( elemental preference: %.0f )" ability1 ability2 ability3 myelement
endif

set levelmod to me.GetLevel
set levelmod to sqrt levelmod

set levelups to me.GetItemCount MOOTokenCreatureXP


; INCREASE EXISTING SPELLS AND ABILITIES ( NOT DISEASES )

; loop through all spells
; for all clones -> increase magnitude + duration

let MOO.tempitem := me.GetSpells
set i to ar_Size MOO.tempitem

while i > 0
	set i to i - 1
	let myspell := MOO.tempitem[i]

	; do not remove diseases
	if GetSpellType myspell != 1 && IsClonedForm myspell

		set j to GetMagicItemEffectCount myspell
		while j
			set j to j - 1

			set myoldmagnitude to GetNthEffectItemMagnitude myspell j
			set mynewmagnitude to myoldmagnitude + ( ( myoldmagnitude * 0.5 ) / levelmod )
			SetNthEffectItemMagnitude mynewmagnitude myspell j

			if MOO.ini_debug
				printc "[MOO] %n has effect increased from %.0f to %.0f ( %n )." me myoldmagnitude mynewmagnitude myspell
			endif

;			set myduration to GetNthEffectItemDuration myspell j
;			set myduration to myduration + ( ( myduration * 0.5 ) / levelmod )
;			SetNthEffectItemDuration myduration myspell j

		loop
	endif
loop


; ADD NEW SPELLS BASED ON LEVEL

; spells
; 0 none
; 1 natural
; 2 magic (can learn other spells)

set creaturetype to Call MOOTameGetTamableFunctionScript me 3
if creaturetype != 2
	if MOO.ini_debug
		printc "[MOO] %n cannot learn spells." me
	endif
	return
endif

set i to me.GetLevel
set myspell to 0

if i == 1 || i == 7 || i == 13 || i == 19 || i == 25 || i == 31 || i == 37 || i == 43 || i == 49 || i == 55 || i == 61 || i == 67
	if ability1 == 0
		set myspell to Call MOOTameUpdateAbility1FunctionScript me
	endif
elseif i == 3 || i == 9 || i == 15 || i == 21 || i == 27 || i == 33 || i == 39 || i == 45 || i == 51 || i == 57 || i == 63 || i == 69
	if ability2 == 0
		set myspell to Call MOOTameUpdateAbility2FunctionScript me
	endif
elseif i == 5 || i == 11 || i == 17 || i == 23 || i == 29 || i == 35 || i == 41 || i == 47 || i == 53 || i == 59 || i == 65 || i == 71
	if ability3 == 0
		set myspell to Call MOOTameUpdateAbility3FunctionScript me
	endif
endif

if myspell
	set myspell to CloneForm myspell
	me.AddSpellNS myspell

	messageex "%n has learned a new spell!" me

	if MOO.ini_debug
		set ability1 to me.GetItemCount MOOTokenCreatureAbility1
		set ability2 to me.GetItemCount MOOTokenCreatureAbility2
		set ability3 to me.GetItemCount MOOTokenCreatureAbility3
		printc "[MOO] Added %n to %n ( abilities: %.0f %.0f %.0f )" myspell me ability1 ability2 ability3
	endif
endif


; UPDATE SHADERS

Call MOOTameUpdateShadersFunctionScript me

End