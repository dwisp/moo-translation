Maskar's Oblivion Overhaul.esp
0x584F76
MOOHelperGetObjectsAboveGroundFunctionScript
Scn MOOHelperGetObjectsAboveGroundFunctionScript

ref myplacer
float myitemradius

ref me
ref mycell

float myx
float myy
float myz
float terrainheight

short found28 ; static

short mymask

Begin Function { myplacer myitemradius }

; INIT
set mycell to myplacer.GetParentCell
if mycell == 0
	return
endif

; CHECK FOR OBJECTS ABOVE GROUND LEVEL
set me to GetFirstRefInCell mycell 28
while me
	; ONLY ALLOW ROCKS
	if me.CompareModelPath "Rocks\"
		set myx to me.GetPos x
		set myy to me.GetPos y
		set myz to me.GetPos z
		set terrainheight to GetTerrainHeight myx myy
		if myz > terrainheight
			set found28 to 1
			break
		endif
	else
		set found28 to 1
		break
	endif

	set me to GetNextRef
loop


set mymask to found28

SetFunctionValue mymask

End
