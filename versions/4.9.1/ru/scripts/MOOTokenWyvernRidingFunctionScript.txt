Maskar's Oblivion Overhaul.esp
0x36C67A
MOOTokenWyvernRidingFunctionScript
Scn MOOTokenWyvernRidingFunctionScript

ref me

; script partly reuses Akatosh Mount mod script (needs improving)

float XPos    		; X Position 
float YPos    		; Y Position
float ZPos    		; Z Position
float XAngle           ; Flight Planes X Rotation Angle
float Height           ; Players X Angle, Used To Rotate FlightPlane
float Direction        ; Direction ShadowDrake Is Facing


Begin Function { me }

		If MOOCellMountFlightPlane.GetInSameCell me == 0
			MOOCellMountFlightPlane.MoveTo me
		Endif

	;Set FlightPlane to Under ShadowDrake when player is riding him and is running

				Set XPos to me.GetPos X
				Set YPos to me.GetPos Y
				Set ZPos to me.GetPos Z
				Set Direction to me.GetAngle Z
				Set Height to (Player.GetAngle X)

	; Adjusts Height to avoid the player from creating an unpassable wall or falling of the FlightPlane

					if Height < -30
						Set Height to -30
					elseif Height > 40
						Set Height to 40
					endif

	; Reposition the FlightPlane so it is always under the ShadowDrake in Flight

					If me.GetDistance MOOCellMountFlightPlane > 400
						MOOCellMountFlightPlane.SetPos X, XPos
						MOOCellMountFlightPlane.SetPos Y, YPos
						MOOCellMountFlightPlane.SetPos Z, ZPos
					Endif

	; Sets the Tilt of the FlightPlane Based on ShadowDrake Facing Direction and Player "X" Angle
		
					If Direction > 0  && Direction < 90
						MOOCellMountFlightPlane.SetAngle X, Height
						Set Height to -Height
						MOOCellMountFlightPlane.SetAngle Y, Height
					Endif

					If Direction > 90 && Direction < 180
						Set Height to -Height
						MOOCellMountFlightPlane.SetAngle X, Height
						MOOCellMountFlightPlane.SetAngle Y, Height
					Endif

					If Direction > 180 && Direction < 270
						MOOCellMountFlightPlane.SetAngle Y, Height
						Set Height to -Height
						MOOCellMountFlightPlane.SetAngle X, Height
					Endif

					If Direction > 270 && Direction < 360
						MOOCellMountFlightPlane.SetAngle X, Height
						MOOCellMountFlightPlane.SetAngle Y, Height
					Endif

	; Makes it so if you are riding ShadowDrake and Running the FlightIdle will play

				me.PickIdle

End