Maskar's Oblivion Overhaul.esp
0x4337C4
MOOActivatorSummonedPortalScript
Scn MOOActivatorSummonedPortalScript

short init
ref mycaster

ref me
float time
float nexttime
ref myspawn
short mylevel
float mypos

Begin Gamemode

set me to GetSelf

if init
	set init to 0

	; NEW

	Update3d
	me.PlaySound3d SPLConjurationCast

	set time to 60
	set nexttime to time - Rand 10 20
	return
endif

if IsFormValid mycaster
if mycaster.GetDead
	set time to 0
endif
endif

set time to time - GetSecondsPassed
if time < 0

	set mycaster to 0

	; DISABLE

	MoveTo MOOCellMarker
	Disable
elseif time < nexttime
	set mylevel to player.GetLevel
	set myspawn to LL1DaedricBeast100
	set myspawn to CalcLeveledItem myspawn mylevel

	if myspawn

		; SPAWN

		set myspawn to me.PlaceAtMe myspawn
		myspawn.MoveTo me

		set mypos to me.GetPos x
		myspawn.SetPos x mypos
		set mypos to me.GetPos y
		myspawn.SetPos y mypos
		set mypos to me.GetPos z
		myspawn.SetPos z mypos

		myspawn.PlaySound3d AMBOblivionGateSpawn

		set nexttime to time - Rand 5 15

	endif
endif

End