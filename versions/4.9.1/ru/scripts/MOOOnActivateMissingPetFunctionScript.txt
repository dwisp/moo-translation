Maskar's Oblivion Overhaul.esp
0x4D3587
MOOOnActivateMissingPetFunctionScript
Scn MOOOnActivateMissingPetFunctionScript

ref me

ref mybase
ref mypet

Begin Function { me }

set mybase to me.GetBaseObject

if mybase == MOOMissingCat01 || mybase == MOOMissingCat02 || mybase == MOOMissingCat03
	set mypet to MOOMiscMissingCat
elseif mybase == MOOMissingDackel
	set mypet to MOOMiscMissingDog
elseif mybase == MOOMissingPuppy
	set mypet to MOOMiscMissingPuppy
else ; rabbit
	set mypet to MOOMiscMissingRabbit
endif

if mypet
	player.AddItemNS mypet 1
	me.Disable

	MessageEx "���� ������(�) %n!" me
endif

End