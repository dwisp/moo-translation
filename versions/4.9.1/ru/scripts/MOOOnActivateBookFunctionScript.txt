Maskar's Oblivion Overhaul.esp
0x21A26A
MOOOnActivateBookFunctionScript
Scn MOOOnActivateBookFunctionScript

ref mybook

Begin Function { mybook }

; GRAY FOX
if mybook.CompareModelPath "Clutter\Books\WantedPoster02"
	AddTopic TGGrayFox
	AddTopic TGHieronymusLex

; A STRANGE DOOR
elseif mybook.CompareModelPath "MOO\Clutter\Quests\Notices\warningSE.nif"
	if MOO.SELoaded == 0
		return
	endif
	SEGateMapMarker.SetMapMarkerVisible 1
endif

End