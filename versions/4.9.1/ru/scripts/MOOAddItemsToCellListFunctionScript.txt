Maskar's Oblivion Overhaul.esp
0x2D4239
MOOAddItemsToCellListFunctionScript
Scn MOOAddItemsToCellListFunctionScript

ref me

ref myitem
short i

Begin Function { me }

; me is container with items - add these to the cell list so they are dynamically added to npcs and goblins

if me.IsFlora

	set myitem to me.GetIngredient
	AddToLeveledList MOOLL0LootCell myitem 1 1

elseif me.IsContainer
	let MOO.mydata := me.GetBaseItems
	let i := ar_Size MOO.mydata
	if i
		let myitem := MOO.mydata[0]["item"]
		let myitem := GetNthLevItem 0 myitem
		
		AddToLeveledList MOOLL0LootCell myitem 1 1
		AddToLeveledList MOOLL0LootCell myitem 1 1
		AddToLeveledList MOOLL0LootCell myitem 1 1
	endif
endif

End