Maskar's Oblivion Overhaul.esp
0x239601
MOOSetNpcCombatStyleFunctionScript
Scn MOOSetNpcCombatStyleFunctionScript

ref me
short npctype

ref myclass
short i

Begin Function { me npctype }

set myclass to me.GetClass

if GetClassSpecialization myclass == 1 ; magic

	set i to Rand 0 3
	if i == 0
		me.SetCombatStyle DefaultLich
	elseif i == 1
		me.SetCombatStyle MankarCamoranStyle
	else
		me.SetCombatStyle DefaultAtronachFire
	endif

elseif IsMajorC 28 myclass ; marksman

	set i to Rand 0 3
	if i == 0
		me.SetCombatStyle DefaultArcher
	elseif i == 1
		me.SetCombatStyle MankarCamoranStyle
	else
		me.SetCombatStyle DefaultSkeletonRanged
	endif

else
	set i to Rand 0 7
	if i == 0
		me.SetCombatStyle DefaultGoblinWarlord
	elseif i == 1
		me.SetCombatStyle DefaultAtronachStorm
	elseif i == 2
		me.SetCombatStyle DefaultXivilai
	elseif i == 3
		me.SetCombatStyle DefaultBearBrown
	elseif i == 4
		me.SetCombatStyle DefaultClannfear
	elseif i == 5
		me.SetCombatStyle DefaultOgre
	else
		me.SetCombatStyle DefaultSkeletonChampion
	endif

endif

End