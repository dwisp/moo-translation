Maskar's Oblivion Overhaul.esp
0x54EF04
MOOActivatorEventActorPlaceOnLoadFloraFunctionScript
Scn MOOActivatorEventActorPlaceOnLoadFloraFunctionScript

ref me

ref mycreature
float myz

ref myplacedcreature

Begin Function { me }

if me.CompareModelPath "MOO\architecture\mushroom\"

	if me.CompareModelPath "_2.nif"
		set mycreature to MOOCreatureMushroomB
	elseif me.CompareModelPath "_3.nif"
		set mycreature to MOOCreatureMushroomC
	elseif me.CompareModelPath "_4.nif"
		set mycreature to MOOCreatureMushroomD
	else
		set mycreature to MOOCreatureMushroom
	endif

	if GetRandomPercent < MOO.ini_spawn_mushrooms
		set myplacedcreature to me.PlaceAtMe mycreature
		set myz to 150 + myplacedcreature.GetPos z
		myplacedcreature.SetPos z myz
	endif
endif

SetFunctionValue myplacedcreature

End