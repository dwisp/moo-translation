Maskar's Oblivion Overhaul.esp
0x301D76
MOOTameBackupFollowersAIFunctionScript
Scn MOOTameBackupFollowersAIFunctionScript

ref me

short i

Begin Function { me }

; base ai
set i to ( me.GetAv Aggression ) / 5

me.AddItemNS MOOTokenCreatureAggression i
me.SetAv Aggression 0

; ai package
me.AddScriptPackage MOOAIFollowerWander

; disposition
set i to 100 - ( me.GetDisposition player )
me.ModDisposition player i

; factions
me.SetFactionRank PlayerFaction 0
me.SetOwnership PlayerFaction

; enable low level processing
if me.HasLowLevelProcessing == 0
	me.SetLowLevelProcessing 1
endif

; respawn, essential
me.SetActorRespawns 0

if MOO.ini_follower_essential
	me.SetRefEssential 1
endif

; horse check
me.AddItemNS MOOTokenHorse 1

End