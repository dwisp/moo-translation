Maskar's Oblivion Overhaul.esp
0x2C9056
MOOGetVeinFromRegionFunctionScript
Scn MOOGetVeinFromRegionFunctionScript

short type

ref rock1
ref rock2
ref rock3
ref rock4
ref rock5
ref rock6

ref myvein

Begin Function { type }

; type == CRock01-6

if MOO.ini_debug
	printc "[MOO] Regional ore vein placed."
endif

if MOO.myregion == 10 ; Hammerfell
	set rock1 to MOORock1Orichalcum
	set rock2 to MOORock2Orichalcum
	set rock3 to MOORock3Orichalcum
	set rock4 to MOORock4Orichalcum
	set rock5 to MOORock5Orichalcum
	set rock6 to MOORock6Orichalcum

elseif MOO.myregion == 11 ; Skyrim
	set rock1 to MOORock1Stalhrim
	set rock2 to MOORock2Stalhrim
	set rock3 to MOORock3Stalhrim
	set rock4 to MOORock4Stalhrim
	set rock5 to MOORock5Stalhrim
	set rock6 to MOORock6Stalhrim

elseif MOO.myregion == 12 ; Morrowind
	set rock1 to MOORock1Ebony
	set rock2 to MOORock2Ebony
	set rock3 to MOORock3Ebony
	set rock4 to MOORock4Ebony
	set rock5 to MOORock5Ebony
	set rock6 to MOORock6Ebony

elseif MOO.myregion == 13 || MOO.myregion ==24 ; Black Marsh + Topal Bay
	set rock1 to MOORock1Malachite
	set rock2 to MOORock2Malachite
	set rock3 to MOORock3Malachite
	set rock4 to MOORock4Malachite
	set rock5 to MOORock5Malachite
	set rock6 to MOORock6Malachite

elseif MOO.myregion == 14 || MOO.myregion ==21 ; Elsweyr + West Weald (East)
	set rock1 to MOORock1Quicksilver
	set rock2 to MOORock2Quicksilver
	set rock3 to MOORock3Quicksilver
	set rock4 to MOORock4Quicksilver
	set rock5 to MOORock5Quicksilver
	set rock6 to MOORock6Quicksilver

elseif MOO.myregion == 15 || MOO.myregion ==22 || MOO.myregion ==23 ; Valenwood + West Weald (West) + Gold Coast
	set rock1 to MOORock1Moonstone
	set rock2 to MOORock2Moonstone
	set rock3 to MOORock3Moonstone
	set rock4 to MOORock4Moonstone
	set rock5 to MOORock5Moonstone
	set rock6 to MOORock6Moonstone

elseif MOO.myregion == 20 || MOO.myregion ==25 || MOO.myregion ==26 || MOO.myregion ==27 ; Great Forest + Niban Bay + Niban Basin + Imperial City
	set rock1 to MOORock1Corundum
	set rock2 to MOORock2Corundum
	set rock3 to MOORock3Corundum
	set rock4 to MOORock4Corundum
	set rock5 to MOORock5Corundum
	set rock6 to MOORock6Corundum

else
	set rock1 to MOORock1Iron
	set rock2 to MOORock2Iron
	set rock3 to MOORock3Iron
	set rock4 to MOORock4Iron
	set rock5 to MOORock5Iron
	set rock6 to MOORock6Iron

endif

if type == 1
	set myvein to rock1
elseif type == 2
	set myvein to rock2
elseif type == 3
	set myvein to rock3
elseif type == 4
	set myvein to rock4
elseif type == 5
	set myvein to rock5
elseif type == 6
	set myvein to rock6
endif


SetFunctionValue myvein

End