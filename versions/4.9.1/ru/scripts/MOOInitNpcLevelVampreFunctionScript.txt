Maskar's Oblivion Overhaul.esp
0x3FF361
MOOInitNpcLevelVampreFunctionScript
Scn MOOInitNpcLevelVampreFunctionScript

ref me

Begin Function { me }

if me.GetAV Confidence == 100
	if me.GetInFaction MOOVampireFaction
		Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_vampire_ancient_boss MOO.ini_levelmax_vampire_ancient_boss MOO.ini_levelmod_vampire_ancient_boss
	else
		Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_vampire_boss MOO.ini_levelmax_vampire_boss MOO.ini_levelmod_vampire_boss
	endif
elseif me.GetInFaction MOOVampireFaction
	Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_vampire_ancient_grunt MOO.ini_levelmax_vampire_ancient_grunt MOO.ini_levelmod_vampire_ancient_grunt
else
	Call MOOSetLevelNpcFunctionScript me MOO.ini_levelmin_vampire_grunt MOO.ini_levelmax_vampire_grunt MOO.ini_levelmod_vampire_grunt
endif

End