Maskar's Oblivion Overhaul.esp
0x29B4E3
MOOInitSEFunctionScript
Scn MOOInitSEFunctionScript

Begin Function {  }

SEHaskillREF.MoveTo SE01HaskillMoveMarker

if MOO.ini_add_notice_SE
	set SE01Door1REF.journalvar to 2 ; disable SE introduction message
	if GetStageDone SE01Door 10 == 0
		SetStage SE01Door  10
	endif
endif

End