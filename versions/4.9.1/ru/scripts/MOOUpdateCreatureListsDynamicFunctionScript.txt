Maskar's Oblivion Overhaul.esp
0x3D7D02
MOOUpdateCreatureListsDynamicFunctionScript
Scn MOOUpdateCreatureListsDynamicFunctionScript

Begin Function {  }

; ======================================================================================================
; BANDIT
; ======================================================================================================

; MOOLL2RegionBandit

; update LL1
Call MOOUpdateLeveledCreatureFunctionScript LL1BanditMelee100
Call MOOUpdateLeveledCreatureFunctionScript LL1BanditMissile100 
Call MOOUpdateLeveledCreatureFunctionScript LL1BanditWizard100

; copy list to vanilla list
Call MOOCopyLeveledListFunctionScript LL1BanditMelee100 MOOLL0VanillaBanditMelee
Call MOOCopyLeveledListFunctionScript LL1BanditMissile100 MOOLL0VanillaBanditMissile
Call MOOCopyLeveledListFunctionScript LL1BanditWizard100 MOOLL0VanillaBanditWizard

; replace lists
Call MOOReplaceListFunctionScript LL1BanditMelee100 MOOLL2VanillaBanditMelee
Call MOOReplaceListFunctionScript LL1BanditMelee75 MOOLL2VanillaBanditMelee
Call MOOReplaceListFunctionScript LL1BanditMelee50 MOOLL2VanillaBanditMelee
Call MOOReplaceListFunctionScript LL1BanditMelee25 MOOLL2VanillaBanditMelee
Call MOOReplaceListFunctionScript LL1BanditCampMelee MOOLL2VanillaBanditMelee

Call MOOReplaceListFunctionScript LL1BanditMissile100 MOOLL2VanillaBanditMissile
Call MOOReplaceListFunctionScript LL1BanditMissile75 MOOLL2VanillaBanditMissile
Call MOOReplaceListFunctionScript LL1BanditMissile50 MOOLL2VanillaBanditMissile
Call MOOReplaceListFunctionScript LL1BanditMissile25 MOOLL2VanillaBanditMissile
Call MOOReplaceListFunctionScript LL1BanditCampMissile MOOLL2VanillaBanditMissile

Call MOOReplaceListFunctionScript LL1BanditWizard100 MOOLL2VanillaBanditWizard
Call MOOReplaceListFunctionScript LL1BanditWizard75 MOOLL2VanillaBanditWizard
Call MOOReplaceListFunctionScript LL1BanditWizard50 MOOLL2VanillaBanditWizard
Call MOOReplaceListFunctionScript LL1BanditWizard25 MOOLL2VanillaBanditWizard
Call MOOReplaceListFunctionScript LL1BanditCampMagic MOOLL2VanillaBanditWizard


; MOOLL2RegionBanditBoss

Call MOOUpdateLeveledCreatureFunctionScript LL1BanditBossLvl100
Call MOOCopyLeveledListFunctionScript LL1BanditBossLvl100 MOOLL0VanillaBanditBoss
Call MOOReplaceListFunctionScript LL1BanditBossLvl100 MOOLL2VanillaBanditBoss


; ======================================================================================================
; MARAUDER
; ======================================================================================================

; MOOLL2RegionMarauder

; update LL1
Call MOOUpdateLeveledCreatureFunctionScript LL1MarauderMelee100
Call MOOUpdateLeveledCreatureFunctionScript LL1MarauderMissile100 
Call MOOUpdateLeveledCreatureFunctionScript LL1MarauderBattlemage100

; copy list to vanilla list
Call MOOCopyLeveledListFunctionScript LL1MarauderMelee100 MOOLL0VanillaMarauderMelee
Call MOOCopyLeveledListFunctionScript LL1MarauderMissile100 MOOLL0VanillaMarauderMissile
Call MOOCopyLeveledListFunctionScript LL1MarauderBattlemage100 MOOLL0VanillaMarauderBattlemage

; replace lists
Call MOOReplaceListFunctionScript LL1MarauderMelee100 MOOLL2VanillaMarauderMelee
Call MOOReplaceListFunctionScript LL1MarauderMelee75 MOOLL2VanillaMarauderMelee
Call MOOReplaceListFunctionScript LL1MarauderMelee50 MOOLL2VanillaMarauderMelee
Call MOOReplaceListFunctionScript LL1MarauderMelee25 MOOLL2VanillaMarauderMelee

Call MOOReplaceListFunctionScript LL1MarauderMissile100 MOOLL2VanillaMarauderMissile
Call MOOReplaceListFunctionScript LL1MarauderMissile75 MOOLL2VanillaMarauderMissile
Call MOOReplaceListFunctionScript LL1MarauderMissile50 MOOLL2VanillaMarauderMissile
Call MOOReplaceListFunctionScript LL1MarauderMissile25 MOOLL2VanillaMarauderMissile

Call MOOReplaceListFunctionScript LL1MarauderBattlemage100 MOOLL2VanillaMarauderBattlemage
Call MOOReplaceListFunctionScript LL1MarauderBattlemage75 MOOLL2VanillaMarauderBattlemage
Call MOOReplaceListFunctionScript LL1MarauderBattlemage50 MOOLL2VanillaMarauderBattlemage
Call MOOReplaceListFunctionScript LL1MarauderBattlemage25 MOOLL2VanillaMarauderBattlemage


; MOOLL2RegionMarauderBoss

Call MOOUpdateLeveledCreatureFunctionScript LL1MarauderBossLvl100
Call MOOCopyLeveledListFunctionScript LL1MarauderBossLvl100 MOOLL0VanillaMarauderBoss
Call MOOReplaceListFunctionScript LL1MarauderBossLvl100 MOOLL2VanillaMarauderBoss


; ======================================================================================================
; CONJURER
; ======================================================================================================

; MOOLL2RegionConjurer

; update LL1
Call MOOUpdateLeveledCreatureFunctionScript LL1Conjurer100

; copy list to vanilla list
Call MOOCopyLeveledListFunctionScript LL1Conjurer100 MOOLL0VanillaConjurer

; replace lists
Call MOOReplaceListFunctionScript LL1Conjurer100 MOOLL2VanillaConjurer
Call MOOReplaceListFunctionScript LL1Conjurer75 MOOLL2VanillaConjurer
Call MOOReplaceListFunctionScript LL1Conjurer50 MOOLL2VanillaConjurer
Call MOOReplaceListFunctionScript LL1Conjurer25 MOOLL2VanillaConjurer

; MOOLL2RegionConjurerBoss

Call MOOUpdateLeveledCreatureFunctionScript LL1ConjurerBossLvl100
Call MOOCopyLeveledListFunctionScript LL1ConjurerBossLvl100 MOOLL0VanillaConjurerBoss
Call MOOReplaceListFunctionScript LL1ConjurerBossLvl100 MOOLL2VanillaConjurerBoss


; ======================================================================================================
; NECROMANCER
; ======================================================================================================

; MOOLL2RegionNecromancer

; update LL1
Call MOOUpdateLeveledCreatureFunctionScript LL1Necromancer100

; copy list to vanilla list
Call MOOCopyLeveledListFunctionScript LL1Necromancer100 MOOLL0VanillaNecromancer

; replace lists
Call MOOReplaceListFunctionScript LL1Necromancer100 MOOLL2VanillaNecromancer
Call MOOReplaceListFunctionScript LL1Necromancer75 MOOLL2VanillaNecromancer
Call MOOReplaceListFunctionScript LL1Necromancer50 MOOLL2VanillaNecromancer
Call MOOReplaceListFunctionScript LL1Necromancer25 MOOLL2VanillaNecromancer

; MOOLL2RegionNecromancerBoss

Call MOOUpdateLeveledCreatureFunctionScript LL1NecromancerBoss100
Call MOOCopyLeveledListFunctionScript LL1NecromancerBoss100 MOOLL0VanillaNecromancerBoss
Call MOOReplaceListFunctionScript LL1NecromancerBoss100 MOOLL2VanillaNecromancerBoss


End