Maskar's Oblivion Overhaul.esp
0x5B3F13
MOODoActionsFollowersInOblivionFunctionScript
Scn MOODoActionsFollowersInOblivionFunctionScript

ref me
short i

Begin Function {  }

; COMPANIONS
set i to 0
while i < MOO.totalcompanions

	let me := MOO.companions[i]
	if me
	if me.GetInCell MOOCell
		me.MoveTo player
	endif
	endif

	set i to i + 1
loop

; PETS
set i to ar_Size MOOTame.followers

while i > 0
	set i to i - 1

	let me := MOOTame.followers[i]
	if me
	if me.GetInCell MOOCell
		me.MoveTo player
	endif
	endif
loop

set MOO.actionrequired to 0

End