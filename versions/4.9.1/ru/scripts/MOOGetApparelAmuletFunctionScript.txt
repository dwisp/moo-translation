Maskar's Oblivion Overhaul.esp
0x226F5A
MOOGetApparelAmuletFunctionScript
Scn MOOGetApparelAmuletFunctionScript

short i
ref baseitem

Begin Function {  }

set i to Rand 0 11.99

if i == 0
	set baseitem to JewelryAmulet1Bronze
elseif i == 1
	set baseitem to JewelryAmulet2Copper
elseif i == 2
	set baseitem to JewelryAmulet3Jade
elseif i == 3
	set baseitem to JewelryAmulet4Silver
elseif i == 4
	set baseitem to JewelryAmulet5Gold
elseif i == 5
	set baseitem to JewelryAmulet6Jeweled
elseif i == 6
	set baseitem to JewelryNecklace1Bronze
elseif i == 7
	set baseitem to JewelryNecklace2Copper
elseif i == 8
	set baseitem to JewelryNecklace3Jade
elseif i == 9
	set baseitem to JewelryNecklace4Silver
elseif i == 10
	set baseitem to JewelryNecklace5Gold
else
	set baseitem to JewelryNecklace6Jeweled
endif

SetFunctionValue baseitem

End
